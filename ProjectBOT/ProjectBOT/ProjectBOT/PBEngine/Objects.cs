﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectBuild.PBEngine
{
    public class Objects
    {
        public static PBObject[] objects = new PBObject[]
        {
            new PBObject()
            {
                Active=true,
                HP=100,
                HPMAX=100,
                SHIELD=0,
                SHIELDMAX=0,
                height=1f,
                width=1f,
                textureName="Crate",
                Name="Crate",
            },
            new PBObject()
            {
                Active=true,
                HP=200,
                HPMAX=200,
                height=1f,
                width=2f,
                textureName="Crate",
                Name="Big Crate",
            },
            new PBObject()
            {
              Active=true,
              HP=250,
              HPMAX=250,
              height=2f,
              width=2f,
              textureName="Crate",
              Name="Huge Box",
            },
            new PBObject()
            {
                Active=true,
                HP=100,
                HPMAX=100,
                width=1.25f,
                height=1.25f,
                textureName="door",
                Name="Door",
                Indestructable=true,
            },
            new PBObject()
            {
                Active=true,
                width=1.25f,
                height=1.25f,
                textureName="piston",
                Name="Piston",
                Indestructable=true,    
            },
            new PBObject()
            {
              Active=true,
              width=0.2f,
              height=1f,
              textureName="door",
              Name="Small Door",
              Indestructable=true,
            },
            
        };
        public static PBObject FromName(string name)
        {
            foreach (var a in objects)
                if (a.Name == name)
                    return a;
            return null;
        }
    }
}
