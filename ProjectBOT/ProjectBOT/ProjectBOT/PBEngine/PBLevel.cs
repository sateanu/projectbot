﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FarseerPhysics;
using FarseerPhysics.Collision;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.DebugViews;
using System.Diagnostics;
using PBEngine.Effect;
using ProjectBuild.PBEngine.Items;
using System.Threading;
using ProjectBuild.Screens;
using ProjectBuild.GUI;
using System.IO;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace ProjectBuild.PBEngine
{
    using FPath = FarseerPhysics.Common.Path;
    using Path = System.IO.Path;
    
    public class PBLevel : GameScreen
    {
        List<string> textureNames = MainGame.textureNames;
        
        PBGlobal GLOBALS = new PBGlobal();
        internal World World;
        internal ContentManager content;
        internal List<PBScriptAction> Actions = new List<PBScriptAction>();
        internal List<PBObject> NeutralObjects;
        internal Dictionary<string, PBObject> NeutralObjectsScript;
        internal List<PBMob> Mobs;
        internal Dictionary<string, PBMob> MobsScript;
        internal PBPlayer Player;
        internal PBLayer Map;
        internal PBGUIManager GUIManager;
        internal float pauseAlpha;
        internal DebugViewXNA debug;
        internal PBCamera Camera;
        internal SpriteFont gameFont;
        internal PBMessageManager MessageManager;
        internal Dictionary<string, Texture2D> Backgrounds = new Dictionary<string, Texture2D>();
        internal Dictionary<string, Texture2D> Pics = new Dictionary<string, Texture2D>();
        RenderTarget2D _colorMap;
        RenderTarget2D _normalMap;
        RenderTarget2D _depthMap;
        RenderTarget2D _shadowMap;
        List<PBLight> Lights = new List<PBLight>();
       internal Dictionary<string, PBLight> LightsScript = new Dictionary<string, PBLight>();
        internal Queue<PBScriptAction> actionsToBeResolved=new Queue<PBScriptAction>();
        Color AmbientLight = new Color(.1f, .1f, .1f, 1);
        Effect LightEffect;
        Effect LightCombinedEffect;
        VertexPositionColorTexture[] Vertices;
        VertexBuffer vertexBuffer;
        private Texture2D blank;
        List<HealthBar> HpBars = new List<HealthBar>();
        bool IsDebug = false;
        public Vector2 Spawn = new Vector2(5f,5f);
        int Start = 300;
        Effect onHitEffect;
        public PBCampaign Campaign { set; get; }
        bool Loaded = false;
        public PBLevel(string path,PBCampaign campaign=null)
        {
            for(int i=0;i<MainGame.textureNames.Count;i++)
            {
                textureNames[i]=MainGame.textureNames[i];
            }
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            if (campaign != null)
                Campaign = campaign;
            if (NeutralObjects == null)
                NeutralObjects = new List<PBObject>();
            if (Mobs == null)
                Mobs = new List<PBMob>();
            NeutralObjectsScript = new Dictionary<string, PBObject>();
            MobsScript = new Dictionary<string, PBMob>();
            if (World == null)
                World = new World(Vector2.Zero);
            this.path = path;

        }
        public string LVLNAME;
        private void LoadLevel(string path)
        {
            LVLNAME = path;
            //-PBLayer layer = null;
            int[,] map;
            int width;
            int height;
            float f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11,f12,f13;
            string nameEditor;
            string nameInGame;
            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    width = reader.ReadInt32();
                    height = reader.ReadInt32();
                    map = new int[height, width];
                    for (int i = 0; i < height; i++)
                        for (int j = 0; j < width; j++)
                            map[i, j] = reader.ReadInt32();
                    int objCount = reader.ReadInt32();
                    for (int i = 0; i < objCount; i++)
                    {
                        string objtype = reader.ReadString();
                        f1 = (float)reader.ReadDouble();
                        f2 = (float)reader.ReadDouble();
                        f3 = (float)reader.ReadDouble();
                        if (objtype != "SPAWN")
                        {
                            nameEditor = reader.ReadString();
                            nameInGame = reader.ReadString();
                            if (objtype == "MOB")
                                AddPBMob(f1, f2, f3, Monsters.FromName(nameInGame), nameEditor);
                            else if (objtype == "OBJECT")
                                AddPBObject(f1, f2, f3, Objects.FromName(nameInGame), nameEditor);
                            else if (objtype == "DROPI")
                                if (PBGuns.Contains(nameInGame))
                                    MessageManager.AddDrop(new Vector2(f1, f2), PBGuns.FromName(nameInGame));
                                else
                                    MessageManager.AddDrop(new Vector2(f1, f2), PBItems.FromName(nameInGame));
                        }
                        else
                        {
                            Spawn=new Vector2(f1,f2);
                        }


                    }
                    int i1;
                    int lightCount = reader.ReadInt32();
                    for (int i = 0; i < lightCount; i++)
                    {
                        string lightType = reader.ReadString();
                        f1 = (float)reader.ReadDouble();
                        f2 = (float)reader.ReadDouble();
                        f3 = (float)reader.ReadDouble();
                        f4 = (float)reader.ReadDouble();
                        f5 = (float)reader.ReadDouble();
                        i1 = reader.ReadInt32();
                        f6 = (float)reader.ReadDouble();
                        f7 = (float)reader.ReadDouble();
                        f8 = (float)reader.ReadDouble();
                        f9 = (float)reader.ReadDouble();
                        f10 = (float)reader.ReadDouble();
                        f11 = (float)reader.ReadDouble();
                        string scra;
                        int nr = 0;
                        
                        if (lightType == "POINTLIGHT")
                        {

                            while (true)
                            {
                                scra = "Point_" + nr.ToString();
                                if (!LightsScript.Keys.Contains<string>(scra))
                                {
                                    AddPBLight(new PointLight()
                                    {
                                        Position = new Vector3(f6, f7, f8),
                                        Power = f4,
                                        Color = new Vector4(f1, f2, f3, 1f),
                                        Direction = new Vector3(f9, f10, f11),
                                        SpecularPower = f5,
                                        LightDecay = i1,
                                        IsEnabled = true,
                                        ScriptName = scra
                                    });
                                    break;
                                }
                                nr++;
                            }
                        }
                        else if (lightType == "SPOTLIGHT")
                        {

                            while (true)
                            {
                                scra = "Spot_" + nr.ToString();
                                if (!LightsScript.Keys.Contains<string>(scra))
                                {
                                    AddPBLight(new SpotLight()
                                    {
                                        Position = new Vector3(f6, f7, f8),
                                        Power = f4,
                                        Color = new Vector4(f1, f2, f3, 1f),
                                        Direction = new Vector3(f9, f10, f11),
                                        SpecularPower = f5,
                                        LightDecay = i1,
                                        IsEnabled = true,
                                        ScriptName = scra
                                    });
                                    break;
                                }
                                nr++;
                            }
                        }
                    }
                    int eventCount = reader.ReadInt32();
                    string scriptName;
                    PBScriptAction a = new PBScriptAction();
                    bool repeat;
                    int int1, int2, int3, int4, int5, int6;
                    int countforaction = 0;
                    for (int i = 0; i < eventCount; i++)
                    {
                        f1 = (float)reader.ReadDouble();
                        f2 = (float)reader.ReadDouble();
                        f3 = (float)reader.ReadDouble();
                        f4 = (float)reader.ReadDouble();
                        string eventscriptname = reader.ReadString();

                        AddEventObj(f1+f3/2f, f2+f4/2f, f3, f4, eventscriptname);

                        int actionCount = reader.ReadInt32();
                        for (int j = 0; j < actionCount; j++)
                        {
                            string ownName = reader.ReadString();
                            string actor = reader.ReadString();
                            string TYPE = reader.ReadString();
                            PBScriptAction.TriggeredBy act = new PBScriptAction.TriggeredBy();
                            switch (actor)
                            {
                                case "ALL": act = PBScriptAction.TriggeredBy.All; break;
                                case "PAO": act = PBScriptAction.TriggeredBy.PlayerActionOnly; break;
                                case "PO": act = PBScriptAction.TriggeredBy.PlayerOnly; break;
                            }
                            switch (TYPE)
                            {
                                case "LIGHT":
                                    scriptName = reader.ReadString();
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    f3 = (float)reader.ReadDouble();
                                    int1 = reader.ReadInt32();
                                    f4 = (float)reader.ReadDouble();//color
                                    f5 = (float)reader.ReadDouble();
                                    f6 = (float)reader.ReadDouble();
                                    int2 = reader.ReadInt32();
                                    f7 = (float)reader.ReadDouble();
                                    f8 = (float)reader.ReadDouble();//directie
                                    f9 = (float)reader.ReadDouble();
                                    int3 = reader.ReadInt32();
                                    int4 = reader.ReadInt32();//radius
                                    int5 = reader.ReadInt32();
                                    f13 = (float)reader.ReadDouble();//powa
                                    int6 = reader.ReadInt32();
                                    repeat = reader.ReadBoolean();
                                    a = new PBScriptAction(scriptName, f1, f2, f3, int1, f4, f5, f6, int2, f7, f8, f9, int3, int4, int5, f13, int6, act, repeat);
                                    break;
                                case "OBJECT":
                                    scriptName = reader.ReadString();
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    int1 = reader.ReadInt32();
                                    f3 = (float)reader.ReadDouble();
                                    int2 = reader.ReadInt32();
                                    repeat = reader.ReadBoolean();
                                    a = new PBScriptAction(scriptName, f1, f2, int1, f3, int2, act, repeat);
                                    break;
                                case "QUEST":
                                    string mobtype = reader.ReadString();
                                    switch (mobtype)
                                    {
                                        case "MOB":
                                            string mobname = reader.ReadString();
                                            int1 = reader.ReadInt32();
                                            int2 = reader.ReadInt32();
                                            int3 = reader.ReadInt32();
                                            a = new PBScriptAction(true, Monsters.FromName(mobname), int1, int2, int3, act);
                                            break;
                                        case "ITEM":
                                            string itemname = reader.ReadString();
                                            int1 = reader.ReadInt32();
                                            int2 = reader.ReadInt32();
                                            int3 = reader.ReadInt32();
                                            a = new PBScriptAction(false, PBItems.FromName(itemname), int1, int2, int3, act);
                                            break;
                                        case "ROAD":
                                            f1 = (float)reader.ReadDouble();
                                            f2 = (float)reader.ReadDouble();
                                            int1 = reader.ReadInt32();
                                            int2 = reader.ReadInt32();
                                            a = new PBScriptAction(PBConverter.MeterToPixel(new Vector2(f1, f2)), int1, int2, act);
                                            break;
                                    }
                                    break;
                                case "MESSAGE":
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    f3 = (float)reader.ReadDouble();
                                    f4 = (float)reader.ReadDouble();
                                    int1 = reader.ReadInt32();
                                    string txt = reader.ReadString();
                                    f5 = (float)reader.ReadDouble();
                                    f6 = (float)reader.ReadDouble();
                                    f7 = (float)reader.ReadDouble();
                                    string back = reader.ReadString();
                                    string pic = reader.ReadString();
                                    a = new PBScriptAction(new Vector2(f1, f2), new Vector2(f3, f4), txt, int1, new Color(f1, f2, f3), back, pic, act);
                                    break;
                                case "TELEPORT":
                                    string lvltogo = reader.ReadString();
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    a = new PBScriptAction(new Vector2(f1, f2), act, lvltogo);
                                    break;
                                case "SPAWN":
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    string mobtospawn = reader.ReadString();
                                    int1 = reader.ReadInt32();
                                    int2 = reader.ReadInt32();
                                    a = new PBScriptAction(new Vector2(f1, f2), mobtospawn, int1, int2, act);
                                    break;
                                case "SOUND":
                                    string type = reader.ReadString();
                                    string name = reader.ReadString();
                                    a = new PBScriptAction(type, name, act);
                                    break;
                            }
                            a.OwnScriptName = ownName;
                            NeutralObjects[NeutralObjects.Count - 1].AddAction(a);
                        }
                    }
                }
            }
                Map=new PBLayer(map,80,80,World);
        }
        public PBLevel()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            if (NeutralObjects == null)
                NeutralObjects = new List<PBObject>();
            if (Mobs == null)
                Mobs = new List<PBMob>();
            if (World == null)
                World = new World(Vector2.Zero);
            NeutralObjectsScript = new Dictionary<string, PBObject>();
            MobsScript = new Dictionary<string, PBMob>();
            
        }
        void Window_ClientSizeChanged(object sender, EventArgs e)
        {

            Camera = new PBCamera(ScreenManager.GraphicsDevice.Viewport);
            Camera.Follow(Player);
            MessageManager.xpPos = new Vector2(128, ScreenManager.GraphicsDevice.Viewport.Height - 110);
            PresentationParameters pp = ScreenManager.GraphicsDevice.PresentationParameters;
            int w = pp.BackBufferWidth;
            int h = pp.BackBufferHeight;
            SurfaceFormat f = pp.BackBufferFormat;
            //ScreenManager.saveLast(w, h);
            _colorMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h);
            _depthMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h);
            _normalMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h);
            _shadowMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h, false, f, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);
        }
        public List<Texture2D> SplitTexture(Texture2D texture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            int W = texture.Width;
            int H = texture.Height;
            Color[] pixels = new Color[W * H];
            Color[] orPixel = new Color[W * H];
            texture.GetData<Color>(orPixel);
            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = Color.Transparent;
            }
            float[,] UV = new float[,] //POS X, POS Y, %X, %Y, 1f - keep/0f - delete
            { 
                { 0f,0f,1f,0.5f,1f },
                { 0f,0.5f,1f,0.5f,1f },
                { 0f,0f,0.5f,1f,1f},
                { 0.5f,0f,0.5f,1f,1f},
                { 0.5f, 0.5f, 0.5f,0.5f,1f },
                { 0.5f, 0.5f, 0.5f,0.5f,0f },
                { 0f, 0f, 0.5f,0.5f,1f },
                { 0f, 0f, 0.5f,0.5f,0f },
                { 0.5f,0f,0.5f,0.5f,1f},
                { 0.5f,0f,0.5f,0.5f,0f},
                { 0f,0.5f,0.5f,0.5f,1f},
                { 0f,0.5f,0.5f,0.5f,0f},
            };
            int Linii = UV.GetLength(0);
            int Coloane = UV.GetLength(1);

            for (int i = 0; i < Linii; i++)
            {
                Texture2D temp = new Texture2D(ScreenManager.GraphicsDevice, W, H);

                if (UV[i, 4] == 1f)
                {
                    temp.SetData<Color>(orPixel);
                    temp.SetData<Color>(0,
                        new Rectangle((int)(W * UV[i, 0]), (int)(H * UV[i, 1]), (int)(W * UV[i, 2]), (int)(H * UV[i, 3])),
                        pixels, 1,
                        (int)(W * UV[i, 2] * H * UV[i, 3]));
                }
                else
                {
                    float x, y, procX, procY;
                    x = UV[i, 0];
                    y = UV[i, 1];
                    procX = UV[i, 2] + x;
                    procY = UV[i, 3] + y;
                    Color[] t = new Color[texture.Width * texture.Height];
                    for (int py = (int)(y * texture.Height); py < (int)(texture.Height * procY); py++)
                        for (int px = (int)(x * texture.Width); px < (int)(texture.Width * procX); px++)
                        {
                            int index = px + py * texture.Width;
                            t[index] = orPixel[index];
                        }
                    temp.SetData<Color>(t);
                }
                textures.Add(temp);
            }

            return textures;
        }
        public void AddTextureColor(string s)
        {
            Texture2D temp=content.Load<Texture2D>("Tiles\\"+s);
            Map.tileTexturesColor.Add(temp);
            Map.tileTexturesColor.AddRange(SplitTexture(temp));
        }
        public void AddTextureNormal(string s)
        {
            Map.tileTexturesNormal.Add(content.Load<Texture2D>("Tiles\\" + s+"_n"));
        }
        public void AddTextureBoth(string s)
        { 
            AddTextureColor(s);
            //AddTextureNormal(s);
        }
        public override void LoadContent()
        {
            if(!Loaded)
            {
                Loaded = true;
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");
                gameFont = content.Load<SpriteFont>("gamefont");
                MessageManager = new PBMessageManager(this, gameFont);

                /*if (player != null)
                    Player = player;
                else*/
                if (Player == null)
                {
                    Player = new PBPlayer(World, 1f, 1f, Spawn, content, MessageManager);
                    if(Campaign!=null)
                    Campaign.UpdatePlayer(Player,GUIManager);
                }
                PresentationParameters pp = ScreenManager.GraphicsDevice.PresentationParameters;
                int w = pp.BackBufferWidth;
                int h = pp.BackBufferHeight;
                SurfaceFormat f = pp.BackBufferFormat;

                Vertices = new VertexPositionColorTexture[4];
                Vertices[0] = new VertexPositionColorTexture(new Vector3(-1, 1, 0), Color.White, new Vector2(0, 0));
                Vertices[1] = new VertexPositionColorTexture(new Vector3(1, 1, 0), Color.White, new Vector2(1, 0));
                Vertices[2] = new VertexPositionColorTexture(new Vector3(-1, -1, 0), Color.White, new Vector2(0, 1));
                Vertices[3] = new VertexPositionColorTexture(new Vector3(1, -1, 0), Color.White, new Vector2(1, 1));
                vertexBuffer = new VertexBuffer(ScreenManager.GraphicsDevice, typeof(VertexPositionColorTexture), Vertices.Length, BufferUsage.None);
                vertexBuffer.SetData(Vertices);

                _colorMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h);
                _depthMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h);
                _normalMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h);
                _shadowMap = new RenderTarget2D(ScreenManager.GraphicsDevice, w, h, false, f, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);

                LightEffect = content.Load<Effect>("FX/LightEffect");
                LightCombinedEffect = content.Load<Effect>("FX/LightCombinedEffect");
                onHitEffect = content.Load<Effect>("FX/Hit");

                Lights.Add(new SpotLight()
                {
                    Color = Color.LightYellow.ToVector4(),
                    Power = 0f,
                    SpecularPower = 0.2f,
                    LightDecay = 700,
                    Position = new Vector3(0, 0, 15),
                    Direction = new Vector3(0, 1, 0),
                    IsEnabled = true
                });

                debug = new DebugViewXNA(World);
                debug.LoadContent(ScreenManager.GraphicsDevice, content);
                // debug.AppendFlags(DebugViewFlags.DebugPanel);
                Camera = new PBCamera(ScreenManager.GraphicsDevice.Viewport);
                Camera.Follow(Player);
                if (GUIManager == null)
                    GUIManager = new PBGUIManager(content, Player, World, ScreenManager.GraphicsDevice.Viewport);
                else
                {
                    GUIManager.world = World;
                    GUIManager.RefreshGuns();
                }
                if(Campaign!=null)
                Campaign.RestorePlayer(this);
                ScreenManager.Game.Window.ClientSizeChanged += new EventHandler<EventArgs>(Window_ClientSizeChanged);
                internPaused = false;
                Load();
                if (path != null)
                    LoadLevel(path);
                //LoadTextures();
                blank = content.Load<Texture2D>("blank");
                Map.tileTexturesColor = MainGame.TEXTURES;
                Player.LoadTextureC(content.Load<Texture2D>("Mobs/player"));
                Player.LoadTextureN(content.Load<Texture2D>("Mobs/player_n"));
                Player.Position = Spawn;

                foreach (string s in MainGame.picNames)
                {
                    Pics.Add(s, content.Load<Texture2D>("Etc//Pics//" + s));
                }
                foreach (string s in MainGame.backgroundNames)
                {
                    Backgrounds.Add(s, content.Load<Texture2D>("Etc//Background//" + s));
                }

                // MessageManager.AddDrop(new Vector2(4, 8), PBGuns.TROLLMODE.Copy());
                //-----------------------------------------------------Q AICI------------------------------------------------------------------//
                //AddPBQuest(Vector2.Zero, 5, 5);
                //AddPBQuest(new Vector2(200), 5, 5);
                //AddPBQuest(new Vector2(800), 5, 5);
                //AddPBQuest(false, Items.Items.BrokenRobotParts, 5, 200, 200);
                //AddPBQuest(true,Monsters.monsters[1], 4, 1, 1);//MOB COLLECTION HERE
                //AddPBQuest(false, Items.Items.BrokenRobotParts, 1, 1000, 200);
                //MessageManager.AddBigMessage(new Vector2(-200, 300), new Vector2(0, 300), "test1", 100, Color.White, back, pic);
                //MessageManager.AddBigMessage(new Vector2(-200, 300), new Vector2(0, 300), "test2", 100, Color.White, back, pic);
                //MessageManager.AddBigMessage(new Vector2(0, 760), new Vector2(0, 300), "test3", 100, Color.White, back, pic);
                //MessageManager.AddGuiMessage(Vector2.Zero, "E buguit si merita sa crape.", 100, Color.White);
             //    PBEventObject eventobj = new PBEventObject(World, 3f, 3f, new Vector2(2,2), content, MessageManager);
             //    eventobj.AddAction(new PBScriptAction("sound","laserShot",PBScriptAction.TriggeredBy.All));
             //    NeutralObjects.Add(eventobj);
                /* eventobj.AddAction(new PBScriptAction("Crate_0", 5f, 5f, 180, 0f, 0, PBScriptAction.TriggeredBy.All, true));
                 eventobj.AddAction(new PBScriptAction("Crate_0", 1f, 5f, 180, MathHelper.ToRadians(180), 180, PBScriptAction.TriggeredBy.All, true));
                 eventobj.AddAction(new PBScriptAction("Crate_0", 5f, 1f, 180, MathHelper.ToRadians(-180), 180, PBScriptAction.TriggeredBy.All, true));
                 eventobj.AddAction(new PBScriptAction("Pointlight_1", 1000f, 1000f, 100f, 460, 1f, 0f, 1f, 460, 1f, 1f, 0f, 1, 600, 120, 1.6f, 100, PBScriptAction.TriggeredBy.All, true));
                 eventobj.AddAction(new PBScriptAction(true, Monsters.FromID(1), 5, 10, 10, PBScriptAction.TriggeredBy.PlayerActionOnly));
                 eventobj.AddAction(new PBScriptAction(new Vector2(-1,-1), new Vector2(-1,-1), "SUNT MAD CA PIERD ELO", 100, Color.White, "background", "pic1", PBScriptAction.TriggeredBy.PlayerOnly));
                 eventobj.AddAction(new PBScriptAction(new Vector2(10f, 10f), PBScriptAction.TriggeredBy.PlayerActionOnly));
                 eventobj.AddAction(new PBScriptAction(new Vector2(5, 1), "Robot", 3, 300, PBScriptAction.TriggeredBy.PlayerActionOnly));*/
                ScreenManager.Game.ResetElapsedTime();
            }
        }
        public virtual void Load()
        {
            
        }
        private void LoadTextures() //1 din content, 12 din SPLIT pana la final <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        {
           // foreach (string s in textureNames)
           //  AddTextureBoth(s);
            for (int i = 0; i < textureNames.Count; i++)
            {
                AddTextureBoth(textureNames[i]);
            }

        }
        public override void UnloadContent()
        {
            if(Campaign!=null)
            Campaign.UpdatePlayer(Player,GUIManager);
         // content.Unload();
            
        }
        public override void Update(GameTime gameTime,
            bool otherScreenHasFocus,
            bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
            
            if (coveredByOtherScreen||internPaused)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                if (!internPaused)
                {
                    MessageManager.Update(gameTime);
                    if (!waitingForMessageEnd)
                    {
                        if (Start > 0)
                            Start--;
                        if (HpBars.Count != 0)
                            HpBars.Clear();
                        UpdateLights(gameTime);
                        foreach (var obj in NeutralObjects)
                        {
                            obj.Update(gameTime);
                            if (obj.GetType() != typeof(PBEventObject))
                                UpdateAction(obj.FirstAction);
                            if (obj.GetType() == typeof(PBSpawner))
                            {
                                PBSpawner temp = (PBSpawner)obj;
                                if (temp.spawnNow)
                                {
                                    temp.spawnNow = false;
                                    AddPBMob(temp.Position.X, temp.Position.Y, temp.Rotation, Monsters.FromName(temp.mob), temp.ScriptName + "_" + temp.mob + "_" + temp.c);
                                }
                            }
                            else
                                if (obj.GetType() != typeof(PBEventObject))
                                    if (obj.Hover(MouseOnMap, Camera))
                                        HpBars.Add(obj.HpBar);
                        }
                        foreach (var mob in Mobs)
                        {
                            mob.Update(Player, Camera, gameTime);
                            if (mob.Hover(MouseOnMap, Camera))
                                HpBars.Add(mob.HpBar);

                        }
                        Camera.Update(gameTime);
                        Player.Update(gameTime);

                        World.Step((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);
                        for (int i = 0; i < Mobs.Count; i++)
                            if (Mobs[i].Active == false)
                            {
                                Mobs[i].Body.Dispose();
                                if (MobsScript.Count > 0)
                                    if (Mobs[i].ScriptName != null && MobsScript.ContainsKey(Mobs[i].ScriptName))
                                        MobsScript.Remove(Mobs[i].ScriptName);
                                Mobs.RemoveAt(i);
                            }
                        for (int i = 0; i < NeutralObjects.Count; i++)
                            if (NeutralObjects[i].Active == false)
                            {

                                NeutralObjects[i].Body.Dispose();
                                if (NeutralObjectsScript.Count > 0)
                                {
                                    if (NeutralObjects[i].ScriptName != null && NeutralObjectsScript.Keys.Contains<string>(NeutralObjects[i].ScriptName))
                                        NeutralObjectsScript.Remove(NeutralObjects[i].ScriptName);
                                }
                                NeutralObjects.RemoveAt(i);
                            }
                        if (Player.HP <= 0)
                        {
                            Player.HP = 0;
                            ScreenManager.AddScreen(new DeathMenuScreen(this.LVLNAME, Campaign), ControllingPlayer);
                        }

                        if (actionsToBeResolved.Count > 0)
                            ResolveAction(actionsToBeResolved.Dequeue());

                    }
                }
            }
        }
        void UpdateAction(PBScriptAction act)
        {
            if (act != null && act.Active == true)
                if (act.type == PBScriptAction.ScriptType.Object)
                {
                    if (NeutralObjectsScript.ContainsKey(act.Script_Name))
                    {
                        PBObject temp = NeutralObjectsScript[act.Script_Name];
                        if (temp.Body != null)
                        {
                            if (act.newPosition.X == temp.Position.X && act.newPosition.Y == temp.Position.Y&&act.repeat==false)
                            {
                                act.timeToMove = 0;
                            }
                            if (act.timeToMove > 0 && act.timeToMove <= act.timeToMoveMax)
                            {
                                Vector2 veloc = (-temp.Position + act.newPosition) / act.timeToMove;
                                if (!act.returning)
                                {
                                    act.newPositionUpdate = veloc;
                                    act.timeToMove--;
                                    temp.Position += veloc;
                                }
                                else
                                {
                                    act.timeToMove++;
                                    temp.Position -= act.newPositionUpdate;
                                }
                            }
                            if (act.timeToRotate > 0 && act.timeToRotate <= act.timeToRotateMax)
                            {
                                float rot = (-temp.Rotation + act.newRotation) / act.timeToRotate;
                                if (!act.returning)
                                {
                                    act.newRotationUpdate = rot;
                                    act.timeToRotate--;
                                    temp.Rotation += rot;
                                }
                                else
                                {
                                    act.timeToRotate++;
                                    temp.Rotation -= act.newRotationUpdate;
                                }
                            }
                            if ((act.timeToMove <= 0 && act.timeToRotate <= 0) || (act.timeToMove > act.timeToMoveMax && act.timeToRotate > act.timeToRotateMax))
                            {
                                if (!act.repeat)
                                {
                                   /* if (act.returning == false)
                                    {
                                        act.returning = true;
                                        act.timeToRotate = 1;
                                        act.timeToMove = 1;
                                    }*/
                                    if (act.returning == false)
                                        act.Active = false;
                                }
                                else
                                {
                                    if (act.returning == false)
                                    {
                                        act.returning = true;
                                        act.timeToRotate = 1;
                                        act.timeToMove = 1;
                                    }
                                    else
                                    {
                                        act.returning = false;
                                        act.timeToRotate = act.timeToRotateMax;
                                        act.timeToMove = act.timeToMoveMax;
                                    }
                                }
                            }
                        }
                        else
                        {
                            act.Active = false;
                            NeutralObjectsScript.Remove(act.Script_Name);
                        }
                    }
                    else
                        act.Active = false;
                }
                else //LUMINI
                    if (act.type == PBScriptAction.ScriptType.Light)
                    {
                        if (LightsScript.ContainsKey(act.Script_Name))
                        {
                            PBLight temp = LightsScript[act.Script_Name];
                            if (act.timeToMoveLight > 0 && act.timeToMoveLightMax >= act.timeToMoveLight)
                            {
                                Vector3 veloc = (-temp.Position + act.newPositionLight) / act.timeToMoveLight;
                                veloc.Z = 0;
                                if (!act.returning)
                                {
                                    act.newPositionLightUpdate = veloc;
                                    act.timeToMoveLight--;
                                    temp.Position += (veloc);
                                }
                                else
                                {
                                    act.timeToMoveLight++;
                                    temp.Position -= act.newPositionLightUpdate;
                                }
                            }
                            if (act.timeToColorLight > 0 && act.timeToColorLight <= act.timeToColorLightMax)
                            {
                                Vector3 transf = (-new Vector3(temp.Color.X, temp.Color.Y, temp.Color.Z) + act.newColorLight) / act.timeToColorLight;
                                if (!act.returning)
                                {
                                    act.newColorLightUpdate = transf;
                                    act.timeToColorLight--;
                                    temp.Color += new Vector4(transf.X, transf.Y, transf.Z, 0);
                                }
                                else
                                {
                                    act.timeToColorLight++;
                                    temp.Color -=new Vector4(act.newColorLightUpdate.X,act.newColorLightUpdate.Y,act.newColorLightUpdate.Z,0);
                                }
                            }
                            //PROBLEM AICI E PROBLEMA CU ROTATIA LUMINII
                            if (act.timeToRotateLight > 0 && act.timeToRotateLight <= act.timeToRotateLightMax)
                            {
                                Vector2 rot = (-new Vector2(temp.Direction.X,temp.Direction.Y) + new Vector2(act.newDirectionLight.X,act.newDirectionLight.Y)) / act.timeToRotateLight;

                                /*float newRot = (float)Math.Atan2(Convert.ToDouble(act.newDirectionLight.X),
                                    Convert.ToDouble(act.newDirectionLight.Y));
                                float currentRot=(float)Math.Atan2(Convert.ToDouble(temp.Direction.X),
                                    Convert.ToDouble(temp.Direction.Y));*/


                                float rotS = (act.newRotationLightAngle - temp.Angle)/act.timeToRotateLight;
                                float x;
                                float y;
                               // Lights[0].Direction = new Vector3((float)-Math.Sin(Convert.ToDouble(Player.topRot)), (float)Math.Cos(Convert.ToDouble(Player.topRot)), 0);
                                if (!act.returning)
                                {
                                    temp.Angle += rotS;
                                    x = (float)Math.Sin(Convert.ToDouble((temp.Angle)));
                                    y = (float)Math.Cos(Convert.ToDouble((temp.Angle))); 
                                    rot = new Vector2(x, y);
                                    act.newDirectionLightUpdate = new Vector3(rot, 0);
                                    act.newRotationUpdateLightAngle = rotS;
                                    act.timeToRotateLight--;
                                    
                                    temp.Direction = new Vector3(rot, 0);
                                }
                                else
                                {
                                    act.timeToRotateLight++;
                                    temp.Angle -= act.newRotationUpdateLightAngle;
                                    x = (float)Math.Sin(Convert.ToDouble((temp.Angle)));
                                    y = (float)Math.Cos(Convert.ToDouble((temp.Angle)));
                                    rot = new Vector2(x, y); 
                                    temp.Direction = new Vector3(rot, 0);
                                }
                            }

                            if (act.timeToPowerLight > 0 && act.timeToPowerLight <= act.timeToPowerLightMax)
                            {
                                float powr = (-temp.Power + act.newPower) / act.timeToPowerLight;
                                if (!act.returning)
                                {
                                    act.newPowerUpdate = powr;
                                    act.timeToPowerLight--;
                                    temp.Power += powr;
                                }
                                else
                                {
                                    act.timeToPowerLight++;
                                    temp.Power -= act.newPowerUpdate;
                                }
                            }
                            if (act.timeToRadiusLight > 0 && act.timeToRadiusLight <= act.timeToRadiusLightMax)
                            {
                                int rad = (-temp.LightDecay + act.newRadius) / act.timeToRadiusLight;
                                if (!act.returning)
                                {
                                    act.newRadiusUpdate = rad;
                                    act.timeToRadiusLight--;
                                    temp.LightDecay += rad;
                                }
                                else
                                {
                                    act.timeToRadiusLight++;
                                    temp.LightDecay -= act.newRadiusUpdate;
                                }
                            }
                            if ((act.timeToMoveLight <= 0 && act.timeToRadiusLight <= 0 && act.timeToPowerLight <= 0 && act.timeToColorLight <= 0 && act.timeToRotateLight <= 0) ||
                                act.timeToMoveLight > act.timeToMoveLightMax && act.timeToRadiusLight > act.timeToRadiusLightMax && act.timeToPowerLight > act.timeToPowerLightMax && act.timeToColorLight > act.timeToColorLightMax && act.timeToRotateLight > act.timeToRotateLightMax)
                            {
                                if (!act.repeat)
                                    act.Active = false;
                                else
                                {
                                    if (act.returning == false)
                                    {
                                        act.returning = true;
                                        act.timeToRotateLight = 1;
                                        act.timeToPowerLight = 1;
                                        act.timeToMoveLight = 1;
                                        act.timeToRadiusLight = 1;
                                        act.timeToColorLight = 1;
                                    }
                                    else
                                    {
                                        act.returning = false;
                                        act.timeToRotateLight = act.timeToRotateLightMax;
                                        act.timeToPowerLight = act.timeToPowerLightMax;
                                        act.timeToMoveLight = act.timeToMoveLightMax;
                                        act.timeToRadiusLight = act.timeToRadiusLightMax;
                                        act.timeToColorLight = act.timeToColorLightMax;
                                    }
                                }
                            }
                        }
                    }
        }
        /// <summary>
        /// Valuarea vectorului mouse-ului pe harta, cu camera, cu toate.
        /// </summary>
        Vector2 MouseOnMap;
        void UpdateLights(GameTime gameTime)
        {
            
            Lights[0].Position =new Vector3(PBConverter.MeterToPixel(Player.Position),Lights[0].Position.Z);
            Lights[0].Direction = new Vector3((float)-Math.Sin(Convert.ToDouble(Player.topRot)),(float)Math.Cos(Convert.ToDouble(Player.topRot)), 0);
            foreach (var l in Lights)
                UpdateAction(l.FirstAction);
        }
        KeyboardState keyboardState ;
        KeyboardState prevKeyboardState ;

        Vector2 closestObject(Vector2 relVector)
        {
            Vector2 result=relVector;
            float minDist=float.MaxValue;
            foreach (PBMob mob in Mobs)
            {
                float dist=Vector2.Distance(relVector,PBConverter.MeterToPixel(mob.Position));
                if (dist < minDist)
                {
                    minDist = dist;
                    result = PBConverter.MeterToPixel(mob.Position);
                }
            }

            return (result);
        }
        Vector2 ConvertToMap(Vector2 vector)
        {
            float x=vector.X + PBConverter.MeterToPixel(Camera.position.X) * Camera.Scale - ScreenManager.GraphicsDevice.Viewport.Width / 2;
            x /= Camera.Scale;
            float y = vector.Y + PBConverter.MeterToPixel(Camera.position.Y) * Camera.Scale - ScreenManager.GraphicsDevice.Viewport.Height / 2;
            y /= Camera.Scale;
            return new Vector2(x,y);
        }
        Vector2 ConvertFromMap(Vector2 vector)
        {
            float x = vector.X;
            x *= Camera.Scale;
            float y = vector.Y;
            y *= Camera.Scale;
            x -= PBConverter.MeterToPixel(Camera.position.X) * Camera.Scale - ScreenManager.GraphicsDevice.Viewport.Width / 2;
            y -= PBConverter.MeterToPixel(Camera.position.Y) * Camera.Scale - ScreenManager.GraphicsDevice.Viewport.Height / 2;
            return new Vector2(x, y);
        }
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            Vector2 m = PBGlobal.mouseIconPosition;
            base.HandleInput(input);
            int playerIndex = (int)ControllingPlayer.Value;

            keyboardState = input.CurrentKeyboardStates[playerIndex];
            prevKeyboardState = input.LastKeyboardStates[playerIndex];
            
            MouseOnMap = ConvertToMap(m);

            if (input.IsPauseGame(ControllingPlayer) && !internPaused)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(LVLNAME,Campaign), ControllingPlayer);
            }
            else
            {
                GUIManager.Update(ScreenManager.GraphicsDevice.Viewport,input);
                if (!internPaused)
                {
                    if (!waitingForMessageEnd)
                    {
                        if (keyboardState.IsKeyDown(Keys.L) && prevKeyboardState.IsKeyUp(Keys.L))
                            AddPBLight(new PointLight()
                            {
                                Color = Color.White.ToVector4(),
                                Power = 1f,
                                SpecularPower = 0.2f,
                                LightDecay = 400,
                                Position = new Vector3(MouseOnMap, 0),
                                IsEnabled = true,
                                ScriptName = "Point_" + Lights.Count
                            });

                        if (keyboardState.IsKeyDown(Keys.M) && prevKeyboardState.IsKeyUp(Keys.M))
                            AddPBMob(PBConverter.PixelToMeter(MouseOnMap), 0f, Monsters.FromName("Robot"));
                        /* if (keyboardState.IsKeyDown(Keys.F1) && prevKeyboardState.IsKeyUp(Keys.F1))
                             AmbientLight = Color.Black;
                         if (keyboardState.IsKeyDown(Keys.F2) && prevKeyboardState.IsKeyUp(Keys.F2))
                             AmbientLight = Color.White;
                         if (keyboardState.IsKeyDown(Keys.F3) && prevKeyboardState.IsKeyUp(Keys.F3))
                             AmbientLight = new Color(.1f, .1f, .1f, 1);
                         if (keyboardState.IsKeyDown(Keys.F4) && prevKeyboardState.IsKeyUp(Keys.F4))
                             AmbientLight = Color.DarkBlue;
                         if (KeyPressed(Keys.F5))
                             AmbientLight = new Color(.5f, .5f, .5f, 1f);
                         if (KeyPressed(Keys.F6))
                             AmbientLight = new Color(.7f, .7f, .7f, 1f);
                         if (KeyPressed(Keys.F7))
                             AmbientLight = Color.OrangeRed;
                         if (keyboardState.IsKeyDown(Keys.C) && prevKeyboardState.IsKeyUp(Keys.C))
                             AddPBMob(PBConverter.PixelToMeter(MouseOnMap), 0f, Monsters.monsters[1]);
                         if (keyboardState.IsKeyDown(Keys.Z) && prevKeyboardState.IsKeyUp(Keys.Z))
                             AddPBMob(PBConverter.PixelToMeter(MouseOnMap), 0f, Monsters.FromID(4)); 
                         if (keyboardState.IsKeyDown(Keys.X) && prevKeyboardState.IsKeyUp(Keys.X))
                             AddPBObject(PBConverter.PixelToMeter(MouseOnMap), 0f, Objects.FromName("Crate"));*/
                        if (keyboardState.IsKeyDown(Keys.F) && prevKeyboardState.IsKeyUp(Keys.F)||
                            GamePadPressed(input,Buttons.B))
                            if (Lights[0].ActualPower > 0)
                                Lights[0].ActualPower = 0f;
                            else
                                Lights[0].ActualPower = 0.8f;

                        if (input.IsButtonPressTime(Buttons.LeftTrigger, ControllingPlayer, out this.playerIndex, 0.001f))
                        {
                            Viewport v=ScreenManager.GraphicsDevice.Viewport;
                            Vector2 temp= ConvertFromMap(closestObject(MouseOnMap));
                            if (temp.X < v.Width && temp.X > 0 && temp.Y > 0 && temp.Y < v.Height)
                                PBGlobal.mouseIconPosition = temp;
                        }
                        if (IsDebug)
                        {
                            if (input.IsScroolDown())
                            {
                                Camera.Scale -= 0.05f;
                            }
                            if (input.IsScroolUp())
                            {
                                Camera.Scale += 0.05f;
                            }
                            Camera.Scale = MathHelper.Clamp(Camera.Scale, 0.5f, 20f);
                        }

                        if (KeyPressed(Keys.I) || KeyPressed(Keys.E)||GamePadPressed(input,Buttons.Y))
                        { previousAimPosition = PBGlobal.mouseIconPosition; GUIManager.ShowOrHide(); internPaused = true; }
                        if (keyboardState.IsKeyDown(Keys.F10) && prevKeyboardState.IsKeyUp(Keys.F10))
                            IsDebug = IsDebug ^ true;
                        Player.HandleInput(input, ControllingPlayer.Value, Camera, content);

                    }
                    else
                    {
                        if (KeyPressed(Keys.I) || KeyPressed(Keys.E)||GamePadPressed(input,Buttons.Y))
                        { GUIManager.ShowOrHide(); internPaused = true; previousAimPosition = PBGlobal.mouseIconPosition; }
                        if (KeyPressed(Keys.G)||GamePadPressed(input,Buttons.X))
                            MessageManager.KillCurrentBigMessage();

                    }
                    
                }
                else
                {
                    if (GUIManager.Shown && (KeyPressed(Keys.I) || KeyPressed(Keys.Escape) || KeyPressed(Keys.E)||GamePadPressed(input,Buttons.Y)))
                    { GUIManager.ShowOrHide(); internPaused = false; PBGlobal.mouseIconPosition=previousAimPosition; }
                }
            }
        }
        PlayerIndex playerIndex;
        bool KeyPressed(Keys key)
        {
            return keyboardState.IsKeyDown(key)&&prevKeyboardState.IsKeyUp(key);
        }
        bool GamePadPressed(InputState state,Buttons button)
        {
            return state.IsNewButtonPress(button, ControllingPlayer.Value,out playerIndex);
        }
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice GD = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            
            GD.SetRenderTarget(_colorMap);
            GD.Clear(Color.Transparent);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null,null, Camera.View);
            Map.DrawColorNoWalls(Camera, spriteBatch);
            Player.DrawPE(spriteBatch);
            MessageManager.DrawDropC(spriteBatch);
            foreach (var obj in NeutralObjects)
            {
                obj.DrawColor(spriteBatch);
            }
            foreach (var mob in Mobs)
            {
                mob.DrawColor(spriteBatch);
            }
            foreach (var bar in HpBars)
                bar.DrawC(spriteBatch);
            Player.DrawColor(spriteBatch);
            MessageManager.DrawParticles(spriteBatch);
            Map.DrawColorOnlyWalls(Camera, spriteBatch);
            spriteBatch.End();
            GD.SetRenderTarget(null);

            /*GD.SetRenderTarget(_normalMap);
            GD.Clear(Color.Transparent);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, Camera.View);
            Map.DrawNormal(Camera, spriteBatch);
            foreach (var obj in NeutralObjects)
                obj.DrawNormal(spriteBatch);
            MessageManager.DrawDropC(spriteBatch);
            foreach (var mob in Mobs)
                mob.DrawNormal(spriteBatch);
            foreach (var bar in HpBars)
                bar.DrawN(spriteBatch);
            spriteBatch.DrawString(gameFont, "ProjectBot", Vector2.Zero, Color.Purple);
            Player.DrawNormal(spriteBatch);
            spriteBatch.End();
            GD.SetRenderTarget(null);
            */
            /*GD.SetRenderTarget(_depthMap);
            GD.Clear(Color.Transparent);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.View);
            //Map.DrawDepth(Player, Camera, spriteBatch, blank);
            spriteBatch.DrawString(gameFont, "Naspaa", Vector2.Zero, Color.DarkGray);
            spriteBatch.End();
            GD.SetRenderTarget(null);*/

            GenerateShadowMap();
            GD.Clear(Color.Black);
            DrawCombinedMaps(spriteBatch);

            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.View);
            MessageManager.DrawMessages(spriteBatch);
            Player.DrawQuest(spriteBatch);
            Player.DrawOnMap(spriteBatch);
            spriteBatch.End();
            if (IsDebug)
            {
                debug.RenderDebugData(ref Camera.Projection, ref Camera.DebugView);
                spriteBatch.Begin();
                spriteBatch.DrawString(gameFont, (1 / (float)gameTime.ElapsedGameTime.TotalSeconds).ToString(), Vector2.Zero, Color.White);
                spriteBatch.DrawString(gameFont, Camera.Position.ToString(), new Vector2(0, 30), Color.White);
                //spriteBatch.DrawString(gameFont, Lights[2].Position.ToString(), new Vector2(0, 60), Color.White);
                //spriteBatch.DrawString(gameFont, (Lights[2].Position-new Vector3(Camera.Position,0)).ToString(), new Vector2(0, 90), Color.White);
                spriteBatch.DrawString(gameFont, new Vector2(Mouse.GetState().X, Mouse.GetState().Y).ToString(), new Vector2(0, 130), Color.White);
                spriteBatch.DrawString(gameFont, PBConverter.MeterToPixel(Player.Position).ToString(), new Vector2(0, 160), Color.White);
                spriteBatch.End();
                DrawDebugRenderTargets(spriteBatch);
            }
            
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }

            DrawGUI(spriteBatch);
            
            
        }

        private void DrawGUI(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            Player.DrawRest(spriteBatch, ScreenManager.GraphicsDevice.Viewport);
            waitingForMessageEnd=MessageManager.DrawGuiMessages(spriteBatch);
            Player.DrawQuestDetails(spriteBatch);
            GUIManager.Draw(spriteBatch);
            spriteBatch.End();
        }



        private void DrawCombinedMaps(SpriteBatch spriteBatch)
        {
            LightCombinedEffect.CurrentTechnique = LightCombinedEffect.Techniques[0];
            LightCombinedEffect.Parameters["ambient"].SetValue(1f);
            LightCombinedEffect.Parameters["lightAmbient"].SetValue(3f);
            LightCombinedEffect.Parameters["ambientColor"].SetValue(AmbientLight.ToVector4());
            LightCombinedEffect.Parameters["ColorMap"].SetValue(_colorMap);
            LightCombinedEffect.Parameters["ShadingMap"].SetValue(_shadowMap);
            //LightCombinedEffect.Parameters["NormalMap"].SetValue(_normalMap);
            LightCombinedEffect.CurrentTechnique.Passes[0].Apply();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, LightCombinedEffect);
            spriteBatch.Draw(_colorMap, Vector2.Zero, Color.White);
            spriteBatch.End();
        }

        private Texture2D GenerateShadowMap()
        {
            ScreenManager.GraphicsDevice.SetRenderTarget(_shadowMap);
            ScreenManager.GraphicsDevice.Clear(Color.Transparent);
            Vector3 cameraScalePos;
            foreach (var light in Lights)
            {
                if (!light.IsEnabled)
                    continue;

                ScreenManager.GraphicsDevice.SetVertexBuffer(vertexBuffer);
                // -new Vector3(PBConverter.PixelToMeter(Camera.position), 0);
                cameraScalePos = light.Position * Camera.Scale;         
                cameraScalePos += new Vector3(ScreenManager.GraphicsDevice.Viewport.Width / 2, ScreenManager.GraphicsDevice.Viewport.Height / 2, 0);
                cameraScalePos -=new Vector3( PBConverter.MeterToPixel(Camera.position) * Camera.Scale,0);
                LightEffect.Parameters["lightPosition"].SetValue(cameraScalePos);
                //cameraScalePos *= Camera.Scale;
                LightEffect.Parameters["lightStrength"].SetValue(light.ActualPower);
                LightEffect.Parameters["NormalMap"].SetValue(_normalMap);
                LightEffect.Parameters["lightDecay"].SetValue(light.LightDecay*Camera.Scale);
                LightEffect.Parameters["lightColor"].SetValue(light.Color);
                LightEffect.Parameters["coneDirection"].SetValue(light.Direction);
                LightEffect.Parameters["screenHeight"].SetValue(ScreenManager.GraphicsDevice.Viewport.Height);
                LightEffect.Parameters["screenWidth"].SetValue(ScreenManager.GraphicsDevice.Viewport.Width);
                LightEffect.Parameters["lightAngle"].SetValue(0f);
                LightEffect.Parameters["DepthMap"].SetValue(_depthMap);
                LightEffect.Parameters["specularStrength"].SetValue(light.SpecularPower);
                
                if (light.LightType == LightType.Point)
                    LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredPointLight"];
                else
                if (light.LightType==LightType.Spot)
                    LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredSpotLight"];

                LightEffect.CurrentTechnique.Passes[0].Apply();

                ScreenManager.GraphicsDevice.BlendState = BlendBlack;

                ScreenManager.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, Vertices, 0, 2);

            }
            foreach (var mob in Mobs)
            {
                foreach (PBBullet bullet in mob.Gun.Bullets)
                {
                    LightEffect.Parameters["lightStrength"].SetValue(0.7f);
                    cameraScalePos =new Vector3(PBConverter.MeterToPixel(bullet.body.Position) * Camera.Scale,15);
                    cameraScalePos += new Vector3(ScreenManager.GraphicsDevice.Viewport.Width / 2, ScreenManager.GraphicsDevice.Viewport.Height / 2, 0);
                    cameraScalePos -= new Vector3(PBConverter.MeterToPixel(Camera.position) * Camera.Scale, 0);
                    LightEffect.Parameters["lightPosition"].SetValue(cameraScalePos);
                    LightEffect.Parameters["NormalMap"].SetValue(_normalMap);
                    LightEffect.Parameters["lightDecay"].SetValue(50f*Camera.Scale);
                    LightEffect.Parameters["lightColor"].SetValue(bullet.color.ToVector4());
                    LightEffect.Parameters["coneDirection"].SetValue(Vector3.Zero);
                    LightEffect.Parameters["screenHeight"].SetValue(ScreenManager.GraphicsDevice.Viewport.Height);
                    LightEffect.Parameters["screenWidth"].SetValue(ScreenManager.GraphicsDevice.Viewport.Width);
                    LightEffect.Parameters["lightAngle"].SetValue(0f);
                    LightEffect.Parameters["DepthMap"].SetValue(_depthMap);
                    LightEffect.Parameters["specularStrength"].SetValue(0.1f);
                    LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredPointLight"];
                    LightEffect.CurrentTechnique.Passes[0].Apply();

                    ScreenManager.GraphicsDevice.BlendState = BlendBlack;

                    ScreenManager.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, Vertices, 0, 2);
                }
            }
            if(Player.Gun!=null)
            foreach (PBBullet bullet in Player.Gun.Bullets)
            {
                LightEffect.Parameters["lightStrength"].SetValue(0.7f);
                cameraScalePos = new Vector3(PBConverter.MeterToPixel(bullet.body.Position) * Camera.Scale, 15);
                cameraScalePos += new Vector3(ScreenManager.GraphicsDevice.Viewport.Width / 2, ScreenManager.GraphicsDevice.Viewport.Height / 2, 0);
                cameraScalePos -= new Vector3(PBConverter.MeterToPixel(Camera.position) * Camera.Scale, 0);
                LightEffect.Parameters["lightPosition"].SetValue(cameraScalePos);
                LightEffect.Parameters["NormalMap"].SetValue(_normalMap);
                LightEffect.Parameters["lightDecay"].SetValue(50f*Camera.Scale);
                LightEffect.Parameters["lightColor"].SetValue(bullet.color.ToVector4());
                LightEffect.Parameters["coneDirection"].SetValue(Vector3.Zero);
                LightEffect.Parameters["screenHeight"].SetValue(ScreenManager.GraphicsDevice.Viewport.Height);
                LightEffect.Parameters["screenWidth"].SetValue(ScreenManager.GraphicsDevice.Viewport.Width);
                LightEffect.Parameters["lightAngle"].SetValue(0f);
                LightEffect.Parameters["DepthMap"].SetValue(_depthMap);
                LightEffect.Parameters["specularStrength"].SetValue(2f);
                LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredPointLight"];
                LightEffect.CurrentTechnique.Passes[0].Apply();

                ScreenManager.GraphicsDevice.BlendState = BlendBlack;

                ScreenManager.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, Vertices, 0, 2);
            }
            ScreenManager.GraphicsDevice.SetRenderTarget(null);

            return _shadowMap;
        }
        public void AddPBLight(PBLight li)
        {
            li.Angle = (float)Math.Atan2(Convert.ToDouble(li.Direction.X), Convert.ToDouble(li.Direction.Y));
            float t = (float)(li.Angle * 180 / Math.PI);
            Lights.Add(li);
            LightsScript.Add(li.ScriptName, li);
        }
        public void AddPBObject(PBObject obj)
        {
            NeutralObjects.Add(obj);
        }
        public void AddPBMob(PBMob mob)
        {
            Mobs.Add(mob);
        }
        public void AddPBMob(Vector2 position, float rotation, PBMob mob)
        {
            Mobs.Add(new PBMob(World, mob.height, mob.width, position, content, MessageManager, mob.Name, rotation, mob.textureName, mob.ID, mob.HP, mob.dropXp, mob.Gun,mob.st));
        }
        public void AddPBMob(float x,float y, float rotation, PBMob mob)
        {
            Mobs.Add(new PBMob(World, mob.height, mob.width, new Vector2(x,y), content, MessageManager, mob.Name, rotation, mob.textureName, mob.ID, mob.HP, mob.dropXp, mob.Gun,mob.st));
        }
        public void AddPBMob(float x, float y, float rotation, PBMob mob,string scriptName)
        {
            Mobs.Add(new PBMob(World, mob.height, mob.width, new Vector2(x, y), content, MessageManager, mob.Name, rotation, mob.textureName, mob.ID, mob.HP, mob.dropXp, mob.Gun, mob.st));
            Mobs[Mobs.Count - 1].ScriptName = scriptName;
            Mobs[Mobs.Count - 1].Indestructable = mob.Indestructable;
            Mobs[Mobs.Count - 1].SHIELD = mob.SHIELD;
            Mobs[Mobs.Count - 1].SHIELDMAX = mob.SHIELDMAX;
            Mobs[Mobs.Count - 1].ShieldRegen = mob.ShieldRegen;
            Mobs[Mobs.Count - 1].ShieldRegenDelay = mob.ShieldRegenDelay;
            MobsScript.Add(scriptName, Mobs[Mobs.Count - 1]);
        }
        public void AddPBObject(Vector2 position, float rotation, PBObject obj)
        {
            NeutralObjects.Add(new PBObject(World, obj.height, obj.width, position, content, MessageManager, obj.textureName, rotation,obj.Name));
        }
        public void AddPBObject(float x,float y, float rotation, PBObject obj)
        {
            NeutralObjects.Add(new PBObject(World, obj.height, obj.width, new Vector2(x,y), content, MessageManager, obj.textureName, rotation,obj.Name));
        }
        public void AddPBObject(float x, float y, float rotation, PBObject obj,string scriptName)
        {
            if (obj == null)
                return;
            NeutralObjects.Add(new PBObject(World, obj.height, obj.width, new Vector2(x, y), content, MessageManager, obj.textureName, rotation, obj.Name,false,obj.Indestructable));
            NeutralObjects[NeutralObjects.Count - 1].ScriptName = scriptName;
            NeutralObjects[NeutralObjects.Count - 1].HPMAX = obj.HPMAX;
            NeutralObjects[NeutralObjects.Count - 1].HP = obj.HP;
            NeutralObjects[NeutralObjects.Count - 1].SHIELD = obj.SHIELD;
            NeutralObjects[NeutralObjects.Count - 1].SHIELDMAX = obj.SHIELDMAX;
            NeutralObjects[NeutralObjects.Count - 1].ShieldRegen = obj.ShieldRegen;
            NeutralObjects[NeutralObjects.Count - 1].ShieldRegenDelay = obj.ShieldRegenDelay; 
            if (obj.NoHitbox == true)
                NeutralObjects[NeutralObjects.Count - 1].DisableHitbox();
            NeutralObjectsScript.Add(scriptName, NeutralObjects[NeutralObjects.Count - 1]);
            
        }
        public void AddEventObj(float x, float y,float w,float h ,string scriptName)
        {
            NeutralObjects.Add(new PBEventObject(World,h,w,new Vector2(x,y),content,MessageManager));
            NeutralObjects[NeutralObjects.Count - 1].ScriptName = scriptName;
            NeutralObjectsScript.Add(scriptName, NeutralObjects[NeutralObjects.Count - 1]);
        }
        internal void AddSpawner(Vector2 vector2, string p, int p_2, int p_3,string ownscriptname)
        {
            NeutralObjects.Add(new PBSpawner(p, p_3, p_2, World, 0.1f, 0.1f, vector2, content, MessageManager,ownscriptname));
        }
        public void AddPBQuest(Vector2 pos,int xp,int gold)
        {
            Player.AddQuest(pos, xp, gold);
        }
        public void AddPBQuest(bool monster, object ID, int count, int xp, int gold)
        {
            Player.AddQuest(monster, ID, count, xp, gold);
        }
        public static BlendState BlendBlack = new BlendState()
        {
            ColorBlendFunction = BlendFunction.Add,
            ColorSourceBlend = Blend.One,
            ColorDestinationBlend = Blend.One,
            
            AlphaBlendFunction = BlendFunction.Add,
            AlphaSourceBlend = Blend.SourceAlpha,
            AlphaDestinationBlend = Blend.One
        };
        internal bool internPaused;
        private string path;
        private bool waitingForMessageEnd;
        private Vector2 previousAimPosition;

        public void DrawDebugRenderTargets(SpriteBatch spriteBatch)
        {
            // Draw some debug textures
            spriteBatch.Begin();

            Rectangle size = new Rectangle(0, 0, _colorMap.Width / 3, _colorMap.Height / 3);
            var position = new Vector2(0, ScreenManager.GraphicsDevice.Viewport.Height - size.Height);
            spriteBatch.Draw(
                _colorMap,
                new Rectangle(
                    (int)position.X, (int)position.Y,
                    size.Width,
                    size.Height),
                Color.White);

           /* spriteBatch.Draw(
                 _depthMap,
                 new Rectangle(
                     (int)position.X + size.Width, (int)position.Y,
                     size.Width,
                     size.Height),
                 Color.White);*/

            spriteBatch.Draw(
                _normalMap,
                new Rectangle(
                    (int)position.X + size.Width * 1, (int)position.Y,
                    size.Width,
                    size.Height),
                    Color.White);

            spriteBatch.Draw(
                _shadowMap,
                new Rectangle(
                    (int)position.X + size.Width * 2, (int)position.Y,
                    size.Width,
                    size.Height),
                Color.White);

            spriteBatch.End();
        }

        internal void AddAction(PBScriptAction action)
        {
            actionsToBeResolved.Enqueue(action);
        }

        internal void ResolveAction(PBScriptAction action)
        {
            if (action.type == PBScriptAction.ScriptType.Teleport)
            {
                if (action.levelToGo == "SAME")
                {
                    if (action.positionToGo == new Vector2(-1))
                        action.positionToGo = Spawn;
                    Player.Body.SetTransformIgnoreContacts(ref action.positionToGo, 0f);

                }
            }
        }
    }
}
