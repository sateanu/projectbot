﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ProjectBuild.PBEngine
{
    public class PBGlobal
    {
        public static Dictionary<int, string> itemID = new Dictionary<int, string>();
        public static Dictionary<int, string> mobID = new Dictionary<int, string>();
        public static int volume = 100;
        public static int songVolume = 100;
        public static Vector2 mouseIconPosition = Vector2.Zero;
        public static int aimSensivity = 10;
    }
}
