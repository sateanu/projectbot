﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine.Items;

namespace ProjectBuild.PBEngine
{
    public class Monsters
    {
        public static PBMob[] monsters=new PBMob[]
        { 
            new PBMob()
            {
                Active=true,
                HPMAX=100,
                HP=100,
                ID=1,
                height=1f,
                width=1f,
                textureName="mob2",
                Name="Defect Robot",
                dropXp=5,
            },
            new PBMob()
            {
                Active=true,
                HPMAX=500,
                HP=500,
                SHIELDMAX=250,
                SHIELD=250,
                ShieldRegen=10f,
                ShieldRegenDelay=0.5f,
                ID=2,
                height=2f,
                width=2f,
                textureName="mob1",
                Name="Big Robot",
                dropXp=9001,
                Gun=PBGuns.NormalGun,
            },
            new PBMob()
            {
                Active=true,
                HPMAX=150,
                HP=150,
                ID=3,
                height=1f,
                width=1f,
                textureName="mob1",
                Name="Robot",
                dropXp=15,
                Gun=PBGuns.NormalGun,
            },
            new PBMob()
            {
                Active=true,
                HPMAX=120,
                HP=120,
                SHIELD=100,
                ShieldRegen=2.5f,
                ShieldRegenDelay=0.5f,
                ID=4,
                height=1.2f,
                width=1.2f,
                textureName="tower",
                Name="Cannon",
                dropXp=100,
                Gun=PBGuns.CannonGun,
                st=true,
            },
        };
        public static PBMob FromName(string name)
        {
            foreach (var a in monsters)
                if (a.Name == name)
                    return a;
            return null;
        }
        public static PBMob FromID(int ID)
        {
            foreach (var a in monsters)
                if (a.ID == ID)
                    return a;
            return null;
        }
        public static int IdOf(string name)
        {
            foreach (var a in monsters)
                if (a.Name == name)
                    return a.ID;
            return -1;
        }
        public static string NameOf(int ID)
        {
            foreach (var a in monsters)
                if (a.ID == ID)
                    return a.Name;
            return null;
        }
    }

}
