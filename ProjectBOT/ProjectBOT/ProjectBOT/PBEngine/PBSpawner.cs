﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ProjectBuild.PBEngine
{
   public class PBSpawner : PBObject
    {
       public bool spawning;
       public void Start()
       {
           spawning = true;
       }
       public void Stop()
       {
           spawning = false;
       }
       public string mob;
       int tts; //TIME TO SPAWN in ticks
       int count; // mobs to be spawned
       int tt;
      public int c;
       public bool spawnNow;
       public PBSpawner(string mob, int tts, int count,World world,float height,float width, Vector2 position, ContentManager content,PBMessageManager message,string scriptname,string s=null,float rot=0f, string name=null)
           :base(world,height,width,position,content,message,s,rot,name)
       {
           Body.CollidesWith = Category.None;
           tt = 0;
           this.mob = mob;
           this.tts = tts;
           this.count = count;
           c = 0;
           ScriptName = scriptname;
           spawnNow = false;
           spawning = true;
       }
       public override void Update()
       {
           if(spawning)
           if (!spawnNow)
           {
               if (c < count)
                   if (tt == tts)
                   {
                       tt = 0;
                       c++;
                       spawnNow = true;
                   }
                   else
                       tt++;
           }
       }
    }
}
