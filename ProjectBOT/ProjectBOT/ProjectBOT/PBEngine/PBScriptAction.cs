﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ProjectBuild.PBEngine
{
    using QuestType = PBQuest.QuestType;
using Microsoft.Xna.Framework.Graphics;
    using System.ComponentModel; 
    //FAL SA FIE SI USABLE DE MAI MULTE ORI
    //FA SI LISTA DE ACTIUNI
   public class PBScriptAction
   {
       public void AddConsequence(PBScriptAction action)
       {
           if (consequence == null)
               consequence = new List<PBScriptAction>();
           consequence.Add(action);
       }
       public List<PBScriptAction> consequence;
       public string OwnScriptName;
       public enum ScriptType
       {
           Object,
           Light,
           Quest,
           Teleport,
           BigMessage,
           Spawn,
           Sound
       };

       public enum TriggeredBy
       {
           PlayerOnly,
           All,
           PlayerActionOnly
       };

      public bool Active = true;
      public TriggeredBy actor;
      [Category("General")]
      public TriggeredBy Actor { get { return actor; } }
      [Category("General")]
      public string AffectedObject { get { return Script_Name; }  }

      public string Script_Name;

      [Category("General")]
      public ScriptType Type { get { return type; }  }

      public ScriptType type;
      public bool repeat;
      public bool returning;
      public bool queued=false;

      public void Activate()
      {
          Active = true;
          returning = false;
          Used = true;
          timeToMove = timeToMoveMax;
          timeToRotate = timeToRotateMax;
          if(consequence!=null)
          foreach (var act in consequence)
              act.Activate();
      }
      internal void Que()
      {
          queued = true;
          if (consequence != null)
              foreach (var act in consequence)
                  act.Que();
      }
      internal void Deque()
      {
          queued = false;
          if (consequence != null)
              foreach (var act in consequence)
                  act.Deque();
      }

       #region Object Script

      [Category("Object")]
      public Vector2 New_Position { get { return newPosition; } }
      [Category("Object")]
      public int TimeToPosition { get { return timeToMove; } }
      [Category("Object")]
      public float New_Rotation { get { return newRotation; } }
      [Category("Object")]
      public int TimeToRotation { get { return timeToRotate; } }
        
      public Vector2 newPosition;
      public float newRotation;
      public int timeToMove;
      public int timeToMoveMax;
      public int timeToRotate;
      public int timeToRotateMax;
      public Vector2 newPositionUpdate;
      public float newRotationUpdate;
      
       //PUNE VELOCITY SI RESTU
       /// <summary>
       /// Object
       /// </summary>
       /// <param name="scriptName"></param>
       /// <param name="x"></param>
       /// <param name="y"></param>
       /// <param name="t1"></param>
       /// <param name="r"></param>
       /// <param name="t2"></param>
       /// <param name="actor"></param>
       /// <param name="repeat"></param>
       public PBScriptAction(string scriptName,float x,float y,int t1,float r,int t2,TriggeredBy actor,bool repeat)
       {
           Script_Name = scriptName;
           type = ScriptType.Object;
           newPosition = new Vector2(x, y);
           newRotation = r;
           timeToMove = timeToMoveMax = t1;
           timeToRotate = timeToRotateMax = t2;
           this.repeat = repeat;
           this.actor = actor;
       }
#endregion

       #region Light Script

       [Category("Light")]
       public Vector3 New_LightPosition { get { return newPositionLight; } }
       [Category("Light")]
       public int TimeToMove { get { return timeToMoveLight; } }
       [Category("Light")]
       public Vector3 New_Direction { get { return newDirectionLight; } }
       [Category("Light")]
       public int TimeToDirection { get { return timeToRotateLight; } }
       [Category("Light")]
       public Vector3 New_Color { get { return newColorLight; } }
       [Category("Light")]
       public int TimeToColor { get { return timeToColorLight; } }
       [Category("Light")]
       public float New_Power { get { return newPower; } }
       [Category("Light")]
       public int TimeToPower { get { return timeToPowerLight; } }
       [Category("Light")]
       public int New_Radius { get { return newRadius; } }
       [Category("Light")]
       public int TimeToRadius { get { return timeToRadiusLight; } }

       public Vector3 newPositionLight;
       public int timeToMoveLight;
       public int timeToMoveLightMax;
       public Vector3 newPositionLightUpdate;

       public Vector3 newDirectionLight;
       public int timeToRotateLight;
       public int timeToRotateLightMax;
       public Vector3 newDirectionLightUpdate;
       public float newRotationLightAngle;
       public float newRotationUpdateLightAngle;

       public Vector3 newColorLight;
       public int timeToColorLight;
       public int timeToColorLightMax;
       public Vector3 newColorLightUpdate;

       public float newPower;
       public int timeToPowerLight;
       public int timeToPowerLightMax;
       public float newPowerUpdate;

       public int newRadius;
       public int timeToRadiusLight;
       public int timeToRadiusLightMax;
       public int newRadiusUpdate;

       public bool Used=true;
       /// <summary>
       /// Light
       /// </summary>
       /// <param name="scriptName"></param>
       /// <param name="x1"></param>
       /// <param name="y1"></param>
       /// <param name="z1"></param>
       /// <param name="t1"></param>
       /// <param name="r"></param>
       /// <param name="g"></param>
       /// <param name="b"></param>
       /// <param name="t2"></param>
       /// <param name="x2"></param>
       /// <param name="y2"></param>
       /// <param name="z2"></param>
       /// <param name="t3"></param>
       /// <param name="rad"></param>
       /// <param name="t4"></param>
       /// <param name="powr"></param>
       /// <param name="t5"></param>
       /// <param name="actor"></param>
       /// <param name="repeat"></param>
       /// <param name="trBy"></param>
       public PBScriptAction(string scriptName, float x1, float y1,float z1, int t1, float r,float g,float b, int t2,float x2,float y2,float z2,int t3,int rad,int t4,float powr,int t5,TriggeredBy actor ,bool repeat)
       {
           Script_Name = scriptName;
           type = ScriptType.Light;
           newPositionLight = new Vector3(x1, y1, z1);
           timeToMoveLight = timeToMoveLightMax = t1;
           newColorLight = new Vector3(r, g, b);
           timeToColorLight = timeToColorLightMax = t2;
           newDirectionLight = new Vector3(x2, y2, z2);
           timeToRotateLight = timeToRotateLightMax = t3;
           newRadius = rad;
           timeToRadiusLight = timeToRadiusLightMax = t4;
           newPower = powr;
           timeToPowerLight = timeToPowerLightMax = t5;
           this.actor = actor;
           this.repeat = repeat;

           newRotationLightAngle = (float)Math.Atan2(Convert.ToDouble(x2),Convert.ToDouble(y2));
           
       }
       #endregion

       #region Quest Script

       [Category("Quest")]
       public Vector2 Location { get { return location; } }
       [Category("Quest")]
       public QuestType Quest_Type { get { return questType; } }
       [Category("Quest")]
       public bool Is_Monster { get { return monster; } }
       [Category("Quest")]
       public object Object { get { return objID; } }
       [Category("Quest")]
       public int Count { get { return count; } }
       [Category("Quest")]
       public int Xp { get { return xp; } }
       [Category("Quest")]
       public int Gold { get { return gold; } }

      public bool monster;
      public Vector2 location;
      public object objID;
      public int count;
      public int xp;
      public int gold;
      public QuestType questType;
      /// <summary>
      /// Quest Mob/Item
      /// </summary>
      /// <param name="monster"></param>
      /// <param name="objID"></param>
      /// <param name="count"></param>
      /// <param name="xp"></param>
      /// <param name="gold"></param>
      /// <param name="act"></param>
       public PBScriptAction(bool monster,object objID,int count,int xp,int gold,TriggeredBy act)
       {
           if (monster) questType = QuestType.monsterQuest; else questType = QuestType.itemQuest;
           this.monster = monster;
           this.objID = objID;
           this.gold = gold;
           this.count = count;
           this.xp = xp;
           type = ScriptType.Quest;
           actor = act;
       }
       /// <summary>
       /// Quest Location
       /// </summary>
       /// <param name="location"></param>
       /// <param name="xp"></param>
       /// <param name="gold"></param>
       /// <param name="act"></param>
       public PBScriptAction(Vector2 location, int xp, int gold,TriggeredBy act)
       {
           questType = QuestType.roadQuest;
           this.location = location;
           this.gold = gold;
           this.xp = xp;
           type = ScriptType.Quest;
           actor = act;
       }
       #endregion

       #region Teleport Script
       [Category("Teleport")]
       public Vector2 Position_Teleport { get { return positionToGo; } }
       [Category("Teleport")]
       public string Level_Teleport { get { return levelToGo; } }

      public Vector2 positionToGo;
      public string levelToGo;
       /// <summary>
       /// Teleport
       /// </summary>
       /// <param name="ptgo"></param>
       /// <param name="act"></param>
       /// <param name="s"></param>
       public PBScriptAction(Vector2 ptgo,TriggeredBy act ,string s = null)
       {
           actor = act;
           positionToGo = ptgo;
           levelToGo = s;
           type = ScriptType.Teleport;
       }
       #endregion

       #region Big Message
       [Category("Message")]
       public string Text { get { return messageText; } }
       [Category("Message")]
       public string Pic_Name { get { return messagePicName; } }
       [Category("Message")]
       public int Time_Alive { get { return messageTtl; } }


       public Vector2 messagePos;
       public Vector2 messageDest;
       public string messageText;
       public int messageTtl;
       public Color messageColor;
       public string messageBackName;
       public string messagePicName;
       /// <summary>
       /// Message on custom location
       /// </summary>
       /// <param name="v1"></param>
       /// <param name="v2"></param>
       /// <param name="txt"></param>
       /// <param name="ttl"></param>
       /// <param name="col"></param>
       /// <param name="s1"></param>
       /// <param name="s2"></param>
       /// <param name="act"></param>
       public PBScriptAction(Vector2 v1,Vector2 v2,string txt,int ttl,Color col,string s1,string s2,TriggeredBy act)
       {
           type = ScriptType.BigMessage;
           actor = act;
           messageColor = col;
           messageDest = v2;
           messagePos = v1;
           messageText = txt;
           messageTtl = ttl;
           messageBackName = s1;
           messagePicName = s2;
       }
        #endregion

       #region Spawn

       [Category("Spawn")]
       public string Mob_Name { get { return mobToSpawn; } }
       [Category("Spawn")]
       public int Count_Spawns { get { return countToSpawn; } }
       [Category("Spawn")]
       public int Time_Spawns { get { return timeToSpawn; } }
       [Category("Spawn")]
       public Vector2 Location_Spawns { get { return locationToSpawn; } }

       public string mobToSpawn;
       public int countToSpawn;
       public int timeToSpawn;
       public Vector2 locationToSpawn;
       /// <summary>
       /// Spawn
       /// </summary>
       /// <param name="location"></param>
       /// <param name="mob"></param>
       /// <param name="count"></param>
       /// <param name="time"></param>
       /// <param name="act"></param>
       public PBScriptAction(Vector2 location,string mob,int count,int time,TriggeredBy act)
       {
           locationToSpawn = location;
           type = ScriptType.Spawn;
           mobToSpawn = mob;
           countToSpawn = count;
           timeToSpawn = time;
           actor = act;
       }

       public PBScriptAction()
       {
       }
        #endregion

       #region Sound

       [Category("Sound")]
       public string SoundToPlay { get { return soundToBePlayed; } }
       [Category("Sound")]
       public string Sound_Type { get { return soundType; } }

       public string soundToBePlayed;
       public string soundType;

       /// <summary>
       /// Sound
       /// </summary>
       /// <param name="sType"></param>
       /// <param name="soundName"></param>
       public PBScriptAction(string sType, string soundName,TriggeredBy act)
       {
           soundType = sType;
           soundToBePlayed = soundName;
           type = ScriptType.Sound;
           actor = act;
       }


       #endregion


   }
}
