﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using PBEngine.Effect;
using System.IO;
using ProjectBOTEditor;
using System.Diagnostics;
using ProjectBuild.PBEngine.Items;

namespace ProjectBuild.PBEngine
{
    public class PBLayer
    {
        internal List<Texture2D> tileTexturesColor;
        internal List<Texture2D> tileTexturesNormal;
        public int[,] map;
        public int TileWidth;
        public int TileHeight;
        public int MapHeight
        {
            get { return map.GetLength(0); }
        }
        public int MapWidth
        {
            get { return map.GetLength(1); }
        }
        public PBLayer(int[,] map_2, int width, int height, World world, List<Texture2D> tColor, List<Texture2D> tNormal)
        {
            map = (int[,])map_2.Clone();
            TileWidth = width;
            TileHeight = height;
            tileTexturesColor = tColor;
            tileTexturesNormal = tNormal;
        }
        public PBLayer(int width, int height, int mapW, int mapH, World world, List<Texture2D> a, List<Texture2D> b)
        {
            TileWidth = width;
            TileHeight = height;
            map = new int[mapH, mapW];
            tileTexturesColor = a;
            tileTexturesNormal = b;
        }
        public PBLayer(int[,] existingMap, int width, int height, World world)
        {
            map = (int[,])existingMap.Clone();
            TileWidth = width;
            TileHeight = height;
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                {
                    if (map[y, x] < 0) continue;
                    if (map[y, x] >= 10000)
                        BodyFactory.CreateRectangle(world,
                            PBConverter.PixelToMeter((float)TileWidth),
                            PBConverter.PixelToMeter((float)TileHeight),
                            1f,
                            PBConverter.PixelToMeter(new Vector2(x * TileWidth, y * TileHeight) + new Vector2(TileWidth / 2f, TileHeight / 2f)));
                }
            tileTexturesColor = new List<Texture2D>();
            tileTexturesNormal = new List<Texture2D>();
        }
        public void SetCellIndex(int x, int y, int index)
        {
            if(index==-1)
            if (map[y, x] == -1 || map[y, x] == 9999)
                return;
                if (map[y, x] >= 9999)
                    map[y, x] = index + 10000;
                else
                    map[y, x] = index;
        }
        public int GetCellIndex(int x, int y)
        {
            return map[y, x];
        }
        public void SetSolidCell(int x, int y)
        {
            if (map[y, x] < -1)
                return;
            if (map[y, x] >= 9999)
                map[y, x] -= 10000;
            else
                map[y, x] += 10000;
        }
        public void DrawColor(PBCamera camera, SpriteBatch sprite)
        {
            Viewport viewport = camera.Viewport;
            // int startX = (int)camera.Position.X / TileWidth - 1;
            // if (startX < 0) startX = 0;
            //  int startY = (int)camera.Position.Y / TileHeight - 1;
            // if (startY < 0) startY = 0;
            //int mapWidth = (int)(camera.Position.X + viewport.Width) / TileWidth + 1;
            // if (mapWidth > map.GetLength(1))
            //    mapWidth = map.GetLength(1);

            // int mapHeight = (int)(camera.Position.Y + viewport.Height) / TileHeight + 1;
            //if (mapHeight > map.GetLength(0))
            //   mapHeight = map.GetLength(0);
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                {
                    int textureIndex = map[y, x];

                    if (textureIndex == -1||textureIndex==9999)
                        continue;
                    if (textureIndex >= 10000)
                        textureIndex -= 10000;
                    Texture2D texture = tileTexturesColor[textureIndex];
                    sprite.Draw(texture,
                        new Rectangle(
                        x * TileWidth,
                        y * TileHeight,
                        TileWidth,
                        TileHeight)
                        , new Color(255, 255, 255, 255));


                }
        }
        public void DrawColorNoWalls(PBCamera camera, SpriteBatch sprite)
        {
            Viewport viewport = camera.Viewport;
            // int startX = (int)camera.Position.X / TileWidth - 1;
            // if (startX < 0) startX = 0;
            //  int startY = (int)camera.Position.Y / TileHeight - 1;
            // if (startY < 0) startY = 0;
            //int mapWidth = (int)(camera.Position.X + viewport.Width) / TileWidth + 1;
            // if (mapWidth > map.GetLength(1))
            //    mapWidth = map.GetLength(1);

            // int mapHeight = (int)(camera.Position.Y + viewport.Height) / TileHeight + 1;
            //if (mapHeight > map.GetLength(0))
            //   mapHeight = map.GetLength(0);
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                {
                    int textureIndex = map[y, x];

                    if (textureIndex == -1 || textureIndex == 9999)
                        continue;
                    if (textureIndex >= 10000)
                        continue;
                    Texture2D texture = tileTexturesColor[textureIndex];
                    sprite.Draw(texture,
                        new Rectangle(
                        x * TileWidth,
                        y * TileHeight,
                        TileWidth,
                        TileHeight)
                        , new Color(255, 255, 255, 255));


                }
        }
        public void DrawColorOnlyWalls(PBCamera camera, SpriteBatch sprite)
        {
            Viewport viewport = camera.Viewport;
            // int startX = (int)camera.Position.X / TileWidth - 1;
            // if (startX < 0) startX = 0;
            //  int startY = (int)camera.Position.Y / TileHeight - 1;
            // if (startY < 0) startY = 0;
            //int mapWidth = (int)(camera.Position.X + viewport.Width) / TileWidth + 1;
            // if (mapWidth > map.GetLength(1))
            //    mapWidth = map.GetLength(1);

            // int mapHeight = (int)(camera.Position.Y + viewport.Height) / TileHeight + 1;
            //if (mapHeight > map.GetLength(0))
            //   mapHeight = map.GetLength(0);
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                {
                    int textureIndex = map[y, x];

                    if (textureIndex < 10000)
                        continue;
                    if (textureIndex >= 10000)
                        textureIndex -= 10000;
                    Texture2D texture = tileTexturesColor[textureIndex];
                    sprite.Draw(texture,
                        new Rectangle(
                        x * TileWidth,
                        y * TileHeight,
                        TileWidth,
                        TileHeight)
                        , new Color(255, 255, 255, 255));


                }
        }
        public void DrawNormal(PBCamera camera, SpriteBatch sprite)
        {
            Viewport viewport = camera.Viewport;
            Color tint;
            // int startX = (int)camera.Position.X / TileWidth - 1;
            // if (startX < 0) startX = 0;
            //  int startY = (int)camera.Position.Y / TileHeight - 1;
            // if (startY < 0) startY = 0;
            //int mapWidth = (int)(camera.Position.X + viewport.Width) / TileWidth + 1;
            // if (mapWidth > map.GetLength(1))
            //    mapWidth = map.GetLength(1);

            // int mapHeight = (int)(camera.Position.Y + viewport.Height) / TileHeight + 1;
            //if (mapHeight > map.GetLength(0))
            //   mapHeight = map.GetLength(0);
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                {
                    tint = Color.White;
                    int textureIndex = map[y, x];

                    if (textureIndex == -1||textureIndex==9999)
                        continue;
                    if (textureIndex >= 10000)
                    {
                        textureIndex -= 10000;
                        tint = Color.Red;
                    }
                    Texture2D texture = tileTexturesNormal[textureIndex];
                    sprite.Draw(texture,
                        new Rectangle(
                        x * TileWidth,
                        y * TileHeight,
                        TileWidth,
                        TileHeight)
                        , tint);

                }
        }
        public void DrawDebug(PBCamera camera, SpriteBatch sprite, Texture2D tex)
        {
            Viewport viewport = camera.Viewport;
            Color tint = Color.White;
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                {
                    int textureIndex = map[y, x];
                    tint = Color.Transparent;
                    if (textureIndex == -1)
                        tint = Color.Gray;
                    if (textureIndex >= 10000)
                        tint = Color.Red;
                    if (textureIndex == 9999)
                        tint = Color.DarkRed;

                    sprite.Draw(tex,
                        new Rectangle(
                        x * TileWidth,
                        y * TileHeight,
                        TileWidth,
                        TileHeight)
                        , tint);


                }
        }
        public void DrawDepth(PBPlayer player, PBCamera camera, SpriteBatch sprite, Texture2D blank)
        {
            Viewport viewport = camera.Viewport;
            // int startX = (int)camera.Position.X / TileWidth - 1;
            // if (startX < 0) startX = 0;
            //  int startY = (int)camera.Position.Y / TileHeight - 1;
            // if (startY < 0) startY = 0;
            //int mapWidth = (int)(camera.Position.X + viewport.Width) / TileWidth + 1;
            // if (mapWidth > map.GetLength(1))
            //    mapWidth = map.GetLength(1);

            // int mapHeight = (int)(camera.Position.Y + viewport.Height) / TileHeight + 1;
            //if (mapHeight > map.GetLength(0))
            //   mapHeight = map.GetLength(0);
            Color tint = new Color();
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                {
                    tint = Color.White;
                    int textureIndex = map[y, x];

                    if (textureIndex == -1)
                        continue;
                    if (textureIndex >= 10000)
                        textureIndex -= 10000;
                    tint = Color.DarkGray;
                    Texture2D texture = tileTexturesNormal[textureIndex];
                    sprite.Draw(blank,
                        new Rectangle(
                        x * TileWidth,
                        y * TileHeight,
                        TileWidth,
                        TileHeight)
                        , tint);


                }
        }
        public void NewSave(string path, Dictionary<string,PBEditorObject> list, Dictionary<string,PBLight> lights,Dictionary<string,PBEventObject> events)
        {
            using (FileStream wr = new FileStream(path,FileMode.OpenOrCreate))
            {
                using (BinaryWriter writer = new BinaryWriter(wr))
                {
                    SaveLayer(writer);
                    SaveMOL(writer, list, lights,events);
                }
            }
        }
        public static PBLayer NewLoad(string path)
        {
            PBLayer layer = null;
            
            return layer;
        }
        public static PBLayer LoadEditor(string path,ref Dictionary<string,PBEditorObject> list, ref Dictionary<string,PBLight> lights,ref Dictionary<string,PBEventObject> eventObj ,World world, List<Texture2D> tColor, List<Texture2D> tNormal, PBEditorObjects PBEdObj, EditorForm form)
        {
            PBLayer layer = null;
            int[,] map;
            int width;
            int height;
            float f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13;
            string nameEditor;
            string nameInGame;
            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    width = reader.ReadInt32();
                    height = reader.ReadInt32();
                    map = new int[height, width];
                    for (int i = 0; i < height; i++)
                        for (int j = 0; j < width; j++)
                            map[i, j] = reader.ReadInt32();
                    int objCount = reader.ReadInt32();
                    for (int i = 0; i < objCount; i++)
                    {
                        string objtype = reader.ReadString();
                         f1 =(float) reader.ReadDouble();
                         f2 =(float) reader.ReadDouble();
                         f3 =(float) reader.ReadDouble();
                         if (objtype != "SPAWN")
                         {
                             nameEditor = reader.ReadString();
                             nameInGame = reader.ReadString();
                             if(PBEdObj.objectDictionary.ContainsKey(nameInGame))
                             form.AddObject(new PBEditorObject()
                             {
                                 body = BodyFactory.CreateRectangle(world, PBEdObj.objectDictionary[nameInGame].Width, PBEdObj.objectDictionary[nameInGame].Height, 1f, new Vector2(f1, f2)),
                                 name = nameEditor,
                                 type = PBEdObj.objectDictionary[nameInGame].type,
                                 Width = PBEdObj.objectDictionary[nameInGame].Width,
                                 Height = PBEdObj.objectDictionary[nameInGame].Height,
                                 rotation = f3,
                                 blank = form.blankTexture,
                                 textureC = PBEdObj.objectDictionary[nameInGame].textureC,
                                 textureN = PBEdObj.objectDictionary[nameInGame].textureN
                             });
                         }
                         else
                         {
                             form.AddObject(new PBEditorObject()
                             {
                                 body = BodyFactory.CreateRectangle(world, 1f, 1f, 1f, new Vector2(f1,f2)),
                                 name = "Player",
                                 type = PBEditorObject.Type.Spawn,
                                 Width = 1f,
                                 Height = 1f,
                                 rotation = f3,
                                 blank = form.blankTexture,
                                 textureC = PBEdObj.objectDictionary["Player"].textureC,
                                 textureN = PBEdObj.objectDictionary["Player"].textureN
                             });
                         }
                           

                    }
                    int i1;
                    int lightCount = reader.ReadInt32();
                    for (int i = 0; i < lightCount; i++)
                    {
                        string lightType = reader.ReadString();
                        f1 =(float) reader.ReadDouble();
                        f2 = (float)reader.ReadDouble();
                        f3 = (float)reader.ReadDouble();
                        f4 = (float)reader.ReadDouble();
                        f5 = (float)reader.ReadDouble();
                        i1 = reader.ReadInt32();
                        f6 = (float)reader.ReadDouble();
                        f7 = (float)reader.ReadDouble();
                        f8 = (float)reader.ReadDouble();
                        f9 = (float)reader.ReadDouble();
                        f10 = (float)reader.ReadDouble();
                        f11 = (float)reader.ReadDouble();
                        if(lightType=="POINTLIGHT")
                        form.AddLight(new PointLight()
                        {
                            Position = new Vector3(f6,f7,f8),
                            Power = f4,
                            Color = new Vector4(f1, f2, f3,1f),
                            Direction = new Vector3(f9, f10, f11),
                            SpecularPower = f5,
                            LightDecay = i1,
                            IsEnabled = true
                        });
                        else if(lightType=="SPOTLIGHT")
                            form.AddLight(new SpotLight()
                            {
                                Position = new Vector3(f6, f7, f8),
                                Power = f4,
                                Color = new Vector4(f1, f2, f3, 1f),
                                Direction = new Vector3(f9, f10, f11),
                                SpecularPower = f5,
                                LightDecay = i1,
                                IsEnabled = true
                            });
                    }
                    //ADD SCRIPT LOAD ----------------------------------------------------------------------------------------------------------------//
                    int eventCount = reader.ReadInt32();
                    eventObj = new Dictionary<string, PBEventObject>();
                    string scriptName;
                    PBScriptAction a=new PBScriptAction();
                    bool repeat;
                    int int1, int2, int3, int4, int5, int6;
                    for (int i = 0; i < eventCount; i++)
                    {
                        f1 = (float)reader.ReadDouble();
                        f2 = (float)reader.ReadDouble();
                        f3 = (float)reader.ReadDouble();
                        f4 = (float)reader.ReadDouble();
                        string eventscriptname = reader.ReadString();
                        int actionCount = reader.ReadInt32();
                        eventObj.Add(eventscriptname, new PBEventObject(world, f3, f4, new Vector2(f1, f2)));
                        System.Windows.Forms.TreeNode node=new System.Windows.Forms.TreeNode(eventscriptname);
                        form.scriptTreeView.Nodes.Add(node);
                        for (int j = 0; j < actionCount; j++)
                        {
                            string ownName = reader.ReadString();
                            string actor = reader.ReadString();
                            string TYPE = reader.ReadString();
                            PBScriptAction.TriggeredBy act=new PBScriptAction.TriggeredBy();
                            switch(actor)
                            {
                                case "ALL": act=PBScriptAction.TriggeredBy.All; break;
                                case "PAO": act=PBScriptAction.TriggeredBy.PlayerActionOnly; break;
                                case "PO": act=PBScriptAction.TriggeredBy.PlayerOnly; break;
                            }
                            switch (TYPE)
                            {
                                case "LIGHT":
                                    scriptName = reader.ReadString();
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    f3 = (float)reader.ReadDouble();
                                    int1 = reader.ReadInt32();
                                    f4 = (float)reader.ReadDouble();//color
                                    f5 = (float)reader.ReadDouble();
                                    f6 = (float)reader.ReadDouble();
                                    int2 = reader.ReadInt32();
                                    f7 = (float)reader.ReadDouble();
                                    f8 = (float)reader.ReadDouble();//directie
                                    f9 = (float)reader.ReadDouble();
                                    int3 = reader.ReadInt32();
                                    int4 = reader.ReadInt32();//radius
                                    int5 = reader.ReadInt32();
                                    f13 = (float)reader.ReadDouble();//powa
                                    int6 = reader.ReadInt32();
                                    repeat=reader.ReadBoolean();
                                    a = new PBScriptAction(scriptName, f1, f2, f3, int1, f4, f5, f6, int2, f7, f8, f9, int3, int4, int5, f13, int6, act, repeat);
                                    eventObj[eventscriptname].AddAction(a);
                                    break;
                                case "OBJECT":
                                    scriptName = reader.ReadString();
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    int1 = reader.ReadInt32();
                                    f3 = (float)reader.ReadDouble();
                                    int2 = reader.ReadInt32();
                                    repeat=reader.ReadBoolean();
                                    a = new PBScriptAction(scriptName, f1, f2, int1, f3, int2, act, repeat);
                                    eventObj[eventscriptname].AddAction(a);
                                    break;
                                case "QUEST":
                                    string mobtype = reader.ReadString();
                                    switch (mobtype)
                                    {
                                        case "MOB": 
                                            string mobname=reader.ReadString();
                                            int1=reader.ReadInt32();
                                            int2=reader.ReadInt32();
                                            int3=reader.ReadInt32();
                                            a=new PBScriptAction(true,Monsters.FromName(mobname),int1,int2,int3,act);
                                            eventObj[eventscriptname].AddAction(a);
                                            break;
                                        case "ITEM": 
                                            string itemname=reader.ReadString();
                                            int1=reader.ReadInt32();
                                            int2=reader.ReadInt32();
                                            int3=reader.ReadInt32();
                                            a=new PBScriptAction(false,PBItems.FromName(itemname),int1,int2,int3,act);
                                            eventObj[eventscriptname].AddAction(a);
                                            break;
                                        case "ROAD": 
                                            f1 = (float)reader.ReadDouble();
                                            f2 = (float)reader.ReadDouble();
                                            int1=reader.ReadInt32();
                                            int2=reader.ReadInt32();
                                            a = new PBScriptAction(new Vector2(f1, f2), int1, int2, act);
                                            eventObj[eventscriptname].AddAction(a);
                                            break;
                                    }
                                    break;
                                case "MESSAGE":
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    f3 = (float)reader.ReadDouble();
                                    f4 = (float)reader.ReadDouble();
                                    int1 = reader.ReadInt32();
                                    string txt = reader.ReadString();
                                    f5 = (float)reader.ReadDouble();
                                    f6 = (float)reader.ReadDouble();
                                    f7 = (float)reader.ReadDouble();
                                    string back = reader.ReadString();
                                    string pic = reader.ReadString();
                                    a = new PBScriptAction(new Vector2(f1, f2), new Vector2(f3, f4), txt, int1, new Color(f1, f2, f3), back, pic, act);
                                    eventObj[eventscriptname].AddAction(a);
                                    break;
                                case "TELEPORT":
                                    string lvltogo = reader.ReadString();
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    a = new PBScriptAction(new Vector2(f1, f2), act, lvltogo);
                                    eventObj[eventscriptname].AddAction(a);
                                    break;
                                case "SPAWN":
                                    f1 = (float)reader.ReadDouble();
                                    f2 = (float)reader.ReadDouble();
                                    string mobtospawn=reader.ReadString();
                                    int1=reader.ReadInt32();
                                    int2 = reader.ReadInt32();
                                    a = new PBScriptAction(new Vector2(f1, f2), mobtospawn, int1, int2, act);
                                    eventObj[eventscriptname].AddAction(a);
                                    break;
                                case "SOUND":
                                    string type = reader.ReadString();
                                    string name = reader.ReadString();
                                    a = new PBScriptAction(type, name, act);
                                    eventObj[eventscriptname].AddAction(a);
                                    break;
                            }
                            a.OwnScriptName = ownName;
                            node.Nodes.Add(ownName);
                            PBEdObj.actions.Add(ownName, a);
                        }
                    }
                }
            }
            layer = new PBLayer(map, 80, 80, world, tColor, tNormal);
            return layer;
        }
        
        /// <summary>
        /// Save Mobs, Objects, Lights
        /// </summary>
        /// <param name="stringB">Saves on a string builder</param>
        /// <param name="lights">The collection of lights you save</param>
        /// <param name="list">The collection of mobs and objects you save</param>
        private void SaveMOL(BinaryWriter writer, Dictionary<string,PBEditorObject> list, Dictionary<string,PBLight> lights,Dictionary<string,PBEventObject> events)
        {
            string nameEditor = null;
            string nameInGame = null;
            double f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11;
            int i1;
            writer.Write(list.Count);
            foreach (string key in list.Keys)
            {
                PBEditorObject l = list[key];
                if (l.type == PBEditorObject.Type.Object)
                    writer.Write("OBJECT");
                else
                    if (l.type == PBEditorObject.Type.Mob)
                        writer.Write("MOB");

                    else
                        if (l.type == PBEditorObject.Type.Spawn)
                            writer.Write("SPAWN");
                        else
                            if (l.type == PBEditorObject.Type.Drop)
                            {
                                writer.Write("DROPI");
                            }
                f1 = l.position.X;
                f2 = l.position.Y;
                f3 = l.rotation;
                if (l.type != PBEditorObject.Type.Spawn)
                {
                    nameEditor = l.name;
                    nameInGame = l.name.Remove(l.name.LastIndexOf("_"));
                }
                writer.Write(f1);
                writer.Write(f2);
                writer.Write(f3);
                if (l.type != PBEditorObject.Type.Spawn)
                {
                    writer.Write(nameEditor);
                    writer.Write(nameInGame);
                }
                
            }
            writer.Write(lights.Count);
            foreach (string key in lights.Keys)
            {
                PBLight l = lights[key];
                if (l.LightType == LightType.Point)
                    writer.Write("POINTLIGHT");

                if (l.LightType == LightType.Spot)
                    writer.Write("SPOTLIGHT");

                f1 = l.Color.X;
                f2 = l.Color.Y;
                f3 = l.Color.Z;
                f4 = l.Power;
                f5 = l.SpecularPower;
                i1 = l.LightDecay;
                f6 = l.Position.X;
                f7 = l.Position.Y;
                f8 = l.Position.Z;
                f9 = l.Direction.X;
                f10 = l.Direction.Y;
                f11 = l.Direction.Z;
                writer.Write(f1);
                writer.Write(f2);
                writer.Write(f3);
                writer.Write(f4);
                writer.Write(f5);
                writer.Write(i1);
                writer.Write(f6);
                writer.Write(f7);
                writer.Write(f8);
                writer.Write(f9);
                writer.Write(f10);
                writer.Write(f11);
            }
            writer.Write(events.Count);
            foreach (string key in events.Keys)
            {
                writer.Write(Convert.ToDouble(events[key].Position.X));
                writer.Write(Convert.ToDouble(events[key].Position.Y));
                writer.Write(Convert.ToDouble(events[key].width));
                writer.Write(Convert.ToDouble(events[key].height));
                writer.Write(key);
                writer.Write(events[key].Actions.Count);
                foreach (PBScriptAction a in events[key].Actions)
                {
                    writer.Write(a.OwnScriptName);
                    switch (a.actor)
                    {
                        case PBScriptAction.TriggeredBy.All:
                            writer.Write("ALL");
                            break;
                        case PBScriptAction.TriggeredBy.PlayerActionOnly:
                            writer.Write("PAO");
                            break;
                        case PBScriptAction.TriggeredBy.PlayerOnly:
                            writer.Write("PO");
                            break;
                    }
                    switch (a.type)
                    {
                        case PBScriptAction.ScriptType.Light:
                            writer.Write("LIGHT");
                            writer.Write(a.Script_Name);
                            writer.Write(Convert.ToDouble(a.newPositionLight.X));
                            writer.Write(Convert.ToDouble(a.newPositionLight.Y));
                            writer.Write(Convert.ToDouble(a.newPositionLight.Z));
                            writer.Write(a.timeToMoveLight);
                            writer.Write(Convert.ToDouble(a.newColorLight.X));
                            writer.Write(Convert.ToDouble(a.newColorLight.Y));
                            writer.Write(Convert.ToDouble(a.newColorLight.Z));
                            writer.Write(a.timeToColorLight);
                            writer.Write(Convert.ToDouble(a.newDirectionLight.X));
                            writer.Write(Convert.ToDouble(a.newDirectionLight.Y));
                            writer.Write(Convert.ToDouble(a.newDirectionLight.Z));
                            writer.Write(a.timeToRotateLight);
                            writer.Write(a.newRadius);
                            writer.Write(a.timeToRadiusLight);
                            writer.Write(Convert.ToDouble(a.newPower));
                            writer.Write(a.timeToPowerLight);
                            writer.Write(a.repeat);
                            break;
                        case PBScriptAction.ScriptType.Object:
                            writer.Write("OBJECT");
                            writer.Write(a.Script_Name);
                            writer.Write(Convert.ToDouble(a.newPosition.X));
                            writer.Write(Convert.ToDouble(a.newPosition.Y));
                            writer.Write(a.timeToMove);
                            writer.Write(Convert.ToDouble(a.newRotation));
                            writer.Write(a.timeToRotate);
                            writer.Write(a.repeat);
                            break;
                        case PBScriptAction.ScriptType.Quest:
                            writer.Write("QUEST");
                            switch (a.questType)
                            {
                                case PBQuest.QuestType.monsterQuest:
                                    writer.Write("MOB");
                                    PBMob b = (PBMob)a.objID;
                                    writer.Write(b.Name);
                                    writer.Write(a.count);
                                    writer.Write(a.xp);
                                    writer.Write(a.gold);
                                    break;
                                case PBQuest.QuestType.itemQuest:
                                    writer.Write("ITEM");
                                    PBItem c = (PBItem)a.objID;
                                    writer.Write(c.name);
                                    writer.Write(a.count);
                                    writer.Write(a.xp);
                                    writer.Write(a.gold);
                                    break;
                                case PBQuest.QuestType.roadQuest:
                                    writer.Write("ROAD");
                                    writer.Write(Convert.ToDouble(a.location.X));
                                    writer.Write(Convert.ToDouble(a.location.Y));
                                    writer.Write(a.xp);
                                    writer.Write(a.gold);
                                    break;
                            }
                            break;
                        case PBScriptAction.ScriptType.BigMessage:
                            writer.Write("MESSAGE");
                            writer.Write(Convert.ToDouble(a.messagePos.X));
                            writer.Write(Convert.ToDouble(a.messagePos.Y));
                            writer.Write(Convert.ToDouble(a.messageDest.X));
                            writer.Write(Convert.ToDouble(a.messageDest.Y));
                            writer.Write(a.messageTtl);
                            writer.Write(a.messageText);
                            writer.Write(Convert.ToDouble(a.messageColor.ToVector3().X));
                            writer.Write(Convert.ToDouble(a.messageColor.ToVector3().Y));
                            writer.Write(Convert.ToDouble(a.messageColor.ToVector3().Z));
                            writer.Write(a.messageBackName);
                            writer.Write(a.messagePicName);
                            break;
                        case PBScriptAction.ScriptType.Teleport:
                            writer.Write("TELEPORT");
                            if (a.levelToGo == null)
                                writer.Write("SAME");
                            else
                                writer.Write(a.levelToGo);
                            writer.Write(Convert.ToDouble(a.positionToGo.X));
                            writer.Write(Convert.ToDouble(a.positionToGo.Y));
                            break;
                        case PBScriptAction.ScriptType.Spawn:
                            writer.Write("SPAWN");
                            writer.Write(Convert.ToDouble(a.locationToSpawn.X));
                            writer.Write(Convert.ToDouble(a.locationToSpawn.Y));
                            writer.Write(a.mobToSpawn);
                            writer.Write(a.countToSpawn);
                            writer.Write(a.timeToSpawn);
                            break;
                        case PBScriptAction.ScriptType.Sound:
                            writer.Write("SOUND");
                            writer.Write(a.soundType);
                            writer.Write(a.soundToBePlayed);
                            break;
                    }

                }
            }
        }
        /// <summary>
        /// alt save vechi...
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="list"></param>
        /// <param name="lights"></param>
        private void SaveExtras(ref StringBuilder builder, List<PBEditorObject> list, List<PBLight> lights)
        {
            builder.AppendLine("//OBJ");
            string name;
            foreach (var l in list)
            {
                float x = l.position.X;
                float y = l.position.Y;
                name = l.name;
                if (l.name.LastIndexOf("_") > 0)
                    name = l.name.Remove(l.name.LastIndexOf("_"));
                if (l.type == PBEditorObject.Type.Object)
                {
                    /*builder.AppendLine("AddPBObject(new PBObject(World," +
                             SaveFloat(l.Width) + "," +
                             SaveFloat(l.Height) + "," +
                             "new Vector2(float.Parse(\"" + x.ToString() + "\"),float.Parse(\"" + y.ToString() + "\")),content, MessageManager,\"Crate\",float.Parse(\"" + l.rotation.ToString() + "\")));");
                    builder.AppendLine("//" + l.name);*/
                    builder.AppendLine("AddPBObject(" + SaveFloat(x) + "," + SaveFloat(y) + "," + SaveFloat(l.rotation) + ",Objects.FromName(\"" + name + "\"));");
                    builder.AppendLine("//" + l.name);
                }
                else
                    if (l.type == PBEditorObject.Type.Spawn)
                        builder.AppendLine("Spawn=new Vector2(float.Parse(\"" + x.ToString() + "\"),float.Parse(\"" + y.ToString() + "\"));");
                    else
                    {
                        if (l.type == PBEditorObject.Type.Mob)
                            /*builder.AppendLine("AddPBMob(new PBMob(World," +
                        SaveFloat(l.Width) + "," +
                          SaveFloat(l.Height) + "," +
                        "new Vector2(float.Parse(\"" + x.ToString() + "\"),float.Parse(\"" + y.ToString() + "\")),content, MessageManager,float.Parse(\"" + l.rotation.ToString() + "\")));");*/
                            builder.AppendLine("AddPBMob(" + SaveFloat(x) + "," + SaveFloat(y) + "," + SaveFloat(l.rotation) + ",Monsters.FromName(\"" + name + "\"));");

                        builder.AppendLine("//" + l.name);
                    }
            }
            builder.AppendLine("//LIGHT");
            foreach (var l in lights)
            {
                if (l.LightType == LightType.Point)
                    builder.AppendLine("AddPBLight(new PointLight() {");
                if (l.LightType == LightType.Spot)
                    builder.AppendLine("AddPBLight(new SpotLight() {");
                builder.AppendLine("Color=new Vector4(" + SaveFloat(l.Color.X) + "," + SaveFloat(l.Color.Y) + "," + SaveFloat(l.Color.Z) + "," + SaveFloat(l.Color.W) + "),");
                builder.AppendLine("Power=" + SaveFloat(l.Power) + ",");
                builder.AppendLine("SpecularPower=" + SaveFloat(l.SpecularPower) + ",");
                builder.AppendLine("LightDecay=" + l.LightDecay.ToString() + ",");
                builder.AppendLine("Position=new Vector3(" + SaveFloat(l.Position.X) + "," + SaveFloat(l.Position.Y) + "," + SaveFloat(l.Position.Z) + "),");
                builder.AppendLine("Direction=new Vector3(" + SaveFloat(l.Direction.X) + "," + SaveFloat(l.Direction.Y) + "," + SaveFloat(l.Direction.Z) + "),");
                builder.AppendLine("IsEnabled=true");
                builder.AppendLine("});");

            }
        }
        string SaveFloat(float f)
        {
            return "float.Parse(\"" + f.ToString() + "\")";
        }
        private void SaveLayer(BinaryWriter writer)
        {
            writer.Write(MapWidth);
            writer.Write(MapHeight);

            for (int y = 0; y < MapHeight; y++)
            {
                for (int x = 0; x < MapWidth; x++)
                {
                    writer.Write(map[y, x]);
                }
            }
        }
        /// <summary>
        /// save vechi...
        /// </summary>
        /// <param name="builder"></param>
        private void SaveMap(ref StringBuilder builder)
        {
            string c;
            for (int y = 0; y < MapHeight; y++)
            {
                c = "{";
                for (int x = 0; x < MapWidth; x++)
                {
                    c += (map[y, x].ToString() + ",");
                }
                c += "},";
                builder.AppendLine(c);
            }
        }
        static bool readMap = false;
        static bool readObjects = false;
        static bool readLights = false;
        private int[,] map_2;
        private int width;
        private int height;
        private World world;
        private List<Texture2D> tColor;
        private List<Texture2D> tNormal;
        /// <summary>
        /// vechiu load
        /// </summary>
        /// <param name="path"></param>
        /// <param name="list"></param>
        /// <param name="lights"></param>
        /// <param name="world"></param>
        /// <param name="tColor"></param>
        /// <param name="tNormal"></param>
        /// <param name="PBEdObj"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        public static PBLayer Load(string path, ref List<PBEditorObject> list, ref List<PBLight> lights, World world, List<Texture2D> tColor, List<Texture2D> tNormal, PBEditorObjects PBEdObj, EditorForm form)
        {
            PBLayer layer;
            List<List<int>> tempMap = new List<List<int>>();
            int width;
            int height;
            LightType type = LightType.Point;
            Vector4 color = Vector4.Zero;
            float power = 0f;
            float specpower = 0f;
            int decay = 0;
            Vector3 position = Vector3.Zero;
            Vector3 direction = Vector3.Zero;
            using (StreamReader reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    string Line = reader.ReadLine();
                    Line.Trim();

                    if (Line.Contains("int[,] map = new int[,] {"))
                    {
                        readMap = true;
                        Line = reader.ReadLine();
                    }
                    if (Line.Contains("//OBJ"))
                    {
                        readObjects = true;
                        Line = reader.ReadLine();
                        readLights = false;
                    }
                    if (Line.Contains("//LIGHT"))
                    {
                        readLights = true;
                        Line = reader.ReadLine();
                        readObjects = false;
                    }
                    if (Line.Contains("//ENDMAP"))
                    {
                        readMap = false;
                    }
                    if (Line.Contains("base.load()"))
                    {
                        readLights = false;
                    }
                    if (readMap)
                    {
                        Line = Line.Replace("{", "");
                        Line = Line.Replace("},", "");
                        Line = Line.Replace("}", "");
                        Line = Line.Remove(Line.Length - 1);
                        string[] numbers = Line.Split(',');
                        List<int> row = new List<int>();
                        foreach (var n in numbers)
                        {
                            if (!string.IsNullOrEmpty(n))
                                row.Add(int.Parse(n));
                        }
                        if (row.Count > 0)
                            tempMap.Add(row);
                    }
                    if (readObjects)
                    {
                        Line = Line.Trim();
                        if (Line.Contains("AddPBObject"))
                        {
                            Line = Line.Replace("AddPBObject(", "");
                            Line = Line.Remove(Line.LastIndexOf("Objects.FromName"));
                        }
                        else
                            if (Line.Contains("AddPBMob"))
                            {
                                Line = Line.Replace("AddPBMob(", "");
                                Line = Line.Remove(Line.LastIndexOf("Monsters.FromName"));
                            }
                        Line = Line.Replace("(\"", "");
                        Line = Line.Replace("\"", "");
                        Line = Line.Replace(")", "");
                        Line = Line.Replace(";", "");
                        Line = Line.Replace("new Vector2(", "");
                        if (Line.Contains("Spawn"))
                        {
                            Line = Line.Replace("Spawn = ", "");
                            Line = Line.Replace("Spawn=", "");
                            string[] temp = Line.Split(',');
                            float[] nr = new float[3];
                            int cont = 0;
                            for (int i = 0; i < temp.Length; i++)
                            {
                                if (temp[i].Contains("content"))
                                    break;
                                nr[cont] = float.Parse(temp[i].Replace("float.Parse", ""));
                                if (!temp[i + 1].Contains("float.Parse"))
                                {
                                    nr[cont] += float.Parse(temp[i + 1]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[i + 1].Length));

                                    i++;
                                }
                                cont++;
                            }
                            form.AddObject(new PBEditorObject()
                            {
                                body = BodyFactory.CreateRectangle(world, 1f, 1f, 1f, new Vector2(nr[0], nr[1])),
                                name = "Player",
                                type = PBEditorObject.Type.Spawn,
                                Width = 1f,
                                Height = 1f,
                                rotation = 0f,
                                blank = form.blankTexture,
                                textureC = PBEdObj.objectDictionary["Player"].textureC,
                                textureN = PBEdObj.objectDictionary["Player"].textureN
                            });
                        }
                        else
                        {
                            Line = Line.Trim();
                            string[] temp = Line.Split(',');
                            float[] nr = new float[5];
                            int cont = 0;
                            float rot = 0f;
                            int goTo = temp.Length - 1;
                            if (temp[temp.Length - 3].Contains("float.Parse"))
                            {
                                goTo -= 2;
                                rot = float.Parse(temp[temp.Length - 3].Replace("float.Parse", ""));
                                rot += float.Parse(temp[temp.Length - 2]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[temp.Length - 2].Length));
                                if (temp[temp.Length - 3].Contains('-') && rot > 0) rot *= -1;
                            }
                            else
                                if (temp[temp.Length - 2].Contains("float.Parse"))
                                {
                                    goTo--;
                                    rot = float.Parse(temp[temp.Length - 2].Replace("float.Parse", ""));
                                }
                            for (int i = 0; i < goTo; i++)
                            {
                                if (temp[i].Contains("content"))
                                    break;
                                nr[cont] = float.Parse(temp[i].Replace("float.Parse", ""));
                                if (!temp[i + 1].Contains("float.Parse"))
                                {
                                    nr[cont] += float.Parse(temp[i + 1]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[i + 1].Length));

                                    i++;
                                }
                                cont++;
                            }
                            string nam = null;
                            Line = reader.ReadLine();
                            Line = Line.Trim();
                            Line = Line.Replace("//", "");
                            nam = Line.Remove(Line.LastIndexOf("_"));
                            //Line = Line.Remove(Line.LastIndexOf("_"));
                            form.AddObject(new PBEditorObject()
                            {
                                body = BodyFactory.CreateRectangle(world, PBEdObj.objectDictionary[nam].Width, PBEdObj.objectDictionary[nam].Height, 1f, new Vector2(nr[0], nr[1])),
                                name = Line,
                                type = PBEdObj.objectDictionary[nam].type,
                                Width = PBEdObj.objectDictionary[nam].Width,
                                Height = PBEdObj.objectDictionary[nam].Height,
                                rotation = rot,
                                blank = form.blankTexture,
                                textureC = PBEdObj.objectDictionary[nam].textureC,
                                textureN = PBEdObj.objectDictionary[nam].textureN
                            });


                        }
                    }
                    if (readLights)
                    {
                        Line = Line.Trim();
                        if (Line.Contains("});"))
                        {
                            if (type == LightType.Point)
                            {
                                form.AddLight(new PointLight()
                                {
                                    Position = position,
                                    Power = power,
                                    Color = color,
                                    Direction = direction,
                                    SpecularPower = specpower,
                                    LightDecay = decay,
                                    IsEnabled = true
                                });
                            }
                            else
                                form.AddLight(new SpotLight()
                                {
                                    Position = position,
                                    Power = power,
                                    Color = color,
                                    Direction = direction,
                                    SpecularPower = specpower,
                                    LightDecay = decay,
                                    IsEnabled = true
                                });
                        }

                        if (Line.Contains("PointLight"))
                            type = LightType.Point;
                        else
                            if (Line.Contains("SpotLight"))
                                type = LightType.Spot;
                        Line = Line.Replace("(\"", "");
                        Line = Line.Replace("\"", "");
                        Line = Line.Replace(")", "");
                        Line = Line.Replace(";", "");
                        if (Line.Contains("Color"))
                        {
                            Line = Line.Replace("Color=new Vector4(", "");
                            Line = Line.Replace("Color = new Vector4(", "");
                            string[] temp = Line.Split(',');
                            float[] nr = new float[5];
                            int cont = 0;
                            for (int i = 0; i < temp.Length - 1; i++)
                            {
                                if (temp[i].Contains("content"))
                                    break;
                                nr[cont] = float.Parse(temp[i].Replace("float.Parse", ""));
                                if (!temp[i + 1].Contains("float.Parse") && temp[i + 1] != "")
                                {
                                    nr[cont] += float.Parse(temp[i + 1]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[i + 1].Length));

                                    i++;
                                }
                                cont++;
                            }
                            color = new Vector4(nr[0], nr[1], nr[2], nr[3]);
                        }
                        if (Line.Contains("SpecularPower"))
                        {
                            Line = Line.Replace("SpecularPower = ", "");
                            Line = Line.Replace("SpecularPower=", "");
                            string[] temp = Line.Split(',');
                            float[] nr = new float[5];
                            int cont = 0;
                            for (int i = 0; i < temp.Length - 1; i++)
                            {
                                if (temp[i].Contains("content"))
                                    break;
                                nr[cont] = float.Parse(temp[i].Replace("float.Parse", ""));
                                if (!temp[i + 1].Contains("float.Parse") && temp[i + 1] != "")
                                {
                                    nr[cont] += float.Parse(temp[i + 1]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[i + 1].Length));

                                    i++;
                                }
                                cont++;
                            }
                            specpower = nr[0];
                        }
                        else
                            if (Line.Contains("Power"))
                            {
                                Line = Line.Replace("Power=", "");
                                Line = Line.Replace("Power = ", "");
                                string[] temp = Line.Split(',');
                                float[] nr = new float[5];
                                int cont = 0;
                                for (int i = 0; i < temp.Length - 1; i++)
                                {
                                    if (temp[i].Contains("content"))
                                        break;
                                    nr[cont] = float.Parse(temp[i].Replace("float.Parse", ""));
                                    if (!temp[i + 1].Contains("float.Parse") && temp[i + 1] != "")
                                    {
                                        nr[cont] += float.Parse(temp[i + 1]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[i + 1].Length));

                                        i++;
                                    }
                                    cont++;
                                }
                                power = nr[0];
                            }

                        if (Line.Contains("LightDecay"))
                        {
                            Line = Line.Replace("LightDecay = ", "");
                            Line = Line.Replace("LightDecay=", "");
                            Line = Line.Remove(Line.Length - 1);
                            decay = int.Parse(Line);

                        }
                        if (Line.Contains("Position"))
                        {
                            Line = Line.Replace("Position = new Vector3(", "");
                            Line = Line.Replace("Position=new Vector3(", "");
                            string[] temp = Line.Split(',');
                            float[] nr = new float[5];
                            int cont = 0;
                            for (int i = 0; i < temp.Length - 1; i++)
                            {
                                if (temp[i].Contains("content"))
                                    break;
                                nr[cont] = float.Parse(temp[i].Replace("float.Parse", ""));
                                if (!temp[i + 1].Contains("float.Parse") && temp[i + 1] != "")
                                {
                                    nr[cont] += float.Parse(temp[i + 1]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[i + 1].Length));

                                    i++;
                                }
                                cont++;
                            }
                            position = new Vector3(nr[0], nr[1], nr[2]);
                        }
                        if (Line.Contains("Direction"))
                        {
                            Line = Line.Replace("Direction = new Vector3(", "");
                            Line = Line.Replace("Direction=new Vector3(", "");
                            string[] temp = Line.Split(',');
                            float[] nr = new float[5];
                            int[] neg = new int[3];
                            neg[0] = neg[1] = neg[2] = 1;
                            int cont = 0;
                            for (int i = 0; i < temp.Length - 1; i++)
                            {
                                if (temp[i].Contains("content"))
                                    break;
                                nr[cont] = float.Parse(temp[i].Replace("float.Parse", ""));
                                if (nr[cont] == 0 && temp[i].Contains("-")) neg[cont] = -1;
                                if (!temp[i + 1].Contains("float.Parse") && temp[i + 1] != "")
                                {
                                    nr[cont] += float.Parse(temp[i + 1]) / (float)Math.Pow(Convert.ToDouble(10), Convert.ToDouble(temp[i + 1].Length));

                                    i++;
                                }
                                cont++;
                            }
                            direction = new Vector3(nr[0] * neg[0], nr[1] * neg[1], nr[2] * neg[2]);
                        }

                    }
                }
                width = tempMap[0].Count;
                height = tempMap.Count;
                int[,] map = new int[height, width];
                for (int i = 0; i < height; i++)
                    for (int j = 0; j < width; j++)
                        map[i, j] = tempMap[i][j];
                layer = new PBLayer(map, 80, 80, world, tColor, tNormal);
            }
            return layer;
        }
    }
}
