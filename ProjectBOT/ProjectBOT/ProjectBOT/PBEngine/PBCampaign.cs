﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProjectBuild.PBEngine
{
   public class PBCampaign
    {
       public List<PBLevel> Levels = new List<PBLevel>();
       List<string> levelNames = new List<string>();
       ScreenManager screenManager { get; set; }
       public string CampainName = null;
       PBPlayer Player = null;
       public PBCampaign(ScreenManager screenManager, string path)
       {
           DirectoryInfo dir = new DirectoryInfo(path);
           FileInfo main = dir.GetFiles("*.pbcmp")[0];
           using (StreamReader reader = new StreamReader(main.Open(FileMode.Open)))
           {
               string line;
               line = reader.ReadLine();
               CampainName = line;
               line = reader.ReadLine();
               int number = int.Parse(line);
               for (int i = 0; i < number; i++)
               {
                   line = reader.ReadLine();
                   if(path.Contains("Content\\PreLevels\\"))
                       levelNames.Add(path+line);
                   else
                   levelNames.Add(System.Windows.Forms.Application.StartupPath+"\\levels\\"+dir.Name+"\\"+line) ;
               }
           }

           this.screenManager = screenManager;
           for (int i = 0; i < levelNames.Count; i++)
               Levels.Add(new PBLevel(levelNames[i],this));
       }
       public void RestorePlayer(PBLevel level)
       {
           if (Player != null)
               level.Player = Player;
           if (Player.GUIManager != null)
           {
               level.Player.GUIManager = Player.GUIManager;
               level.GUIManager = Player.GUIManager;
           }
       }
       public void UpdatePlayer(PBPlayer player,ProjectBuild.GUI.PBGUIManager manager)
       {
           Player = player;
           Player.GUIManager = manager;
       }
       public int IndexOf(string name)
       {
           int index = 0;
           for(int i=0;i<levelNames.Count;i++)
           {
               string s=levelNames[i];
               string temp=s.Remove(0,s.LastIndexOf("\\")+1);
               if (temp == name)
               {
                   index = i;
                   break;
               }
           }
           return index;
       }
       public void EnterLevel(PBLevel actualLvl,int i)
       {
           if (actualLvl != null)
           {
               Player = actualLvl.Player;
               Player.GUIManager = actualLvl.GUIManager;
           }
           //PBLevel lvl = Levels[i];
           PBLevel lvl = new PBLevel(levelNames[i], this);
           if (Player != null)
           {
               
               lvl.Player = Player;
               lvl.Player.Body = FarseerPhysics.Factories.BodyFactory.CreateRectangle(lvl.World, 1f, 1f, 1f, lvl.Spawn);
               lvl.Player.Body.CreateFixture(new FarseerPhysics.Collision.Shapes.CircleShape((float)Math.Sqrt(2)/2f,1f));
               lvl.Player.Body.BodyType = FarseerPhysics.Dynamics.BodyType.Dynamic;
               lvl.Player.Body.FixedRotation = true;
               lvl.Player.Body.CollidesWith = FarseerPhysics.Dynamics.Category.All & 
                   ~FarseerPhysics.Dynamics.Category.Cat9 & 
                   ~FarseerPhysics.Dynamics.Category.Cat31;
               lvl.Player.Body.CollisionCategories = FarseerPhysics.Dynamics.Category.All & ~FarseerPhysics.Dynamics.Category.Cat9;
               lvl.Player.Body.FixtureList[1].CollidesWith = FarseerPhysics.Dynamics.Category.Cat10;
               lvl.Player.Body.FixtureList[1].CollisionCategories = FarseerPhysics.Dynamics.Category.All & FarseerPhysics.Dynamics.Category.Cat31;
               lvl.Player.goodCat = FarseerPhysics.Dynamics.Category.Cat10;
               lvl.Player.Body.SleepingAllowed = false;
               lvl.Player.Body.UserData = lvl.Player;
               //lvl.Player.Body = Player.Body;
               //lvl.World.BodyList.Add(lvl.Player.Body);
               lvl.Player.Position = lvl.Spawn;
               lvl.Player.GUIManager = Player.GUIManager;
               lvl.GUIManager = Player.GUIManager;
           }
           LoadingScreen.Load(screenManager, true, 0, lvl);
       }


       internal void Restart()
       {
           Player.Reset();
           Player.GUIManager.Reset();
           EnterLevel(null, 0);
       }
    }
}
