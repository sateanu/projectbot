﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ProjectBuild.PBEngine.Items;
using Microsoft.Xna.Framework.Content;
using ProjectBuild.GUI;
using Microsoft.Xna.Framework.Graphics;
using PBEngine.Effect;

namespace ProjectBuild.PBEngine
{
    public class PBPlayer : PBObject
    {
        MouseState mouse;
        MouseState prevMouse;
        public PBGun Gun;
        public bool IsGun = false;
        public PBGUIManager GUIManager;
        int count = 1;
        public float Speed = 1f;
        public int FireRate { get { return Gun.FireRate; } set { Gun.FireRate = value; } }
       public int Xp;
       public int XpRequired;
       public int Level;
       public int StatPoints;
       public bool ShowRequestedAction=false;
       XPBar XpBar;
       Texture2D senile;
       float unghiSenile;
       PBParticleEngine particleEngine;
       public int Gold=0;
       List<PBQuest> quests;
       PBAnimation questAnim;
       public int DMG = 0;
       public int FireRT = 0;
       public PBPlayer(World world, float height, float width)
            : base(world, height, width)
        {
            Body.BodyType = BodyType.Dynamic;
        }
        public PBPlayer(World world, float height, float width,Vector2 position,ContentManager content,PBMessageManager message)
            : base(world, height, width,position,content,message)
        {
            Body.BodyType = BodyType.Dynamic;
            Body.FixedRotation = true;
            Body.CollidesWith = Category.All & ~Category.Cat9 & ~Category.Cat31;
            Body.CollisionCategories = Category.All & ~Category.Cat9 ;
            Body.SleepingAllowed = false;
            if (SHIELDMAX > 0)
            {
                Body.FixtureList[1].CollidesWith = Category.Cat10;
                Body.FixtureList[1].CollisionCategories =Category.All & Category.Cat31;
                goodCat = Category.Cat10;
            }
            //Gun = new PBGun(PBGun.GunType.Bullet, this,world,content,20,30);
            HpBar = new GUI.HealthBar(this, content, 2);
            SBar = new ShieldBar(this, content, 2);
            Reset();
            XpBar = new XPBar(this, content);
            senile = content.Load<Texture2D>("Mobs/senile");
            List<Texture2D>t=new List<Texture2D>();
            t.Add(content.Load<Texture2D>("FX/circle"));
            quests = new List<PBQuest>();
            particleEngine = new PBParticleEngine(t, PBConverter.MeterToPixel(Position),20);
            Texture2D qanimTex=content.Load<Texture2D>("FX/qAnim");
            questAnim = new PBAnimation(qanimTex, qanimTex.Width, qanimTex.Height, 5,1, 0.2f, true);
            questAnim.Activate();
            this.prevHp = HP;
        }

       public float topRot;
       public int RemovePoints;
        internal void HandleInput(InputState input,PlayerIndex index,PBCamera camera,ContentManager content)
        {
            PlayerIndex outIndex;
            KeyboardState keyboardState = input.CurrentKeyboardStates[(int)index];
            Vector2 movement = Vector2.Zero;
            mouse = Mouse.GetState();
            
            if (keyboardState.IsKeyDown(Keys.A)||
                input.IsButtonPress(Buttons.DPadLeft,index,out outIndex))
                movement.X--;
            if (keyboardState.IsKeyDown(Keys.D) ||
                input.IsButtonPress(Buttons.DPadRight, index, out outIndex))
                movement.X++;
            if (keyboardState.IsKeyDown(Keys.W) ||
                input.IsButtonPress(Buttons.DPadDown, index, out outIndex))
                movement.Y--;
            if (keyboardState.IsKeyDown(Keys.S) ||
                input.IsButtonPress(Buttons.DPadDown, index, out outIndex))
                movement.Y++;

            if (input.GamePadWasConnected[0])
            {
                movement.X += input.CurrentGamePadStates[0].ThumbSticks.Left.X;
                movement.Y -= input.CurrentGamePadStates[0].ThumbSticks.Left.Y;
            }

            //if (input.IsButtonPress(Buttons.RightThumbstickUp, index, out outIndex))
               // PBGlobal.mouseIconPosition.Y--;
            if (input.GamePadWasConnected[0])
            {
                PBGlobal.mouseIconPosition.X += input.CurrentGamePadStates[0].ThumbSticks.Right.X*PBGlobal.aimSensivity;
                PBGlobal.mouseIconPosition.Y -= input.CurrentGamePadStates[0].ThumbSticks.Right.Y*PBGlobal.aimSensivity;
            }

            if (movement == Vector2.Zero)
                Body.LinearVelocity *= 0.7f;
            else
            {
                movement.Normalize();
                Body.LinearVelocity += movement * Speed ;
                Body.LinearVelocity *= 0.8f;
                unghiSenile = (float)Math.Atan2(Convert.ToDouble(Body.LinearVelocity.X), -Convert.ToDouble(Body.LinearVelocity.Y));
            }

            Vector2 m = PBGlobal.mouseIconPosition;
            Body.Rotation = unghiSenile;
            topRot = (float)Math.Atan2(
                 Convert.ToDouble(camera.Position.X + m.X - PBConverter.MeterToPixel(Position.X)),
                -Convert.ToDouble(camera.Position.Y + m.Y - PBConverter.MeterToPixel(Position.Y)));
            if (IsGun != false && 
                (mouse.LeftButton == ButtonState.Pressed||
                input.CurrentGamePadStates[(int)index].IsButtonDown(Buttons.RightShoulder))&& 
                !MessageManager.pBLevel.GUIManager.IsOn&&
                !new Rectangle((int)m.X+(int)camera.Position.X,(int)m.Y+(int)camera.Position.Y,1,1)
                .Intersects(new Rectangle((int)(Body.Position.X*64f)-40,(int)(Body.Position.Y*64f)-40,80,80)))
            { 
                Gun.Shoot(new Vector2(m.X, m.Y), camera,ref count,Category.Cat9,topRot,DMG,FireRT);
            }
            prevMouse = mouse;
        }
        public void LevelUp()
        {
            Xp = Xp - XpRequired;
            XpRequired =(int)((float)XpRequired*1.5f);
            Level++;

            StatPoints += 1;
            HP = HPMAX;
            MessageManager.AddMessage(PBConverter.MeterToPixel(this.Position), "LEVEL UP!", 100, Color.Yellow);
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            ShowRequestedAction = false;
            if (quests.Count > 0)
            {
                if (quests[0].Active == false)
                {
                    quests.RemoveAt(0);
                }
                else
                {
                    if (quests[0].questType == PBQuest.QuestType.roadQuest)
                        quests[0].checkRoad();
                    if (quests[0].questType == PBQuest.QuestType.itemQuest)
                        quests[0].CheckItem();
                }
            }
            if (Gun != null)
                Gun.Update(MessageManager);
            while (Xp >= XpRequired)
            {
                LevelUp();
            }
            particleEngine.EmitterLocation = PBConverter.MeterToPixel(new Vector2((float)(Math.Cos(unghiSenile) * 0.38f * 1 + Body.Position.X + Math.Sin((unghiSenile)) * 0.1f),
                                (float)(Math.Sin(unghiSenile) * 0.38f * 1 + Body.Position.Y - Math.Cos(unghiSenile) * 0.1f)));
            particleEngine.EmitterLocation2 = PBConverter.MeterToPixel(new Vector2((float)(Math.Cos(unghiSenile) * 0.38f * -1 + Body.Position.X + Math.Sin((unghiSenile)) * 0.1f),
                                (float)(Math.Sin(unghiSenile) * 0.38f * -1 + Body.Position.Y - Math.Cos(unghiSenile) * 0.1f)));
            particleEngine.Update(Vector2.Zero,new Color(242,92,13),0f,0.01f);
            questAnim.Update(gameTime);
        }
        public override void DrawColor(Microsoft.Xna.Framework.Graphics.SpriteBatch SB)
        {
            SB.Draw(senile, PBConverter.MeterToPixel(Position), null, Color.White, unghiSenile, new Vector2(32f, 32f), 1f, SpriteEffects.None, 0f);
            base.DrawColor(SB,topRot);
            if (Gun != null)
                Gun.DrawC(SB);
        }
        public void DrawOnMap(SpriteBatch SB)
        {
            if (ShowRequestedAction)
                SB.DrawString(MessageManager.pBLevel.gameFont, "Press action button!", PBConverter.MeterToPixel(Position) -
                    new Vector2(0, PBConverter.MeterToPixel(width)) - MessageManager.pBLevel.gameFont.MeasureString("Press action button!") / 2f, Color.Orange);
        }
        public void DrawPE(SpriteBatch SB)
        {
            particleEngine.DrawC(SB);
        }
        public override void DrawNormal(Microsoft.Xna.Framework.Graphics.SpriteBatch SB)
        {
            particleEngine.DrawN(SB);
            base.DrawNormal(SB,topRot);
            if (Gun != null)
                Gun.DrawN(SB);
        }
        public void DrawRest(Microsoft.Xna.Framework.Graphics.SpriteBatch SB,Microsoft.Xna.Framework.Graphics.Viewport view)
        {
            HpBar.DrawC(SB);
            SBar.DrawC(SB);
            XpBar.Draw(SB, view);
        }

        internal void AddQuest(Vector2 pos, int xp, int gold)
        {
            quests.Add(new PBQuest(this,pos,xp,gold));
        }
        internal void AddQuest(bool monster, object ID,int count, int xp, int gold)
        {
            quests.Add(new PBQuest(this,
                monster == true ? PBQuest.QuestType.monsterQuest : PBQuest.QuestType.itemQuest,
                ID, count, xp, gold));
        }
        internal void Killed(PBMob ID)
        {
            if (quests.Count > 0)
                quests[0].CheckMob(ID);
        }

        internal void GetDrop(PBItem pBItem)
        {
            GUIManager.AddItem(pBItem);
            if (quests.Count > 0)
                quests[0].CheckItem();
        }

        internal void DrawQuestDetails(SpriteBatch spriteBatch)
        {
            if(quests.Count>0)
            MessageManager.DrawQuestDetail(quests[0], spriteBatch);
        }

        internal void DrawQuest(SpriteBatch spriteBatch)
        {
            if (quests.Count > 0)
            {
                if (quests[0].questType == PBQuest.QuestType.roadQuest)
                {
                    questAnim.Draw(spriteBatch, quests[0].targetPosition);
                }
            }
        }

        internal void Reset()
        {
            Xp = 0;
            XpRequired = 25;
            Level = 1;
            HPMAX = HP = 300;
            Speed = 1f;
            Gold = 0;
            DMG = 0;
            FireRT = 0;
            SHIELD = SHIELDMAX = 100;
            ShieldRegen = 1.5f;
            ShieldRegenDelay = 1f;
        }
    }
}
