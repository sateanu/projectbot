﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectBuild.PBEngine.Items;
using PBEngine.Effect;

namespace ProjectBuild.PBEngine
{
   public class PBMessageManager
    {
        public PBLevel pBLevel;
        SpriteFont font;
        List<PBMessage> messages;
        List<PBMessage> guiMessages;
        List<PBMessage> delete;
        List<PBMessage> guidelete;
        List<PBBigMessage> bigMessages;
        List<PBBigMessage> bigdelete;
        List<PBDrop> drops;
        List<PBLight> lights;
        List<PBParticle> particles;
        public Vector2 xpPos;
        Queue<List<PBScriptAction>> queuedScript;
       public PBMessageManager(PBLevel pBLevel,SpriteFont font)
        {
            this.pBLevel = pBLevel;
            this.font = font;
            xpPos = new Vector2(128, pBLevel.ScreenManager.GraphicsDevice.Viewport.Height-110);
            messages = new List<PBMessage>();
            guiMessages = new List<PBMessage>();
            delete = new List<PBMessage>();
            guidelete = new List<PBMessage>();
            drops = new List<PBDrop>();
            bigMessages = new List<PBBigMessage>();
            bigdelete = new List<PBBigMessage>();
            particles = new List<PBParticle>();
            queuedScript = new Queue<List<PBScriptAction>>();
        }
       float time = 1f;
       public void Update(GameTime gameTime)
       {
           if (queuedScript.Count > 0)
           {
               
               time -= gameTime.TotalGameTime.Seconds * 0.001f;
               if (time <= 0)
               {
                   time = 0.01f;
                   List<PBScriptAction> actions = queuedScript.Dequeue();
                   foreach (PBScriptAction act in actions)
                   {
                               act.Activate();
                               act.Deque();
                   }
               }
           }

           if(delete.Count>0)
           delete.Clear();
           foreach (var mess in messages)
           {
               if (mess.Active == false)
                   delete.Add(mess);
               mess.Update();
           }
           if (guiMessages.Count > 0)
           {
               if (guiMessages[0].Active == false)
                   guidelete.Add(guiMessages[0]);
               guiMessages[0].Update(new Vector2(1.2f, 0));
               //guiMessages[0].LowerAlpha();
           }
           if (bigMessages.Count > 0)
           {
               if (bigMessages[0].Active == false)
               {
                   bigdelete.Add(bigMessages[0]);
               }
               else
               {
                   bigMessages[0].Update();
               }
           } 
           foreach (var drop in drops)
                 if (drop.Active)
                   drop.Update();
           foreach (var mess in delete)
               messages.Remove(mess);
           foreach (var mess in guidelete)
               guiMessages.Remove(mess);
           foreach (var mess in bigdelete)
               bigMessages.Remove(mess); 
           for (int i = 0; i < drops.Count; i++)
               if (drops[i].Active == false)
                   drops.RemoveAt(i);
           for(int i=0;i<particles.Count;i++)
           {
               if (particles[i].Size < 0 || particles[i].TTL < 0)
                   particles.RemoveAt(i);
               else
                   particles[i].Update(0.01f);
           }
       }
       internal void KillCurrentBigMessage()
       {
           if (bigMessages.Count > 0)
               bigdelete.Add(bigMessages[0]);
       }
       public void DrawMessages(SpriteBatch spriteBatch)
       {
           foreach (var mess in messages)
               mess.Draw(spriteBatch);
            
       }
       public bool DrawGuiMessages(SpriteBatch spriteBatch)
       {
           if(guiMessages.Count>0)
               guiMessages[0].DrawOnUi(spriteBatch);
           if (bigMessages.Count > 0)
           {
               bigMessages[0].DrawBig(spriteBatch);
               return true;
           }
           return false;
       }
       
       public void DrawDropC(SpriteBatch spriteBatch)
       {
           foreach (var drop in drops)
               drop.Draw(spriteBatch);
       }
       public void DrawParticles(SpriteBatch spriteBatch)
       {
           foreach (var par in particles)
               par.DrawC(spriteBatch);
       }
       public void AddParticle(Vector2 pos, Color col, int ttl, float r)
       {
           particles.Add(new PBParticle(pBLevel.content.Load<Texture2D>("FX//circle"), pos, new Vector2(-(float)Math.Sin(Convert.ToDouble(r)),(float)Math.Cos(Convert.ToDouble(r)))*1.5f, r, 0f, col, 0.44f, ttl));
       }
       public void AddMessage(Vector2 position, string text, int ttl, Color color)
       {
           messages.Add(new PBMessage(position, text, ttl, font, color));
       }
       public void AddGuiMessage(Vector2 position, string text, int ttl, Color color)
       {
           guiMessages.Add(new PBMessage(position, text, ttl, font, color));
       }
       public void AddDrop(Vector2 position, PBGun gun)
       {
           drops.Add(new PBDrop(pBLevel.World,gun,position,pBLevel.content));
       }
       public void AddDrop(Vector2 position, PBItem item)
       {
           drops.Add(new PBDrop(pBLevel.World, item, position, pBLevel.content));
       }
       public void AddDrop(Vector2 position, int dropMoney)
       {
           drops.Add(new PBDrop(pBLevel.World, dropMoney, position, pBLevel.content));
       }
       public void AddBigMessage(Vector2 pos,Vector2 dest,string text,int ttl, Color color,Texture2D back,Texture2D pic)
       {
           if (pos == new Vector2(-1,-1))
               pos = new Vector2(0 - pic.Width - font.MeasureString(text).X, pBLevel.ScreenManager.GraphicsDevice.Viewport.Width / 3f);
           if (dest == new Vector2(-1,-1))
               dest = new Vector2(0, pBLevel.ScreenManager.GraphicsDevice.Viewport.Width / 3f);
           bigMessages.Add(new PBBigMessage(pos, dest, text, ttl, font, color,back,pic));
       }
       internal void GiveXp(int dropXp)
       {
           pBLevel.Player.Xp += dropXp;
       }
       public void AddLight(PBLight light)
       {
           lights.Add(light);
       }
       public void EnqueActionsFromEvent(List<PBScriptAction> actions)
       {
           foreach (var act in actions)
               act.Que();
           queuedScript.Enqueue(actions);
       }
       public void AddAction(PBScriptAction action)
       {
           if (action.type == PBScriptAction.ScriptType.Object)
           {
               string ed = action.Script_Name;
               if (pBLevel.NeutralObjectsScript.ContainsKey(ed))
               {
                   pBLevel.NeutralObjectsScript[ed].AddAction(action,false);
               }
           }
           else
               if (action.type == PBScriptAction.ScriptType.Light)
               {
                   string ed = action.Script_Name;
                   if (pBLevel.LightsScript.ContainsKey(ed))
                       pBLevel.LightsScript[ed].AddAction(action);
               }
               else
                   if (action.type == PBScriptAction.ScriptType.Quest)
                   {
                       if (action.questType == PBQuest.QuestType.roadQuest)
                           pBLevel.AddPBQuest(action.location, action.xp, action.gold);
                       else
                           pBLevel.AddPBQuest(action.monster, action.objID, action.count, action.xp, action.gold);
                   }
                   else
                       if (action.type == PBScriptAction.ScriptType.BigMessage)
                       {
                           AddBigMessage(action.messagePos, action.messageDest, action.messageText, action.messageTtl, Color.White, pBLevel.Backgrounds[action.messageBackName], pBLevel.Pics[action.messagePicName]);
                       }
                       else
                           if (action.type == PBScriptAction.ScriptType.Teleport)
                           {
                               if (action.levelToGo == "SAME")
                               {
                                   pBLevel.AddAction(action);
                                   /*if (action.positionToGo == new Vector2(-1))
                                       action.positionToGo = pBLevel.Spawn;
                                   pBLevel.Player.Body.SetTransformIgnoreContacts(ref action.positionToGo,0f);*/

                               }
                               else
                                   if (action.levelToGo == "END")
                                   {
                                       LoadingScreen.Load(pBLevel.ScreenManager, false, pBLevel.ControllingPlayer,
                                           new BackgroundScreen("darktile",BackgroundScreen.BackgroundType.Tile,Vector2.Zero),
               new BackgroundScreen("nameBackground",BackgroundScreen.BackgroundType.Simple,new Vector2(30,30)),
                                           new ProjectBuild.Screens.EndGameScreen(pBLevel.Player.Gold,pBLevel.LVLNAME,pBLevel.Campaign));
                                   }
                                   else
                                   {
                                       try
                                       {
                                           pBLevel.Campaign.EnterLevel(pBLevel, pBLevel.Campaign.IndexOf(action.levelToGo));
                                       }
                                       catch (Exception ex)
                                       {
                                           LoadingScreen.Load(pBLevel.ScreenManager, false, pBLevel.ControllingPlayer, 
                                               new BackgroundScreen("darktile", BackgroundScreen.BackgroundType.Tile, Vector2.Zero),
                                               new BackgroundScreen("nameBackground", BackgroundScreen.BackgroundType.Simple, new Vector2(30, 30)), 
                                               new ProjectBuild.Screens.EndGameScreen(pBLevel.Player.Gold, pBLevel.LVLNAME, pBLevel.Campaign));
                                       }
                                   }
                           }
                           else
                               if (action.type == PBScriptAction.ScriptType.Spawn)
                               {
                                   pBLevel.AddSpawner(action.locationToSpawn, action.mobToSpawn, action.countToSpawn, action.timeToSpawn,action.OwnScriptName);
                               }
                               else
                                   if (action.type == PBScriptAction.ScriptType.Sound)
                                   {
                                       if (action.soundType == "song")
                                           MainGame.SoundSystem.PlaySong(action.soundToBePlayed);
                                       else
                                           if (action.soundType == "sound")
                                               MainGame.SoundSystem.PlaySound(action.soundToBePlayed);
                                   }
       }
       internal void DrawQuestDetail(PBQuest pBQuest, SpriteBatch spriteBatch)
       {
           spriteBatch.DrawString(font, pBQuest.progressString,
               new Vector2(pBLevel.ScreenManager.GraphicsDevice.Viewport.Width - font.MeasureString(pBQuest.progressString).X,
                   pBLevel.ScreenManager.GraphicsDevice.Viewport.Height / 3) + new Vector2(0.1f,0.2f), Color.Yellow);
           spriteBatch.DrawString(font, pBQuest.progressString,
               new Vector2(pBLevel.ScreenManager.GraphicsDevice.Viewport.Width - font.MeasureString(pBQuest.progressString).X,
                   pBLevel.ScreenManager.GraphicsDevice.Viewport.Height / 3), Color.White);
           
       }

       
    }
}
