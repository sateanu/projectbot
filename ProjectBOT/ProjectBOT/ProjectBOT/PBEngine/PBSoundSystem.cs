﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using System.Windows.Forms;

namespace ProjectBuild.PBEngine
{
    public class PBSoundSystem
    {
        Dictionary<string, SoundEffect> soundEffects;
        Dictionary<string, Song> songs;

        int volume { get { return PBGlobal.volume; } set { PBGlobal.volume = value; } }

        public bool SongIsPlayed;

        public PBSoundSystem(ContentManager content)
        {
            songs = new Dictionary<string, Song>();
            soundEffects = new Dictionary<string, SoundEffect>();
            DirectoryInfo dir = new DirectoryInfo(Application.StartupPath + "//Content//Sounds");
            foreach (FileInfo f in dir.GetFiles("*.xnb"))
            {
                soundEffects.Add(f.Name.Remove(f.Name.LastIndexOf('.')), content.Load<SoundEffect>("Sounds\\" + f.Name.Remove(f.Name.LastIndexOf('.'))));
            }
            dir = new DirectoryInfo(Application.StartupPath + "//Content//Songs");
            foreach (FileInfo f in dir.GetFiles("*.xnb"))
            {
                songs.Add(f.Name.Remove(f.Name.LastIndexOf('.')), content.Load<Song>("Songs\\" + f.Name.Remove(f.Name.LastIndexOf('.'))));
            }
        }
        /*
         *  if (tabControl3.SelectedIndex == 0)
            {
                if (listBox6.SelectedIndex >= 0)
                {
                    Game1.SoundSystem.PlaySong(listBox6.SelectedItem as string);
                }
            }else
                if (tabControl3.SelectedIndex == 1)
                {
                    if (listBox7.SelectedIndex >= 0)
                        Game1.SoundSystem.PlaySound(listBox7.SelectedItem as string);
                }
         * */
        public List<string> getSongNames()
        {
            List<string> names = new List<string>();
            foreach (string s in songs.Keys)
                names.Add(s);
            return names;
        }

        public List<string> getSoundEffectNames()
        {
            List<string> names = new List<string>();
            foreach (string s in soundEffects.Keys)
                names.Add(s);
            return names;
        }

        public void Update()
        {
            
        }
        public void PlaySound(string soundName)
        {
            soundEffects[soundName].Play(volume/100f, 0f, 0f);
        }
        public void PlaySong(string songName)
        {
            MediaPlayer.Play(songs[songName]);
            SongIsPlayed = true;
        }

        public void PauseSong()
        {
            MediaPlayer.Pause();
        }
        public void ResumeSong()
        {
            if(SongIsPlayed)
            MediaPlayer.Resume();
        }
        public void StopSong()
        {
            MediaPlayer.Stop();
            SongIsPlayed = false;
        }
    }
}
