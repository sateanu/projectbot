﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using ProjectBuild.PBEngine.Items;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectBuild.PBEngine
{
    public class PBMob : PBObject
    {
        public PBGun Gun;
        int count = 1;
        public int dropXp=4;
        Vector2 wanderVector;
        public int ID=1;
        public string Name;
        //public string textureName;
        public bool st;
        public PBMob(World world, float height, float width)
            : base(world, height, width)
        {
            Body.BodyType = BodyType.Dynamic;
        }
        public PBMob()
            :base()
        {

        }
        public override string ToString()
        {
            return Name;
        }
        public PBMob(World world, float height, float width, Vector2 position, ContentManager content, PBMessageManager message,string name,float rot = 0f, string s = null,int ID=1,int HP=100,int XP=0,PBGun gun=null,bool st=false)
            : base(world, height, width, position,content,message,s,rot,name)
        {
            if(st==false)
            Body.BodyType = BodyType.Dynamic;
            
            Body.CollidesWith = Category.All & ~Category.Cat10 & ~Category.Cat3;
            Body.CollisionCategories = Category.All & ~Category.Cat10;
            if (SHIELDMAX > 0)
            {
                Body.FixtureList[1].CollidesWith = Category.Cat9;
                Body.FixtureList[1].CollisionCategories = Category.Cat31;
                goodCat = Category.Cat9;
            } 
            LoadTextureC(content.Load<Texture2D>("Mobs/" + s));
           // LoadTextureN(content.Load<Texture2D>("Mobs/"+s+"_n"));
            if (gun == null)
            {
                Gun = PBGuns.EnemyGun.Copy();
                Gun.BindTo(this, content, world);
            }
            else
            {
                Gun = gun.Copy();
                Gun.BindTo(this, content, world);
            }
            Name=name;
            dropXp = XP;
            this.HPMAX = HP;
            this.HP = HP;
            this.ID = ID;
            this.prevHp = HP;
        }

       
        
        public override void Update(PBPlayer player,PBCamera camera,GameTime gameTime)
        {
            base.Update(player,camera,gameTime);
            if (HP <= 0)
            {
                Random r = new Random();
                Active = false;
                MessageManager.GiveXp(dropXp);
                if (r.Next(100) <= 30)
                    MessageManager.AddDrop(this.Position, 5);
                else if (r.Next(100) >= 90)
                    MessageManager.AddDrop(this.Position, Items.PBItems.FromID(r.Next(Items.PBItems.Count)+1));
                
                //player.GetDrop(Items.Items.BrokenRobotParts);
                player.Killed(this);
            }
            if (Active)
            {
                Gun.Update(MessageManager);
                if ((player.Position - Position).Length() <= 5)
                {
                    Body.Rotation = (float)Math.Atan2(Convert.ToDouble(-PBConverter.MeterToPixel(Position.X - player.Position.X)), Convert.ToDouble(PBConverter.MeterToPixel(Position.Y - player.Position.Y)));
                    Body.LinearVelocity = Vector2.Zero;
                    if (new Random().Next(100) <= 15)
                    {
                        Gun.Shoot(PBConverter.MeterToPixel(player.Position) - camera.Position, camera,ref count, Category.Cat10);
                    }
                }
                else
                    if ((player.Position - Position).Length() <= 13)
                    {
                        Body.Rotation = (float)Math.Atan2(Convert.ToDouble(-PBConverter.MeterToPixel(Position.X - player.Position.X)), Convert.ToDouble(PBConverter.MeterToPixel(Position.Y - player.Position.Y)));
                        Vector2 go = player.Position - Position;
                        go.Normalize();
                        Body.LinearVelocity = go * 1.4f;

                    }
                    else
                    {
                        Body.LinearVelocity = Vector2.Zero;
                    }
            }
        }

        private void Wander()
        {
            Body.LinearVelocity = wanderVector * 1.3f;
            Body.Rotation = (float)Math.Atan2(Convert.ToDouble(-PBConverter.MeterToPixel(wanderVector.X-Position.X)), Convert.ToDouble(PBConverter.MeterToPixel(wanderVector.Y-Position.Y)));
        }
        public override void DrawColor(Microsoft.Xna.Framework.Graphics.SpriteBatch SB)
        {
            if (Active)
            {
                base.DrawColor(SB);
                Gun.DrawC(SB);
            }

        }
        public override void DrawNormal(Microsoft.Xna.Framework.Graphics.SpriteBatch SB)
        {
            if (Active)
            {
                base.DrawNormal(SB);
                Gun.DrawN(SB);
            }
        }
    }
}
