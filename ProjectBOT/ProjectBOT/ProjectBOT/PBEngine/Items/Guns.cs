﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace ProjectBuild.PBEngine.Items
{
    class PBGuns
    {
        ContentManager content;
        public PBGuns(ContentManager ct)
        {
            this.content = ct;
        }
        
        public static PBGun NormalGun = new PBGun()
        {
            DMG = 10,
            FireRate = 35,
            textureName = "normalGun",
            Name="Weak Gun",
            color = Color.LightYellow
        }.Copy();
        public static PBGun EnemyGun = new PBGun()
        {
            DMG = 1,
            FireRate = 35,
            textureName = "enemyGun",
            Name="Gun of Failure",
            color=Color.LightGreen
        }.Copy();
        public static PBGun SpeedyGonzales = new PBGun()
        {
            DMG=1,
            FireRate=3,
            textureName = "SpeedyGonzales",
            Name="LAG MACHINE",
            color=Color.LightBlue
        }.Copy();
        public static PBGun TROLLMODE = new PBGun()
        {
            DMG=50,
            FireRate=5,
            textureName = "enemyGun",
            Name="Overpowered Gun",
            color=Color.Pink
        }.Copy();
        public static PBGun BasicGun = new PBGun()
        {
            DMG = 15,
            FireRate = 15,
            textureName = "SpeedyGonzales",
            Name = "Basic Gun",
            color = Color.Red
        }.Copy();
        public static PBGun CannonGun = new PBGun()
        {
            DMG = 20,
            FireRate = 20,
            textureName = "SpeedyGonzales",
            Name = "Cannon Gun",
            color = Color.Blue
        }.Copy();
        public static PBGun RainbowGun = new PBGun()
        {
            DMG = 10,
            FireRate = 35,
            textureName = "SpeedyGonzales",
            Name = "Rainbow Gun",
            color = Color.Transparent
        }.Copy();

        public static PBGun[] Guns = new PBGun[]
        {
            NormalGun,
            EnemyGun,
            SpeedyGonzales,
            TROLLMODE,
            BasicGun,
            CannonGun,
            RainbowGun,
        };
        public static PBGun FromName(string name)
        {
            foreach (var gun in Guns)
            {
                if (gun.Name == name)
                    return gun;
            }
            return null;
        }
        public static bool Contains(string name)
        {
            foreach (var gun in Guns)
            {
                if (gun.Name == name)
                    return true;
            }
            return false;
        }
    }
}
