﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ProjectBuild.PBEngine.Items
{
   public class PBItem
    {
     internal Texture2D InvPic;
      public string name;
      public int count;
      public int ID;
      public string texstring;
      public override string ToString()
      {
          return name;
      }

      public PBItem()
      {}
      public PBItem(int ID, string name, Texture2D InvPic)
      {
          this.InvPic = InvPic;
          this.ID = ID;
          this.name = name;
      }
      public void LoadTexture(ContentManager content)
      {
          InvPic = content.Load<Texture2D>("Items//"+texstring);
      }
      internal void DrawInvPic(Vector2 position, SpriteBatch spriteBatch,ProjectBuild.GUI.Cell cell)
      {
          spriteBatch.Draw(InvPic, new Rectangle((int)position.X, (int)position.Y, cell.Width, cell.Height), Color.White);
      }
      public StringBuilder ToStringDetails()
      {
          StringBuilder a = new StringBuilder();
          a.AppendLine(name.ToString() + " (" + count.ToString() + ")");
          return a;
      }
      public PBItem Copy()
      {
          return new PBItem()
          {
              name = this.name,
              texstring = this.texstring,
              count = this.count,
              ID = this.ID,
          };
      }
    }
}
