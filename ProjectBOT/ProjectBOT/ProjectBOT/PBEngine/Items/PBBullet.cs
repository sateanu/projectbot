﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using PBEngine.Effect;

namespace ProjectBuild.PBEngine.Items
{
    public class PBBullet
    {
        public Body body;
        public bool Active;
        public Color color;
        Texture2D textureN;
        Texture2D textureC;
        Vector2 target;
        int DMG;
        int TTL;
       public float Rotation;
        PBParticleEngine particleC;
        PBParticleEngine particleN;
        public PBBullet(World world,Vector2 start,Vector2 target,Texture2D a,Texture2D b,int dmg,Category cat,Color color)
        {
            body = BodyFactory.CreateRectangle(world, .1f, .1f, 1f,start);
            body.IsBullet = true;
            body.FixedRotation = true;
            body.BodyType = BodyType.Dynamic;
            body.CollisionCategories = cat;
            body.CollidesWith = Category.All & ~Category.Cat3;
            body.UserData = this;
            Vector2 t = PBConverter.PixelToMeter(target);
            this.target =PBConverter.PixelToMeter(target)-(start);
            this.target.Normalize();
            Active = true;
            body.OnCollision += new OnCollisionEventHandler(body_OnCollision);
            textureC = a;
            textureN = b;
            DMG = dmg;
            this.color = color;
            TTL = 80;
            Rotation = (float)Math.Atan2(
                Convert.ToDouble(-PBConverter.MeterToPixel(start.X) + target.X),
                Convert.ToDouble(PBConverter.MeterToPixel(start.Y) - target.Y));
            List<Texture2D> tC=new List<Texture2D>();
            tC.Add(a);
            List<Texture2D> tN=new List<Texture2D>();
            tN.Add(b);
            particleC = new PBParticleEngine(tC, PBConverter.MeterToPixel(body.Position),9);
            particleN = new PBParticleEngine(tN, PBConverter.MeterToPixel(body.Position),9);
        }

        bool body_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            Active = false;
            if (fixtureB.Body.UserData!=null&&fixtureB.Body.UserData.GetType() ==typeof(PBBullet))
            {
                fixtureB.Body.Dispose();
                return true;
            }

            if (fixtureB.Body.UserData != null)
            {
                PBObject temp = (PBObject)fixtureB.Body.UserData;
                double y=fixtureA.Body.Position.Y-fixtureB.Body.Position.Y;
                double x=fixtureA.Body.Position.X-fixtureB.Body.Position.X;
                temp.TakeDamage(DMG,(float)Math.Atan2(x,-y),
                    PBConverter.MeterToPixel(fixtureA.Body.Position));
            }
            fixtureA.Body.Dispose();
            return true;
        }
        public void Update()
        {
            if (Active)
            {
                TTL--;
                if (TTL <= 0)
                {
                    Active = false;
                    body.Dispose();
                }
                body.LinearVelocity = target*15f;
                particleC.EmitterLocation = PBConverter.MeterToPixel(body.Position);
                particleN.EmitterLocation = PBConverter.MeterToPixel(body.Position);
                particleC.Update(body.LinearVelocity/1.8f, color,Rotation ,0.01f);
                particleN.Update(body.LinearVelocity/1.8f, color,Rotation ,0.01f);
            }
        }
        public void DrawN(SpriteBatch SB)
        {
            if (Active && textureN != null)
            {
                SB.Draw(textureN, PBConverter.MeterToPixel(body.Position), null, Color.White, Rotation, origin, 1f, SpriteEffects.None, 0f);
                particleN.DrawC(SB);
            }
        }
        public void DrawC(SpriteBatch SB)
        {
            if (Active && textureC != null)
            {
                SB.Draw(textureC, PBConverter.MeterToPixel(body.Position), null, color, Rotation, origin, 1f, SpriteEffects.None, 0f);
                particleC.DrawC(SB);
            }
        }

        public Vector2 origin { get { return new Vector2(textureC.Width / 2, textureC.Height / 2); } }
    }
}   
