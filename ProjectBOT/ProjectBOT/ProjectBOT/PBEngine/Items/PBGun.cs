﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace ProjectBuild.PBEngine.Items
{
    public class PBGun
    {
        public enum GunType
        {
            Bullet,
            Laser,
            Lighting
        }

        public override string  ToString()
        {
            return Name;
        }
       
        GunType gunType;
        World world;
        public List<PBBullet> Bullets;
        List<PBBullet> deletable;
        PBObject _obj;
        internal Texture2D InvPic;
        Texture2D bulletTextureC;
        Texture2D bulletTextureN;
        public int DMG;
        public int FireRate;
        int FR;
        public bool Binded = false;
        public string Name;
        public string textureName;
        public Color color=Color.White;

        public StringBuilder ToStringDetails()
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine(Name.ToUpper());
            s.AppendLine("Damage: " + DMG.ToString());
            s.AppendLine("FireRate: " + FireRate.ToString());
            return s;
        }
        public void LoadTexture(ContentManager content)
        {
            bulletTextureC = content.Load<Texture2D>("Weapons/bullet");
           bulletTextureN = content.Load<Texture2D>("Weapons/bullet_n");
           InvPic = content.Load<Texture2D>("Weapons\\" + textureName);
        }
        public PBGun()
        {
            Bullets = new List<PBBullet>();
            deletable = new List<PBBullet>();
        }
        public PBGun(GunType type, int dmg, int fireRate, ContentManager content)
        {
            gunType = type;
            DMG = dmg;
            FireRate = fireRate;
            InvPic = content.Load<Texture2D>("GUI/Plus1");
            Binded = false;
        }
        public void BindTo(PBObject obj, ContentManager content, World world)
        {
            Binded = true;
            _obj = obj;
            this.world = world;
            Bullets = new List<PBBullet>();
            deletable = new List<PBBullet>();
            bulletTextureC = content.Load<Texture2D>("Weapons/bullet");
            bulletTextureN = content.Load<Texture2D>("Weapons/bullet_n");
        }
        public PBGun(GunType type,PBObject obj,World world,ContentManager content,int dmg,int fireRate,string name)
        {
            gunType = type;
            _obj=obj;
            this.Name = name;
            this.world = world;
            Bullets = new List<PBBullet>();
            deletable = new List<PBBullet>();
            bulletTextureC = content.Load<Texture2D>("Weapons/bullet");
            bulletTextureN = content.Load<Texture2D>("Weapons/bullet_n");
            DMG = dmg;
            FireRate = fireRate;
            InvPic = content.Load<Texture2D>("Weapons/"+name);
        }
        public void Shoot(Vector2 target, PBCamera camera, ref int count, Category cat)
        {
            if (FR == 0)
            {
                FR = FireRate;
                Bullets.Add(new PBBullet(world,
                    new Vector2((float)(Math.Cos(_obj.Body.Rotation) * 0.38f * count + _obj.Body.Position.X + Math.Sin((_obj.Body.Rotation)) * 0.6f),
                                (float)(Math.Sin(_obj.Body.Rotation) * 0.38f * count + _obj.Body.Position.Y - Math.Cos(_obj.Body.Rotation) * 0.6f)),
                    target + camera.Position +
                    new Vector2((float)Math.Cos(_obj.Body.Rotation) * 0.38f * 64f * count,
                     (float)Math.Sin(_obj.Body.Rotation) * 0.38f * 64f * count),
                    bulletTextureC, bulletTextureN, DMG, cat, color));
                count = count * -1;
                MainGame.SoundSystem.PlaySound("laserShotEnemy");
            }
        }
        public void Shoot(Vector2 target, PBCamera camera, ref int count, Category cat,float angle)
        {
            if (FR == 0)
            {
                Color color = this.color;
                if (color == Color.Transparent)
                {
                    Random r = new Random();
                    double R = r.NextDouble();
                    double G = r.NextDouble();
                    double B = r.NextDouble();
                    color = new Color((float)R, (float)G, (float)B);
                } 
                FR = FireRate;
                Bullets.Add(new PBBullet(world,
                    new Vector2((float)(Math.Cos(angle) * 0.38f * count + _obj.Body.Position.X + Math.Sin((angle)) * 0.6f),
                                (float)(Math.Sin(angle) * 0.38f * count + _obj.Body.Position.Y - Math.Cos(angle) * 0.6f)),
                    target + camera.Position +
                    new Vector2((float)Math.Cos(angle) * 0.38f * 64f * count,
                     (float)Math.Sin(angle) * 0.38f * 64f * count),
                    bulletTextureC, bulletTextureN, DMG, cat, color));
                count = count * -1;
                MainGame.SoundSystem.PlaySound("laserShot");
            }
        }
        public void Shoot(Vector2 target, PBCamera camera, ref int count, Category cat, float angle,int dmg,int firerate)
        {
            if (FR-firerate <= 0)
            {
                Color color = this.color;
                if (color == Color.Transparent)
                {
                    Random r = new Random();
                    double R = r.NextDouble();
                    double G = r.NextDouble();
                    double B = r.NextDouble();
                    color = new Color((float)R, (float)G, (float)B);
                } 
                FR = FireRate;
                Bullets.Add(new PBBullet(world,
                    new Vector2((float)(Math.Cos(angle) * 0.38f * count + _obj.Body.Position.X + Math.Sin((angle)) * 0.6f),
                                (float)(Math.Sin(angle) * 0.38f * count + _obj.Body.Position.Y - Math.Cos(angle) * 0.6f)),
                    target + camera.Position +
                    new Vector2((float)Math.Cos(angle) * 0.38f * 64f * count,
                     (float)Math.Sin(angle) * 0.38f * 64f * count),
                    bulletTextureC, bulletTextureN, DMG+dmg, cat, color));
                count = count * -1;
                MainGame.SoundSystem.PlaySound("laserShot");
            }
        }
        public void Update(PBMessageManager MM)
        {
            Color color = this.color;
            if (color == Color.Transparent)
            {
                Random r = new Random();
                double R = r.NextDouble();
                double G = r.NextDouble();
                double B = r.NextDouble();
                color = new Color((float)R, (float)G, (float)B);
            } 
            if (FR > 0) FR--;
            foreach (var bullet in Bullets)
            { 
                bullet.Update();
                if (bullet.Active == false)
                {
                    deletable.Add(bullet);
                    MM.AddParticle(PBConverter.MeterToPixel(bullet.body.Position), bullet.color, 60, bullet.Rotation);
                }
            }
            foreach (var b in deletable)
            { Bullets.Remove(b);  }
            deletable.Clear();

        }
        public void DrawC(SpriteBatch SB)
        {
            foreach (var bullet in Bullets)
                bullet.DrawC(SB);
        }
        public void DrawN(SpriteBatch SB)
        {
            foreach (var bullet in Bullets)
                bullet.DrawN(SB);
        }


        internal void DrawInvPic(Vector2 position, SpriteBatch spriteBatch,ProjectBuild.GUI.Cell cell)
        {
            Color color = this.color;
            if (color == Color.Transparent)
            {
                Random r = new Random();
                double R = r.NextDouble();
                double G = r.NextDouble();
                double B = r.NextDouble();
                color = new Color((float)R, (float)G, (float)B);
            } 
            spriteBatch.Draw(MainGame.PlaceHolder, new Rectangle((int)position.X, (int)position.Y, cell.Width, cell.Height), color);
            spriteBatch.Draw(InvPic, new Rectangle((int)position.X,(int)position.Y,cell.Width,cell.Height), Color.White);
        }
        public PBGun Copy()
        {
            return new PBGun()
            {
                DMG = this.DMG,
                Name=this.Name,
                FireRate=this.FireRate,
                color=this.color,
                textureName=this.textureName 
            };
        }
    }
}
