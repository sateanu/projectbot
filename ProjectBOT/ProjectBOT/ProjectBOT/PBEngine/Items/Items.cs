﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectBuild.PBEngine.Items
{
    class PBItems
    {
        public static PBItem BrokenRobotParts = new PBItem()
        {
            name="Broken Robot Part",
            ID=1,
            texstring="brokenrobotparts",
            count=1
        }.Copy();

        public static PBItem Screw = new PBItem()
        {
            name = "Robot Screw",
            ID = 2,
            texstring = "screw",
            count = 1
        }.Copy();

        public static PBItem MetalAlloy = new PBItem()
        {
            name = "Metal Alloy",
            ID = 3,
            texstring = "metalalloy",
            count = 1
        }.Copy();

        public static PBItem EnergySource = new PBItem()
        {
            name = "Energy Source",
            ID = 4,
            texstring = "energybattery",
            count = 1
        }.Copy();

        public static PBItem[] Items = new PBItem[]
        {
           BrokenRobotParts,
           Screw,
           MetalAlloy,
           EnergySource
        };
        public static PBItem FromName(string name)
        {
            foreach (var i in Items)
                if (i.name == name)
                    return i;
                return null;
        }
        public static PBItem FromID(int id)
        {
            foreach (var i in Items)
                if (i.ID == id)
                    return i;
            return null;
        }
        public static int IdOf(string name)
        {
            foreach (var i in Items)
                if (i.name == name)
                    return i.ID;
            return -1;
        }
        public static int Count
        {
            get
            {
                return Items.Length;
            }
        }
        public static string NameOf(int ID)
        {
            foreach (var i in Items)
                if (i.ID == ID)
                    return i.name;
            return null;
        }
    }
}
