﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ProjectBuild.PBEngine.Items;

namespace ProjectBuild.PBEngine
{
   public class PBQuest
    {
       public enum QuestType
       {
           roadQuest,
           itemQuest,
           monsterQuest
       }

       public bool Active = true;
        public QuestType questType;
       PBPlayer Player;

      public Vector2 targetPosition;
       Rectangle targetPosRectangle;

       PBItem itemIdReq;
       int itemsReq;
       int itemsGot;

       PBMob mobIdReq;
       int mobReq;
       int mobGot;

       int xp;
       int gold;

      public string storyString;
      public string progressString
       {
           get
           {
               switch (questType)
               {
                   case QuestType.monsterQuest:
                       return string.Format("Find and kill {0} ! ( {1}/{2} )", mobIdReq.Name, mobGot, mobReq);
                      
                   case QuestType.itemQuest:
                       return string.Format("Obtain {0} ! ( {1}/{2} )", itemIdReq.name, itemsGot, itemsReq);

                   case QuestType.roadQuest:
                       return "Find and move to the located spot!";

                   default:
                       return " ";
                       
               }
           }
       }
       public void SetStory(string s)
       {
           storyString = s;
       }
       public PBQuest(PBPlayer player,Vector2 position, int xp, int gold)
       {
           Player = player;
           questType = QuestType.roadQuest;
           targetPosition = position;
           targetPosRectangle = new Rectangle((int)position.X - 32,(int)position.Y - 32, 64, 64);
           this.xp = xp;
           this.gold = gold;
       }

       public PBQuest(PBPlayer player,QuestType type,object ID,int count, int xp, int gold)
       {
           Player = player;
           questType = type;
           switch (type)
           {
               case QuestType.monsterQuest:
                   mobGot = 0;
                   mobIdReq =(PBMob)ID;
                   mobReq = count;
                   break;
               case QuestType.itemQuest:
                   itemIdReq =(PBItem)ID;
                   itemsGot = 0;
                   itemsReq = count;
                   break;
           }
           this.xp = xp;
           this.gold = gold;
       }
       public void checkRoad()
       {
           if (new Rectangle((int)PBConverter.MeterToPixel(Player.Body.Position.X),
               (int)PBConverter.MeterToPixel(Player.Body.Position.Y),1,1).Intersects(targetPosRectangle))
               Done();
       }

       public void CheckItem()
       {
           if (questType == QuestType.itemQuest)
           {
               if (Player.GUIManager.CheckItem(ref itemsGot,itemIdReq.ID, itemsReq))
               {
                   Player.GUIManager.RemoveItem(itemIdReq.ID, itemsReq);
                   Done(); 
               }
           }
       }
       public void CheckMob(PBMob mobID)
       {
           if (questType == QuestType.monsterQuest)
           {
               if (mobID.ID == mobIdReq.ID)
                   mobGot++;
               if (mobGot == mobReq)
                   Done();
           }
       }
       private void Done()
       {
           this.Active = false;
           Player.Gold += gold;
           Player.Xp += xp;
           StringBuilder str = new StringBuilder();
           str.AppendLine("Quest Complete!");
           str.AppendLine("+" + gold.ToString() + "GOLD");
           str.AppendLine("+" + xp.ToString() + "XP");
           Player.MessageManager.AddGuiMessage(Player.MessageManager.xpPos,str.ToString(),100,Color.White);
       }

    }
}
