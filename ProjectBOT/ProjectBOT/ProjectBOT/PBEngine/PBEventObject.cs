﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ProjectBuild.PBEngine
{
    using TriggeredBy = PBScriptAction.TriggeredBy;
    using Microsoft.Xna.Framework.Input;
    public class PBEventObject : PBObject
    {
        
        public PBEventObject(World world, float height, float width, Vector2 position)
            : base(world, height, width, position)
        {
            this.Body.OnCollision += new OnCollisionEventHandler(Body_OnCollision);
            this.Body.OnSeparation += new OnSeparationEventHandler(Body_OnSeparation);
            this.Body.CollidesWith = Category.All & ~Category.Cat9 & ~Category.Cat10;
            this.Body.CollisionCategories = Category.Cat30;
            this.Body.SleepingAllowed = false;
            this.Body.IsSensor = true;

        }
        public PBEventObject(World world, float height, float width, Vector2 position, ContentManager content, PBMessageManager message, string s = null, float rot = 0f, string name = null)
            : base(world, height, width, position,content,message,s,rot,name,true)
        {
            this.Body.OnCollision += new OnCollisionEventHandler(Body_OnCollision);
            this.Body.OnSeparation += (Body_OnSeparation);
            this.Body.CollidesWith = Category.All & ~Category.Cat9 & ~Category.Cat10;
            this.Body.CollisionCategories = Category.Cat30;
            this.Body.SleepingAllowed = false;
            this.Body.IsSensor = true;
        }

        void Body_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            
        }
        public bool collided;
        public bool prevCollided;
        public override void Update(GameTime gameTime)
        {
            prevCollided = collided;
            collided = false;
        }
        bool Body_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            TriggeredBy temp=TriggeredBy.All;
            if (fixtureB.Body.UserData.GetType() == typeof(PBPlayer)) 
            {
                foreach(var a in Actions)
                    if(a.actor==PBScriptAction.TriggeredBy.PlayerActionOnly)
                    {
                        PBPlayer tempP = (PBPlayer)fixtureB.Body.UserData;
                        tempP.ShowRequestedAction = true;
                    }

                if(Keyboard.GetState().IsKeyDown(Keys.G)||GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.X))
                temp=TriggeredBy.PlayerActionOnly;
                else
                temp = TriggeredBy.PlayerOnly;
            }
            SendActions(temp);
            return false;
        }
        void SendActions(TriggeredBy temp)
        {
            List<PBScriptAction> actions = new List<PBScriptAction>();
            foreach (var act in Actions)
            {
                if (act.Used==true&&(act.actor == temp || act.actor==PBScriptAction.TriggeredBy.All))
                {
                    collided = true;
                    if (act.Active)
                    MessageManager.AddAction(act);

                    if (act.type == PBScriptAction.ScriptType.Sound&&act.soundType=="sound")
                    {
                        if (prevCollided == false && collided == true)
                        {
                            act.Active = true;
                            act.Used = true;
                        }
                       else
                          if (prevCollided && collided == true)
                          {
                               act.Used = true;
                               act.Active = false;
                          }
                    }
                    else
                        act.Used = false;

                    if (act.type == PBScriptAction.ScriptType.Object && act.repeat == false)
                    {
                        if (act.queued == false&&act.Active==false)
                            actions.Add(act);
                        act.Used = true;
                        act.Active = true;
                       
                    }
                    if (act.type == PBScriptAction.ScriptType.Teleport)
                        actions.Add(act);

                    
                }
                
            }
            if (actions.Count > 0)
                MessageManager.EnqueActionsFromEvent(actions);
        }
    }
}
