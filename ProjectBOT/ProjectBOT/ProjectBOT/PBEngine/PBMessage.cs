﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectBuild.PBEngine
{
   public class PBMessage
    {
       protected Vector2 position;
       protected string text;
       protected int TTL;
        public bool Active;
        protected SpriteFont font;
       public Color color;
       public Color orColor;
       public void FadeToAlpha(float alpha)
       {
           color = orColor * alpha;
       }
        public PBMessage(Vector2 position, string text, int ttl,SpriteFont font,Color color)
        {
            this.position = position;
            this.text = text;
            this.TTL = ttl;
            this.font = font;
            this.color = color;
            orColor = color;
            Active = true;
            alpha = 0f;
        }
        public void Update()
        {
            if (Active)
            {
                if (alpha <= 1f)
                {
                    FadeToAlpha(alpha);
                    alpha += 0.05f;
                } 
                TTL--;
                position.Y -= 1f;
                if (TTL <= 0)
                    Active = false;
            }
        }
        internal float alpha = 0f;
        public void Update(Vector2 dir)
        {
            if (Active)
            {
                if (alpha <= 1f)
                {
                    FadeToAlpha(alpha);
                    alpha += 0.5f;
                }
                TTL--;
                position += dir;
                if (TTL <= 0)
                    Active = false;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, position,color);
        }

        public void DrawOnUi(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, position, color);
        }
    }
}
