﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectBuild.PBEngine
{
    public class PBBigMessage : PBMessage
    {
        Texture2D backGround;
        Texture2D picture;
        Vector2 velocity;
        Vector2 dest;
        public PBBigMessage(Vector2 pos,Vector2 dest,string t,int ttl,SpriteFont font,Color color,Texture2D back,Texture2D pic):base(pos,t,ttl,font,color)
        {
            velocity = dest - pos;
            velocity.Normalize();
            this.dest = dest;
            picture = pic;
            backGround = back;
            alpha = 0f;
        }
        public void Update()
        {
            if (Active)
            {
                if (!Moving())
                {
                    TTL--;
                    if (TTL <= 0)
                        Active = false;
                }

                    if (alpha <= 1f)
                    {
                        FadeToAlpha(alpha);
                        Vector2 t = velocity;
                        t.Normalize();

                        alpha += t.X*0.01f;
                    }
            }
        }
        bool Moving()
        {
            if (Finished())
                return false;
            position += velocity*5;
            return true;
        }
        bool Finished()
        {
            bool doneX = false;
            bool doneY = false;
            if (velocity.X > 0)
            {
                if (position.X >= dest.X)
                { dest.X = position.X; doneX = true; }
            }
            else
                if (velocity.X < 0)
                {
                    if (position.X <= dest.X)
                    { dest.X = position.X; doneX = true; }
                }
            if (velocity.Y > 0)
            {
                if (position.Y >= dest.Y)
                { dest.Y = position.Y; doneY = true; }
            }
            else
                if (velocity.Y < 0)
                {
                    if (position.Y <= dest.Y)
                    { dest.Y = position.Y; doneY = true; }
                }
            if (velocity.Y == 0) doneY = true;
            if (velocity.X == 0) doneX = true;
            return doneX&&doneY;
        }
        public void DrawBig(SpriteBatch spriteBatch)
        {
            if (Active)
            {
                string textToWrite = text;
                StringBuilder b = new StringBuilder();
                int fr = 25;
                int i=0;
                string[] cuvinte=textToWrite.Trim().Split(' ');
                int max=cuvinte.Length;
                int j = 0;
                string linieMax="";
                int linMax=0;
                while (i < max)
                {
                    int l = 0;
                    while (l < fr && l<text.Length && j<max)
                    {
                        l += cuvinte[j].Length;
                        j++;
                    }
                    string line = "";
                    for (; i < j; i++)
                    {
                        line += cuvinte[i] + " ";
                    }
                    if (line.Length > linMax)
                    {
                        linMax = line.Length;
                        linieMax = line;
                    }
                    b.AppendLine(line);
                }
              //  text.Length < fr + 1 ?
                //    (int)(font.MeasureString(text).X) + picture.Width :

                int offSet = 10;
                int widthText = (int)(font.MeasureString(linieMax).X + picture.Width);
                Rectangle r=new Rectangle((int)position.X,(int)position.Y,widthText+offSet,picture.Height);
                spriteBatch.Draw(backGround, r, Color.White*alpha);
                if(picture!=null)
                    spriteBatch.Draw(picture, position, Color.White*alpha);
                spriteBatch.DrawString(font,b.ToString(),new Vector2(offSet+picture.Width,0)+position,color,0f,Vector2.Zero,1f,SpriteEffects.None,0f);
            }
        }
    }
}
