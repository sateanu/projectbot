﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PBEngine.Effect
{
    class SpotLight : PBLight
    {
        public SpotLight()
            : base(LightType.Spot)
        {

        }
        public override PBLight DeepCopy()
        {
            var newLight = new SpotLight();
            CopyBaseFields(newLight);

            return newLight;
        }
    }
}
