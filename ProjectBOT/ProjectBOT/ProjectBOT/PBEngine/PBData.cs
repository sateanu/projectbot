﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace ProjectBuild.PBEngine
{
   public class PBData
    {
        [STAThread]
       public static void Upload(string fileToUpload,string folder=null)
        {
            try
            {
                if (folder != null)
                    folder += "//";
                FileInfo toUpload = new FileInfo(fileToUpload);
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://project-build.site90.com//ProjectBuildMaps//" + folder + toUpload.Name);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential("a3205051", "moondance1ftp");
                Stream ftpStream = request.GetRequestStream();
                FileStream file = File.OpenRead(fileToUpload);
                int lenght = 1024;
                byte[] buffer = new byte[lenght];
                int bytesRead = 0;
                do
                {
                    bytesRead = file.Read(buffer, 0, lenght);
                    ftpStream.Write(buffer, 0, bytesRead);
                }
                while (bytesRead != 0);
                file.Close();
                ftpStream.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public static void Download(string fileToDownload, string locationToDownload,string folder=null)
        {
            if (folder != null)
                folder += "//";
            FileInfo toDownload = new FileInfo(fileToDownload);
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://project-build.site90.com//ProjectBuildMaps//"+ folder + toDownload.Name);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("a3205051", "moondance1ftp");
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream ftpStream = response.GetResponseStream();
            FileStream file = File.Create(locationToDownload + toDownload.Name);
            int lenght = 1024;
            byte[] buffer = new byte[lenght];
            int bytesRead = 0;
            do
            {
                bytesRead = ftpStream.Read(buffer, 0, lenght);
                file.Write(buffer, 0, bytesRead);
            }
            while (bytesRead != 0);
            file.Close();
            ftpStream.Close();
        }
        public static void MakeDirectory(string directoryUri)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://project-build.site90.com//ProjectBuildMaps//" + directoryUri);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential("a3205051", "moondance1ftp");
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            }
            catch (WebException wex)
            {
                MessageBox.Show(wex.Message + " \r \n " + "Folder with same name probably!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " \r \n " + "Folder with same name probably!");
            }
        }
    }
}
