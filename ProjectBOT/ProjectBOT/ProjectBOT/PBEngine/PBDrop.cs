﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using ProjectBuild.PBEngine.Items;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace ProjectBuild.PBEngine
{
   public class PBDrop
    {
       Body body;
       public bool Active=true;
       PBGun gun;
       public enum Type
       {
           Gun,
           Item,
           Money
       }
       public Type DropType;
       Texture2D tex;
       float angle=0f;
       private PBItem item;
       private int moneyDrop;
       public PBDrop(World world,PBGun gun,Vector2 position,ContentManager content)
       {
           this.gun = gun;
           body = BodyFactory.CreateRectangle(world, 1f, 1f, 1f, position);
           body.CollisionCategories = Category.Cat3;
           body.OnCollision += new OnCollisionEventHandler(body_OnCollision);
           tex = content.Load<Texture2D>("Weapons\\dropGun");
           DropType = Type.Gun;
       }
       public PBDrop(World world, PBItem item, Vector2 position, ContentManager content)
       {
           this.item = item;
           body = BodyFactory.CreateRectangle(world, 1f, 1f, 1f, position);
           body.CollisionCategories = Category.Cat3;
           body.OnCollision += new OnCollisionEventHandler(body_OnCollision);
           tex = content.Load<Texture2D>("Weapons\\item");
           DropType = Type.Item;
       }
       public PBDrop(World world, int moneyDrop, Vector2 position, ContentManager content)
       {
           this.moneyDrop = moneyDrop;
           body = BodyFactory.CreateRectangle(world, 1f, 1f, 1f, position);
           body.CollisionCategories = Category.Cat3;
           body.OnCollision += new OnCollisionEventHandler(body_OnCollision);
           tex = content.Load<Texture2D>("Weapons\\money");
           DropType = Type.Money;
       }
       bool body_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
       {
           PBPlayer temp = (PBPlayer)fixtureB.Body.UserData;
           if (DropType == Type.Gun)
               temp.GUIManager.AddGun(gun);
           else
               if (DropType == Type.Item)
                   temp.GUIManager.AddItem(item);
               else
                   if (DropType == Type.Money)
                       temp.Gold += moneyDrop;
           Active = false;
           body.Dispose();
           return false;
       }
      public void Update()
       {
           angle += 0.01f;
       }
      public void Draw(SpriteBatch SB)
       {
           SB.Draw(tex, PBConverter.MeterToPixel(body.Position), null, Color.White, angle, new Vector2(tex.Width / 2, tex.Height / 2), 1f, SpriteEffects.None, 0f);
       }

    }
}
