﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using ProjectBuild.GUI;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace ProjectBuild.PBEngine
{
    struct AngleShield
    {
        public float angle;
        public float time;
        public Vector2 position;
    }

    public class PBObject
    {
        public Vector2 Position
        {
            get { return Body.Position; }
            set { Body.Position = value; }
        }
        public Texture2D TextureC;
        public Texture2D TextureN;
        public Vector2 Size;
        public float Rotation { get { return Body.Rotation; } set { Body.Rotation = value; } }
        public Body Body;
        public bool Active = true;
        Vector2 Origin { get { return new Vector2(TextureC.Width / 2, TextureC.Height / 2); } }
        public int Width
        {
            get
            {
                if(TextureC!=null)
                    return TextureC.Width;
                return (int)w;
            }
        }
        public int Height
        {
            get
            {
                if (TextureC != null)
                    return TextureC.Height;
                return (int)h;
            }
        }
        float w, h;
        public int HP = 100;
        public int HPMAX = 100;
        public int SHIELD = 100;
        public int SHIELDMAX = 100;
        public float ShieldRegen = 1.5f;
        public float ShieldRegenDelay = 1f;
        public HealthBar HpBar;
        public ShieldBar SBar;
        protected int prevHp;
        protected int prevShield;
        public List<PBScriptAction> Actions = new List<PBScriptAction>();
        public PBMessageManager MessageManager;
        public float height;
        public string textureName;
        public string Name;
        public string ScriptName;
        public float width;
        public float perc = 0f;
        public World world { set; get; }
        Vector2 originalPosition;
        public bool Indestructable = false;
        public bool NoHitbox = false;

        public bool Hover(Vector2 mouse, PBCamera camera)
        {
            if (Indestructable == true) return false;

            return (new Rectangle((int)mouse.X,(int)mouse.Y,1,1).Intersects(
                new Rectangle((int)(PBConverter.MeterToPixel(Position.X)-w/2),
                    (int)(PBConverter.MeterToPixel(Position.Y)-h/2),
                    (int)w,(int)h)));
        }
        public PBObject(World world,float height,float width)
        {
            Body = BodyFactory.CreateRectangle(world, width, height, 1f);
            Body.FixtureList.Add(FixtureFactory.AttachCircle(1f, 0f, Body));
            this.world = world;
        }
        public PBObject(World world, float width, float height, Vector2 vector2)
        {
            Body = BodyFactory.CreateRectangle(world, width, height, 1f, vector2);
            Body.FixtureList.Add(FixtureFactory.AttachCircle(1f, 0f, Body));
            this.width = width;
            this.height = height;
            this.world = world;
        }

        public PBObject()
        {
            
        }

        public float diagonala(float width, float height)
        {
            double w=width;
            double h=height;
            return (float)Math.Sqrt(w * w + h * h);
        }
        public void DisableHitbox()
        {
            Body.CollidesWith = Category.None;
            Body.CollisionCategories = Category.None;
        }

        public PBObject(World world, float height, float width, Vector2 position,ContentManager content,PBMessageManager message,string s=null,float rot=0f,string name=null,bool noShield=false,bool immune=false)
        {
            this.world = world;
            Body = BodyFactory.CreateRectangle(world, width, height, 1f, position);
            originalPosition = position;
            originalRotation = rot;
            this.width = width;
            this.height = height;
            this.Indestructable = immune;
            HpBar = new HealthBar(this, content,1);
            MessageManager = message;
            Body.UserData = this;
            Body.Rotation = rot;
            Body.CollidesWith = Category.All & ~Category.Cat31;
            Body.CollisionCategories = Category.All & ~Category.Cat1 ;
            if (SHIELDMAX > 0 && !noShield && !Indestructable)
            {
                FixtureFactory.AttachCircle(diagonala(width, height) / 2f, 1f, Body);
                Body.FixtureList[1].CollidesWith = Category.Cat9 | Category.Cat10;
                Body.FixtureList[1].CollisionCategories = Category.All & Category.Cat31;
                Body.FixtureList[1].CollisionGroup=-1;
                goodCat = Category.Cat9 | Category.Cat10;
            } 
            prevHp = HPMAX;
            this.Name = name;
            this.w = width * 64f;
            this.h = height * 64f;
            if (s != null)
            {
                TextureC = content.Load<Texture2D>("Mobs\\"+s);
                //TextureN = content.Load<Texture2D>("Mobs\\" + s + "_n");
            }
            shieldTex = content.Load<Texture2D>("FX\\shield");
        }
        public void LoadTextureN(Texture2D t)
        { TextureN = t; }
        public void LoadTextureC(Texture2D t)
        { TextureC = t; }
        public virtual void Update()
        {
            Random r = new Random();
            if (perc > 0)
            {
                perc -= 0.015f;
            }
            if (prevHp != HP)
            {
                Vector2 t = Vector2.Zero;
                Vector2 t2 = PBConverter.MeterToPixel(this.Position);
                t.X = r.Next((int)t2.X-Width, (int)t2.X);
                t.Y = r.Next((int)t2.Y-Height, (int)t2.Y);

                MessageManager.AddMessage(t,
                   (HP - prevHp).ToString(), 50, Color.White);
            }
            /*for (int i = 0; i < Actions.Count; i++)
            {
                if (Actions[i].Active == false)
                    Actions.RemoveAt(i);
            }*/
            prevHp = HP;
            if (HP <= 0)
            {
                HP = 0;
                Active = false; MessageManager.AddMessage(PBConverter.MeterToPixel(this.Position),
                        "Death", 50, Color.White);
            }
            if (SHIELDMAX>0 && !Indestructable)
            {
                if (SHIELD <= 0)
                {
                    Body.FixtureList[1].CollidesWith = Category.None;
                }
                else
                    if(Body.FixtureList.Count>1)
                    Body.FixtureList[1].CollidesWith = goodCat;
            }
            
        }
        internal Category goodCat;
        public virtual void Update(PBPlayer player, PBCamera camera)
        {
            Update();
        }
        public virtual void Update(PBPlayer player, PBCamera camera,GameTime gameTime)
        {
            Update();
            UpdateAngleShield(gameTime);
            UpdateRegenShield(gameTime);
        }
        
        public virtual void Update(GameTime gameTime)
        {
            Update();
            UpdateAngleShield(gameTime);
            UpdateRegenShield(gameTime);
        }
        TimeSpan prevTime;
        float tempregen = 0;
        void UpdateRegenShield(GameTime gameTime)
        {
            if (gameTime.TotalGameTime - prevTime > TimeSpan.FromSeconds(ShieldRegenDelay))
            {
                prevTime = gameTime.TotalGameTime;
                tempregen += ShieldRegen;
                SHIELD += (int)tempregen;
                if (SHIELD > SHIELDMAX)
                    SHIELD = SHIELDMAX;
                tempregen -= (int)tempregen;
            }
        }
        void UpdateAngleShield(GameTime gameTime)
        {
            for (int i = 0; i < angleForShield.Count; i++)
            {
                AngleShield a = angleForShield[i];
                if (angleForShield[i].time < 0f)
                    angleForShield.RemoveAt(i);
                else
                    a.time -= (float)(gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);
                if (angleForShield.Count > i)
                    angleForShield[i] = a;
            }
        }
        public virtual void DrawColor(SpriteBatch SB)
        {

            if (Active)
            {
                if(TextureC!=null)
                SB.Draw(TextureC, new Rectangle((int)PBConverter.MeterToPixel(Position.X), (int)PBConverter.MeterToPixel(Position.Y),
                    (int)PBConverter.MeterToPixel(width), (int)PBConverter.MeterToPixel(height))
                    , null, Color.White, Rotation, Origin, SpriteEffects.None, 0);
                foreach (var a in angleForShield)
                    DrawShield(SB,a);
            }
        }
        void DrawShield(SpriteBatch SB,AngleShield a)
        {
            SB.Draw(shieldTex, new Rectangle(
                (int)(a.position.X+PBConverter.MeterToPixel(Position.X)),
                (int)(a.position.Y+PBConverter.MeterToPixel(Position.Y)),
                    (int)width*shieldTex.Width, (int)height*shieldTex.Height),
                    null,new Color(a.time,a.time,a.time,a.time), a.angle, new Vector2(shieldTex.Width/2,shieldTex.Height/2),SpriteEffects.None, 0);
        }
        public virtual void DrawColor(SpriteBatch SB,float angle)
        {

            if (Active)
            {
                if(TextureC!=null)
                SB.Draw(TextureC, new Rectangle((int)PBConverter.MeterToPixel(Position.X), (int)PBConverter.MeterToPixel(Position.Y),
                    (int)PBConverter.MeterToPixel(width), (int)PBConverter.MeterToPixel(height))
                    , null, Color.White, angle, Origin, SpriteEffects.None, 0);
                foreach(var a in angleForShield)
                    DrawShield(SB, a);
            }
        }
        public virtual void DrawNormal(SpriteBatch SB)
        {
            if (Active)
            {
                if(TextureN!=null)
                SB.Draw(TextureN, new Rectangle((int)PBConverter.MeterToPixel(Position.X), (int)PBConverter.MeterToPixel(Position.Y),
                    (int)PBConverter.MeterToPixel(width), (int)PBConverter.MeterToPixel(height))
                    , null, Color.White, Rotation, Origin, SpriteEffects.None, 0);
            }
        }
        public virtual void DrawNormal(SpriteBatch SB,float angle)
        {
            if (Active && TextureC != null)
            {
                SB.Draw(TextureN, new Rectangle((int)PBConverter.MeterToPixel(Position.X), (int)PBConverter.MeterToPixel(Position.Y),
                    (int)PBConverter.MeterToPixel(width), (int)PBConverter.MeterToPixel(height))
                    , null, Color.White, angle, Origin, SpriteEffects.None, 0);

            }
        }
        internal void AddAction(PBScriptAction action,bool editor=true)
        {
            if (!Actions.Contains(action))
            {
                Actions.Add(action);
                if (action.repeat == false && editor == false)
                {
                     PBScriptAction a=new PBScriptAction(action.Script_Name, originalPosition.X, originalPosition.Y, action.timeToMoveMax,originalRotation,
                        action.timeToRotateMax, action.actor, false);
                     action.AddConsequence(a);
                     Actions.Add(a);
                }
            }
        }
        string actionWay="right";
        private Texture2D shieldTex;
        public PBScriptAction FirstAction
        {
            get
            {
                if (Actions.Count == 0) return null;
                if (actionWay == "right")
                {
                    for (int i = 0; i <= Actions.Count - 1; i++)
                    {
                        if (Actions[i].Active == true && (Actions[i].returning == false||Actions[i].repeat==false))
                            return Actions[i];
                    }
                    actionWay = "left";
                }
                else
                    if (actionWay == "left")
                    {
                        for (int i = Actions.Count-1; i >=0; i--)
                        {
                            if (Actions[i].Active == true && Actions[i].returning == true)
                                return Actions[i];
                        }
                        actionWay="right";
                    }
                return null;
            }
        }
        float hitFromAngle;
        List<AngleShield> angleForShield = new List<AngleShield>();
        private float originalRotation;
        internal void TakeDamage(int DMG,float angle,Vector2 pos)
        {
            if (!Indestructable)
            {
                prevShield = SHIELD;
                if (SHIELD > 0)
                {
                    SHIELD -= DMG;
                    AngleShield a = new AngleShield();
                    a.angle = angle;
                    a.time = 1f;
                    a.position = pos - PBConverter.MeterToPixel(Position);
                    angleForShield.Add(a);
                }
                else
                    HP -= DMG;
                if (SHIELD < 0)
                {
                    HP += SHIELD;
                    SHIELD = 0;
                }
                perc = 0.9f;
            }
        }
    }
}
