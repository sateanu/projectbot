﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectBuild.PBEngine
{
    public class PBAnimation
    {
        Texture2D texture;
        int height;
        int width;
        int bigHeight;
        int bigWidth;
        bool repeat = false;
        bool Active = false;
        int indexX;
        int indexY;
        int maxIndex;
        TimeSpan time;
        TimeSpan lastTime;
        private float angle;
        int cX, cY;
        public bool Finished=false;
        public PBAnimation(Texture2D tex, int bW, int bH, int countX,int countY, float t, bool repeat,float angle=0f)
        {
            texture = tex;
            height = bH/countY;
            width = bW / countX;
            bigHeight = bH;
            bigWidth = bW;
            time =TimeSpan.FromSeconds(t);
            this.repeat = repeat;
            indexX = 0;
            indexY = 0;
            maxIndex = countX+countY-1;
            cX = countX;
            cY = countY;

        }
        public PBAnimation(Texture2D tex, int countX, int countY, float t, bool repeat, float angle = 0f)
        {
            texture = tex;
            height = tex.Height / countY;
            width = tex.Width / countX;
            bigHeight = tex.Height;
            bigWidth = tex.Width;
            time = TimeSpan.FromSeconds(t);
            this.repeat = repeat;
            indexX = 0;
            indexY = 0;
            maxIndex = countX + countY - 1;
            cX = countX;
            cY = countY;

        }
       public void Update(GameTime gameTime)
        {
            if (Active)
            {
                if (gameTime.TotalGameTime - lastTime > time)
                {
                    lastTime = gameTime.TotalGameTime;
                    indexX++;
                    if (indexX+indexY > maxIndex-1)
                    {
                        if (repeat)
                        {
                            indexY = 0;
                            indexX = 0;
                        }
                        else
                        { Active = false; Finished = true; }
                    }
                    else
                        if (indexX > cX)
                        {
                            indexX = 0;
                            indexY++;
                        }
                }
            }
        }
       public void Activate()
       {
           Active = true;
           indexX = 0;
           indexY = 0;
       }
       internal void Draw(SpriteBatch spriteBatch, Vector2 position)
       {
           if (Active)
           {
               spriteBatch.Draw(texture, position ,
                   new Rectangle(indexX * width, indexY*height, width, height),
                   Color.White, angle, new Vector2(width / 2, height / 2), 1f, SpriteEffects.None, 0f);
           }
       }
    }
}
