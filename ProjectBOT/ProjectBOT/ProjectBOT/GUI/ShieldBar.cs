﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectBuild.GUI
{
   public class ShieldBar
    {
        PBObject Target;
        Texture2D texture;
        int a;
        SpriteFont font;
        Rectangle rectangle
        {
            get
            {
                return new Rectangle(0, 0,
                  Math.Min((int)((float)(Target.SHIELD) / (float)(Target.SHIELDMAX) * 100f) * texture.Width / 100, texture.Width), texture.Height);
            }
        }
        Vector2 position
        {
            get
            {
                return new Vector2(PBConverter.MeterToPixel(Target.Body.Position.X) - Target.Width / 2,
                    PBConverter.MeterToPixel(Target.Body.Position.Y) - Target.Height);
            }
        }
        public ShieldBar(PBObject obj, ContentManager content, int a)
        {
            Target = obj;
            this.a = a;
            font = content.Load<SpriteFont>("gamefont");
            if (a == 2)
            { texture = content.Load<Texture2D>("GUI/HpBarPlayer"); }
            else
                texture = content.Load<Texture2D>("GUI/HpBar");
        }
        public void DrawC(SpriteBatch spriteBatch)
        {
            if (a == 1)
            {
                spriteBatch.DrawString(font, 
                    Target.Name + " " + Target.SHIELD.ToString() + "/" + Target.SHIELDMAX.ToString(),
                    position - new Vector2(0, font.LineSpacing), Color.White);

                spriteBatch.Draw(texture, position, rectangle, 
                    new Color(1 - Target.HP / 100f, Target.HP / 100f, 0));
            }
            else
            {
                spriteBatch.DrawString(font, Target.SHIELD.ToString() + "/" + Target.SHIELDMAX.ToString(),
                    new Vector2(75, 75+2*texture.Height) - new Vector2(0, font.LineSpacing), Color.White);

                spriteBatch.Draw(texture, new Vector2(75, 75+2*texture.Height), rectangle, 
                    new Color(0f,0.5f,1f),0f, Vector2.Zero,
                    1f, SpriteEffects.None, 0f);
            }
        }
    }
}
