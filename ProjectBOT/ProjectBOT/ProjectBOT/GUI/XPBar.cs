﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ProjectBuild.GUI
{
    public class XPBar
    {
        PBPlayer Player;
        Texture2D alphatex;
        Texture2D texture;
        SpriteFont font;
        private Rectangle rectangle
        {
            get
            {
                return new Rectangle(0, 0,
                   (int)((float)Player.Xp * (float)texture.Width / (float)Player.XpRequired), texture.Height);
            }
        }
        public XPBar(PBPlayer player, ContentManager content)
        {
            Player = player;
            texture = content.Load<Texture2D>("GUI/XPBAR");
            alphatex = content.Load<Texture2D>("GUI/XPBARALPHA");
            font = content.Load<SpriteFont>("gamefont");
        }
        public void Draw(SpriteBatch spriteBatch, Viewport viewport)
        {
            Vector2 v = new Vector2(0, viewport.Height - texture.Height);
            Rectangle rec = new Rectangle(0, viewport.Height - texture.Height, viewport.Width, texture.Height);
            spriteBatch.Draw(texture, v, rectangle, Color.LightBlue, 0f, Vector2.Zero, new Vector2((float)viewport.Width / texture.Width, 1), SpriteEffects.None, 1f);
            spriteBatch.Draw(alphatex, v, null, Color.White, 0f, Vector2.Zero, new Vector2((float)viewport.Width / texture.Width, 1), SpriteEffects.None, 1f);
            spriteBatch.DrawString(font, "Level " + Player.Level.ToString() + " (" + Player.Xp.ToString() + "/" + Player.XpRequired.ToString() + ") ", v - new Vector2(0, 32), Color.White);
            spriteBatch.DrawString(font, "Gold: " + Player.Gold.ToString(), v - new Vector2(0, 58), Color.White);
        }
    }
}
