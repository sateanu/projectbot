﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ProjectBuild.PBEngine;
using ProjectBuild.PBEngine.Items;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Input;
namespace ProjectBuild.GUI
{
  public class PBGUIManager
    {

      struct GuiPosition
      {
         public int tabIndex, cellIndex;
         public Vector2 position;
      }
      struct GuiSettings
      {
          public int cellsPerLine;
          public int cellLines;
          public float YSpace;
          public float XSpace;
          public int TotalCells()
          {
              return cellLines * cellsPerLine;
          }
      }
        List<Cell> cells;
        Cell WeapondCell;
        List<Stats> stats;
        Texture2D texture;
        Vector2 position;
        Texture2D toolTipTex;
        public bool IsOn;
        PBPlayer Player;
        public int StatPoints
        {
            get { return Player.StatPoints; }
            set { Player.StatPoints = value; }
        }
        public int RemovePoints
        {
            get { return Player.RemovePoints; }
            set { Player.RemovePoints=value; }
        }
        List<PBGun> guns;
        SpriteFont font;
        Texture2D cellTexture;
        internal ContentManager content;
        internal World world;
        PBGun  tempGun;
        PBItem tempItem;
        Cell.Type tempType;
        bool IsTempSlotOccupied = false;
        bool IsTooltipOn = false;
        StringBuilder TooltipString;
        GuiPosition guiPosition;
        public void ShowOrHide()
        {
            if (IsOn == true)
            {
                IsOn = false;
                if (IsTempSlotOccupied)
                {
                    foreach (var cell in cells)
                    {
                        if (!cell.IsOccupied)
                        {
                            if (tempType == Cell.Type.Gun)
                            {
                                cell.StoreGun(tempGun);
                                tempGun = null;
                                IsTempSlotOccupied = false;
                                break;
                            }
                            else
                                if (tempType == Cell.Type.Item)
                                {
                                    cell.StoreItem(tempItem);
                                    tempItem = null;
                                    IsTempSlotOccupied = false;
                                    break;
                                }
                        }
                    }
                }
            }
            else
            {
                IsOn = true;
                guiPosition.cellIndex = 0;
                guiPosition.tabIndex = 1;
                guiPosition.position = cells[0].Center;
                PBGlobal.mouseIconPosition = guiPosition.position;
            }
        }

        GuiSettings Setting;

        string remainedPoints { get { return " Points: " + StatPoints.ToString(); } }
        string pointsToRemove { get { return " Remove points: " + RemovePoints.ToString(); } }
      
      public PBGUIManager(ContentManager content, PBPlayer player, World world, Viewport viewport)
        {
            font = content.Load<SpriteFont>("gamefont");
            IsOn = false;
            this.content = content;
            this.world = world;
            cells = new List<Cell>();
            stats = new List<Stats>();
            guns = new List<PBGun>();
            cellTexture = content.Load<Texture2D>("GUI/cellTexture");
            texture = content.Load<Texture2D>("GUI/STATSUI");
            toolTipTex = content.Load<Texture2D>("Etc//Background//background");
            Player = player;
            Player.GUIManager = this;
            position = new Vector2(viewport.Width / 2, viewport.Height / 2) - new Vector2(texture.Width / 2, texture.Height / 2);
            WeapondCell = new Cell(position + new Vector2(25, 33), cellTexture, this);
            WeapondCell.OnMouseClickEmpty += new Cell.FiredEvent(wepEmpty);
            WeapondCell.OnMouseClickOccupied += new Cell.FiredEvent(wepOccupied);
            WeapondCell.OnMouseHover += new Cell.FiredEvent(cellHover);
            StatPoints = 100;
            RemovePoints = 100;

            Setting.cellLines = 6;
            Setting.cellsPerLine = 5;
            Setting.XSpace = 35;
            Setting.YSpace = 40;

            Vector2 temp = new Vector2(position.X + 388, position.Y + 20);

            Stats SMAXstat = new Stats(temp, content);
            SMAXstat.ClickPlus += new Stats.FiredEvent(SMaxPlus);
            SMAXstat.ClickMinus += new Stats.FiredEvent(SMaxMinus);
            SMAXstat.Text = "Shield "+Player.SHIELDMAX;
            temp.Y += 32;

            Stats SRegenstat = new Stats(temp, content);
            SRegenstat.ClickPlus += new Stats.FiredEvent(SRegenPlus);
            SRegenstat.ClickMinus += new Stats.FiredEvent(SRegenMinus);
            SRegenstat.Text = "Shield Regen " + Player.ShieldRegen;
            temp.Y += 32;

            Stats SRegenDelaystat = new Stats(temp, content);
            SRegenDelaystat.ClickPlus += new Stats.FiredEvent(SRegenDelayPlus);
            SRegenDelaystat.ClickMinus += new Stats.FiredEvent(SRegenDelayMinus);
            SRegenDelaystat.Text = "Shield Delay " + Player.ShieldRegenDelay;
            temp.Y += 32;

            Stats Speedstat = new Stats(temp, content);
            Speedstat.ClickPlus += new Stats.FiredEvent(SpeedPlus);
            Speedstat.ClickMinus += new Stats.FiredEvent(SpeedMinus);
            Speedstat.Text = "Speed " +Player.Speed;
            temp.Y += 32;

            Stats Dmgstat = new Stats(temp, content);
            Dmgstat.ClickPlus += new Stats.FiredEvent(DmgPlus);
            Dmgstat.ClickMinus += new Stats.FiredEvent(DmgMinus);
            Dmgstat.Text = "Damage +"+Player.DMG;
            temp.Y += 32;

            Stats FireRatestat = new Stats(temp, content);
            FireRatestat.ClickPlus += new Stats.FiredEvent(FireRatePlus);
            FireRatestat.ClickMinus += new Stats.FiredEvent(FireRateMinus);
            FireRatestat.Text = "Fire Rate +" +Player.FireRT;
            temp.Y += 32;

            stats.Add(SMAXstat);
            stats.Add(SRegenstat);
            stats.Add(SRegenDelaystat);
            stats.Add(Speedstat);
            stats.Add(Dmgstat);
            stats.Add(FireRatestat);
            Vector2 t = position + new Vector2(200, 20);
            for (int i = 0; i < Setting.TotalCells(); i++)
            {
                if (i != 0 && i % Setting.cellsPerLine == 0)
                { t.Y += Setting.YSpace; t.X = position.X + 200; }
                cells.Add(new Cell(t, cellTexture, this));
                cells[i].OnMouseClickEmpty += new Cell.FiredEvent(cellEmpty);
                cells[i].OnMouseClickOccupied += new Cell.FiredEvent(cellOccupied);
                cells[i].OnMouseHover += new Cell.FiredEvent(cellHover);
                cells[i].OnMouseRightClickOccupied += new Cell.FiredEvent(cellRightClick);
                t.X += Setting.XSpace;

            }
            //cells.Add(new Cell(position + new Vector2(200, 20), cellTexture, this));

         /*   cells[0].StoreGun(PBGuns.EnemyGun.Copy());
            cells[1].StoreGun(PBGuns.NormalGun.Copy());
            cells[9].StoreGun(PBGuns.SpeedyGonzales.Copy());
            cells[3].StoreGun(PBGuns.TROLLMODE.Copy());
            PBGun g = PBGuns.BasicGun.Copy();
            g.BindTo(Player, content, world);
            Player.IsGun = true;
            Player.Gun = g;
            WeapondCell.StoreGun(g);
            tempType = Cell.Type.Gun;*/
        }
        #region CELLS
        void wepEmpty(object sender)
        {
            if (IsTempSlotOccupied&&tempType==Cell.Type.Gun)
            {
                Cell c = (Cell)sender;
                c.StoreGun(tempGun);
                tempGun.BindTo(Player, content, world);
                if(Player.Gun!=null)
                tempGun.Bullets = Player.Gun.Bullets;
                Player.Gun = tempGun;
                Player.IsGun = true;
                tempGun = null;
                IsTempSlotOccupied = false;
            }
        }
        void wepOccupied(object sender)
        {
            if(tempType==Cell.Type.Gun)
            if (IsTempSlotOccupied)
            {
                Cell c = (Cell)sender;
                PBGun aux = c.GetGun();
                c.StoreGun(tempGun);
                tempGun.BindTo(Player, content, world);
                tempGun.Bullets = Player.Gun.Bullets;
                Player.Gun = tempGun;
                Player.IsGun = true;
                tempGun = aux;
            }
            else
            {
                Cell c = (Cell)sender;
                IsTempSlotOccupied = true;
                tempGun = c.GetGun();
                Player.Gun.Bullets = tempGun.Bullets;
                Player.IsGun = false;
            }
        }
        void cellEmpty(object sender)
        {
            if (IsTempSlotOccupied)
                if (tempType == Cell.Type.Gun)
                {
                    Cell c = (Cell)sender;
                    c.StoreGun(tempGun);
                    tempGun = null;
                    IsTempSlotOccupied = false;
                }
                else if (tempType == Cell.Type.Item)
                {
                    Cell c = (Cell)sender;
                    c.StoreItem(tempItem);
                    tempItem = null;
                    IsTempSlotOccupied = false;
                }

        }
        void cellOccupied(object sender)
        {
            if (IsTempSlotOccupied)
            {
                Cell c = (Cell)sender;
                if (tempType == Cell.Type.Gun)
                {
                    if (c.type == Cell.Type.Gun)
                    {
                        PBGun aux = c.GetGun();
                        c.StoreGun(tempGun);
                        tempGun = aux;
                        tempType = Cell.Type.Gun;
                    }
                    else //ITEM
                    {
                        PBItem aux = c.GetItem();
                        c.StoreGun(tempGun);
                        tempGun = null;
                        tempItem = aux;
                        tempType = Cell.Type.Item;
                    }
                }
                else if (tempType == Cell.Type.Item)
                {
                    if (c.type == Cell.Type.Gun)
                    {
                        PBGun aux = c.GetGun();
                        c.StoreItem(tempItem);
                        tempItem = null;
                        tempGun = aux;
                        tempType = Cell.Type.Gun;
                    }
                    else //ITEM
                    {
                        PBItem aux = c.GetItem();
                        c.StoreItem(tempItem);
                        tempItem = aux;
                        tempType = Cell.Type.Item;
                    }
                }   
            }
            else
            {
                Cell c = (Cell)sender;
                if (c.type == Cell.Type.Gun)
                {
                    IsTempSlotOccupied = true;
                    tempType = Cell.Type.Gun;
                    tempGun = c.GetGun();
                }
                if (c.type == Cell.Type.Item)
                {
                    IsTempSlotOccupied = true;
                    tempType = Cell.Type.Item;
                    tempItem = c.GetItem();
                }
            }
        }
        void cellHover(object sender)
        {
            Cell c = (Cell)sender;
            if (c.IsOccupied)
            {
                TooltipString = c.toolTip;
                IsTooltipOn = true;
            }
        }
        void cellRightClick(object sender)
        {
            Cell c = (Cell)sender;
            if (c.type == Cell.Type.Gun)
            {
                if (WeapondCell.IsOccupied)
                {
                    PBGun aux = c.GetGun();
                    PBGun aux2 = WeapondCell.GetGun();
                    c.StoreGun(aux2);
                    WeapondCell.StoreGun(aux);
                    aux.BindTo(Player, content, world);
                    if (Player.Gun != null)
                        aux.Bullets = Player.Gun.Bullets;
                    Player.Gun = aux;
                    Player.IsGun = true;
                }
                else
                {
                    PBGun aux = c.GetGun();
                    WeapondCell.StoreGun(aux);
                    aux.BindTo(Player, content, world);
                    if (Player.Gun != null)
                        aux.Bullets = Player.Gun.Bullets;
                    Player.Gun = aux;
                    Player.IsGun = true;
                    tempType = Cell.Type.Gun;
                }
            }
        }

#endregion
        #region STATS
        #region ShieldMaxStat
        void SMaxPlus(object sender)
        {
            if (StatPoints > 0)
            {
                Player.SHIELDMAX += 10;
                StatPoints--;
                Stats a = (Stats)sender;
                a.Text = "Shield " + Player.SHIELDMAX;
            }
        }
        void SMaxMinus(object sender)
        {
            if (Player.SHIELDMAX > 100 && RemovePoints>0)
            {
                RemovePoints--;
                Player.SHIELDMAX -= 10;
                StatPoints++;
                Stats a = (Stats)sender;
                a.Text = "Shield " + Player.SHIELDMAX;
            }
        }
        #endregion
        #region ShieldRegenstat
        void SRegenPlus(object sender)
        {
            if (StatPoints > 0)
            {
                Player.ShieldRegen += 0.5f;
                StatPoints--;
                Stats a = (Stats)sender;
                a.Text = "Shield Regen " + Player.ShieldRegen;
            }
        }
        void SRegenMinus(object sender)
        {
            if (Player.ShieldRegen > 1f&&RemovePoints>0)
            {
                RemovePoints--;
                Player.ShieldRegen -= 0.5f;
                StatPoints++;
                Stats a = (Stats)sender;
                a.Text = "Shield Regen " + Player.ShieldRegen;
            }
        }
        #endregion
        #region ShieldRegenDelaystat
        void SRegenDelayPlus(object sender)
        {
            if (StatPoints > 0)
            {
                if (Player.ShieldRegenDelay > 0.1f)
                {
                    Player.ShieldRegenDelay -= 0.1f;
                    Player.ShieldRegenDelay = (float)Math.Round(Convert.ToDouble(Player.ShieldRegenDelay), 3, MidpointRounding.ToEven);
                    StatPoints--;
                    Stats a = (Stats)sender;
                    a.Text = "Shield Delay " + Player.ShieldRegenDelay;
                }
                else
                    if (Player.ShieldRegenDelay > 0.01f)
                    {
                        Player.ShieldRegenDelay -= 0.01f;
                        Player.ShieldRegenDelay = (float)Math.Round(Convert.ToDouble(Player.ShieldRegenDelay), 3, MidpointRounding.ToEven);
                        StatPoints--;
                        Stats a = (Stats)sender;
                        a.Text = "Shield Delay " + Player.ShieldRegenDelay;
                    }
            }
        }
        void SRegenDelayMinus(object sender)
        {
            if (RemovePoints > 0)
            {
                RemovePoints--;
                if (Player.ShieldRegenDelay < 0.1f)
                    Player.ShieldRegenDelay += 0.01f;
                else
                    Player.ShieldRegenDelay += 0.1f;
                Player.ShieldRegenDelay = (float)Math.Round(Convert.ToDouble(Player.ShieldRegenDelay), 3, MidpointRounding.ToEven);
                StatPoints++;
                Stats a = (Stats)sender;
                a.Text = "Shield Delay " + Player.ShieldRegenDelay;
            }
            
        }
        #endregion
        #region Speedstat
        void SpeedPlus(object sender)
        {
            if(StatPoints>0)
            { Player.Speed += 0.1f;
            Player.Speed = (float)Math.Round(Convert.ToDouble(Player.Speed), 2, MidpointRounding.ToEven);
            StatPoints--;
            Stats a = (Stats)sender;
            a.Text = "Speed " + Player.Speed;
            }
        }
        void SpeedMinus(object sender)
        {
            if (Player.Speed > 1f && RemovePoints > 0)
            {
                RemovePoints--;
                Player.Speed -= 0.1f;
                Player.Speed = (float)Math.Round(Convert.ToDouble(Player.Speed), 2, MidpointRounding.ToEven);
                StatPoints++;
                Stats a = (Stats)sender;
                a.Text = "Speed " + Player.Speed;
            }
        }
        #endregion
        #region Dmgstat
        void DmgPlus(object sender)
        {
            if (StatPoints > 0)
            {
                StatPoints--;
                Player.DMG++;
                Stats a = (Stats)sender;
                a.Text = "Damage +" + Player.DMG;
            }
        }
        void DmgMinus(object sender)
        {
            if (Player.DMG > 0 && RemovePoints>0)
            {
                RemovePoints--;
                StatPoints++;
                Player.DMG--;
                Stats a = (Stats)sender;
                a.Text = "Damage +" + Player.DMG;
            }
        }
        #endregion
        #region FireRatestat
        void FireRatePlus(object sender)
        {
            if (StatPoints > 0  )
            {
                Player.FireRT += 1;
                StatPoints--;
                Stats a = (Stats)sender;
                a.Text = "FireRate +" + Player.FireRT;
            }
        }
        void FireRateMinus(object sender)
        {
            if (Player.FireRT > 0 &&RemovePoints>0)
            {
                RemovePoints--;
                Player.FireRT -= 1;
                StatPoints++;
                Stats a = (Stats)sender;
                a.Text = "FireRate +" + Player.FireRT;
            }
        }
        #endregion
        #endregion
        Vector2 prevPos;
        public void AddGun(PBGun obj)
        {
            for (int i = 0; i < cells.Count; i++)
                if (!cells[i].IsOccupied)
                {
                    cells[i].StoreGun(obj);
                    Player.MessageManager.AddMessage(PBConverter.MeterToPixel(Player.Position),
                        "+" + obj.Name, 100, Color.White);
                    break;
                }
        }
        internal void AddItem(PBItem pBItem)
        {
            if (pBItem == null)
                return;
            bool done = false;
            for (int i = 0; i < cells.Count; i++)
                    if (cells[i].type == Cell.Type.Item)
                        if (pBItem.ID == cells[i].Item.ID)
                        {
                            cells[i].Item.count++;
                            Player.MessageManager.AddMessage(PBConverter.MeterToPixel(Player.Position),
                        "+" + pBItem.name, 100, Color.White);
                            done = true;
                            break;
                        }
            if (!done)
            for (int i = 0; i < cells.Count; i++)
                if (!cells[i].IsOccupied)
                {
                    cells[i].StoreItem(pBItem);
                    Player.MessageManager.AddMessage(PBConverter.MeterToPixel(Player.Position),
                        "+" + pBItem.name, 100, Color.White);
                    break;
                }
        }
        public void Update(Viewport viewport,InputState input)
        {
            if (IsOn)
            {
                prevPos = position;
                position = new Vector2(viewport.Width / 2, viewport.Height / 2) - new Vector2(texture.Width / 2, texture.Height / 2);
                if (position != prevPos)
                {
                    Vector2 t = position - prevPos;
                    foreach (var stat in stats)
                    {
                        stat.buttonMinus.position += t;
                        stat.buttonPlus.position += t;
                        stat.textPos += t;
                    }
                    foreach (var cell in cells)
                    {
                        cell.position += t;
                    }
                    WeapondCell.position += t;
                }
                IsTooltipOn = false;
                TooltipString = null;
                UpdateInput(input);
                foreach (var stat in stats)
                    stat.Update(input);
                foreach (var cell in cells)
                    cell.Update(input);
                WeapondCell.Update(input);
            }
        }
        void UpdatePosition(int posIndex, int tabIndex)
        {
            guiPosition.cellIndex = posIndex;
            guiPosition.tabIndex = tabIndex % 3;
            if (guiPosition.tabIndex < 0)
                guiPosition.tabIndex = 2;
            switch (guiPosition.tabIndex)
            {
                case 0:
                    guiPosition.position = WeapondCell.Center;
                    PBGlobal.mouseIconPosition = guiPosition.position;
                    break;
                case 1:
                    guiPosition.position = cells[posIndex].Center;
                    PBGlobal.mouseIconPosition = guiPosition.position;
                    break;
                case 2:
                    if (posIndex % 2 == 0)
                        guiPosition.position = stats[posIndex/2].buttonMinus.Center;
                    else
                        guiPosition.position = stats[(posIndex-1)/2].buttonPlus.Center;
                    PBGlobal.mouseIconPosition = guiPosition.position;
                    break;
            }
        }
        private void UpdateInput(InputState input)
        {
            PlayerIndex playerIndex;
            if (input.IsNewButtonPress(Buttons.RightShoulder,PlayerIndex.One,out playerIndex))
                UpdatePosition(0, guiPosition.tabIndex + 1);
            if (input.IsNewButtonPress(Buttons.LeftShoulder, PlayerIndex.One, out playerIndex))
                UpdatePosition(0, guiPosition.tabIndex - 1);

            int tempIndex;
            int line;

            if (input.IsNewButtonPress(Buttons.LeftThumbstickLeft, PlayerIndex.One, out playerIndex) ||
                input.IsNewButtonPress(Buttons.DPadLeft, PlayerIndex.One, out playerIndex))
            {
                switch (guiPosition.tabIndex)
                {
                    case 1:
                        tempIndex = guiPosition.cellIndex - 1;
                        line = (guiPosition.cellIndex / Setting.cellsPerLine);
                        if (tempIndex < line * Setting.cellsPerLine)
                            tempIndex = line * Setting.cellsPerLine+Setting.cellsPerLine-1;
                        UpdatePosition(tempIndex,guiPosition.tabIndex);
                        break;
                    case 2:
                        tempIndex = guiPosition.cellIndex - 1;
                        line = (guiPosition.cellIndex / 2);
                        if (tempIndex < line * 2)
                            tempIndex = line * 2 + 1;
                        UpdatePosition(tempIndex,guiPosition.tabIndex);
                        break;
                }
            }

            if (input.IsNewButtonPress(Buttons.LeftThumbstickRight, PlayerIndex.One, out playerIndex) ||
                input.IsNewButtonPress(Buttons.DPadRight, PlayerIndex.One, out playerIndex))
            {
                switch (guiPosition.tabIndex)
                {
                    case 1:
                        tempIndex = guiPosition.cellIndex + 1;
                        line = (guiPosition.cellIndex / Setting.cellsPerLine);
                        if (tempIndex > line * Setting.cellsPerLine+Setting.cellsPerLine-1)
                            tempIndex = line * Setting.cellsPerLine;
                        UpdatePosition(tempIndex, guiPosition.tabIndex);
                        break;
                    case 2:
                        tempIndex = guiPosition.cellIndex + 1;
                        line = (guiPosition.cellIndex / 2);
                        if (tempIndex > line * 2 + 1)
                            tempIndex = line * 2;
                        UpdatePosition(tempIndex, guiPosition.tabIndex);
                        break;
                }
            }

            if (input.IsNewButtonPress(Buttons.LeftThumbstickUp, PlayerIndex.One, out playerIndex) ||
                input.IsNewButtonPress(Buttons.DPadUp, PlayerIndex.One, out playerIndex))
            {
                switch (guiPosition.tabIndex)
                {
                    case 1:
                        tempIndex = guiPosition.cellIndex - Setting.cellsPerLine;
                        line = ((tempIndex-Setting.cellsPerLine+1) / Setting.cellsPerLine);
                        if (line < 0)
                            tempIndex = (Setting.cellLines-1) * Setting.cellsPerLine + guiPosition.cellIndex%Setting.cellsPerLine;
                        UpdatePosition(tempIndex, guiPosition.tabIndex);
                        break;
                    case 2:
                        tempIndex = guiPosition.cellIndex - 2;
                        line = ((tempIndex-1)/2);
                        if (line < 0)
                            tempIndex = (stats.Count - 1)*2+guiPosition.cellIndex % 2;
                        UpdatePosition(tempIndex, guiPosition.tabIndex);
                        break;
                }
            }

            if (input.IsNewButtonPress(Buttons.LeftThumbstickDown, PlayerIndex.One, out playerIndex) ||
                input.IsNewButtonPress(Buttons.DPadDown, PlayerIndex.One, out playerIndex))
            {
                switch (guiPosition.tabIndex)
                {
                    case 1:
                        tempIndex = guiPosition.cellIndex + Setting.cellsPerLine;
                        line = (tempIndex / Setting.cellsPerLine);
                        if (line > Setting.cellLines-1)
                            tempIndex = 0 + guiPosition.cellIndex % Setting.cellsPerLine;
                        UpdatePosition(tempIndex, guiPosition.tabIndex);
                        break;
                    case 2:
                        tempIndex = guiPosition.cellIndex +2;
                        line = ((tempIndex) / 2);
                        if (line > stats.Count-1)
                            tempIndex = guiPosition.cellIndex % 2;
                        UpdatePosition(tempIndex, guiPosition.tabIndex);
                        break;
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsOn)
            {
                spriteBatch.Draw(texture, position, Color.White);
                foreach (var stat in stats)
                    stat.Draw(spriteBatch);
                foreach (var cell in cells)
                    cell.Draw(spriteBatch);

                spriteBatch.DrawString(font, remainedPoints,
                    new Vector2(position.X + texture.Width, position.Y + texture.Height) -
                     new Vector2(200,60), Color.White,0f,
                    Vector2.Zero
                    ,0.8f,SpriteEffects.None,0f);

                spriteBatch.DrawString(font,pointsToRemove,
                    new Vector2(position.X + texture.Width, position.Y + texture.Height) -
                    new Vector2(200,35), Color.White,0f,
                    Vector2.Zero
                    , 0.8f, SpriteEffects.None, 0f);

                if (IsTempSlotOccupied)
                {
                    if (tempType == Cell.Type.Gun)
                        tempGun.DrawInvPic(new Vector2(PBGlobal.mouseIconPosition.X - WeapondCell.Width / 2, +PBGlobal.mouseIconPosition.Y - WeapondCell.Height / 2), spriteBatch,WeapondCell);
                    else
                        if (tempType == Cell.Type.Item)
                            tempItem.DrawInvPic(new Vector2(PBGlobal.mouseIconPosition.X - WeapondCell.Width / 2, PBGlobal.mouseIconPosition.Y - WeapondCell.Height / 2), spriteBatch,WeapondCell);
                }
                WeapondCell.Draw(spriteBatch);
                if (IsTooltipOn)
                {
                    spriteBatch.Draw(toolTipTex, new Rectangle((int)PBGlobal.mouseIconPosition.X, (int)PBGlobal.mouseIconPosition.Y+font.LineSpacing,(int) font.MeasureString(TooltipString).X, font.LineSpacing * 3), Color.Gray);
                    spriteBatch.DrawString(font, TooltipString, new Vector2((int)PBGlobal.mouseIconPosition.X, (int)PBGlobal.mouseIconPosition.Y + font.LineSpacing), Color.White);
                }
            }
        }
      
        internal bool CheckItem(ref int itemsGot,int itemIdReq, int itemsReq)
        {
            bool found = false;
            foreach(Cell cell in cells)
                if (cell.type == Cell.Type.Item)
                {
                    if (itemIdReq == cell.Item.ID)
                        if (cell.Item.count >= itemsReq)
                        {
                            found = true;
                            itemsGot = itemsReq;
                            break;
                        }
                        else
                        {
                            itemsGot = cell.Item.count;
                            break;
                        }
                }


            return found;
        }

        internal void RemoveItem(int itemID, int count)
        {
            foreach (Cell cell in cells)
                if (cell.type == Cell.Type.Item)
                {
                    if (itemID == cell.Item.ID)
                    {
                        if (cell.Item.count - count <= 0)
                        {
                            cell.RemoveItem();
                        }
                        else
                            cell.Item.count -= count;
                    }
                }
        }

        internal void RefreshGuns()
        {
            if (WeapondCell.Gun != null)
                WeapondCell.Gun.BindTo(Player, content, world);
            foreach(Cell c in cells)
            {
                if(c.Gun!=null)
                c.Gun.BindTo(Player, content, world);
            }
        }

        internal void Reset()
        {
            StatPoints = 5;
            stats[0].Text = "Shield Capacity " + Player.SHIELDMAX;
            stats[1].Text = "Shield Regen " + Player.ShieldRegen;
            stats[2].Text = "Shield Delay " + Player.ShieldRegenDelay;
            stats[3].Text = "Speed " + Player.Speed;
            stats[4].Text = "Damage " + Player.DMG;
            stats[5].Text = "Fire Rate " + Player.FireRT;
            foreach (Cell cell in cells)
            {
                cell.GetGun();
            }
            WeapondCell.GetGun();
            Player.Gun = null;
            Player.IsGun = false;
        }

        public bool Shown
        {
            get { return IsOn; }
            private set { IsOn = value; }
        }
    }
}
