﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ProjectBuild.GUI
{
   public class HealthBar
    {
       internal PBObject Target;
       internal Texture2D texture;
       internal int a;
       internal SpriteFont font;
       internal Rectangle rectangle
        {
            get
            {
                return new Rectangle(0,0,
                  Math.Min((int)((float)(Target.HP)/(float)(Target.HPMAX)*100f) * texture.Width/100,texture.Width), texture.Height);
            }
        }
       internal Vector2 position
        {
            get
            {
                return new Vector2(PBConverter.MeterToPixel(Target.Body.Position.X)-Target.Width/2,
                    PBConverter.MeterToPixel(Target.Body.Position.Y)-Target.Height);
            }
        }
        public HealthBar(PBObject obj,ContentManager content,int a)
        { 
            Target = obj;
            this.a=a;
            font = content.Load<SpriteFont>("gamefont");
            if (a == 2)
            { texture = content.Load<Texture2D>("GUI/HpBarPlayer");  }
            else
                texture = content.Load<Texture2D>("GUI/HpBar");
        }
        public void DrawC(SpriteBatch spriteBatch)
        {
            if (a == 1)
            {
                spriteBatch.DrawString(font,Target.Name+" " + Target.HP.ToString() + "/" + Target.HPMAX.ToString(), position - new Vector2(0, font.LineSpacing), Color.White);
                spriteBatch.Draw(texture, position, rectangle, new Color(1 - Target.HP / 100f, Target.HP / 100f, 0));
            }
            else
            {
                spriteBatch.DrawString(font, Target.HP.ToString() + "/" + Target.HPMAX.ToString(), new Vector2(75,75) - new Vector2(0, font.LineSpacing), Color.White);
                spriteBatch.Draw(texture, new Vector2(75, 75), rectangle, new Color(1 - Target.HP / 100f, Target.HP / 200f, 0), 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            }
        }
        public void DrawN(SpriteBatch spriteBatch)
        {
            if(a==1)spriteBatch.Draw(texture, position,rectangle ,Color.Purple);
        }
    }
}
