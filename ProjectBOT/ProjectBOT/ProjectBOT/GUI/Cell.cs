﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectBuild.PBEngine.Items;
using Microsoft.Xna.Framework.Input;
using ProjectBuild.PBEngine;

namespace ProjectBuild.GUI
{
    class Cell
    {
        public enum Type
        {
            None,
            Gun,
            Item
        }
        public Type type;
        public delegate void FiredEvent(object sender);

        public FiredEvent OnMouseClickEmpty;
        public FiredEvent OnMouseClickOccupied;
        public FiredEvent OnMouseHover;
        public FiredEvent OnMouseRightClickEmpty;
        public FiredEvent OnMouseRightClickOccupied;

        public Vector2 Center
        {
            get
            {
                return position + new Vector2(texture.Width / 2, texture.Height / 2);
            }
        }

        Texture2D texture;
       public Vector2 position;
        PBGUIManager manager;
        PBGun _Gun;
        public PBGun Gun { get { return _Gun; } private set { _Gun = value; } }
       public PBItem Item;
        public StringBuilder toolTip
        {
            get
            {
                if (_Gun != null)
                    return _Gun.ToStringDetails();
                else
                    return Item.ToStringDetails();
            }
        }
       public bool IsOccupied = false;
        private bool mouseOver;
        Rectangle rec
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y,
                    texture.Width, texture.Height);
            }
        }
        MouseState prevMouse;
        public int Width { get { return texture.Width; } }
        public int Height { get { return texture.Height; } }

        public Cell(Vector2 pos,Texture2D tex,PBGUIManager mg)
        {
            position = pos;
            texture = tex;
            manager = mg;
        }
        public Cell()
        {
        }
        public Cell(Cell cell)
        {
            texture = cell.texture;
            Item = cell.Item;
            type = cell.type;
            _Gun = cell.GetGun();
            IsOccupied = cell.IsOccupied;
            OnMouseClickOccupied = cell.OnMouseClickOccupied;
            OnMouseClickEmpty = cell.OnMouseClickEmpty;
            OnMouseHover = cell.OnMouseHover;
        }
        public void StoreGun(PBGun gun)
        {
            _Gun = gun;
            _Gun.LoadTexture(manager.content);
            type = Type.Gun;
            IsOccupied = true;
        }
        
        public PBGun GetGun()
        {
            IsOccupied = false;
            PBGun t = _Gun;
            _Gun = null;
            type = Type.None;
            return t;
        }
        public PBGun GetGun(ProjectBuild.PBEngine.PBObject obj)
        {
            IsOccupied = false;
            PBGun t = _Gun;
            _Gun = null;
            type = Type.None;
            t.BindTo(obj, manager.content, manager.world);
            return t;
        }
       
        public void Update(InputState input)
        {
            PlayerIndex playerIndex;
            Vector2 mouseCoord = new Vector2(PBGlobal.mouseIconPosition.X, PBGlobal.mouseIconPosition.Y);
            Rectangle mouseRec = new Rectangle((int)mouseCoord.X, (int)mouseCoord.Y, 1, 1);
            if (rec.Intersects(mouseRec))
            {
                mouseOver = true;
                if (OnMouseHover != null)
                    OnMouseHover(this);
            }
            else
                if (mouseOver)
                {
                    mouseOver = false;
                }
            if (mouseOver && 
                ((Mouse.GetState().LeftButton == ButtonState.Released && prevMouse.LeftButton == ButtonState.Pressed)||
                input.IsNewButtonPress(Buttons.A,PlayerIndex.One,out playerIndex)))
            {
                if (!IsOccupied)
                {
                    if (OnMouseClickEmpty != null)
                        OnMouseClickEmpty(this);
                }
                else
                    if (OnMouseClickOccupied != null)
                        OnMouseClickOccupied(this);
            }
            if (mouseOver && 
                ((Mouse.GetState().RightButton == ButtonState.Released && prevMouse.RightButton == ButtonState.Pressed)||
                input.IsNewButtonPress(Buttons.X,PlayerIndex.One,out playerIndex)))
            {
                if (!IsOccupied)
                {
                    if (OnMouseRightClickEmpty != null)
                        OnMouseRightClickEmpty(this);
                }
                else
                    if (OnMouseRightClickOccupied != null)
                        OnMouseRightClickOccupied(this);
            }
            prevMouse = Mouse.GetState();
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
            if (IsOccupied)
                if (_Gun != null)
                    _Gun.DrawInvPic(position, spriteBatch,this);
                else
                    Item.DrawInvPic(position, spriteBatch,this);
        }
        public Cell Copy()
        {
            return new Cell(position, texture, manager)
            {
                _Gun=this._Gun,
                Item=this.Item,
                type=this.type,
                IsOccupied=this.IsOccupied,
                OnMouseClickEmpty=this.OnMouseClickEmpty,
                OnMouseClickOccupied=this.OnMouseClickOccupied,
                OnMouseHover=this.OnMouseHover,
                mouseOver=this.mouseOver,
                manager=this.manager

            };
        }

        internal void DrawPic(SpriteBatch spriteBatch)
        {
            if (IsOccupied)
                if (_Gun != null)
                    _Gun.DrawInvPic(position, spriteBatch,this);
                else
                    Item.DrawInvPic(position,spriteBatch,this);
        }
        public void StoreItem(PBItem item)
        {
            Item = item;
            Item.LoadTexture(manager.content);
            type = Type.Item;
            IsOccupied = true;
        }
        public PBItem GetItem()
        {
            IsOccupied = false;
            PBItem t = Item;
            Item = null;
            type = Type.None;
            return t;
        }

        internal void RemoveItem()
        {
            Item = null;
            IsOccupied = false;
            type = Type.None;
        }

    }
}
