﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ProjectBuild.PBEngine;

namespace ProjectBuild.GUI
{
    class PBButton
    {
        public delegate void FiredEvent(object sender);

        public FiredEvent OnMouseOver;
        public FiredEvent OnMouseClick;
        public FiredEvent OnMouseOut;

        bool mouseOver = false;
        MouseState prevMouse;
        public Vector2 Center
        {
            get
            {
                return position +
                  new Vector2(texture[0].Width / 2, texture[0].Height / 2);
            }
        }
        Texture2D[] texture = new Texture2D[2];
       public Vector2 position;
        int index;
        Rectangle rect
        {
            get
            {
                return new Rectangle((int)position.X,
                    (int)position.Y,
                    texture[0].Width,
                    texture[0].Height);
            }
        }
        public PBButton(Vector2 position,Texture2D texture1,Texture2D texture2)
        {
            index = 0;
            this.position = position;
            this.texture[0] = texture1;
            this.texture[1] = texture2;
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture[index], rect, Color.White);
        }
        public void Update(InputState input)
        {
            PlayerIndex playerIndex;
            Vector2 mouseCoord = new Vector2(PBGlobal.mouseIconPosition.X, PBGlobal.mouseIconPosition.Y);
            Rectangle mouseRec = new Rectangle((int)mouseCoord.X, (int)mouseCoord.Y, 1, 1);
            if (rect.Intersects(mouseRec))
            {
                mouseOver = true;
                if (OnMouseOver != null)
                    OnMouseOver(this);
            }
            else
                if (mouseOver)
                {
                    mouseOver = false;
                    if (OnMouseOut != null)
                        OnMouseOut(this);
                }
            if (mouseOver && (Mouse.GetState().LeftButton == ButtonState.Pressed||
                input.IsButtonPress(Buttons.A,PlayerIndex.One,out playerIndex)))
                index = 1;
            else
                index = 0;
            if (mouseOver &&(( Mouse.GetState().LeftButton == ButtonState.Released && prevMouse.LeftButton == ButtonState.Pressed)||
                input.IsNewButtonPress(Buttons.A,PlayerIndex.One,out playerIndex)))
            {
                
                if (OnMouseClick != null)
                    OnMouseClick(this);
            }

            prevMouse = Mouse.GetState();
        }



    }
}
