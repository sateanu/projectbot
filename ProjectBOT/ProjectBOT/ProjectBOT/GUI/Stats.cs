﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectBuild.GUI
{
    class Stats
    {
        public delegate void FiredEvent(object sender);
        public FiredEvent ClickPlus;
        public FiredEvent ClickMinus;
        public PBButton buttonPlus;
       public PBButton buttonMinus;
        public string Text;
        SpriteFont font;
       public Vector2 textPos;
        public Stats(Vector2 position,ContentManager content)
        {
            buttonMinus = new PBButton(position,
                content.Load<Texture2D>("GUI/Minus1"),
                content.Load<Texture2D>("GUI/Minus2"));
            buttonPlus = new PBButton(position + new Vector2(32, 0),
                content.Load<Texture2D>("GUI/Plus1"),
                content.Load<Texture2D>("GUI/Plus2"));
            font = content.Load<SpriteFont>("gamefont");
            textPos = new Vector2(position.X + 64, position.Y);
            buttonPlus.OnMouseClick += new PBButton.FiredEvent(PlusClick);
            buttonMinus.OnMouseClick += new PBButton.FiredEvent(MinusClick);
        }
        void PlusClick(object sender)
        {
            if (ClickPlus != null)
                ClickPlus(this);
        }

        void MinusClick(object sender)
        {
            if (ClickMinus != null)
                ClickMinus(this);
        }
        
        public void Update(InputState input)
        {
            buttonMinus.Update(input);
            buttonPlus.Update(input);
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            buttonPlus.Draw(spriteBatch);
            buttonMinus.Draw(spriteBatch);
            spriteBatch.DrawString(font, Text, textPos, Color.White,0f,Vector2.Zero,0.8f,SpriteEffects.None,0f);
        }
    }
}
