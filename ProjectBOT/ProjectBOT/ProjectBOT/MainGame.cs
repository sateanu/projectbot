using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using ProjectBuild.PBEngine;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectBuild
{
    public class MainGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ScreenManager screenManager;
        Texture2D mouseTex;
        public static Texture2D PlaceHolder;
        public static List<string> textureNames=new List<string>();
        public static List<string> backgroundNames=new List<string>();
        public static List<string> picNames=new List<string>();
        public static PBSoundSystem SoundSystem;
        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;
            graphics.SynchronizeWithVerticalRetrace = true;
            screenManager = new ScreenManager(this,graphics);
            Components.Add(screenManager);
            //this.IsMouseVisible = true;
            Window.AllowUserResizing = true;
            Window.Title = "Project Build";
            screenManager.AddScreen(new BackgroundScreen("darktile",BackgroundScreen.BackgroundType.Tile,Vector2.Zero),null);
            screenManager.AddScreen(new BackgroundScreen("nameBackground",BackgroundScreen.BackgroundType.Simple,new Vector2(30,30)),null);
            screenManager.AddScreen(new MainMenuScreen(), null);
        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        public List<Texture2D> SplitTexture(Texture2D texture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            int W = texture.Width;
            int H = texture.Height;
            Color[] pixels = new Color[W * H];
            Color[] orPixel = new Color[W * H];
            texture.GetData<Color>(orPixel);
            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = Color.Transparent;
            }
            float[,] UV = new float[,] //POS X, POS Y, %X, %Y, 1f - keep/0f - delete
            { 
                { 0f,0f,1f,0.5f,1f },
                { 0f,0.5f,1f,0.5f,1f },
                { 0f,0f,0.5f,1f,1f},
                { 0.5f,0f,0.5f,1f,1f},
                { 0.5f, 0.5f, 0.5f,0.5f,1f },
                { 0.5f, 0.5f, 0.5f,0.5f,0f },
                { 0f, 0f, 0.5f,0.5f,1f },
                { 0f, 0f, 0.5f,0.5f,0f },
                { 0.5f,0f,0.5f,0.5f,1f},
                { 0.5f,0f,0.5f,0.5f,0f},
                { 0f,0.5f,0.5f,0.5f,1f},
                { 0f,0.5f,0.5f,0.5f,0f},
            };
            int Linii = UV.GetLength(0);
            int Coloane = UV.GetLength(1);

            for (int i = 0; i < Linii; i++)
            {
                Texture2D temp = new Texture2D(GraphicsDevice, W, H);

                if (UV[i, 4] == 1f)
                {
                    temp.SetData<Color>(orPixel);
                    temp.SetData<Color>(0,
                        new Rectangle((int)(W * UV[i, 0]), (int)(H * UV[i, 1]), (int)(W * UV[i, 2]), (int)(H * UV[i, 3])),
                        pixels, 1,
                        (int)(W * UV[i, 2] * H * UV[i, 3]));
                }
                else
                {
                    float x, y, procX, procY;
                    x = UV[i, 0];
                    y = UV[i, 1];
                    procX = UV[i, 2] + x;
                    procY = UV[i, 3] + y;
                    Color[] t = new Color[texture.Width * texture.Height];
                    for (int py = (int)(y * texture.Height); py < (int)(texture.Height * procY); py++)
                        for (int px = (int)(x * texture.Width); px < (int)(texture.Width * procX); px++)
                        {
                            int index = px + py * texture.Width;
                            t[index] = orPixel[index];
                        }
                    temp.SetData<Color>(t);
                }
                textures.Add(temp);
            }

            return textures;
        }
        Thread loadThread;
        PBAnimation animation;
        SpriteFont font;
        protected override void LoadContent()
        {
            SoundSystem = new PBSoundSystem(Content);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            PlaceHolder = Content.Load<Texture2D>("blank");
            mouseTex = Content.Load<Texture2D>("mouse");
            textureNames = new List<string>();
            font = Content.Load<SpriteFont>("gamefont");
            loadThread = new Thread(Load);
            loadThread.Start();
            animation = new PBAnimation(Content.Load<Texture2D>("loading"),8, 1,0.06f, true, 0);
            animation.Activate();
        }
        void Load()
        {
            DirectoryInfo dir = new DirectoryInfo(System.Windows.Forms.Application.StartupPath + "//Content//Tiles");
            foreach (FileInfo f in dir.GetFiles("*.xnb"))
            {
                string a = f.Name.Replace(".xnb", "");
                a = a.Replace("_n", "");
                a = a.Replace(" - Copy", "");
                if (!textureNames.Contains(a))
                    textureNames.Add(a);
            }
            textureNames.Sort();

            dir = new DirectoryInfo(System.Windows.Forms.Application.StartupPath + "//Content//Etc//Pics");
            foreach (FileInfo f in dir.GetFiles("*.xnb"))
            {
                string a = f.Name.Replace(".xnb", "");
                if (!picNames.Contains(a))
                    picNames.Add(a);
            }
            dir = new DirectoryInfo(System.Windows.Forms.Application.StartupPath + "//Content//Etc//Background");
            foreach (FileInfo f in dir.GetFiles("*.xnb"))
            {
                string a = f.Name.Replace(".xnb", "");
                if (!backgroundNames.Contains(a))
                    backgroundNames.Add(a);
            }
            TEXTURES = new List<Texture2D>();
            IMAGES = new List<System.Drawing.Image>();
            List<string> ts = new List<string>();
            for (int i = 0; i < textureNames.Count; i++)
            {
                string s = textureNames[i];
                ts.Add(s);
                Texture2D temp = Content.Load<Texture2D>("Tiles\\" + s);
                TEXTURES.Add(temp);
                IMAGES.Add(ProjectBuild.PBEditor.EditMethods.Texture2Image(temp));
                List<Texture2D> splits = SplitTexture(temp);
                foreach (Texture2D t in splits)
                    IMAGES.Add(ProjectBuild.PBEditor.EditMethods.Texture2Image(t));
                TEXTURES.AddRange(splits);
                for (int j = 0; j < str.Length; j++)
                    ts.Add(s + str[j]);
            }
            textureNames = ts;

        }
        string[] str =
        {
            "_jos","_sus","_dreapta","_stanga",
            "_colt_stanga_sus","_dreapta_jos",
            "_colt_dreapta_jos","_stanga_sus",
            "_colt_stanga_jos","_dreapta_sus",
            "_colt_dreapta_sus","_stanga_jos"
        };
        private float time=0f;
        private MouseState prev;
        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (prev.X != Mouse.GetState().X || prev.Y != Mouse.GetState().Y)
            {
                PBGlobal.mouseIconPosition.X = prev.X;
                PBGlobal.mouseIconPosition.Y = prev.Y;
            }
            if (loadThread.IsAlive)
            {
                animation.Update(gameTime);
                if (time < 100f)
                    time += 0.1f;
            }
            else
            {
                prev = Mouse.GetState();
                PBGlobal.mouseIconPosition = Vector2.Clamp(PBGlobal.mouseIconPosition,
                    new Vector2(GraphicsDevice.Viewport.X, GraphicsDevice.Viewport.Y),
                    new Vector2(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height));
                SoundSystem.Update();
                base.Update(gameTime);
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);


            if (!loadThread.IsAlive)
            {
                base.Draw(gameTime);
                spriteBatch.Begin();
                spriteBatch.Draw(mouseTex, new Vector2(PBGlobal.mouseIconPosition.X - mouseTex.Width / 2, PBGlobal.mouseIconPosition.Y - mouseTex.Height / 2), Color.White);
                spriteBatch.End();
            }
            else
            {
                spriteBatch.Begin();
                animation.Draw(spriteBatch, new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height/2));
                spriteBatch.DrawString(font, "LOADING..."+((int)time).ToString()+"%", new Vector2(GraphicsDevice.Viewport.Width / 2 - 54, GraphicsDevice.Viewport.Height / 2 + 54), Color.White);
                spriteBatch.End();
            }

        }
        public static List<Texture2D> TEXTURES { get; private set; }
        public static List<System.Drawing.Image> IMAGES { get; private set; }
    }
}
