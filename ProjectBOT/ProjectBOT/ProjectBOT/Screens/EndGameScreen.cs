﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine;
using Microsoft.Xna.Framework;

namespace ProjectBuild.Screens
{
    class EndGameScreen : MenuScreen
    {
        PBCampaign cpg = null;
       public EndGameScreen(int money,string lvl,PBCampaign campaign=null)
           : base("Campaign Finished"+"\n"+"You gained $"+money,true)
       {
           MainGame.SoundSystem.StopSong();
           MenuEntry menuGO = new MenuEntry("Return to Menu");
           MenuEntry retry = new MenuEntry("Retry the level!");
           menuGO.Selected += menugo;
           retry.Selected += retryLvl;
           MenuEntries.Add(retry);
           MenuEntries.Add(menuGO);
           this.lvl = lvl;
           cpg = campaign;
       }
       string lvl;
       void retryLvl(object sender, PlayerIndexEventArgs e)
       {
           if (cpg == null)
               LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new ProjectBuild.PBEngine.PBLevel(lvl));
           else
               cpg.Restart();
       }
       void menugo(object sender, PlayerIndexEventArgs e)
       {
           LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen("darktile",BackgroundScreen.BackgroundType.Tile,Vector2.Zero),
               new BackgroundScreen("nameBackground",BackgroundScreen.BackgroundType.Simple,new Vector2(30,30)),
               new MainMenuScreen());
       }

    }
}
