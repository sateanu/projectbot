﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ProjectBuild.PBEngine;

namespace ProjectBuild.Screens
{
    class LocalCampaignScreen : MenuScreen
    {
        public LocalCampaignScreen() :
            base("Select campain", false, true)
        {
            string path = Application.StartupPath;
            DirectoryInfo dir = new DirectoryInfo(path);
            if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
            {
                dir.CreateSubdirectory("levels");
            }
            dir = new DirectoryInfo(path + "\\levels\\");
            foreach (var d in dir.GetDirectories())
            {
                bool good=false;
                FileInfo[] info = d.GetFiles("*.pbcmp");
                if (info.Count() > 0)
                {
                    FileInfo[] levels = d.GetFiles("*.pblvl");
                    try
                    {
                        using (StreamReader reader = info[0].OpenText())
                        {
                            string line = reader.ReadLine();
                            int count = int.Parse(reader.ReadLine());
                            for (int i = 0; i < count; i++)
                            {
                                string l = reader.ReadLine();
                                foreach (var txt in levels)
                                {
                                    if (txt.Name != l)
                                    {
                                        good = false;
                                    }
                                    else
                                    {
                                        good = true;
                                        break;
                                    }
                                }
                                if (good == false)
                                    break;
                                
                            }
                        }
                    
                    if (good)
                    {
                        MenuEntry a = new MenuEntry(d.Name);
                        a.Selected += new EventHandler<PlayerIndexEventArgs>(a_Selected);
                        MenuEntries.Add(a);
                    }
                    }
                    catch(Exception e){}
                }
                
            }
        }
        void a_Selected(object sender, PlayerIndexEventArgs e)
        {
                MenuEntry entry = (MenuEntry)sender;
                PBCampaign campaign = new PBCampaign(ScreenManager, Application.StartupPath + "\\levels\\" + entry.Text);
                MainGame.SoundSystem.StopSong();
                campaign.EnterLevel(null, 0);
        }
    }
}
