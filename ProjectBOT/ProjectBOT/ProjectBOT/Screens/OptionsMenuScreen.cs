#region Using Statements
using Microsoft.Xna.Framework;
using System.Windows.Forms;
using ProjectBuild.PBEngine;
using ProjectBuild.Screens;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;
#endregion

namespace ProjectBuild
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways.
    /// </summary>
    class OptionsMenuScreen : MenuScreen
    {
        #region Fields
        MenuEntry fullScreen;
        #endregion
        
        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public OptionsMenuScreen()
            : base("Options")
        {
            // Create our menu entries.

            MenuInputDelay = 0.3f;
            MenuEntry back = new MenuEntry("Back");
            fullScreen = new MenuEntry("Toggle FullScreen");
            MenuEntry controllerHelp = new MenuEntry("Show controller map");
            MenuEntry volume = new MenuEntry("FX Volume: " + PBGlobal.volume.ToString());
            MenuEntry volumesong = new MenuEntry("Music Volume: " + PBGlobal.songVolume.ToString());
            MenuEntry sensivity = new MenuEntry("Aim Sensivity: " + PBGlobal.aimSensivity.ToString());
            // Hook up menu event handlers.
            fullScreen.Selected += (fullScreen_Selected);
            back.Selected += OnCancel;
            volume.SelectedOnLeft += new System.EventHandler<PlayerIndexEventArgs>(volume_SelectedOnLeft);
            volume.SelectedOnRight += new System.EventHandler<PlayerIndexEventArgs>(volume_SelectedOnRight);
            volumesong.SelectedOnLeft += new System.EventHandler<PlayerIndexEventArgs>(volumesong_SelectedOnLeft);
            volumesong.SelectedOnRight += new System.EventHandler<PlayerIndexEventArgs>(volumesong_SelectedOnRight);
            sensivity.SelectedOnLeft += new System.EventHandler<PlayerIndexEventArgs>(sensivity_SelectedOnLeft);
            sensivity.SelectedOnRight += new System.EventHandler<PlayerIndexEventArgs>(sensivity_SelectedOnRight);
            controllerHelp.Selected += new System.EventHandler<PlayerIndexEventArgs>(controllerHelp_Selected);
            // Add entries to the menu.
            MenuEntries.Add(back);
            MenuEntries.Add(fullScreen);
            MenuEntries.Add(controllerHelp);
            MenuEntries.Add(volume);
            MenuEntries.Add(volumesong);
            MenuEntries.Add(sensivity);
        }

        void controllerHelp_Selected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new ImageScreen("Etc//Background//controllerHelp",
                ImageScreen.BackgroundType.Simple),e.PlayerIndex);
        }

        void sensivity_SelectedOnRight(object sender, PlayerIndexEventArgs e)
        {
            if (PBGlobal.aimSensivity < 20)
                PBGlobal.aimSensivity++;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift))
                PBGlobal.aimSensivity += 9;
            PBGlobal.aimSensivity =(int)MathHelper.Clamp(PBGlobal.aimSensivity, 0f, 20f);
            MenuEntry entry = (MenuEntry)sender;
            entry.Text = "Aim Sensivity: " + PBGlobal.aimSensivity.ToString();
        }

        void sensivity_SelectedOnLeft(object sender, PlayerIndexEventArgs e)
        {
            if (PBGlobal.aimSensivity > 0)
                PBGlobal.aimSensivity--;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift))
                PBGlobal.aimSensivity -= 9;
            PBGlobal.aimSensivity = (int)MathHelper.Clamp(PBGlobal.aimSensivity, 0f, 20f);
            MenuEntry entry = (MenuEntry)sender;
            entry.Text = "Aim Sensivity: " + PBGlobal.aimSensivity.ToString();
        }

        void volumesong_SelectedOnRight(object sender, PlayerIndexEventArgs e)
        {
            if (PBGlobal.songVolume < 100)
                PBGlobal.songVolume++;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift))
                PBGlobal.songVolume += 9;
            PBGlobal.songVolume = (int)MathHelper.Clamp(PBGlobal.songVolume, 0f, 100f);
            MenuEntry entry = (MenuEntry)sender;
            entry.Text = "Music Volume: " + PBGlobal.songVolume.ToString();
            MediaPlayer.Volume = PBGlobal.songVolume/100f;
        }

        void volumesong_SelectedOnLeft(object sender, PlayerIndexEventArgs e)
        {
            if (PBGlobal.songVolume > 0)
                PBGlobal.songVolume--;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift))
                PBGlobal.songVolume -= 9;
            PBGlobal.songVolume = (int)MathHelper.Clamp(PBGlobal.songVolume, 0f, 100f);
            MenuEntry entry = (MenuEntry)sender;
            entry.Text = "Music Volume: " + PBGlobal.songVolume.ToString();
            MediaPlayer.Volume = PBGlobal.songVolume/100f;
        }

        void volume_SelectedOnRight(object sender, PlayerIndexEventArgs e)
        {
            if (PBGlobal.volume < 100)
                PBGlobal.volume++;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift))
                PBGlobal.volume += 9;
            PBGlobal.volume = (int)MathHelper.Clamp(PBGlobal.volume, 0f, 100f);
            MenuEntry entry = (MenuEntry)sender;
            entry.Text = "FX Volume: " + PBGlobal.volume.ToString();
        }

        void volume_SelectedOnLeft(object sender, PlayerIndexEventArgs e)
        {
            if (PBGlobal.volume > 0)
                PBGlobal.volume--;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift))
                PBGlobal.volume -= 9;
            PBGlobal.volume = (int)MathHelper.Clamp(PBGlobal.volume, 0f, 100f);
            MenuEntry entry = (MenuEntry)sender;
            entry.Text = "FX Volume: " + PBGlobal.volume.ToString();
        }

        void fullScreen_Selected(object sender, PlayerIndexEventArgs e)
        {

            if (!ScreenManager.gr.IsFullScreen)
            {
//                if (ScreenManager.loadLast().X == ScreenManager.gr.PreferredBackBufferWidth && ScreenManager.loadLast().Y == ScreenManager.gr.PreferredBackBufferHeight)
  //                  ScreenManager.saveLast(ScreenManager.gr.PreferredBackBufferWidth, ScreenManager.gr.PreferredBackBufferHeight);
    //            else
                //ScreenManager.saveLast(ScreenManager.gr.PreferredBackBufferWidth, ScreenManager.gr.PreferredBackBufferHeight);
                ScreenManager.gr.IsFullScreen = true;
                ScreenManager.gr.PreferredBackBufferWidth = ScreenManager.gr.GraphicsDevice.Adapter.CurrentDisplayMode.Width;
                ScreenManager.gr.PreferredBackBufferHeight = ScreenManager.gr.GraphicsDevice.Adapter.CurrentDisplayMode.Height;
                ScreenManager.gr.ApplyChanges();
            }
            else
            {
                ScreenManager.gr.IsFullScreen = false;
                ScreenManager.gr.PreferredBackBufferWidth = (int)ScreenManager.loadLast().X;
                ScreenManager.gr.PreferredBackBufferHeight = (int)ScreenManager.loadLast().Y;
                ScreenManager.gr.ApplyChanges();
               //ScreenManager.saveLast(ScreenManager.gr.GraphicsDevice.Adapter.CurrentDisplayMode.Width, ScreenManager.gr.GraphicsDevice.Adapter.CurrentDisplayMode.Height);

            }
        }
        

        /// <summary>
        /// Fills in the latest values for the options screen menu text.
        /// </summary>


        #endregion

        #region Handle Input


        #endregion
    }
}
