#region Using Statements
using Microsoft.Xna.Framework;
using System.Windows.Forms;
using ProjectBuild.PBEngine;
#endregion

namespace ProjectBuild
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    class PauseMenuScreen : MenuScreen
    {
        #region Initialization
        PBCampaign cpg = null;
        string levelname;
        /// <summary>
        /// Constructor.
        /// </summary>
        public PauseMenuScreen(string lvl,PBCampaign campaign=null)
            : base("Paused")
        {
            MainGame.SoundSystem.PauseSong();
            cpg = campaign;
            levelname=lvl;
            // Create our menu entries.
            MenuEntry resumeGameMenuEntry = new MenuEntry("Resume Game");
            MenuEntry optionEntry = new MenuEntry("Options");
            MenuEntry goEditor = new MenuEntry("Edit this map");
            MenuEntry retryEntry = new MenuEntry("Retry");
            MenuEntry quitGameMenuEntry = new MenuEntry("Quit Game");

            // Hook up menu event handlers.
            resumeGameMenuEntry.Selected += OnCancel;
            quitGameMenuEntry.Selected += QuitGameMenuEntrySelected;
            optionEntry.Selected += new System.EventHandler<PlayerIndexEventArgs>(optionEntry_Selected);
            goEditor.Selected += onEditor;
            retryEntry.Selected += new System.EventHandler<PlayerIndexEventArgs>(retryEntry_Selected);
            // Add entries to the menu.
            MenuEntries.Add(resumeGameMenuEntry);
            MenuEntries.Add(optionEntry);
            MenuEntries.Add(goEditor);
            MenuEntries.Add(retryEntry);
            MenuEntries.Add(quitGameMenuEntry);
        }

        void retryEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            if (cpg == null)
                LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new ProjectBuild.PBEngine.PBLevel(levelname));
            else
                cpg.Restart();
        }

        void optionEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new OptionsMenuScreen(), ControllingPlayer);
        }



        #endregion

        #region Handle Input
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            base.OnCancel(playerIndex);
            MainGame.SoundSystem.ResumeSong();
        }

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        void QuitGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            const string message = "Are you sure you want to quit this game?";

            MessageBoxScreen confirmQuitMessageBox = new MessageBoxScreen(message);

            confirmQuitMessageBox.Accepted += ConfirmQuitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmQuitMessageBox, ControllingPlayer);
        }
        void onEditor(object sender, PlayerIndexEventArgs e)
        {
            ProjectBOTEditor.EditorForm a = new ProjectBOTEditor.EditorForm(ScreenManager,levelname);
            if (ScreenManager.gr.IsFullScreen)
                ScreenManager.gr.ToggleFullScreen();
            Application.EnableVisualStyles();
            LoadingScreen.Load(ScreenManager, false, PlayerIndex.One, new ProjectBuild.Screens.EditorScreen());
            a.ShowDialog();
        }

        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to quit" message box. This uses the loading screen to
        /// transition from the game back to the main menu screen.
        /// </summary>
        void ConfirmQuitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen("darktile",BackgroundScreen.BackgroundType.Tile,Vector2.Zero),
               new BackgroundScreen("nameBackground",BackgroundScreen.BackgroundType.Simple,new Vector2(30,30)),
                                                           new MainMenuScreen());
            MainGame.SoundSystem.StopSong();
        }


        #endregion
    }
}
