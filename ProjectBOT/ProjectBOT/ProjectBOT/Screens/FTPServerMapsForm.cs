﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using ProjectBuild.PBEngine;

namespace ProjectBuild.Screens
{
    public partial class FTPServerMapsForm : Form
    {
        ScreenManager SM;
        public FTPServerMapsForm(ScreenManager screenManager)
        {
            InitializeComponent();
            SM = screenManager;
        }

        private void FTPServerMapsForm_Load(object sender, EventArgs e)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://project-build.site90.com//ProjectBuildMaps//");
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential("a3205051", "moondance1ftp");
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                line = reader.ReadLine();
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                        listBox1.Items.Add(line);
                }
                while (line != null);
            }
            catch (WebException we)
            {
                MessageBox.Show(we.Message, "Error");
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Error");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                MessageBox.Show("Select a map!", "Error");
            else
            {
                string path = Application.StartupPath;
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
                {
                    dir.CreateSubdirectory("levels");
                }
                PBData.Download(listBox1.SelectedItem as string,path + "\\levels\\");
                Close();
                LoadingScreen.Load(SM, true, Microsoft.Xna.Framework.PlayerIndex.One, new PBLevel(path + "\\levels\\" + listBox1.SelectedItem as string));
            }
        }
    }
}
