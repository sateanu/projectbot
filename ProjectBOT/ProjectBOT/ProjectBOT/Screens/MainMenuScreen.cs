#region Using Statements
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ProjectBuild.PBEngine;
using ProjectBuild.Screens;
using System.Threading;
#endregion

namespace ProjectBuild
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class MainMenuScreen : MenuScreen
    {
        #region Initialization


        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public MainMenuScreen()
            : base("Main Menu")
        {
            // Create our menu entries.
            MenuEntry playGameMenuEntry = new MenuEntry("Play test campaign");
            MenuEntry playGameMenuEntry2 = new MenuEntry("Local Maps");
            MenuEntry playGameMenuEntry3 = new MenuEntry("Online Maps");
            MenuEntry playGameMenuEntry4 = new MenuEntry("Play Demo Level");
            MenuEntry onlineCampaign = new MenuEntry("Online Campaign");
            MenuEntry playCampain = new MenuEntry("Local Campaigns");
            MenuEntry optionsMenuEntry = new MenuEntry("Options");
            MenuEntry editorMenuEntry = new MenuEntry("Editor");
            MenuEntry exitMenuEntry = new MenuEntry("Exit");
            // Hook up menu event handlers.
            playGameMenuEntry.Selected += PlayGameMenuEntrySelected;
            playGameMenuEntry2.Selected += playGameMenuEntry2_Selected;
            playGameMenuEntry3.Selected += playGameMenuEntry3Selected;
            playGameMenuEntry4.Selected += playGameMenuEntry4Selected;
            optionsMenuEntry.Selected += OptionsMenuEntrySelected;
            exitMenuEntry.Selected += OnCancel;
            editorMenuEntry.Selected += OnEditor;
            playCampain.Selected += onCampaign;
            onlineCampaign.Selected += onlineCampaignSelected;
            // Add entries to the menu.
            //MenuEntries.Add(playGameMenuEntry);
            //MenuEntries.Add(playGameMenuEntry4);
            MenuEntries.Add(playGameMenuEntry2);
            MenuEntries.Add(playGameMenuEntry3);
            MenuEntries.Add(playCampain);
            MenuEntries.Add(onlineCampaign);
            MenuEntries.Add(optionsMenuEntry);
            MenuEntries.Add(editorMenuEntry);
            MenuEntries.Add(exitMenuEntry);
        }

        void onlineCampaignSelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new FtpCampaignScreen(), e.PlayerIndex);
        }

        void onCampaign(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new LocalCampaignScreen(), e.PlayerIndex);
        }

        [STAThread]
        void OnEditor(object sender, PlayerIndexEventArgs e)
        {
            ProjectBOTEditor.EditorForm a = new ProjectBOTEditor.EditorForm(ScreenManager);
           // try
           // {
                if(ScreenManager.gr.IsFullScreen)
                ScreenManager.gr.ToggleFullScreen();
                Application.EnableVisualStyles();
                LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new ProjectBuild.Screens.EditorScreen());
                //Thread.Sleep(500);
                a.ShowDialog();
          //  }
           /* catch (Exception error)
            {
                MessageBox.Show("Error loading the editor: " + error.ToString());
                a.Close();
            }*/
        }
        void playGameMenuEntry4Selected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new PBLevel("Content\\PreLevels\\DemoLevel.pblvl"));
        }
        void playGameMenuEntry2_Selected(object sender, PlayerIndexEventArgs e)
        {
            //new LocalMapsForm(ScreenManager).ShowDialog();
            ScreenManager.AddScreen(new LocalMapsScreen(), e.PlayerIndex);
            //LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new LocalMapsScreen());
        }
        void playGameMenuEntry3Selected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new FTPServereMapsScreen(), e.PlayerIndex);
        }


        #endregion

        #region Handle Input


        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        void PlayGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            try
            {
                PBCampaign campain = new PBCampaign(ScreenManager, "Content\\PreLevels\\CampaignBeta\\");
                campain.EnterLevel(null, 0);
                MainGame.SoundSystem.StopSong();
               // MainGame.SoundSystem.PlaySong("song1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        void OptionsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new OptionsMenuScreen(), e.PlayerIndex);
        }


        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            const string message = "Are you sure you want to exit Project Bot?";

            MessageBoxScreen confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
        }


        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to exit" message box.
        /// </summary>
        void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }


        #endregion
    }
}
