﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ProjectBuild.PBEngine;

namespace ProjectBuild.Screens
{
    public partial class LocalMapsForm : Form
    {
        ScreenManager SM;
        public LocalMapsForm(ScreenManager SM)
        {
            this.SM = SM;
            InitializeComponent();
        }

        private void LocalMapsForm_Load(object sender, EventArgs e)
        {
            string path = Application.StartupPath;
            DirectoryInfo dir = new DirectoryInfo(path);
            if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
            {
                dir.CreateSubdirectory("levels");
            }
            dir = new DirectoryInfo(Application.StartupPath + "//levels//");
            foreach (FileInfo f in dir.GetFiles("*.pblvl"))
            {
                listBox1.Items.Add(f);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                MessageBox.Show("Select a map!", "Error");
            else
            {
                string path = Application.StartupPath;
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
                {
                    dir.CreateSubdirectory("levels");
                }
                Close();
                LoadingScreen.Load(SM, true, Microsoft.Xna.Framework.PlayerIndex.One, new PBLevel(path + "\\levels\\" + listBox1.SelectedItem as string));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
