﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ProjectBuild.PBEngine;
using System.Windows.Forms;

namespace ProjectBuild.Screens
{
    class LocalMapsScreen : MenuScreen
    {
        public LocalMapsScreen()
            : base("Choose a map!",false,true)
        {
            MenuInputDelay = 0.25f;
            string path = Application.StartupPath;
            DirectoryInfo dir = new DirectoryInfo(path);
            if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
            {
                dir.CreateSubdirectory("levels");
            }
            dir = new DirectoryInfo(Application.StartupPath + "//levels//");
            foreach (FileInfo f in dir.GetFiles("*.pblvl"))
            {
                string lvlName = f.Name.Remove(f.Name.LastIndexOf('.'));
                MenuEntry a = new MenuEntry(lvlName);
                a.Selected += new EventHandler<PlayerIndexEventArgs>(a_Selected);
                MenuEntries.Add(a);
            }
        }

        void a_Selected(object sender, PlayerIndexEventArgs e)
        {
            MenuEntry mEntry = (MenuEntry)sender;
            MainGame.SoundSystem.StopSong();
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new PBLevel(Application.StartupPath + "//levels//" + mEntry.Text + ".pblvl"));
        }
    }
}
