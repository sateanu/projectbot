﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Windows.Forms;
using ProjectBuild.PBEngine;

namespace ProjectBuild.Screens
{
    class FtpCampaignScreen:MenuScreen
    {
        public FtpCampaignScreen()
            : base("Online Campaigns",false,true)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://project-build.site90.com//ProjectBuildMaps//");
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential("a3205051", "moondance1ftp");
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                line = reader.ReadLine();
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                        if (!line.Contains(".pblvl"))
                        {
                            MenuEntry a = new MenuEntry(line);
                            a.Selected += new EventHandler<PlayerIndexEventArgs>(a_Selected);
                            MenuEntries.Add(a);
                        }
                }
                while (line != null);
            }
            catch (WebException we)
            {
                MessageBox.Show(we.Message, "Error");
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Error");
            }
        }
        void a_Selected(object sender, PlayerIndexEventArgs e)
        {
            MenuEntry entry = (MenuEntry)sender;
            string campaignName = entry.Text;
            string path = Application.StartupPath;
            DirectoryInfo dir = new DirectoryInfo(path);
            if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
            {
                dir.CreateSubdirectory("levels");
            }
            dir = new DirectoryInfo(path + "\\levels\\");
            dir.CreateSubdirectory(campaignName);
            dir = new DirectoryInfo(path + "\\levels\\" + campaignName);
            try
            {
                ProjectBuild.PBEngine.PBData.Download(campaignName + ".pbcmp", path + "\\levels\\"+campaignName+"\\", campaignName);

                using (StreamReader reader = new StreamReader(path + "\\levels\\" + campaignName + "\\" + campaignName+".pbcmp"))
                {
                    string line = reader.ReadLine();
                    int nr = int.Parse(reader.ReadLine());
                    for(int i=0;i<nr;i++)
                    {
                        line = reader.ReadLine();
                        ProjectBuild.PBEngine.PBData.Download(line,path+"\\levels\\"+campaignName+"\\",campaignName);
                    }  
                }

                PBCampaign campaign = new PBCampaign(ScreenManager, path + "\\levels\\" + campaignName);
                MainGame.SoundSystem.StopSong();
                campaign.EnterLevel(null, 0);
            }
            catch (WebException wex)
            {
                MessageBox.Show(wex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
