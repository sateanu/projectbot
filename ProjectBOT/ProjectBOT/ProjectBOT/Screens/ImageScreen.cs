﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectBuild.Screens
{
    class ImageScreen : MenuScreen
    {
        public enum BackgroundType
        {
            Simple,
            Tile,
            Full
        }
        ContentManager content;
        Texture2D backgroundTexture;
        BackgroundType type;
        Vector2 position;
        private string imageName;
        public ImageScreen(string imgName, BackgroundType type)
            : base("")
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            this.type = type;
            this.imageName = imgName;
        }
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            backgroundTexture = content.Load<Texture2D>(imageName);
        }
        public override void UnloadContent()
        {
            content.Unload();
        }
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
        }
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);
            spriteBatch.Begin();

            switch (type)
            {
                case BackgroundType.Full:
                    spriteBatch.Draw(backgroundTexture, fullscreen,
                                     new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));
                    break;
                case BackgroundType.Tile:
                    int x = viewport.Width / backgroundTexture.Width + 1;
                    int y = viewport.Height / backgroundTexture.Height + 1;
                    for (int i = 0; i < x; i++)
                        for (int j = 0; j < y; j++)
                        {
                            spriteBatch.Draw(backgroundTexture, new Vector2(i * backgroundTexture.Width, j * backgroundTexture.Height),
                                new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));
                        }
                    break;
                case BackgroundType.Simple:
                    position = new Vector2(ScreenManager.GraphicsDevice.Viewport.Width / 2 - backgroundTexture.Width / 2,
                        ScreenManager.GraphicsDevice.Viewport.Height / 2 - backgroundTexture.Height / 2);
                    spriteBatch.Draw(backgroundTexture, position,
                        new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));
                    break;
            }
            spriteBatch.End();
        }
    }
}
