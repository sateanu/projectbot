﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine;
using Microsoft.Xna.Framework;

namespace ProjectBuild.Screens
{
    class DeathMenuScreen : MenuScreen
    {
        string lvl;
        PBCampaign cpg=null;
        public DeathMenuScreen(string level,PBCampaign campaign=null) 
            : base("Death",true)
        {
            MainGame.SoundSystem.StopSong();
            MenuEntry menuGO = new MenuEntry("Main Menu");
            MenuEntry restart = new MenuEntry("Restart Game");
            MenuEntry quit = new MenuEntry("Exit Game");
            lvl = level;
            cpg = campaign;
            menuGO.Selected += new EventHandler<PlayerIndexEventArgs>(menuGO_Selected);
            restart.Selected += new EventHandler<PlayerIndexEventArgs>(restart_Selected);
            quit.Selected += new EventHandler<PlayerIndexEventArgs>(quit_Selected);

            MenuEntries.Add(menuGO);
            MenuEntries.Add(restart);
            MenuEntries.Add(quit);
        }

        void quit_Selected(object sender, PlayerIndexEventArgs e)
        {
            const string message = "Are you sure you want to quit this game?";

            MessageBoxScreen confirmQuitMessageBox = new MessageBoxScreen(message);

            confirmQuitMessageBox.Accepted += ConfirmQuitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmQuitMessageBox, ControllingPlayer);
        }
        void ConfirmQuitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }
        void menuGO_Selected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex, new BackgroundScreen("darktile",BackgroundScreen.BackgroundType.Tile,Vector2.Zero),
               new BackgroundScreen("nameBackground",BackgroundScreen.BackgroundType.Simple,new Vector2(30,30)),
                                                           new MainMenuScreen());
        }
        void restart_Selected(object sender, PlayerIndexEventArgs e)
        {
            if (cpg == null)
                LoadingScreen.Load(ScreenManager, false, e.PlayerIndex, new PBLevel(lvl));
            else
                cpg.Restart();
        }
    }
}
