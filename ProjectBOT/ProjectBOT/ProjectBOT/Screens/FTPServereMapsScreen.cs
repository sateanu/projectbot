﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace ProjectBuild.Screens
{
    class FTPServereMapsScreen : MenuScreen
    {
        public FTPServereMapsScreen()
            : base("Choose a map!", false, true)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://project-build.site90.com//ProjectBuildMaps//");
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential("a3205051", "moondance1ftp");
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                line = reader.ReadLine();
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                        if (line.Contains(".pblvl"))
                        {
                            MenuEntry a = new MenuEntry(line.Remove(line.LastIndexOf('.')));
                            a.Selected += new EventHandler<PlayerIndexEventArgs>(a_Selected);
                            MenuEntries.Add(a);
                        }
                        
                }
                while (line != null);
            }
            catch (WebException we)
            {
                MessageBox.Show(we.Message, "Error");
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Error");
            }
        }

        void a_Selected(object sender, PlayerIndexEventArgs e)
        {
            MenuEntry entry = (MenuEntry)sender;
            string path = Application.StartupPath;
            DirectoryInfo dir = new DirectoryInfo(path);
            if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
            {
                dir.CreateSubdirectory("levels");
            }
            ProjectBuild.PBEngine.PBData.Download(entry.Text+".pblvl", path + "\\levels\\");
            MainGame.SoundSystem.StopSong();
            LoadingScreen.Load(ScreenManager, true, Microsoft.Xna.Framework.PlayerIndex.One,
                new ProjectBuild.PBEngine.PBLevel(path + "\\levels\\" + entry.Text+".pblvl"));
        }
    }
}
