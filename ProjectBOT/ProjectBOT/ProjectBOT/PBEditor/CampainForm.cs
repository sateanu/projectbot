﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ProjectBOTEditor;
using System.IO;
using ProjectBuild.PBEngine;

namespace ProjectBuild.PBEditor
{
    public partial class CampainForm : Form
    {
        EditorForm edForm;
        public CampainForm(EditorForm form)
        {
            InitializeComponent();
            edForm = form;
            listBox1.Items.AddRange(edForm.levelList.Items);
            levels = edForm.levels;
            textBox1.Text = edForm.campainName;
        }
        Dictionary<string, string> levels = new Dictionary<string, string>();
        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Project Build Level|*.pblvl";
            openFileDialog1.InitialDirectory = Application.StartupPath + "\\levels\\";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                levels.Add(openFileDialog1.SafeFileName, openFileDialog1.FileName);
                listBox1.Items.Add(openFileDialog1.SafeFileName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                string s = listBox1.SelectedItem as string;
                levels.Remove(s);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                if (MessageBox.Show("Do you want to save the previous map?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    SaveFileDialog save = new SaveFileDialog();
                    save.Filter = "Project Build Level|*.pblvl";
                    save.FileName = edForm.levelName;
                    if (save.ShowDialog() == DialogResult.OK)
                    {
                        edForm.SaveMap(save.FileName);
                    }
                    edForm.OpenMap(levels[listBox1.SelectedItem as string]);
                }
                else
                {
                    edForm.OpenMap(levels[listBox1.SelectedItem as string]);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveCampaign();
        }
        void SaveCampaign()
        {
            string campaignName = textBox1.Text;
            try
            {
                string path = Application.StartupPath + "\\levels\\" + campaignName;
                Directory.CreateDirectory(path);
                using (StreamWriter writer = File.CreateText(path + "\\" + campaignName + ".pbcmp"))
                {
                    writer.WriteLine(campaignName);
                    writer.WriteLine(listBox1.Items.Count);
                    foreach (string s in listBox1.Items)
                    {
                        writer.WriteLine(s);
                    }
                }
                foreach (string s in listBox1.Items)
                {
                    if (path + "\\" + s == levels[s])
                    {
                    }
                    else
                    {
                        if (File.Exists(path + "\\" + s))
                            File.Delete(path + "\\" + s);
                        File.Copy(levels[s],
                            path + "\\" + s);
                    }
                }
                // FileInfo file = new FileInfo(Application.StartupPath + "\\levels\\" + levels[);
                MessageBox.Show(string.Format("Campaign {0} saved succesfully!", textBox1.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != null)
            {
                SaveCampaign();
                PBData.MakeDirectory(textBox1.Text);
                string path = Application.StartupPath;
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
                {
                    dir.CreateSubdirectory("levels");
                }

                dir = new DirectoryInfo(path + "\\levels\\");
                dir.CreateSubdirectory(textBox1.Text);
                //UPLOAD TO FTP AND DOWNLOAD FROM FTP
                foreach (string s in levels.Keys)
                {
                    string pathlvl = levels[s];
                    PBData.Upload(pathlvl, textBox1.Text);
                }
                PBData.Upload(path+"\\levels\\" + textBox1.Text + "\\" +textBox1.Text + ".pbcmp", textBox1.Text);
                MessageBox.Show(string.Format("Campaign {0} uploaded succesfully!", textBox1.Text));
            }
            else
                MessageBox.Show("Please name your campaign!");
        }
    }
}
