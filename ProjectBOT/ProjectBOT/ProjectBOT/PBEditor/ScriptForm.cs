﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ProjectBOTEditor;
using ProjectBuild.PBEngine;

namespace ProjectBuild.PBEditor
{
    public partial class ScriptForm : Form
    {
        PBEditorObjects PBE;
        public bool OkPressed;
        public bool NeedPosition;
        public PBScriptAction action;
        public bool SpawnPosition;
        public void Reload(PBEditorObjects PBEd, TreeNodeCollection allObjects, ListBox.ObjectCollection lights,ListBox.ObjectCollection levels)
        {
            PBE = PBEd;
            foreach (TreeNode node in allObjects)
            {
                treeView1.Nodes.Add((TreeNode)node.Clone());
            }
            listBox1.Items.AddRange(lights);
            foreach (string s in PBE.mobNames)
            {
                listBox3.Items.Add(s);
                listBox5.Items.Add(s);
            }
            foreach (string s in PBE.itemNoGunsNames)
            {
                listBox4.Items.Add(s);
            }
            foreach (string s in PBE.picNames)
            {
                listBox2.Items.Add(s);
            }
            foreach (string s in MainGame.SoundSystem.getSongNames())
            {
                listBox6.Items.Add(s);
            }
            foreach (string s in MainGame.SoundSystem.getSoundEffectNames())
            {
                listBox7.Items.Add(s);
            } 
            levelsNamesList.Items.AddRange(levels);
        }
        public ScriptForm(PBEditorObjects PBEd, TreeNodeCollection allObjects, ListBox.ObjectCollection lights, ListBox.ObjectCollection levels)
        {
            PBE = PBEd;


            InitializeComponent();
            foreach (TreeNode node in allObjects)
            {
                treeView1.Nodes.Add((TreeNode)node.Clone());
            }
            listBox1.Items.AddRange(lights);
            foreach (string s in PBE.mobNames)
            {
                listBox3.Items.Add(s);
                listBox5.Items.Add(s);
            }
            foreach (string s in PBE.itemNoGunsNames)
            {
                listBox4.Items.Add(s);
            }
            foreach (string s in PBE.picNames)
            {
                listBox2.Items.Add(s);
            }
            foreach (string s in MainGame.SoundSystem.getSongNames())
            {
                listBox6.Items.Add(s);
            }
            foreach (string s in MainGame.SoundSystem.getSoundEffectNames())
            {
                listBox7.Items.Add(s);
            } 
            levelsNamesList.Items.AddRange(levels);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex < 0)
                MessageBox.Show("Please choose a trigger type!");
            else
                if (tabControl1.SelectedIndex == 0 && treeView1.SelectedNode == null)
                    MessageBox.Show("Select a mob to move!");
                else
                    if (tabControl1.SelectedIndex == 1 && listBox1.SelectedIndex < 0)
                        MessageBox.Show("Select a light to move!");
                    else
                        if (tabControl1.SelectedIndex == 2 && tabControl2.SelectedIndex == 0 && listBox5.SelectedIndex < 0)
                            MessageBox.Show("Select a mob for quest!");
                        else
                            if (tabControl1.SelectedIndex == 2 && tabControl2.SelectedIndex == 1 && listBox4.SelectedIndex < 0)
                                MessageBox.Show("Select a item for quest!");
                            else
                                if (tabControl1.SelectedIndex == 5 && listBox3.SelectedIndex < 0)
                                    MessageBox.Show("Select a mob to spawn!");
                                else
                                    if (tabControl1.SelectedIndex == 3 && listBox2.SelectedIndex < 0)
                                        MessageBox.Show("Select a picture for message!");
                                    else
                                        if (tabControl1.SelectedIndex == 4 && levelsNamesList.SelectedIndex < 0 && radioButton2.Checked)
                                            MessageBox.Show("Select a level to teleport to!");
                                        else
                                            if (tabControl1.SelectedIndex == 6 && tabControl3.SelectedIndex == 0 && listBox6.SelectedIndex < 0)
                                                MessageBox.Show("Select song to play!");
                                            else
                                                if (tabControl1.SelectedIndex == 6 && tabControl3.SelectedIndex == 1 && listBox7.SelectedIndex < 0)
                                                    MessageBox.Show("Select sound effect to play!");
                                                else
                                            try
                                            {
                                                OkPressed = true;
                                                if (tabControl1.SelectedIndex == 0 ||
                                                    tabControl1.SelectedIndex == 1 ||
                                                    (tabControl1.SelectedIndex == 2 && tabControl2.SelectedIndex == 2) ||   //MODIFICAT AICI, STERGE PARANTEZE DACA NU MAI MERGE
                                                    (tabControl1.SelectedIndex == 4 && radioButton5.Checked) ||
                                                    tabControl1.SelectedIndex == 5)
                                                    NeedPosition = true;

                                                else
                                                    NeedPosition = false;
                                                EditorForm.isFocused = true;
                                                PBScriptAction.TriggeredBy trgBy = new PBScriptAction.TriggeredBy();
                                                switch (comboBox3.SelectedIndex)
                                                {
                                                    case 0:
                                                        trgBy = PBScriptAction.TriggeredBy.All;
                                                        break;
                                                    case 1:
                                                        trgBy = PBScriptAction.TriggeredBy.PlayerOnly;
                                                        break;
                                                    case 2:
                                                        trgBy = PBScriptAction.TriggeredBy.PlayerActionOnly;
                                                        break;
                                                }
                                                bool repeat = false;
                                                if (checkBox1.Checked)
                                                    repeat = true;
                                                switch (tabControl1.SelectedIndex)
                                                {
                                                    //Object
                                                    case 0:
                                                        action = new PBScriptAction(treeView1.SelectedNode.Text, 0f, 0f, (int)numericUpDown14.Value, 0f, (int)numericUpDown15.Value, trgBy, repeat);
                                                        break;
                                                    //Lights
                                                    case 1:
                                                        Microsoft.Xna.Framework.Color tempCol = new Microsoft.Xna.Framework.Color(pictureBox3.BackColor.R, pictureBox3.BackColor.G, pictureBox3.BackColor.B, pictureBox3.BackColor.A);
                                                        action = new PBScriptAction(listBox1.SelectedItem as string, 0f, 0f, 0f, (int)numericUpDown7.Value, tempCol.ToVector3().X, tempCol.ToVector3().Y, tempCol.ToVector3().Z,
                                                            (int)numericUpDown9.Value, 0f, 0f, 0f, (int)numericUpDown8.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (float)numericUpDown12.Value / 100f, (int)numericUpDown13.Value, trgBy, repeat);
                                                        break;
                                                    //Quest
                                                    case 2:
                                                        switch (tabControl2.SelectedIndex)
                                                        {
                                                            //MOB
                                                            case 0:
                                                                action = new PBScriptAction(true, Monsters.FromName(listBox5.SelectedItem as string), (int)numericUpDown4.Value, (int)numericUpDown5.Value, (int)numericUpDown6.Value, trgBy);
                                                                break;
                                                            //ITEM
                                                            case 1:
                                                                action = new PBScriptAction(false, ProjectBuild.PBEngine.Items.PBItems.FromName(listBox4.SelectedItem as string), (int)numericUpDown4.Value, (int)numericUpDown5.Value, (int)numericUpDown6.Value, trgBy);
                                                                break;
                                                            //LOCATION
                                                            case 2:
                                                                action = new PBScriptAction(Microsoft.Xna.Framework.Vector2.Zero, (int)numericUpDown5.Value, (int)numericUpDown6.Value, trgBy);
                                                                break;
                                                        }
                                                        break;
                                                    //Messages
                                                    case 3:
                                                        action = new PBScriptAction(new Microsoft.Xna.Framework.Vector2(-1), new Microsoft.Xna.Framework.Vector2(-1), textBox1.Text, (int)numericUpDown3.Value, Microsoft.Xna.Framework.Color.White, "background", listBox2.SelectedItem as string, trgBy);
                                                        break;
                                                    //Teleport
                                                    case 4:
                                                        string where = null;
                                                        if (radioButton2.Checked) where = levelsNamesList.SelectedItem as string;
                                                        if (radioButton3.Checked) where = "END";
                                                        if (radioButton4.Checked) action = new PBScriptAction(new Microsoft.Xna.Framework.Vector2(-1), trgBy, where);
                                                        else
                                                            action = new PBScriptAction(Microsoft.Xna.Framework.Vector2.Zero, trgBy, where);
                                                        break;
                                                    //Spawn
                                                    case 5:
                                                        action = new PBScriptAction(Microsoft.Xna.Framework.Vector2.Zero, listBox3.SelectedItem as string, (int)numericUpDown2.Value, (int)numericUpDown1.Value, trgBy);
                                                        break;
                                                    case 6:
                                                        string actiontype= (tabControl3.SelectedIndex==0) ? "song" : "sound" ;
                                                        string soundName = (tabControl3.SelectedIndex == 0) ? listBox6.SelectedItem as string : listBox7.SelectedItem as string;
                                                        action = new PBScriptAction(actiontype, soundName, trgBy);
                                                        break;

                                                }


                                                Close();
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("Wrong data!" + "\n" + ex);
                                            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OkPressed = false;
            EditorForm.isFocused = true;
            Close();
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl2.SelectedIndex == 2)
                numericUpDown4.ReadOnly = true;
            else
                numericUpDown4.ReadOnly = false;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                if (treeView1.SelectedNode.Text == "Player")
                {
                    pictureBox2.Image = PBE.nameImages["Player"];
                }
                else
                    pictureBox2.Image = PBE.nameImages[PBE.textureNameTOName[treeView1.SelectedNode.Text.Remove(treeView1.SelectedNode.Text.LastIndexOf("_"))]];
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ColorDialog a = new ColorDialog();
            if (a.ShowDialog() == DialogResult.OK)
            {
                pictureBox3.BackColor = a.Color;
            }
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox3.SelectedIndex >= 0)
            {
                string s = listBox3.SelectedItem as string;
                //s=s.Remove(s.LastIndexOf("_"));
                pictureBox4.Image = PBE.nameImages[PBE.textureNameTOName[s]];
                label28.Text = PBE.objectDetails[listBox3.SelectedItem as string];
            }
        }

        private void listBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox5.SelectedIndex >= 0)
            {
                string s = listBox5.SelectedItem as string;
                //s = s.Remove(s.LastIndexOf("_"));
                pictureBox5.Image = PBE.nameImages[PBE.textureNameTOName[s]];
                label27.Text = PBE.objectDetails[listBox5.SelectedItem as string];
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex >= 0)
            {
                pictureBox1.Image = PBE.nameImages[listBox2.SelectedItem as string];
            }
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox4.SelectedIndex >= 0)
            {
                string s = listBox4.SelectedItem as string;
                pictureBox6.Image = PBE.nameImages[PBE.textureNameTOName[s]];
            }
        }

        private void ScriptForm_Load(object sender, EventArgs e)
        {
            tabControl1.TabPages[0].ToolTipText = "Object movement script. " + '\n' + "-Select object from list." + '\n' + "-Put time in ticks for position/rotation. " + '\n' + "(if 0 no movement/rotation done) " + '\n' + "-After pressing ok select a position and rotation.";
            tabControl1.TabPages[1].ToolTipText = "Light movement script. " + '\n' + "-Select light from list." + '\n' + "-Put time in ticks for attributes. " + '\n' + "(if 0 the attribute is skipped) " + '\n' + "-After pressing ok select a position and rotation.";
            tabControl1.TabPages[2].ToolTipText = "Quest script." + '\n' + "-Select objective for quest." + '\n' + "-Put reward.";
            tabControl1.TabPages[3].ToolTipText = "Messages script." + '\n' + "-Select picture." + '\n' + "-Put text and time for text on screen.";
            tabControl1.TabPages[4].ToolTipText = "Teleport script." + '\n' + "-Select where and how.";
            tabControl1.TabPages[5].ToolTipText = "Spawn script." + '\n' + "-Select mob to spawn." + '\n' + "-Put time to spawn in ticks and number of spawns";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (tabControl3.SelectedIndex == 0)
            {
                if (listBox6.SelectedIndex >= 0)
                {
                    MainGame.SoundSystem.PlaySong(listBox6.SelectedItem as string);
                }
            }else
                if (tabControl3.SelectedIndex == 1)
                {
                    if (listBox7.SelectedIndex >= 0)
                        MainGame.SoundSystem.PlaySound(listBox7.SelectedItem as string);
                }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MainGame.SoundSystem.StopSong();
        }
    }
}