﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProjectBOTEditor
{
    public partial class SizeSetting : Form
    {
        public bool OkPressed = false;
        public SizeSetting()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == ""||textBox3.Text==null)
            {
                MessageBox.Show("Please enter a name for the map!");
            }
            else
            {
                EditorForm.isFocused = true;
                OkPressed = true;
                Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            EditorForm.isFocused = true;
            OkPressed = false;
            Close();
        }

    }
}
