﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProjectBuild.PBEditor
{
    public class EditMethods
    {
        public static System.Drawing.Image Texture2Image(Microsoft.Xna.Framework.Graphics.Texture2D texture)
        {
            if (texture == null)
            {
                return null;
            }
            if (texture.IsDisposed)
            {
                return null;
            }

            MemoryStream ms = new MemoryStream();
            texture.SaveAsPng(ms, texture.Width, texture.Height);
            ms.Seek(0, SeekOrigin.Begin);
            System.Drawing.Image bmp2 = System.Drawing.Bitmap.FromStream(ms);
            ms.Close();
            ms = null;
            return bmp2;
        }
    }
}
