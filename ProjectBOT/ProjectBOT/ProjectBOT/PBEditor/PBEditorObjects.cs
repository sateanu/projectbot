﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Dynamics;
using System.IO;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using FarseerPhysics.Factories;
using ProjectBuild.PBEngine;
using System.Drawing;
using Microsoft.Xna.Framework.Content;
using ProjectBuild.PBEditor;
using ProjectBuild.PBEngine.Items;

namespace ProjectBOTEditor
{
    public class PBEditorObjects
    {
        public List<Texture2D>[] objectTextures = new List<Texture2D>[2];
        public List<Image> objectImages = new List<Image>();
        public List<string> neutralNames = new List<string>();
        public List<string> mobNames = new List<string>();
        public List<string> objectNames = new List<string>();
        public List<string> itemNames = new List<string>();
        public List<string> itemNoGunsNames = new List<string>();
        public List<string> picNames = new List<string>();
        public Dictionary<string, PBEditorObject> objectDictionary = new Dictionary<string, PBEditorObject>();
        public Dictionary<string, Texture2D> nameTextureC = new Dictionary<string, Texture2D>();
        public Dictionary<string, Texture2D> nameTextureN = new Dictionary<string, Texture2D>();
        public Dictionary<string, Image> nameImages = new Dictionary<string, Image>();
        public Dictionary<string, string> textureNameTOName = new Dictionary<string, string>();
        public Dictionary<string, string> objectDetails = new Dictionary<string, string>();
        public List<PBEventObject> eventObjects = new List<PBEventObject>();
        public Dictionary<string, PBScriptAction> actions = new Dictionary<string, PBScriptAction>();
        public Dictionary<string, ScriptForm> actionForms = new Dictionary<string, ScriptForm>();

        List<string> nameList = new List<string>(); //no repeat
        public PBEditorObjects(GraphicsDevice GD,ContentManager content)
        {
            string path = Path.GetDirectoryName(Application.ExecutablePath);
            objectTextures[0] = new List<Texture2D>();
            objectTextures[1] = new List<Texture2D>();
            foreach (PBItem i in PBItems.Items)
            {
                if (!nameList.Contains(i.texstring))
                {
                    nameList.Add(i.texstring);
                    Texture2D temp=content.Load<Texture2D>("Items\\"+i.texstring);
                    nameImages.Add(i.texstring, EditMethods.Texture2Image(temp));
                    nameTextureC.Add(i.texstring, temp);
                }
                itemNames.Add(i.name);
                itemNoGunsNames.Add(i.name);
                objectDetails.Add(i.name, " A regular item, you may \n need it for quests.");
                textureNameTOName.Add(i.name, i.texstring);
                PBEditorObject a = new PBEditorObject();
                a.textureC = nameTextureC[i.texstring];
                a.type = PBEditorObject.Type.Drop;
                a.name = i.name;
               // a.Height = PBConverter.PixelToMeter((float)nameTextureC[i.texstring].Height);
               // a.Width = PBConverter.PixelToMeter((float)nameTextureC[i.texstring].Width);
                a.Width = 1f;
                a.Height = 1f;
                objectDictionary.Add(i.name, a);
            }
            foreach (PBGun g in PBGuns.Guns)
            {
                if (!nameList.Contains(g.textureName))
                {
                    nameList.Add(g.textureName);
                    Texture2D temp = content.Load<Texture2D>("Weapons\\" + g.textureName);
                    nameImages.Add(g.textureName, EditMethods.Texture2Image(temp));
                    nameTextureC.Add(g.textureName, temp);
                }
                objectDetails.Add(g.Name, g.ToStringDetails().ToString());
                itemNames.Add(g.Name);
                textureNameTOName.Add(g.Name, g.textureName);
                PBEditorObject a = new PBEditorObject();
                a.textureC = nameTextureC[g.textureName];
                a.type = PBEditorObject.Type.Drop;
                a.name = g.Name;
                a.Height = 1f;
                a.Width = 1f;
              //  a.Height = PBConverter.PixelToMeter((float)nameTextureC[g.textureName].Height);
               // a.Width = PBConverter.PixelToMeter((float)nameTextureC[g.textureName].Width);
                objectDictionary.Add(g.Name, a);
            }
            foreach (string s in ProjectBuild.MainGame.picNames)
            {
                picNames.Add(s);
                nameImages.Add(s, EditMethods.Texture2Image(content.Load<Texture2D>("Etc\\Pics\\" + s)));
            }
            foreach (PBMob m in Monsters.monsters)
            {
                if (!nameList.Contains(m.textureName))
                {
                    nameList.Add(m.textureName);
                    Texture2D temp=content.Load<Texture2D>("Mobs\\"+m.textureName);
                    nameImages.Add(m.textureName, EditMethods.Texture2Image(temp));
                    nameTextureC.Add(m.textureName,temp);
                    //nameTextureN.Add(m.textureName, content.Load<Texture2D>("Mobs\\" + m.textureName+"_n"));
                }
            } foreach (PBObject m in Objects.objects)
            {
                if (!nameList.Contains(m.textureName))
                {
                    nameList.Add(m.textureName);
                    Texture2D temp = content.Load<Texture2D>("Mobs\\" + m.textureName);
                    nameImages.Add(m.textureName, EditMethods.Texture2Image(temp));
                    nameTextureC.Add(m.textureName, temp);
                    //nameTextureN.Add(m.textureName, content.Load<Texture2D>("Mobs\\" + m.textureName+"_n"));
                }
            }
            objectNames.Add("Player");
            nameList.Add("Player");
            Texture2D temport = content.Load<Texture2D>("Mobs\\Player");
            nameImages.Add("Player", EditMethods.Texture2Image(temport));
            nameTextureC.Add("Player", temport);
            nameTextureN.Add("Player", content.Load<Texture2D>("Mobs\\Player_n"));
            objectDictionary.Add("Player", Player());
            textureNameTOName.Add("Player", "Player");
            objectDetails.Add("Player", "The Spawn \n Where you put it \n the player will spawn");

            neutralNames.Add("None");
            objectDetails.Add("None", "Nothing, a object without \n texture");
            objectDictionary.Add("None", None());
            textureNameTOName.Add("None", "None");
            
            /*objectNames.Add("Mob1");
            objectNames.Add("Crate");


            string path = Path.GetDirectoryName(Application.ExecutablePath);
            objectTextures[0] = new List<Texture2D>();
            objectTextures[1] = new List<Texture2D>();

            string appPath = path + "\\Content\\Mobs\\" + objectNames[0] + ".png";
            FileStream stream = new FileStream(appPath, FileMode.Open, FileAccess.Read);
            objectImages.Add(Image.FromStream(stream));
            stream.Dispose();
            appPath = path + "\\Content\\Mobs\\" + objectNames[1] + ".png";
            stream = new FileStream(appPath, FileMode.Open, FileAccess.Read);
            objectImages.Add(Image.FromStream(stream));
            stream.Dispose();
            
            objectTextures[0].Add(Texture2D.FromStream(GD, new FileStream(path + "\\Content\\Mobs\\mob1.png", FileMode.Open)));
            objectTextures[1].Add(Texture2D.FromStream(GD, new FileStream(path + "\\Content\\Mobs\\mob1_n.png", FileMode.Open)));
            objectTextures[0].Add(Texture2D.FromStream(GD, new FileStream(path + "\\Content\\Mobs\\Crate.png", FileMode.Open)));
            objectTextures[1].Add(Texture2D.FromStream(GD, new FileStream(path + "\\Content\\Mobs\\Crate_n.png", FileMode.Open)));

            objectDictionary.Add(objectNames[0], Mob());
            objectDictionary.Add(objectNames[1], Crate());
            objectDictionary.Add(objectNames[2], Player());*/


            foreach (PBMob mob in Monsters.monsters)
            {
                objectNames.Add(mob.Name);
                mobNames.Add(mob.Name);
                objectImages.Add(nameImages[mob.textureName]);
                textureNameTOName.Add(mob.Name, mob.textureName);
                objectTextures[0].Add(nameTextureC[mob.textureName]);
                //objectTextures[1].Add(nameTextureN[mob.textureName]);
                PBEditorObject a = new PBEditorObject();
                a.textureC = nameTextureC[mob.textureName];
                a.textureN = nameTextureC[mob.textureName];
                a.type = PBEditorObject.Type.Mob;
                a.name = mob.Name;
                a.Height = mob.height;
                a.Width = mob.width;
                objectDictionary.Add(mob.Name, a);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Name: " + mob.Name);
                if (mob.Indestructable == false)
                {
                    sb.AppendLine("HP: " + mob.HPMAX);
                    sb.AppendLine("XP: " + mob.dropXp);
                    sb.AppendLine("SHIELD: " + mob.SHIELDMAX);
                    sb.AppendLine("S. REGEN: " + mob.ShieldRegen);
                    sb.AppendLine("S. REGEN DELAY: " + mob.ShieldRegenDelay);
                }
                else
                    sb.AppendLine("Indestructable object");
                sb.AppendLine("Width: " + mob.width);
                sb.AppendLine("Height: " + mob.height);
                if (mob.st == true)
                    sb.AppendLine("Static mob (cannot move) !");
                objectDetails.Add(mob.Name, sb.ToString());

            } foreach (PBObject mob in Objects.objects)
            {
                objectNames.Add(mob.Name);
                neutralNames.Add(mob.Name);
                objectImages.Add(nameImages[mob.textureName]);
                textureNameTOName.Add(mob.Name, mob.textureName);
                objectTextures[0].Add(nameTextureC[mob.textureName]);
                objectTextures[1].Add(nameTextureC[mob.textureName]);
                PBEditorObject a = new PBEditorObject();
                a.textureC = nameTextureC[mob.textureName];
                //a.textureN = nameTextureC[mob.textureName];
                a.type = PBEditorObject.Type.Object;
                a.name = mob.Name;
                a.Height = mob.height;
                a.Width = mob.width;
                objectDictionary.Add(mob.Name, a);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Name: " + mob.Name);
                if (mob.Indestructable == false)
                {
                    sb.AppendLine("HP: " + mob.HPMAX);
                    sb.AppendLine("SHIELD: " + mob.SHIELDMAX);
                    sb.AppendLine("S. REGEN: " + mob.ShieldRegen);
                    sb.AppendLine("S. REGEN DELAY: " + mob.ShieldRegenDelay);
                }
                sb.AppendLine("Width: " + mob.width);
                sb.AppendLine("Height: " + mob.height);
                objectDetails.Add(mob.Name, sb.ToString());

            }

        }
        public  PBEditorObject Mob()
        {
            PBEditorObject A = new PBEditorObject();
            A.textureC = objectTextures[0][0];
            A.textureN = objectTextures[1][0];
            A.type = PBEditorObject.Type.Mob;
            return A;
        }
        public PBEditorObject Crate()
        {
            PBEditorObject A = new PBEditorObject();
            A.textureC = objectTextures[0][1];
            A.textureN = objectTextures[1][1];
            return A;
        }
        public PBEditorObject Player()
        {
            PBEditorObject A = new PBEditorObject();
            A.textureC = nameTextureC["Player"];
            A.textureN = nameTextureN["Player"];
            A.name = "Player";
            A.type = PBEditorObject.Type.Spawn;
            return A;
        }
        public PBEditorObject None()
        {
            PBEditorObject A = new PBEditorObject();
            A.name = "None";
            A.type = PBEditorObject.Type.None;
            return A;
        }
    }
}
