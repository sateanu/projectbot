﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using ProjectBuild.PBEngine;
using System.Drawing;

namespace ProjectBOTEditor
{
    using Rectangle = Microsoft.Xna.Framework.Rectangle;
    using Color = Microsoft.Xna.Framework.Color;
   public class PBEditorObject
    {
       public enum Type
       {
           Object,
           Mob,
           Spawn,
           None,
           Drop
       }
       public Type type = Type.Object;
       public Texture2D textureC;
       public Texture2D textureN;
       public Image image;
       public Texture2D blank;
       public Vector2 position { get { return body.Position; } set { body.Position = value; } }
       public float rotation { get { return body.Rotation; } set { body.Rotation = value; } }
       public Body body;
       public string name;
       public float Width;
       public float Height;
       Rectangle blankRec { get { return new Rectangle((int)(position.X * 64f), (int)(position.Y * 64f), (int)(Width * 64f), (int)(Height * 64f)); } }
       public Vector2 origin { get { if(textureC==null)return Vector2.Zero; else return new Vector2(textureC.Width / 2, textureC.Height / 2); } }
       public PBEditorObject()
       {

       }
       public PBEditorObject(Texture2D  texC,Texture2D texN,Texture2D blank,World world,float Width,float Height,Vector2 v,Type type,string name)
       {
           this.textureC = texC;
           this.textureN = texN;
           this.blank = blank;
           this.Width = Width;
           this.Height = Height;
           body = BodyFactory.CreateRectangle(world, Width, Height, 1f,v);
           body.BodyType = BodyType.Dynamic;
           body.CollidesWith = Category.None;
           this.type = type;
           this.name = name;
       }
       public void DrawC(SpriteBatch spriteBatch)
       {
           if(textureC!=null)
           spriteBatch.Draw(textureC, new Rectangle((int)PBConverter.MeterToPixel(position.X), (int)PBConverter.MeterToPixel(position.Y),
                (int)PBConverter.MeterToPixel(Width), (int)PBConverter.MeterToPixel(Height))
                , null, Color.White, rotation, origin, SpriteEffects.None, 0);
       }
       public void DrawN(SpriteBatch spriteBatch)
       {
           if (textureN != null)
               spriteBatch.Draw(textureN, new Rectangle((int)PBConverter.MeterToPixel(position.X), (int)PBConverter.MeterToPixel(position.Y),
                (int)PBConverter.MeterToPixel(Width), (int)PBConverter.MeterToPixel(Height))
                , null, Color.White, rotation, origin, SpriteEffects.None, 0);
       }
       public void DrawFocus(SpriteBatch spriteBatch)
       {
           if (textureC != null){
               Color tin=new Color(144,238,144,100);
               spriteBatch.Draw(blank, new Rectangle((int)PBConverter.MeterToPixel(position.X), (int)PBConverter.MeterToPixel(position.Y),
                (int)PBConverter.MeterToPixel(Width), (int)PBConverter.MeterToPixel(Height))
                , null, tin, rotation, new Vector2(blank.Width/2,blank.Height/2), SpriteEffects.None, 0);
           }
       }
       public PBEditorObject Copy()
       {
           return new PBEditorObject()
           {
               body = this.body,
               textureC = this.textureC,
               textureN = this.textureN,
               rotation = this.rotation,
               image=this.image,
               position = this.position,
               name=this.name

           };
       }

    }
}
