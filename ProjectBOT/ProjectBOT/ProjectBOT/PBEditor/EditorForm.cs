﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using System.IO;
using ProjectBuild.PBEngine;
using ProjectBuild;
using FarseerPhysics.Dynamics;


namespace ProjectBOTEditor
{
    using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
    using Color = Microsoft.Xna.Framework.Color;
    using Image = System.Drawing.Image;
    using Rectangle = Microsoft.Xna.Framework.Rectangle;
    using FarseerPhysics.Factories;
    using System.Drawing.Drawing2D;
    using FarseerPhysics.DebugViews;
    using PBEngine.Effect;
    using Blend = Blend;
    using ProjectBuild.PBEditor;
    public partial class EditorForm : Form
    {
        enum EditorState
        {
            NoLevel,
            WithLevel
        }
        enum ButtonPressed
        {
            Move,
            None,
            Draw,
            Wall,
            Delete,
            PlaceObj,
            EditObj,
            PlaceLight,
            EditLight,
            PlaceSpotLight,
            PlaceScript,
            EditScript,
            PositionScript,
            PositionNeeded
        }
        ButtonPressed bttPress = ButtonPressed.None;
        List<string> textureNames;// { "dirt", "dirt_2", "brick", "blackstone", "iron", "grass_texture", "color_lime", "sand", "carbon1", "carbon2", "carbon3", "carbon4", "carbon5" };
        List<Texture2D> tileTexturesColor;
        List<Texture2D> tileTexturesNormal;
        List<Image> textureImages;
        EditorState EDState = EditorState.NoLevel;
        SpriteBatch spriteBatch;
        Texture2D tileTexture;
        public Texture2D blankTexture;
        Texture2D arrowTexture;
        public GraphicsDevice GraphicsDevice { get { return tileDisplay1.GraphicsDevice; } }
        int[,] map = new int[,] 
        { 
        { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
         { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
          { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
           { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
            { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
             { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
              { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
               { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
                { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
                 { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
                  { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
                   { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
                    { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
                     { 0, 1, 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, }, 
        };
       public string levelName=null;
       public string campainName = null;
        PBLayer Map;
        World world;
        PBCamera Camera;
        DebugViewXNA debug;
        PBEditorObjects PBEditorObj;
        Dictionary<string,PBEditorObject> editorObjects;
        Dictionary<string,PBEventObject> eventObjects=new Dictionary<string,PBEventObject>();
        Dictionary<string, PBScriptAction> scriptActions = new Dictionary<string, PBScriptAction>();
        string editorObjectKey;
        string editorObjectName;
        IntPtr originalHandle;
        SpriteFont font;

        RenderTarget2D _colorMap;
        RenderTarget2D _normalMap;
        RenderTarget2D _depthMap;
        RenderTarget2D _shadowMap;
        Texture2D scriptTexture;
       Dictionary<string,PBLight> Lights = new Dictionary<string,PBLight>();
        Color AmbientLight = Color.White;
        Effect LightEffect;
        Effect LightCombinedEffect;
        VertexPositionColorTexture[] Vertices;
        VertexBuffer vertexBuffer;
        Color lightTempColor;
        ContentManager content;
        float mapX;
        float mapY;
        public List<Texture2D> SplitTexture(Texture2D texture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            int W = texture.Width;
            int H = texture.Height;
            Color[] pixels = new Color[W * H];
            Color[] orPixel = new Color[W * H];
            texture.GetData<Color>(orPixel);
            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = Color.Transparent;
            }
            float[,] UV = new float[,] //POS X, POS Y, %X, %Y, 1f - keep/0f - delete
            { 
                { 0f,0f,1f,0.5f,1f },
                { 0f,0.5f,1f,0.5f,1f },
                { 0f,0f,0.5f,1f,1f},
                { 0.5f,0f,0.5f,1f,1f},
                { 0.5f, 0.5f, 0.5f,0.5f,1f },
                { 0.5f, 0.5f, 0.5f,0.5f,0f },
                { 0f, 0f, 0.5f,0.5f,1f },
                { 0f, 0f, 0.5f,0.5f,0f },
                { 0.5f,0f,0.5f,0.5f,1f},
                { 0.5f,0f,0.5f,0.5f,0f},
                { 0f,0.5f,0.5f,0.5f,1f},
                { 0f,0.5f,0.5f,0.5f,0f},
            };
            int Linii = UV.GetLength(0);
            int Coloane = UV.GetLength(1);

            for (int i = 0; i < Linii; i++)
            {
                Texture2D temp = new Texture2D(GraphicsDevice, W, H);

                if (UV[i, 4] == 1f)
                {
                    temp.SetData<Color>(orPixel);
                    temp.SetData<Color>(0,
                        new Rectangle((int)(W * UV[i, 0]), (int)(H * UV[i, 1]), (int)(W * UV[i, 2]), (int)(H * UV[i, 3])),
                        pixels, 1,
                        (int)(W * UV[i, 2] * H * UV[i, 3]));
                }
                else
                {
                    float x, y, procX, procY;
                    x = UV[i, 0];
                    y = UV[i, 1];
                    procX = UV[i, 2] + x;
                    procY = UV[i, 3] + y;
                    Color[] t = new Color[texture.Width * texture.Height];
                    for (int py = (int)(y * texture.Height); py < (int)(texture.Height * procY); py++)
                        for (int px = (int)(x * texture.Width); px < (int)(texture.Width * procX); px++)
                        {
                            int index = px + py * texture.Width;
                            t[index] = orPixel[index];
                        }
                    temp.SetData<Color>(t);
                }
                textures.Add(temp);
            }

            return textures;
        }
        public EditorForm(ScreenManager screenManager)
        {
            textureNames = new List<string>();
            textureNames.AddRange(MainGame.textureNames);
            this.screenManager = screenManager;
          //  content = screenManager.Game.Content;
            InitializeComponent();
            tileDisplay1.OnInitialize += new EventHandler(tileDisplay1_OnInitialize);
            tileDisplay1.OnDraw += new EventHandler(tileDisplay1_OnDraw);
            Application.Idle += delegate { tileDisplay1.Invalidate(); };
            originalHandle = tileDisplay1.Handle;
            Mouse.WindowHandle = originalHandle;
            int index = -1;
            int count=0;
            foreach (string s in textureNames)
            {
                if (count == 0)
                {
                    textureTree.Nodes.Add(s);
                    count = 12;
                    index++;
                }
                else
                {
                    textureTree.Nodes[index].Nodes.Add(s);
                    count--;
                }
                ListBoxTextureNames.Items.Add(s);
            }
            textureImages = new List<Image>();
            tileTexturesColor = new List<Texture2D>();
            tileTexturesNormal = new List<Texture2D>();
        }
        string lvlname = null;
        public EditorForm(ScreenManager screenManager,string lvlname)
        {
            textureNames = new List<string>();
            textureNames.AddRange(MainGame.textureNames);
            this.screenManager = screenManager;
           // content = screenManager.Game.Content;
            InitializeComponent();
            tileDisplay1.OnInitialize += new EventHandler(tileDisplay1_OnInitialize);
            tileDisplay1.OnDraw += new EventHandler(tileDisplay1_OnDraw);
            Application.Idle += delegate { tileDisplay1.Invalidate(); };
            originalHandle = tileDisplay1.Handle;
            Mouse.WindowHandle = originalHandle;
            int index = -1;
            int count = 0;
            foreach (string s in textureNames)
            {
                if (count == 0)
                {
                    textureTree.Nodes.Add(s);
                    count = 12;
                    index++;
                }
                else
                {
                    textureTree.Nodes[index].Nodes.Add(s);
                    count--;
                }
                ListBoxTextureNames.Items.Add(s);
            }
            textureImages = new List<Image>();
            tileTexturesColor = new List<Texture2D>();
            tileTexturesNormal = new List<Texture2D>();
            this.lvlname = lvlname;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        List<string> tempr = new List<string>();
        private void LoadTextures()
        {
           /* foreach (string s in textureNames)
                AddTextureBoth(s);
            string[] copie=new string[textureNames.Count];
            textureNames.CopyTo(copie);
            for(int ii=0;ii<copie.Length;ii++)
            {
                for (int i = 0; i < 12; i++)
                {
                    string st = tempr[i+ii*12];
                    textureNames.Insert(ii*13+1+i, st);
                    ListBoxTextureNames.Items.Insert(ii*13+1+i, st);
                }
            }*/

            for (int i = 0; i < textureNames.Count; i = i + 1)
            {
                //AddTextureColor(textureNames[i]);
                int w=MainGame.TEXTURES[i].Width;
                int h=MainGame.TEXTURES[i].Height;
                Texture2D tex = new Texture2D(GraphicsDevice,w,h);
                Color[] pix = new Color[w * h];
                MainGame.TEXTURES[i].GetData<Color>(pix);
                tex.SetData<Color>(pix);
                tileTexturesColor.Add(tex);
            }
            //tileTexturesColor.AddRange(Game1.TEXTURES);
            textureImages.AddRange(MainGame.IMAGES);
        }
        string[] str =
        {
            "_jos","_sus","_dreapta","_stanga",
            "_colt_stanga_sus","_dreapta_jos",
            "_colt_dreapta_jos","_stanga_sus",
            "_colt_stanga_jos","_dreapta_sus",
            "_colt_dreapta_sus","_stanga_jos"
        };
        public void AddTextureColor(string s)
        {
                Texture2D temp = content.Load<Texture2D>("Tiles\\"+s);
                //Image image = EditMethods.Texture2Image(temp);
                //textureImages.Add(image);
                tileTexturesColor.Add(temp);
                List<Texture2D> splitText = SplitTexture(temp);
                for (int i=0;i<splitText.Count;i++)
                {
                    Texture2D t=splitText[i];
                    //tempr.Add(s + str[i]);
                    //Image img = EditMethods.Texture2Image(t);
                    //textureImages.Add(img);
                    tileTexturesColor.Add(t);
                }
                
        }
        public void AddTextureNormal(string s)
        {
            tileTexturesNormal.Add(content.Load<Texture2D>("Tiles\\"+s+"_n"));
        }
        public void AddTextureBoth(string s)
        {
            AddTextureColor(s);
           // AddTextureNormal(s);
        }
        PBObject cameraObj;
        void tileDisplay1_OnInitialize(object sender, EventArgs e)
        {
            world = new World(Vector2.Zero);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            content = new ContentManager(tileDisplay1.Services);
            content.RootDirectory = Application.StartupPath + "\\Content";// @"E:\Proiecte\ProjectBOTEditor\DebugView XNA\bin\x86\Debug\Content";
            tileTexture = content.Load<Texture2D>("mark");
            blankTexture = content.Load<Texture2D>("blank");
            scriptTexture = content.Load<Texture2D>("scriptRect");
            arrowTexture = content.Load<Texture2D>("arrow");
                //Map = new PBLayer(map, 80, 80, world);
            LoadTextures();
            Camera = new PBCamera(GraphicsDevice.Viewport);
            Camera.Scale = 1f;
            cameraObj = new PBObject(world, .01f, .01f, Vector2.Zero);
            cameraObj.Body.CollidesWith = Category.None;
            cameraObj.Body.CollisionCategories = Category.None;
            cameraObj.Body.BodyType = BodyType.Dynamic;
            Camera.Follow(cameraObj);
            debug = new DebugViewXNA(world);
            debug.LoadContent(GraphicsDevice, content);
            debug.RemoveFlags(FarseerPhysics.DebugViewFlags.Shape);
            debug.AppendFlags(FarseerPhysics.DebugViewFlags.AABB);
            PBEditorObj = new PBEditorObjects(GraphicsDevice,content);
            editorObjects = new Dictionary<string,PBEditorObject>();
            font = content.Load<SpriteFont>("font");

            PresentationParameters pp = GraphicsDevice.PresentationParameters;
            int w = pp.BackBufferWidth;
            int h = pp.BackBufferHeight;
            SurfaceFormat f = pp.BackBufferFormat;
            Vertices = new VertexPositionColorTexture[4];
            Vertices[0] = new VertexPositionColorTexture(new Vector3(-1, 1, 0), Color.White, new Vector2(0, 0));
            Vertices[1] = new VertexPositionColorTexture(new Vector3(1, 1, 0), Color.White, new Vector2(1, 0));
            Vertices[2] = new VertexPositionColorTexture(new Vector3(-1, -1, 0), Color.White, new Vector2(0, 1));
            Vertices[3] = new VertexPositionColorTexture(new Vector3(1, -1, 0), Color.White, new Vector2(1, 1));
            vertexBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColorTexture), Vertices.Length, BufferUsage.None);
            vertexBuffer.SetData(Vertices);

            _colorMap = new RenderTarget2D(GraphicsDevice, w, h);
            _depthMap = new RenderTarget2D(GraphicsDevice, w, h);
            _normalMap = new RenderTarget2D(GraphicsDevice, w, h);
            _shadowMap = new RenderTarget2D(GraphicsDevice, w, h, false, f, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);


            LightEffect = content.Load<Effect>("FX\\LightEffect");
            LightCombinedEffect = content.Load<Effect>("FX\\LightCombinedEffect");

            if (lvlname != null)
            {
                OpenMap(lvlname);
                lvlname = null;
            }
            toolTip1.SetToolTip(button1, "Left click to move slowly." + '\n' + "Right click to go directly.");
            toolTip1.SetToolTip(button7, "Click to toggle debug mode.");
        }


        void tileDisplay1_OnDraw(object sender, EventArgs e)
        {
            UpdateGame();
            DrawGame();
        }

        private void DrawGame()
        {
            GraphicsDevice GD = GraphicsDevice;

            GD.SetRenderTarget(_colorMap);
            GD.Clear(Color.Transparent);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.View);
            if (Map != null)
            {
                Map.DrawColor(Camera, spriteBatch);
                Map.DrawDebug(Camera, spriteBatch, tileTexture);
                if (!(bttPress == ButtonPressed.PositionNeeded))
                {
                    foreach (string key in editorObjects.Keys)
                    {
                        editorObjects[key].DrawC(spriteBatch);
                    }
                    if (treeView1.SelectedNode != null)
                        if (treeView1.SelectedNode.Index >= 0 && tabControl1.SelectedIndex == 1)
                            editorObjects[treeView1.SelectedNode.Text].DrawFocus(spriteBatch);
                    if (tabControl1.SelectedIndex == 3)
                    {
                        for (int i = 0; i < scriptTreeView.Nodes.Count; i++)
                        {
                            if (scriptTreeView.Nodes[i].Text == ignoreScript)
                                continue;
                            PBEventObject o = eventObjects[scriptTreeView.Nodes[i].Text];
                            Rectangle r = new Rectangle((int)PBConverter.MeterToPixel(o.Position.X), (int)PBConverter.MeterToPixel(o.Position.Y),
                                (int)PBConverter.MeterToPixel(o.width), (int)PBConverter.MeterToPixel(o.height));
                            Color tint = Color.White;
                            if (scriptTreeView.SelectedNode != null && scriptTreeView.SelectedNode.Text == eventObjects.Keys.ElementAt<string>(i))
                                tint = Color.Yellow;
                            spriteBatch.Draw(scriptTexture, r, tint);
                        }
                        if (scriptRectangle != null && scriptRectangle.Width != 0 && scriptRectangle.Height != 0 && scriptRectStarted)
                            spriteBatch.Draw(scriptTexture, scriptRectangle, Color.Green);
                    }
                }
                else
                {
                    if (actionToBeAdded.type == PBScriptAction.ScriptType.Object)
                    {
                        editorObjects[actionToBeAdded.Script_Name].DrawC(spriteBatch);
                    }
                    if(needClicked)
                    spriteBatch.Draw(arrowTexture,needPosLoc,null,Color.Black,needPosRot,new Vector2(arrowTexture.Width/2,arrowTexture.Height/2),1f,SpriteEffects.None,1f);
                }
            }
            spriteBatch.End();
            GD.SetRenderTarget(null);
/*
            GD.SetRenderTarget(_normalMap);
            GD.Clear(Color.Transparent);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.View);
            if (Map != null)
            {
                Map.DrawNormal(Camera, spriteBatch);
                Map.DrawDebug(Camera, spriteBatch, tileTexture);
                foreach (var ob in editorObjects)
                {
                    ob.DrawN(spriteBatch);
                }
                //if (ObjectsListBox.SelectedIndex >= 0 && tabControl1.SelectedIndex == 1)
                //   editorObjects[ObjectsListBox.SelectedIndex].DrawFocus(spriteBatch);

            }
            spriteBatch.End();
            GD.SetRenderTarget(null);*/
            GenerateShadowMap();
            GD.Clear(Color.Black);
            DrawCombinedMaps(spriteBatch);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.View);
            if(tabControl1.SelectedIndex==1)
            foreach (string key in editorObjects.Keys)
            {
                PBEditorObject ob = editorObjects[key];
                spriteBatch.DrawString(font, ob.name, PBConverter.MeterToPixel(ob.position) - new Vector2(ob.Width*32f,ob.Height*32f) - new Vector2(0, font.MeasureString(ob.name).Y), Color.White);
            }
            if(tabControl1.SelectedIndex==2)
            foreach (string key in Lights.Keys)
            {
                PBLight ob = Lights[key];
                spriteBatch.DrawString(font, ob.ScriptName, new Vector2(ob.Position.X, ob.Position.Y) - new Vector2(0, font.MeasureString(ob.ScriptName).Y), Color.White);
            } 
            spriteBatch.End();
            if (debugOn)
                debug.RenderDebugData(ref Camera.Projection, ref Camera.DebugView);
            spriteBatch.Begin();
            spriteBatch.DrawString(font,"Mouse position: "+ mouseState.X.ToString() + " " + mouseState.Y.ToString() , Vector2.Zero, Color.White);
            spriteBatch.DrawString(font, "Mouse position on map: " +mapX.ToString() + " " +mapY.ToString(), new Vector2(0, 15), Color.White);
            spriteBatch.DrawString(font, "Tile position on map: " + cellx.ToString() + " " + celly.ToString(), new Vector2(0, 30), Color.White);
            spriteBatch.DrawString(font, "Camera position: " + PBConverter.MeterToPixel(Camera.position).ToString(), new Vector2(0,45), Color.White);
            spriteBatch.DrawString(font, "Camera position with screen: " + Camera.Position.ToString(), new Vector2(0,60), Color.White);
            spriteBatch.End();
        }
        private void DrawCombinedMaps(SpriteBatch spriteBatch)
        {
            LightCombinedEffect.CurrentTechnique = LightCombinedEffect.Techniques[0];
            LightCombinedEffect.Parameters["ambient"].SetValue(1f);
            LightCombinedEffect.Parameters["lightAmbient"].SetValue(3f);
            LightCombinedEffect.Parameters["ambientColor"].SetValue(AmbientLight.ToVector4());
            LightCombinedEffect.Parameters["ColorMap"].SetValue(_colorMap);
            LightCombinedEffect.Parameters["ShadingMap"].SetValue(_shadowMap);
            //LightCombinedEffect.Parameters["NormalMap"].SetValue(_normalMap);
            LightCombinedEffect.CurrentTechnique.Passes[0].Apply();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, LightCombinedEffect);
            spriteBatch.Draw(_colorMap, Vector2.Zero, Color.White);
            spriteBatch.End();
        }

        private Texture2D GenerateShadowMap()
        {
            GraphicsDevice.SetRenderTarget(_shadowMap);
            GraphicsDevice.Clear(Color.Transparent);
            Vector3 cameraScalePos;
            if (tabControl1.SelectedIndex == 2)
                foreach (string s in Lights.Keys)
                {
                    if (bttPress == ButtonPressed.PositionNeeded )
                        continue;
                    PBLight light = Lights[s];
                    if (!light.IsEnabled)
                        continue;

                    GraphicsDevice.SetVertexBuffer(vertexBuffer);



                    LightEffect.Parameters["lightStrength"].SetValue(light.ActualPower);
                    cameraScalePos = light.Position * Camera.Scale;
                    cameraScalePos += new Vector3(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2, 0);
                    cameraScalePos -= new Vector3(PBConverter.MeterToPixel(Camera.position) * Camera.Scale, 0);
                    LightEffect.Parameters["lightPosition"].SetValue(cameraScalePos);
                    LightEffect.Parameters["NormalMap"].SetValue(_normalMap);
                    LightEffect.Parameters["lightDecay"].SetValue(light.LightDecay*Camera.Scale);
                    LightEffect.Parameters["lightColor"].SetValue(light.Color);
                    LightEffect.Parameters["coneDirection"].SetValue(light.Direction);
                    LightEffect.Parameters["screenHeight"].SetValue(GraphicsDevice.Viewport.Height);
                    LightEffect.Parameters["screenWidth"].SetValue(GraphicsDevice.Viewport.Width);
                    LightEffect.Parameters["lightAngle"].SetValue(0f);
                    LightEffect.Parameters["DepthMap"].SetValue(_depthMap);
                    LightEffect.Parameters["specularStrength"].SetValue(light.SpecularPower);

                    if (light.LightType == LightType.Point)
                        LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredPointLight"];
                    else
                        if (light.LightType == LightType.Spot)
                            LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredSpotLight"];

                    LightEffect.CurrentTechnique.Passes[0].Apply();

                    GraphicsDevice.BlendState = BlendBlack;

                    GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, Vertices, 0, 2);

                }
            else
                if(actionToBeAdded!=null&&actionToBeAdded.Script_Name!=null)
                if (bttPress == ButtonPressed.PositionNeeded && Lights.Keys.Contains<string>(actionToBeAdded.Script_Name))
                {
                    GraphicsDevice.SetVertexBuffer(vertexBuffer);

                    PBLight light = Lights[actionToBeAdded.Script_Name];

                    LightEffect.Parameters["lightStrength"].SetValue(light.ActualPower);
                    cameraScalePos = light.Position * Camera.Scale;
                    cameraScalePos += new Vector3(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2, 0);
                    cameraScalePos -= new Vector3(PBConverter.MeterToPixel(Camera.position) * Camera.Scale, 0);
                    LightEffect.Parameters["lightPosition"].SetValue(cameraScalePos);
                    LightEffect.Parameters["NormalMap"].SetValue(_normalMap);
                    LightEffect.Parameters["lightDecay"].SetValue(light.LightDecay * Camera.Scale);
                    LightEffect.Parameters["lightColor"].SetValue(light.Color);
                    LightEffect.Parameters["coneDirection"].SetValue(light.Direction);
                    LightEffect.Parameters["screenHeight"].SetValue(GraphicsDevice.Viewport.Height);
                    LightEffect.Parameters["screenWidth"].SetValue(GraphicsDevice.Viewport.Width);
                    LightEffect.Parameters["lightAngle"].SetValue(0f);
                    LightEffect.Parameters["DepthMap"].SetValue(_depthMap);
                    LightEffect.Parameters["specularStrength"].SetValue(light.SpecularPower);

                    if (light.LightType == LightType.Point)
                        LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredPointLight"];
                    else
                        if (light.LightType == LightType.Spot)
                            LightEffect.CurrentTechnique = LightEffect.Techniques["DeferredSpotLight"];

                    LightEffect.CurrentTechnique.Passes[0].Apply();

                    GraphicsDevice.BlendState = BlendBlack;

                    GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, Vertices, 0, 2);
                }
            GraphicsDevice.SetRenderTarget(null);

            return _shadowMap;
        }

        public static BlendState BlendBlack = new BlendState()
        {
            ColorBlendFunction = BlendFunction.Add,
            ColorSourceBlend = Blend.One,
            ColorDestinationBlend = Blend.One,

            AlphaBlendFunction = BlendFunction.Add,
            AlphaSourceBlend = Blend.SourceAlpha,
            AlphaDestinationBlend = Blend.One
        };






        MouseState mouseState;
        float cellx;
        float celly;
        float prevcellx;
        float prevcelly;
        int ttl = 160;
        public static bool isFocused = true;
        MouseState prevMouse;
        private bool RefreshViewport;
        private bool debugOn;
        private bool getLightProperties;
        Rectangle scriptRectangle;
        private void UpdateGame()
        {

            mouseState = Mouse.GetState();
            int mx = mouseState.X;
            int my = mouseState.Y;
            prevcellx = cellx;
            prevcelly = celly;
            ttl--;
            /*mapX = mx + Camera.Position.X;
            mapY = my + Camera.Position.Y;
            mapX /= Camera.Scale;
            mapY /= Camera.Scale;*/
            mapX = mx  + PBConverter.MeterToPixel(Camera.position.X) * Camera.Scale-GraphicsDevice.Viewport.Width/2;
            mapY = my  + PBConverter.MeterToPixel(Camera.position.Y) * Camera.Scale-GraphicsDevice.Viewport.Height/2;
            mapX /= Camera.Scale;
            mapY /= Camera.Scale;
           /* mapX = mx/Camera.Scale;
            mapY = my/Camera.Scale;
            mapX -= GraphicsDevice.Viewport.Width / 2;
            mapY -= GraphicsDevice.Viewport.Height / 2;
            mapX -= PBConverter.MeterToPixel(Camera.position.X) / Camera.Scale;
            mapY -= PBConverter.MeterToPixel(Camera.position.Y) / Camera.Scale;*/
            /*cameraScalePos = light.Position * Camera.Scale;
            cameraScalePos += new Vector3(ScreenManager.GraphicsDevice.Viewport.Width / 2, ScreenManager.GraphicsDevice.Viewport.Height / 2, 0);
            cameraScalePos -= new Vector3(PBConverter.MeterToPixel(Camera.position) * Camera.Scale, 0);*/

            if (Map != null && isFocused)
            {
                cellx = mapX / (float)Map.TileWidth;
                celly = mapY / (float)Map.TileHeight;
                if (mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height && mouseState.LeftButton == ButtonState.Pressed)
                {
                    if ((int)celly != (int)prevcelly || (int)cellx != (int)prevcellx)
                        ttl = 0;
                    switch (bttPress)
                    {
                        case ButtonPressed.Move:

                            Vector2 a = PBConverter.PixelToMeter(new Vector2(mouseState.X - GraphicsDevice.Viewport.Width / 2, mouseState.Y - GraphicsDevice.Viewport.Height / 2));
                            cameraObj.Body.LinearVelocity = a / 2;
                            break;
                        case ButtonPressed.Draw:
                            if (cellx >= 0 && celly >= 0 && cellx < Map.MapWidth && celly < Map.MapHeight)
                            {
                                if (!treetexture)
                                {
                                    if (ListBoxTextureNames.SelectedIndex >= 0)
                                        Map.SetCellIndex((int)cellx, (int)celly, ListBoxTextureNames.SelectedIndex);
                                }
                                else
                                    if (textureTree.SelectedNode != null)
                                        Map.SetCellIndex((int)cellx, (int)celly, treeIndex);
                            }
                            break;
                        case ButtonPressed.Wall:
                            if (cellx >= 0 && celly >= 0 && cellx <= Map.MapWidth && celly <= Map.MapHeight)
                            {
                                if (ttl <= 0)
                                { Map.SetSolidCell((int)cellx, (int)celly); ttl = 10; }
                            }
                            break;
                        case ButtonPressed.Delete:
                            if (cellx >= 0 && celly >= 0 && cellx <= Map.MapWidth && celly <= Map.MapHeight)
                            {
                                Map.SetCellIndex((int)cellx, (int)celly, -1);
                            }
                            break;
                        case ButtonPressed.PlaceObj:
                            if (cellx >= 1 && celly >= 1 && cellx < Map.MapWidth && celly < Map.MapHeight && prevMouse.LeftButton == ButtonState.Released)
                            {
                                PBEditorObject b = PBEditorObj.objectDictionary[editorObjectKey];
                                AddObjectFrom(b, PBConverter.PixelToMeter(new Vector2(mapX, mapY)));
                                bttPress = ButtonPressed.None;
                                if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftControl) && editorObjectKey != "Player")
                                {
                                    tileDisplay1.Cursor = Cursors.Cross;
                                    bttPress = ButtonPressed.PlaceObj;
                                }
                            }
                            break;
                        case ButtonPressed.EditObj:
                            if (cellx >= 1 && celly >= 1 && cellx < Map.MapWidth && celly < Map.MapHeight && prevMouse.LeftButton == ButtonState.Released)
                            {
                                if (treeView1.Nodes.Count >= 0)
                                    editorObjects[treeView1.SelectedNode.Text].position = PBConverter.PixelToMeter(new Vector2(mapX, mapY));
                            }
                            break;
                        case ButtonPressed.PlaceLight:
                            if (prevMouse.LeftButton == ButtonState.Released)
                            {
                                string scn;
                                int i = 0;
                                while (true)
                                {
                                    scn = "Point_" + i.ToString();
                                    if (!LightsListBox.Items.Contains(scn))
                                    {
                                        break;
                                    }
                                    i++;
                                }
                                Lights.Add(scn, new PointLight()
                                {
                                    ScriptName = scn,
                                    Color = lightTempColor.ToVector4(),
                                    Power = (float)numericUpDown1.Value / 100f,
                                    LightDecay = (int)numericUpDown3.Value,
                                    Position = new Vector3(mapX, mapY, (float)numericUpDown4.Value),
                                    IsEnabled = true

                                });
                                LightsListBox.Items.Add(scn);
                                tileDisplay1.Cursor = Cursors.Default;
                                bttPress = ButtonPressed.None;
                            }
                            break;
                        case ButtonPressed.PlaceSpotLight:
                            if (prevMouse.LeftButton == ButtonState.Released)
                            {
                                string scn;
                                int i = 0;
                                while (true)
                                {
                                    scn = "Spot_" + i.ToString();
                                    if (!LightsListBox.Items.Contains(scn))
                                    {
                                        break;
                                    }
                                    i++;
                                }
                                Lights.Add(scn, new SpotLight()
                                {
                                    ScriptName = scn,
                                    Color = lightTempColor.ToVector4(),
                                    Power = (float)numericUpDown1.Value / 100f,
                                    LightDecay = (int)numericUpDown3.Value,
                                    Position = new Vector3(mapX, mapY, (float)numericUpDown4.Value),
                                    Direction = new Vector3(0, 1, 0),
                                    IsEnabled = true

                                });
                                LightsListBox.Items.Add(scn);
                                tileDisplay1.Cursor = Cursors.Default;
                                bttPress = ButtonPressed.None;
                            }
                            break;


                    }
                }

                
                switch (bttPress)
                {

                    case ButtonPressed.EditObj:
                        if (mouseState.RightButton == ButtonState.Pressed)
                        {
                            Vector2 temp = new Vector2(mapX, mapY) - PBConverter.MeterToPixel(editorObjects[treeView1.SelectedNode.Text].position);
                            editorObjects[treeView1.SelectedNode.Text].rotation = (float)Math.Atan2(Convert.ToDouble(temp.X), -Convert.ToDouble(temp.Y));
                        }
                        break;
                    case ButtonPressed.Draw:
                    case ButtonPressed.Wall:
                    case ButtonPressed.Move:
                        if (mouseState.RightButton == ButtonState.Pressed && prevMouse.RightButton == ButtonState.Released)
                        {
                            cameraObj.Position = PBConverter.PixelToMeter(new Vector2(mapX, mapY));
                        }
                        break;
                    case ButtonPressed.EditLight: if (LightsListBox.SelectedIndex >= 0)
                        {
                            string s = (string)LightsListBox.SelectedItem;
                            int i = LightsListBox.SelectedIndex;
                            if (i != prevLightIndex)
                            {
                                getLightProperties = false;
                                System.Drawing.Color col = System.Drawing.Color.FromArgb(Lights[s].PureColor.A, Lights[s].PureColor.R, Lights[s].PureColor.G, Lights[s].PureColor.B);
                                pictureBox2.BackColor = col;
                                numericUpDown1.Value = Convert.ToDecimal(Lights[s].Power * 100f);
                                numericUpDown3.Value = Convert.ToDecimal(Lights[s].LightDecay);
                                numericUpDown4.Value = Convert.ToDecimal(Lights[s].Position.Z);
                            }
                            else
                            {
                                Lights[s].Power = (float)numericUpDown1.Value / 100f;
                                Lights[s].LightDecay = (int)numericUpDown3.Value;
                                Lights[s].Position = new Vector3(Lights[s].Position.X, Lights[s].Position.Y, (float)numericUpDown4.Value);
                                Lights[s].Color = new Color(pictureBox2.BackColor.R, pictureBox2.BackColor.G, pictureBox2.BackColor.B, pictureBox2.BackColor.A).ToVector4();
                            }
                            if (mouseState.RightButton == ButtonState.Pressed && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                            {

                                Vector3 temp = new Vector3(mapX, mapY, Lights[s].Direction.Z * 2) - Lights[s].Position;
                                temp.Normalize();
                                Vector2 tempL = new Vector2(mapX, mapY) -
                                    PBConverter.MeterToPixel(new Vector2(Lights[s].Position.X,Lights[s].Position.Y));
                                double angleL = (float)Math.Atan2(Convert.ToDouble(temp.X), -Convert.ToDouble(temp.Y));
                                Console.WriteLine(temp.X+" "+" "+temp.Y+" "+angleL);
                                float y = -(float)Math.Cos(angleL);
                                float x = (float)Math.Sin(angleL);
                                Lights[s].Direction = new Vector3(-temp.X, -temp.Y, 0);

                            }
                            if (mouseState.LeftButton == ButtonState.Pressed && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                            {
                                if (mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height && Map != null && isFocused)
                                {
                                    Vector3 temp = new Vector3(mapX, mapY, Lights[s].Position.Z);
                                    Lights[s].Position = temp;
                                }
                            }
                        }
                        break;
                    case ButtonPressed.PlaceScript:
                        if (mouseState.LeftButton == ButtonState.Pressed && prevMouse.LeftButton == ButtonState.Released && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                        {
                            scriptRectStarted = true;
                            scriptRecLocation = new Vector2(mapX, mapY);
                        }
                        else if (mouseState.LeftButton == ButtonState.Pressed && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                        {
                            scriptRecSize = new Vector2(mapX - scriptRecLocation.X, mapY - scriptRecLocation.Y);
                            if (scriptRecSize.Y < 0 && scriptRecSize.X < 0)
                            {
                                scriptRectangle = new Rectangle((int)(scriptRecLocation.X + scriptRecSize.X), (int)(scriptRecLocation.Y + scriptRecSize.Y), (int)scriptRecSize.X * -1, (int)scriptRecSize.Y * -1);
                            }
                            else
                                if (scriptRecSize.Y < 0 && scriptRecSize.X > 0)
                                {
                                    scriptRectangle = new Rectangle((int)scriptRecLocation.X, (int)mapY, Math.Abs((int)scriptRecLocation.X - (int)mapX), Math.Abs((int)scriptRecLocation.Y - (int)mapY));
                                }
                                else if (scriptRecSize.X < 0 && scriptRecSize.Y > 0)
                                {
                                    scriptRectangle = new Rectangle((int)mapX, (int)scriptRecLocation.Y, Math.Abs((int)scriptRecLocation.X - (int)mapX), Math.Abs((int)scriptRecLocation.Y - (int)mapY));
                                }
                                else
                                    scriptRectangle = new Rectangle((int)scriptRecLocation.X, (int)scriptRecLocation.Y, (int)scriptRecSize.X, (int)scriptRecSize.Y);
                        }

                        else if (mouseState.LeftButton == ButtonState.Released && scriptRectStarted == true)
                        {
                            if (scriptRectangle.Height >= 2&& scriptRectangle.Width >=2)
                            {
                                bttPress = ButtonPressed.None;
                                string a;
                                int i = 0;
                                while (true)
                                {
                                    a = "EventObject_" + i.ToString();
                                    if (!checkNodes(scriptTreeView.Nodes, a))
                                    {
                                        scriptTreeView.Nodes.Add(a);
                                        break;
                                    }
                                    i++;
                                }
                                eventObjects.Add(a, new PBEventObject(world,
                                    PBConverter.PixelToMeter(scriptRectangle.Height),
                                    PBConverter.PixelToMeter(scriptRectangle.Width),
                                    PBConverter.PixelToMeter(new Vector2(scriptRectangle.X, scriptRectangle.Y)),
                                    content, null));

                                scriptTreeView.Sort();
                            }
                            else scriptRectangle = Rectangle.Empty;

                            scriptRectStarted = false;
                        }
                        break;
                    case ButtonPressed.EditScript:
                        if (mouseState.LeftButton == ButtonState.Pressed && prevMouse.LeftButton == ButtonState.Released && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                        {
                            scriptRectStarted = true;
                            scriptRecLocation = new Vector2(mapX, mapY);
                        }
                        else if (mouseState.LeftButton == ButtonState.Pressed && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                        {
                            scriptRecSize = new Vector2(mapX - scriptRecLocation.X, mapY - scriptRecLocation.Y);
                            if (scriptRecSize.Y < 0 && scriptRecSize.X < 0)
                            {
                                scriptRectangle = new Rectangle((int)(scriptRecLocation.X + scriptRecSize.X), (int)(scriptRecLocation.Y + scriptRecSize.Y), (int)scriptRecSize.X * -1, (int)scriptRecSize.Y * -1);
                            }
                            else
                                if (scriptRecSize.Y < 0 && scriptRecSize.X > 0)
                                {
                                    scriptRectangle = new Rectangle((int)scriptRecLocation.X, (int)mapY, Math.Abs((int)scriptRecLocation.X - (int)mapX), Math.Abs((int)scriptRecLocation.Y - (int)mapY));
                                }
                                else if (scriptRecSize.X < 0 && scriptRecSize.Y > 0)
                                {
                                    scriptRectangle = new Rectangle((int)mapX, (int)scriptRecLocation.Y, Math.Abs((int)scriptRecLocation.X - (int)mapX), Math.Abs((int)scriptRecLocation.Y - (int)mapY));
                                }
                                else
                                    scriptRectangle = new Rectangle((int)scriptRecLocation.X, (int)scriptRecLocation.Y, (int)scriptRecSize.X, (int)scriptRecSize.Y);
                        }

                        else if (mouseState.LeftButton == ButtonState.Released && scriptRectStarted == true)
                        {

                            bttPress = ButtonPressed.None;
                            eventObjects[ignoreScript].Body.Dispose();
                            List<PBScriptAction> tempactions = eventObjects[ignoreScript].Actions;
                            eventObjects.Remove(ignoreScript);
                            eventObjects.Add(ignoreScript, new PBEventObject(world,
                                PBConverter.PixelToMeter(scriptRectangle.Height),
                                PBConverter.PixelToMeter(scriptRectangle.Width),
                                PBConverter.PixelToMeter(new Vector2(scriptRectangle.X, scriptRectangle.Y)),
                                content, null));
                            eventObjects[ignoreScript].Actions = tempactions;
                            scriptTreeView.Sort();
                            scriptRectStarted = false;
                            ignoreScript = null;
                        }
                        break;
                    case ButtonPressed.PositionNeeded:
                        if (mouseState.LeftButton == ButtonState.Pressed && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                        {
                            if (cellx >= 1 && celly >= 1 && cellx < Map.MapWidth && celly < Map.MapHeight && prevMouse.LeftButton == ButtonState.Released)
                            {
                                needClicked = true;
                                needPosLoc = new Vector2(mapX, mapY);
                            }
                        }
                        //////////////////////---------------------------------AICIP-------------------------------
                        if (needClicked && mouseState.RightButton == ButtonState.Pressed && mx > 0 && my > 0 && mx < tileDisplay1.Width && my < tileDisplay1.Height)
                        {
                            Vector2 temp = new Vector2(mapX, mapY) - (needPosLoc);
                            needPosRot = (float)Math.Atan2(Convert.ToDouble(temp.X), -Convert.ToDouble(temp.Y));
                        }
                        break;
                }
            }
            prevMouse = mouseState;
            Camera.Update();
            world.Step(1 / 60f);
            cameraObj.Body.LinearVelocity = Vector2.Zero;
            if (RefreshViewport == true)
            {
                float lastScale = Camera.Scale;
                Camera = new PBCamera(tileDisplay1.GraphicsDevice.Viewport);
                Camera.Scale = lastScale;
                Camera.Follow(cameraObj);
                PresentationParameters pp = GraphicsDevice.PresentationParameters;
                int w = pp.BackBufferWidth;
                int h = pp.BackBufferHeight;
                SurfaceFormat f = pp.BackBufferFormat;

                _colorMap = new RenderTarget2D(GraphicsDevice, w, h);
                _depthMap = new RenderTarget2D(GraphicsDevice, w, h);
                _normalMap = new RenderTarget2D(GraphicsDevice, w, h);
                _shadowMap = new RenderTarget2D(GraphicsDevice, w, h, false, f, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);
                RefreshViewport = false;
            }
            prevLightIndex = LightsListBox.SelectedIndex;
        }

        bool scriptRectStarted = false;
        Vector2 scriptRecLocation;
        Vector2 scriptRecSize;
        int prevLightIndex = -5;
        private ScreenManager screenManager;
        public void AddObjectFrom(PBEditorObject obj, Vector2 v)
        {
            string a;
            int i = 0; 
            if (obj.type == PBEditorObject.Type.None)
            {
                while (true)
                {
                    a = obj.name + "_" + i.ToString();
                    if (!checkNodes(treeView1.Nodes, a))
                    {
                        treeView1.Nodes.Add(a);
                        break;
                    }
                    i++;
                }
                editorObjects.Add(a, new PBEditorObject(obj.textureC, obj.textureN, blankTexture, world, 1f, 1f, v, obj.type, a));
                return;
            }
            if (obj.Width == 0) obj.Width = 1;
            if (obj.Height == 0) obj.Height = 1;
            float width =  obj.Width;
           // float width = (float)obj.textureC.Width / 64f * obj.Width;
            float height = obj.Height;
            //float height = (float)obj.textureC.Height / 64f * obj.Height;
            
            if (obj.type != PBEditorObject.Type.Spawn)
            {
                
                while (true)
                {
                    a = obj.name +"_" + i.ToString();
                    if (!checkNodes(treeView1.Nodes,a))
                    {
                        treeView1.Nodes.Add(a);
                        break;
                    }
                    i++;
                }
                editorObjects.Add(a,new PBEditorObject(obj.textureC, obj.textureN, blankTexture, world, width, height, v, obj.type, a));
            }
            else
            {
                if(!editorObjects.Keys.Contains<string>(obj.name))
                editorObjects.Add(obj.name,new PBEditorObject(obj.textureC, obj.textureN, blankTexture, world, width, height, v, obj.type, obj.name));
                treeView1.Nodes.Add(obj.name);
            }
        }
        bool checkNodes(TreeNodeCollection nodes,string str)
                        {
                             bool result=false;
                             for(int i=0;i<nodes.Count;i++)
                             {
                                 if (str == nodes[i].Text)
                                     return true;
                             }
                                 return result;
                         }
        public void AddObject(PBEditorObject obj)
        {
            editorObjects.Add(obj.name,obj);
            treeView1.Nodes.Add(obj.name);
        }
        internal void AddLight(PBLight p)
        {
            string scn;
            int i = 0;
            while (true)
            {
                scn = p.LightType+"_" + i.ToString();
                if (!LightsListBox.Items.Contains(scn))
                {
                    break;
                }
                i++;
            }
            p.ScriptName = scn;
            Lights.Add(scn,p);
            LightsListBox.Items.Add(scn);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            label1.Hide();
            label7.Hide();
            tileDisplay1.Cursor = Cursors.Hand;
            if (bttPress == ButtonPressed.Move)
            {
               if (button18.Visible == true)
                {
                    label7.Hide();
                    bttPress = ButtonPressed.PositionNeeded;
                }
                else
                {
                    label7.Hide();
                    bttPress = ButtonPressed.None;

                }
            }
            else
            {
                label7.Show();
                bttPress = ButtonPressed.Move;
            }
        }
        private static Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }
        private void ListBoxTextureNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListBoxTextureNames.SelectedIndex >= 0)
            {
                label7.Hide();
                pictureBox1.Image = textureImages[ListBoxTextureNames.SelectedIndex];
                bttPress = ButtonPressed.Draw;
                label1.Text = "DRAWING";
                label1.Show();
                treetexture = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label7.Hide();
            tileDisplay1.Cursor = Cursors.Default;
            if (bttPress == ButtonPressed.Draw)
            { bttPress = ButtonPressed.None; label1.Hide(); }
            else
            { bttPress = ButtonPressed.Draw; label1.Text = "Drawing"; label1.Show(); }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label7.Hide();
            tileDisplay1.Cursor = Cursors.Default;
            if (bttPress == ButtonPressed.Wall)
            { bttPress = ButtonPressed.None; label1.Hide(); }
            else
            { bttPress = ButtonPressed.Wall; label1.Text = "Walls"; label1.Show(); }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

            EditorForm.isFocused = true;
            SizeSetting form = new SizeSetting();
            form.ShowDialog();
            if (form.OkPressed == true)
            {
                try
                {
                    levelName = form.textBox3.Text;

                    world.Clear();
                    Lights.Clear();
                    LightsListBox.Items.Clear();
                    editorObjects.Clear();
                    treeView1.Nodes.Clear();
                    scriptTreeView.Nodes.Clear();
                    cameraObj = new PBObject(world, .01f, .01f, Vector2.Zero);
                    cameraObj.Body.CollidesWith = Category.None;
                    cameraObj.Body.CollisionCategories = Category.None;
                    cameraObj.Body.BodyType = BodyType.Dynamic;
                    Camera.Follow(cameraObj);
                    Map = new PBLayer(80, 80,
                        int.Parse(form.textBox1.Text),
                        int.Parse(form.textBox2.Text),
                        world,
                        tileTexturesColor,
                        tileTexturesNormal);
                    levelList.Items.Add(levelName+".pblvl");
                }
                catch
                {
                    MessageBox.Show("Wrong Data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            label7.Hide();
            tileDisplay1.Cursor = Cursors.UpArrow;
            if (bttPress == ButtonPressed.Delete)
            { bttPress = ButtonPressed.None; label1.Hide(); }
            else
            { bttPress = ButtonPressed.Delete; label1.Text = "Deleting"; label1.Show(); }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Project Build Level|*.pblvl";
            save.FileName = levelName;
            if (save.ShowDialog() == DialogResult.OK)
            {
                SaveMap(save.FileName);
            }
        }
        public void SaveMap(string path)
        {

            try
            {
                Map.NewSave(path, editorObjects, Lights, eventObjects);
                MessageBox.Show(string.Format("Map {0} safed succesfully!",levelName));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        bool PlayerInTree()
        {
            foreach (TreeNode n in treeView1.Nodes)
            {
                if (n.Text == "Player")
                    return true;
            }
            return false;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            label7.Hide();
            isFocused = false;
            AddObjectForm form = new AddObjectForm(PBEditorObj,PBEditorObj.objectNames,PBEditorObj.itemNames);
            form.ShowDialog();
            if (form.OkPressed == true)
            {
                if (form.editorObjectKey == "Player" && PlayerInTree())
                {
                    MessageBox.Show("You have a spawn, just move it");
                }
                else
                {
                    bttPress = ButtonPressed.PlaceObj;
                    editorObjectKey = form.editorObjectKey;
                    tileDisplay1.Cursor = Cursors.Cross;
                }

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            label7.Hide();
            if (treeView1.SelectedNode!=null && treeView1.SelectedNode.Index>=0)
            {
                string key = treeView1.SelectedNode.Text;
                editorObjects[key].body.Dispose();
                editorObjects.Remove(key);
                treeView1.Nodes.Remove(treeView1.SelectedNode);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            label7.Hide();
            label8.Show();
            bttPress = ButtonPressed.EditObj;
            string a = e.Node.Text;
            if (a.Contains("_"))
                a = a.Remove(a.LastIndexOf("_"));
            label8.Text = PBEditorObj.objectDetails[a];
        }
        private void tileDisplay1_SizeChanged(object sender, EventArgs e)
        {
            RefreshViewport = true;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            debugOn = debugOn ^ true;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Hide();
            if (bttPress != ButtonPressed.Move && bttPress!=ButtonPressed.PositionNeeded)
                bttPress = ButtonPressed.None;
            if (tabControl1.SelectedIndex == 0 || tabControl1.SelectedIndex == 1 ||tabControl1.SelectedIndex==3)
                AmbientLight = Color.White;
            else
                if (tabControl1.SelectedIndex == 2)
                    AmbientLight = new Color(.1f, .1f, .1f);
        }

        public void DrawDebugRenderTargets(SpriteBatch spriteBatch)
        {
            // Draw some debug textures
            spriteBatch.Begin();

            Rectangle size = new Rectangle(0, 0, _colorMap.Width / 4, _colorMap.Height / 4);
            var position = new Vector2(0, GraphicsDevice.Viewport.Height - size.Height);
            spriteBatch.Draw(
                _colorMap,
                new Rectangle(
                    (int)position.X, (int)position.Y,
                    size.Width,
                    size.Height),
                Color.White);

            spriteBatch.Draw(
                 _depthMap,
                 new Rectangle(
                     (int)position.X + size.Width, (int)position.Y,
                     size.Width,
                     size.Height),
                 Color.White);

            spriteBatch.Draw(
                _normalMap,
                new Rectangle(
                    (int)position.X + size.Width * 2, (int)position.Y,
                    size.Width,
                    size.Height),
                    Color.White);

            spriteBatch.Draw(
                _shadowMap,
                new Rectangle(
                    (int)position.X + size.Width * 3, (int)position.Y,
                    size.Width,
                    size.Height),
                Color.White);

            spriteBatch.End();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            label7.Hide();
            ColorDialog col = new ColorDialog();
            if (col.ShowDialog() == DialogResult.OK)
            {
                lightTempColor = new Color(col.Color.R, col.Color.G, col.Color.B, col.Color.A);
                pictureBox2.BackColor = col.Color;
                if (bttPress == ButtonPressed.PlaceLight)
                    bttPress = ButtonPressed.None;
                else
                    bttPress = ButtonPressed.PlaceLight;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            label7.Hide();
            ColorDialog col = new ColorDialog();
            if (col.ShowDialog() == DialogResult.OK)
            {
                lightTempColor = new Color(col.Color.R, col.Color.G, col.Color.B, col.Color.A);
                pictureBox2.BackColor = col.Color;
                if (bttPress == ButtonPressed.PlaceSpotLight)
                    bttPress = ButtonPressed.None;
                else
                    bttPress = ButtonPressed.PlaceSpotLight;
                isFocused = true;
            }
            else
            {
                isFocused = true;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            label7.Hide();
            if (LightsListBox.SelectedIndex >= 0)
            {
                string key = LightsListBox.SelectedItem as string;
                int index = LightsListBox.SelectedIndex;
                Lights.Remove(key);
                LightsListBox.Items.RemoveAt(index);
            }
        }

        private void LightsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LightsListBox.SelectedIndex >= 0)
            {
                label7.Hide();
                prevLightIndex = -1;
                bttPress = ButtonPressed.EditLight;
                getLightProperties = true;
                propertyGrid2.SelectedObject = Lights[LightsListBox.SelectedItem as string];
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            isFocused = false;
            ColorDialog a = new ColorDialog();
            if (a.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.BackColor = a.Color;
                isFocused = true;
            }
            else
            {
                isFocused = false;
            }
        }
       
        private void fileToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            isFocused = true;
        }

        private void fileToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            isFocused = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.EnableVisualStyles();
            Mouse.WindowHandle = screenManager.Game.Window.Handle;
            if(OnLevel==false)
                LoadingScreen.Load(screenManager, true, PlayerIndex.One,
                   new BackgroundScreen("darktile", BackgroundScreen.BackgroundType.Tile, Vector2.Zero),
                   new BackgroundScreen("nameBackground", BackgroundScreen.BackgroundType.Simple, new Vector2(30, 30)), 
                   new MainMenuScreen());
            tileTexture.Dispose();
            textureImages.Clear();
            _colorMap.Dispose();
            _depthMap.Dispose();
            _normalMap.Dispose();
            _shadowMap.Dispose();
            tileDisplay1.Dispose();
            spriteBatch.Dispose();
            content.Dispose();
            
            tileTexturesColor.Clear();
            tileTexturesNormal.Clear();
            debug.Dispose();
            world.Clear();
            editorObjects.Clear();
            PBEditorObj.nameTextureC.Clear();
            PBEditorObj.nameTextureN.Clear();
            PBEditorObj.objectDetails.Clear();
            PBEditorObj.objectDictionary.Clear();
            PBEditorObj.objectImages.Clear();
            PBEditorObj.objectNames.Clear();
            PBEditorObj.objectTextures[0].Clear();
            PBEditorObj.objectTextures[1].Clear();
            PBEditorObj.textureNameTOName.Clear();

            this.Dispose();

        }

        private void testLoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Project Build Level|*.pblvl";
            open.InitialDirectory=Application.StartupPath+"\\levels\\";
            isFocused = false;
            if (open.ShowDialog() == DialogResult.OK)
            {
                levels.Clear();
                levelList.Items.Clear();
                OpenMap(open.FileName);
            }
            else
            {
                isFocused = false;
            }
        
        }
        public void OpenMap(string path)
        {
            levelName = path.Substring(path.LastIndexOf("\\")+1);
            
            world.Clear();
            Lights.Clear();
            LightsListBox.Items.Clear();
            editorObjects.Clear();
            treeView1.Nodes.Clear();
            PBEditorObj.actionForms.Clear();
            scriptTreeView.Nodes.Clear();
            PBEditorObj.actions.Clear();
            cameraObj = new PBObject(world, .01f, .01f, Vector2.Zero);
            cameraObj.Body.CollidesWith = Category.None;
            cameraObj.Body.CollisionCategories = Category.None;
            cameraObj.Body.BodyType = BodyType.Dynamic;
            Camera.Follow(cameraObj);
            Map = PBLayer.LoadEditor(path, ref editorObjects, ref Lights,ref eventObjects ,world, tileTexturesColor, tileTexturesNormal, PBEditorObj, this);
            foreach(string key in PBEditorObj.actions.Keys)
            {
                ScriptForm a = new ScriptForm(PBEditorObj,treeView1.Nodes, LightsListBox.Items,levelList.Items);
                PBEditorObj.actionForms.Add(key, a);
            }
            isFocused = true;
        }
        bool OnLevel = false;
        private void testTheLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(Map!=null)
            {
            string path = Application.StartupPath;
            DirectoryInfo dir = new DirectoryInfo(path);
            if(!dir.GetDirectories().Contains(new DirectoryInfo(path+"\\levels\\")))
            {
                dir.CreateSubdirectory("levels");
            }
            int i1 = levelName.LastIndexOf('\\')+1;
            int i2 = levelName.LastIndexOf('/')+1;
            levelName = levelName.Substring(Math.Max(i1, i2));
            path += "\\levels\\"+levelName+".pblvl";
            path=path.Replace(".pblvl", "");
            path += ".pblvl";
            SaveMap(path);
            path = path.Substring(path.LastIndexOf("\\") + 1);
            path = "levels\\" + path;
            LoadingScreen.Load(screenManager, true, 0, new PBLevel(path));
            OnLevel = true;
            this.Close();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            bttPress = ButtonPressed.PlaceScript;

        }

        private void button13_Click(object sender, EventArgs e)
        {
            Camera.Scale += 0.1f;
            Camera.Scale =(float) Math.Round(Convert.ToDouble(Camera.Scale), 2, MidpointRounding.ToEven);
            label9.Text = "Scale: " + Camera.Scale.ToString() + "x";
            Camera.SetView();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (Camera.Scale > 0.1f)
            {
                Camera.Scale -= 0.1f;
                Camera.Scale = (float)Math.Round(Convert.ToDouble(Camera.Scale), 2, MidpointRounding.ToEven);
                label9.Text = "Scale: " + Camera.Scale.ToString() + "x";
                Camera.SetView();
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (scriptTreeView.SelectedNode == null)
                MessageBox.Show("Please select an event object!");
            else
                if (!scriptTreeView.SelectedNode.Text.Contains("Event"))
                    MessageBox.Show("Please select an event object! A main node!");
                else
            {
                string key = scriptTreeView.SelectedNode.Text;
                eventObjects[key].Body.Dispose();
                foreach (var ac in eventObjects[key].Actions)
                {
                    string actionkey = ac.OwnScriptName;
                    if (!PBEditorObj.actions.Keys.Contains<string>(actionkey))
                    PBEditorObj.actions.Remove(actionkey);
                    if (!PBEditorObj.actionForms.Keys.Contains<string>(actionkey))
                        PBEditorObj.actionForms.Remove(actionkey);
                }
                eventObjects.Remove(key);
                scriptTreeView.Nodes.Remove(scriptTreeView.SelectedNode);
            }
        }
        string ignoreScript;
        private void button14_Click(object sender, EventArgs e)
        {
            if (scriptTreeView.SelectedNode == null)
                MessageBox.Show("Please select an event object!");
            else
                if (!scriptTreeView.SelectedNode.Text.Contains("Event"))
                    MessageBox.Show("Please select an event object! A main node!");
                else
            {
                bttPress = ButtonPressed.EditScript;
                ignoreScript = scriptTreeView.SelectedNode.Text;
            }
        }

        


        PBScriptAction actionToBeAdded;
        private Vector2 needPosLoc;
        private float needPosRot;
        private bool needClicked = false;
        ScriptForm lastForm;
        private bool canBeUploaded;
        private void button16_Click(object sender, EventArgs e)
        {
            isFocused = false;
            ScriptForm form = new ScriptForm(PBEditorObj,treeView1.Nodes,LightsListBox.Items,levelList.Items);
            form.ShowDialog();
            actionToBeAdded=form.action;
            if (form.OkPressed == true)
            {
                if (form.NeedPosition == true)
                    bttPress = ButtonPressed.PositionNeeded;
                    
                button18.Show();
                button19.Show();
                lastForm = form;
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            bttPress = ButtonPressed.None;
            needClicked = false;
            needPosRot = 0f;
            button19.Hide();
            button18.Hide();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (scriptTreeView.SelectedNode == null)
                MessageBox.Show("Please select an event object!");
            else
            if (!scriptTreeView.SelectedNode.Text.Contains("Event"))
                MessageBox.Show("Please select an event object! A main node!");
            else
            {
                string type = "";
                switch (actionToBeAdded.type)
                {
                    case PBScriptAction.ScriptType.Object:
                        actionToBeAdded.newPosition = PBConverter.PixelToMeter(needPosLoc);
                        actionToBeAdded.newRotation = needPosRot;
                        type = "ObjectScript_";
                        break;
                    case PBScriptAction.ScriptType.Light:
                        actionToBeAdded.newPositionLight = new Vector3((needPosLoc), 0);
                        float x = -(float)Math.Sin(Convert.ToDouble(needPosRot));
                        float y = (float)Math.Cos(Convert.ToDouble(needPosRot));
                        float r = (float)Math.Atan2(Convert.ToDouble(x), Convert.ToDouble(y));
                        actionToBeAdded.newDirectionLight = new Vector3(x, y, 0);
                        type = "LightScript_";
                        break;
                    case PBScriptAction.ScriptType.BigMessage:
                        type = "MessageScript_";
                        break;
                    case PBScriptAction.ScriptType.Quest:
                        type = "QuestScript_";
                            actionToBeAdded.location = PBConverter.PixelToMeter(needPosLoc);
                        break;
                    case PBScriptAction.ScriptType.Spawn:
                        actionToBeAdded.locationToSpawn = PBConverter.PixelToMeter(needPosLoc);
                        type = "SpawnScript_";
                        break;
                    case PBScriptAction.ScriptType.Teleport:
                        type = "TeleportScript_";
                        if (actionToBeAdded.positionToGo == Vector2.Zero)
                            actionToBeAdded.positionToGo = PBConverter.PixelToMeter(needPosLoc);
                        break;
                    case PBScriptAction.ScriptType.Sound:
                        type = "SoundScript_";
                        break;
                }
                string a=null;
                int i = 0;
                bool done = false;
                while (true)
                {
                    a = type + i.ToString();
                    
                        if (!PBEditorObj.actions.Keys.Contains<string>(a))
                        {
                            actionToBeAdded.OwnScriptName = a;
                            scriptTreeView.SelectedNode.Nodes.Add(a);
                            break;
                            
                        }
                        i++; 
                    
                }
                PBEditorObj.actions.Add(a, actionToBeAdded);
                PBEditorObj.actionForms.Add(a, lastForm);
                eventObjects[scriptTreeView.SelectedNode.Text].AddAction(actionToBeAdded);
                button19.Hide();
                button18.Hide();
                bttPress = ButtonPressed.None;
                needClicked = false;
                needPosRot = 0f;
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (scriptTreeView.SelectedNode == null)
                MessageBox.Show("Please select an script object!");
            else
            if (!scriptTreeView.SelectedNode.Text.Contains("Script"))
                MessageBox.Show("Please select an script object! A lower node!");
            else
            {
                string s = scriptTreeView.SelectedNode.Text;
                string eventScript = scriptTreeView.SelectedNode.Parent.Text;
                PBEditorObj.actionForms.Remove(s);
                eventObjects[eventScript].Actions.Remove(PBEditorObj.actions[s]);
                PBEditorObj.actions.Remove(s);
                scriptTreeView.SelectedNode.Remove();
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if (scriptTreeView.SelectedNode == null)
                MessageBox.Show("Please select an script object!");
            if (!scriptTreeView.SelectedNode.Text.Contains("Script"))
                MessageBox.Show("Please select an script object! A lower node!");
            else
            {
                string s = scriptTreeView.SelectedNode.Text;
                bool noform;
                ScriptForm form;
                if (!PBEditorObj.actionForms.Keys.Contains<string>(s))
                {
                    noform = true;
                    form = new ScriptForm(PBEditorObj, treeView1.Nodes, LightsListBox.Items,levelList.Items);
                }
                else
                {  
                    noform = false;
                    form = PBEditorObj.actionForms[s];
                }
                isFocused = false;
                form.ShowDialog();
                

                actionToBeAdded = form.action;
                if (form.OkPressed == true)
                {
                    if (form.NeedPosition == true)
                    {
                        bttPress = ButtonPressed.PositionNeeded;
                    }
                    button18.Show();
                    button19.Show();
                    if (!noform)
                    {
                        PBEditorObj.actionForms.Remove(s);
                        string eventScript = scriptTreeView.SelectedNode.Parent.Text;
                        eventObjects[eventScript].Actions.Remove(PBEditorObj.actions[s]);
                        PBEditorObj.actions.Remove(s);
                        scriptTreeView.SelectedNode.Remove();
                    }
                    lastForm = form;
                }

            }
        }

        private void scriptTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (!e.Node.Text.Contains("Event")&&e.Node.Text!="")
            {
                propertyGrid3.SelectedObject = PBEditorObj.actions[e.Node.Text];
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("For help hover the mouse over the buttons!", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void uploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Map != null)
            {
                canBeUploaded = false;
                foreach (string key in PBEditorObj.actions.Keys)
                    if (PBEditorObj.actions[key].levelToGo == "END")
                        canBeUploaded = true;
                if (canBeUploaded)
                {
                    string path = Application.StartupPath;
                    DirectoryInfo dir = new DirectoryInfo(path);
                    if (!dir.GetDirectories().Contains(new DirectoryInfo(path + "\\levels")))
                    {
                        dir.CreateSubdirectory("levels");
                    }
                    path += "\\levels\\" + levelName + ".pblvl";
                    path = path.Replace(".pblvl", "");
                    path += ".pblvl";
                    SaveMap(path);
                    PBData.Upload(path);
                    path = path.Substring(path.LastIndexOf("\\") + 1);
                    path = "levels\\" + path;
                    this.Close();
                }
                else
                    MessageBox.Show("You need at least one teleport to end script!");
            }
        }

        private void saveAsCampainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isFocused = false;
            if(levelName!=null)
                if (!levels.ContainsKey(levelName))
                {
                    levelName = levelName.Replace(".pblvl", "");
                    levels.Add(levelName, Application.StartupPath + "\\levels\\" + levelName + ".pblvl");
                    SaveMap(Application.StartupPath + "\\levels\\" + levelName + ".pblvl");
                }
                else
                    if(campainName!=null)
                    SaveMap(Application.StartupPath + "\\levels\\" +campainName+"\\"+levelName);

            new CampainForm(this).Show();
        }

       public Dictionary<string, string> levels = new Dictionary<string, string>();
       private bool treetexture;
        
        private void button22_Click(object sender, EventArgs e)
        {
            //Add Level
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Project Build Level|*.pblvl";
            open.InitialDirectory = Application.StartupPath + "\\levels\\";
            if (open.ShowDialog() == DialogResult.OK)
            {
                levels.Add(open.SafeFileName, open.FileName);
                levelList.Items.Add(open.SafeFileName);
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            //New Level
            if (levelName != null)
            {
                if (!levels.ContainsKey(levelName + ".pblvl"))
                {
                    if (!levels.ContainsKey(levelName))
                    {
                        SaveMap(Application.StartupPath + "\\levels\\" + levelName+".pblvl");
                        levels.Add(levelName + ".pblvl", Application.StartupPath + "\\levels\\" + levelName + ".pblvl");
                    }
                }
                else
                {
                    if (saveCheckBox.Checked)
                    {
                        if (campainName != null)
                            SaveMap(Application.StartupPath + "\\levels\\" + campainName + "\\" + levelName);
                        else
                            SaveMap(Application.StartupPath + "\\levels\\" + levelName);
                    }
                    else
                    if (MessageBox.Show("Do you want to save the previous map?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                    {
                        SaveFileDialog save = new SaveFileDialog();
                        save.Filter = "Project Build Level|*.pblvl";
                        save.FileName = levelName;
                        if (save.ShowDialog() == DialogResult.OK)
                        {
                            SaveMap(save.FileName);
                        }
                    }
                }
            }
            EditorForm.isFocused = true;
            SizeSetting form = new SizeSetting();
            form.ShowDialog();
            if (form.OkPressed == true)
            {
                try
                {
                    levelName = form.textBox3.Text;

                    world.Clear();
                    Lights.Clear();
                    LightsListBox.Items.Clear();
                    editorObjects.Clear();
                    treeView1.Nodes.Clear();
                    scriptTreeView.Nodes.Clear();
                    cameraObj = new PBObject(world, .01f, .01f, Vector2.Zero);
                    cameraObj.Body.CollidesWith = Category.None;
                    cameraObj.Body.CollisionCategories = Category.None;
                    cameraObj.Body.BodyType = BodyType.Dynamic;
                    Camera.Follow(cameraObj);
                    Map = new PBLayer(80, 80,
                        int.Parse(form.textBox1.Text),
                        int.Parse(form.textBox2.Text),
                        world,
                        tileTexturesColor,
                        tileTexturesNormal);
                    levelList.Items.Add(levelName + ".pblvl");
                }
                catch
                {
                    MessageBox.Show("Wrong Data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            //Remove Level
            if (levelList.SelectedIndex >= 0)
            {
                string s = levelList.SelectedItem as string;
                levels.Remove(s);
                levelList.Items.RemoveAt(levelList.SelectedIndex);
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            //Open Level
            if(levelName!=null)
            {
                if (levelList.SelectedIndex >= 0)
                {
                    if (!levels.ContainsKey(levelName + ".pblvl"))
                        if (!levels.ContainsKey(levelName))
                        {
                            SaveMap(Application.StartupPath + "\\levels\\" + levelName + ".pblvl");
                            levels.Add(levelName + ".pblvl", Application.StartupPath + "\\levels\\" + levelName + ".pblvl");
                            OpenMap(levels[levelList.SelectedItem as string]);
                        }
                        else
                            if (saveCheckBox.Checked)
                            {
                                if(campainName!=null)
                                SaveMap(Application.StartupPath + "\\levels\\"+campainName+"\\" + levelName);
                                else
                                    SaveMap(Application.StartupPath + "\\levels\\"+ levelName);
                                OpenMap(levels[levelList.SelectedItem as string]);
                            }
                            else
                                if (MessageBox.Show("Do you want to save the previous map?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                                {
                                    SaveFileDialog save = new SaveFileDialog();
                                    save.Filter = "Project Build Level|*.pblvl";
                                    save.FileName = levelName;
                                    if (save.ShowDialog() == DialogResult.OK)
                                    {
                                        SaveMap(save.FileName);
                                        OpenMap(levels[levelList.SelectedItem as string]);
                                    }
                                }
                                else
                                {
                                    OpenMap(levels[levelList.SelectedItem as string]);
                                }
                }
            }
            else
                OpenMap(levels[levelList.SelectedItem as string]);
        }

        private void loadCampainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Project Build Campaign|*pbcmp";
            string path=Application.StartupPath+"\\levels\\";
            
            open.InitialDirectory = path;
            if (open.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    levels.Clear();
                    levelList.Items.Clear(); 
                    using (StreamReader reader = new StreamReader(open.FileName))
                    {
                        string numeCampain = reader.ReadLine();
                        campainName = numeCampain;
                        int nrlvl = int.Parse(reader.ReadLine());
                        for (int i = 0; i < nrlvl; i++)
                        {
                            string lvlname = reader.ReadLine();
                            levelList.Items.Add(lvlname);
                            string temp = open.FileName.Remove(open.FileName.LastIndexOf('\\')) + "\\" + lvlname;
                            levels.Add(lvlname, temp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Campaign cannot be loaded!\n", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        int treeIndex;
        private void textureTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (textureTree.SelectedNode!=null)
            {
                label7.Hide();
                treeIndex = 0;
                if (textureTree.SelectedNode.Parent == null)
                    treeIndex = textureTree.SelectedNode.Index * 13;
                else
                    treeIndex = textureTree.SelectedNode.Parent.Index * 13 + textureTree.SelectedNode.Index+1;
                pictureBox1.Image = textureImages[treeIndex];
                bttPress = ButtonPressed.Draw;
                label1.Text = "DRAWING";
                label1.Show();
                treetexture = true;
            }
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
