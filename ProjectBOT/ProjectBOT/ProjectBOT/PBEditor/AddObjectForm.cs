﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProjectBOTEditor
{
    public partial class AddObjectForm : Form
    {
        public bool OkPressed = false;
        PBEditorObjects PBEdObj;
        public string editorObjectKey;
        private PBEditorObjects PBEditorObj;
        private List<string> list;
        private ListBox.ObjectCollection objectCollection;
        public AddObjectForm(PBEditorObjects ob,List<string> objectNames,List<string> dropNames)
        {
            PBEdObj = ob;
            InitializeComponent();
            foreach (string s in objectNames.ToArray())
                listBox1.Items.Add(s);
            foreach (string s in dropNames)
                listBox2.Items.Add(s);

        }

        private void button1_Click(object sender, EventArgs e)
        {
                OkPressed = true;
                EditorForm.isFocused = true;
                Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OkPressed = false;
            EditorForm.isFocused = true;
            Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                if (PBEdObj.textureNameTOName.Keys.Contains<string>(listBox1.SelectedItem as string) && PBEdObj.nameImages[PBEdObj.textureNameTOName[listBox1.SelectedItem as string]] != null)
                    pictureBox1.Image = PBEdObj.nameImages[PBEdObj.textureNameTOName[listBox1.SelectedItem as string]];
                else
                    pictureBox1.Image = null;
                label1.Text = PBEdObj.objectDetails[listBox1.SelectedItem as string ] ;
                label1.Show();
                editorObjectKey =(string) listBox1.SelectedItem;
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex >= 0)
                pictureBox2.Image = PBEdObj.nameImages[PBEdObj.textureNameTOName[listBox2.SelectedItem as string]];
            label2.Text = PBEdObj.objectDetails[listBox2.SelectedItem as string];
            label2.Show();
            editorObjectKey = (string)listBox2.SelectedItem;
            
        }
    }
}
