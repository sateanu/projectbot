using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework; 
using PBEngine.Effect;
namespace ProjectBuild.Levels
{
    class bacau : PBLevel
    {
        int[,] map = new int[,] { 
{101,101,101,101,101,101,101,101,},
{101,0,0,0,0,0,0,101,},
{101,0,0,0,0,0,0,101,},
{101,0,0,0,101,101,101,101,},
{101,0,0,0,0,0,0,101,},
{101,0,0,0,0,0,0,101,},
{101,0,0,0,0,0,0,101,},
{101,101,101,101,101,101,101,101,},
};
        //ENDMAP
        public bacau()
            : base()
        { Map = new PBLayer(map, 80, 80, World); }
        public override void Load()
        {
            //OBJ
            Spawn = new Vector2(float.Parse("6,555728"), float.Parse("2,771757"));
            AddPBMob(float.Parse("2,665103"), float.Parse("3,053007"), float.Parse("0"), Monsters.FromName("Robot"));
            //Robot_1
            AddPBMob(float.Parse("3,008853"), float.Parse("4,084257"), float.Parse("0"), Monsters.FromName("Robot"));
            //Robot_2
            AddPBMob(float.Parse("3,680728"), float.Parse("2,943632"), float.Parse("0"), Monsters.FromName("Robot"));
            //Robot_3
            AddPBObject(float.Parse("5,258882"), float.Parse("1,904895"), float.Parse("-8,124582E-05"), Objects.FromName("Crate"));
            //Crate_4
            AddPBObject(float.Parse("5,258824"), float.Parse("2,919869"), float.Parse("-7,788349E-05"), Objects.FromName("Crate"));
            //Crate_5
            AddPBMob(float.Parse("7,196353"), float.Parse("7,224882"), float.Parse("-1,592933"), Monsters.FromName("BOSS"));
            //BOSS_6
            AddPBMob(float.Parse("2,618228"), float.Parse("7,053007"), float.Parse("0"), Monsters.FromName("Mad Robot"));
            //Mad Robot_7
            AddPBMob(float.Parse("3,758853"), float.Parse("6,974882"), float.Parse("0"), Monsters.FromName("Mad Robot"));
            //Mad Robot_8
            //LIGHT
            AddPBLight(new PointLight()
            {
                Color = new Vector4(float.Parse("0,5019608"), float.Parse("1"), float.Parse("1"), float.Parse("1")),
                Power = float.Parse("0,55"),
                SpecularPower = float.Parse("0,2"),
                LightDecay = 315,
                Position = new Vector3(float.Parse("315,5666"), float.Parse("339,3924"), float.Parse("20")),
                Direction = new Vector3(float.Parse("0"), float.Parse("0"), float.Parse("0")),
                IsEnabled = true
            });
            base.Load();
        }
    }
} 
