using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework;
namespace ProjectBuild.Levels
{
    class d : PBLevel
    {
        int[,] map = new int[,] { 
{103,103,103,103,103,103,103,103,103,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,103,},
{103,103,103,103,103,103,103,103,3,103,},
        };
        public d()
            : base()
        { Map = new PBLayer(map, 80, 80, World); }
        public override void Load()
        {
            Spawn = new Vector2(float.Parse("6,463398"), float.Parse("10,18661"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("1,807561"), float.Parse("6,030396")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("2,822359"), float.Parse("6,061577")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("3,885273"), float.Parse("6,045986")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("5,057148"), float.Parse("6,030361")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("6,354023"), float.Parse("5,920986")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("7,447773"), float.Parse("5,795986")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("8,666523"), float.Parse("5,702236")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("10,0884"), float.Parse("5,733486")), content, MessageManager, "Crate"));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("10,72902"), float.Parse("4,686611")), content, MessageManager, "Crate"));
            AddPBMob(new PBMob(World, 1, 1, new Vector2(float.Parse("3,463398"), float.Parse("1,780361")), content, MessageManager,"Robot",0f,"mob1"));
            AddPBMob(new PBMob(World, 1, 1, new Vector2(float.Parse("8,604023"), float.Parse("2,108486")), content, MessageManager,"Robot",0f,"mob1"));
            base.Load();
        }
    }
}
