using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework;
using PBEngine.Effect;
namespace ProjectBuild.Levels
{
    class z : PBLevel
    {
        int[,] map = new int[,] { 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
};
        public z()
            : base()
        { Map = new PBLayer(map, 80, 80, World); }
        public override void Load()
        {
            AddPBLight(new PointLight()
            {
                Color = new Vector4(float.Parse("0,1568628"), float.Parse("0,4392157"), float.Parse("0,0627451"), float.Parse("1")),
                Power = float.Parse("2,26"),
                SpecularPower = float.Parse("0,2"),
                LightDecay = 393,
                Position = new Vector3(float.Parse("597,2417"), float.Parse("392,5167"), float.Parse("20")),
                Direction = new Vector3(float.Parse("0"), float.Parse("0"), float.Parse("0")),
                IsEnabled = true
            });
            base.Load();
        }
    }
}
