using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework; 
using PBEngine.Effect;  
namespace ProjectBuild.Levels{
class p :PBLevel { 
int[,] map = new int[,] { 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
};
//ENDMAP
public  p () : base()
{ Map=new PBLayer(map,80,80,World); } 
public override void Load() {
//OBJ
AddPBMob(float.Parse("2,865755"),float.Parse("4,962111"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_0
AddPBMob(float.Parse("5,97513"),float.Parse("3,930861"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_1
AddPBMob(float.Parse("5,97513"),float.Parse("5,618361"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_2
Spawn=new Vector2(float.Parse("3,678255"),float.Parse("2,930861"));
//LIGHT
AddPBLight(new PointLight() {
Color=new Vector4(float.Parse("0"),float.Parse("1"),float.Parse("1"),float.Parse("1")),
Power=float.Parse("1"),
SpecularPower=float.Parse("0,2"),
LightDecay=300,
Position=new Vector3(float.Parse("341,4083"),float.Parse("329,5751"),float.Parse("20")),
Direction=new Vector3(float.Parse("0"),float.Parse("0"),float.Parse("0")),
IsEnabled=true
});
base.Load(); } 
 } 
 } 
