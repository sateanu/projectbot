using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework; 
using PBEngine.Effect;
namespace ProjectBuild.Levels
{
    class a : PBLevel
    {
        int[,] map = new int[,] { 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
};
        //ENDMAP
        public a()
            : base()
        { Map = new PBLayer(map, 80, 80, World); }
        public override void Load()
        {
            //OBJ
            Spawn = new Vector2(float.Parse("1,782167"), float.Parse("1,604693"));
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("4,208599"), float.Parse("1,748819")), content, MessageManager, "Crate", float.Parse("0,1235887")));
            //Object_1
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("3,902858"), float.Parse("2,73364")), content, MessageManager, "Crate", float.Parse("0,1235885")));
            //Object_2
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("3,772588"), float.Parse("3,781915")), content, MessageManager, "Crate", float.Parse("0,04556976")));
            //Object_3
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("2,742854"), float.Parse("4,011431")), content, MessageManager, "Crate", float.Parse("0,05210767")));
            //Object_4
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("1,740611"), float.Parse("3,691858")), content, MessageManager, "Crate", float.Parse("0,06307995")));
            //Object_5
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("4,266542"), float.Parse("5,026568")), content, MessageManager, "Crate", float.Parse("0")));
            //Object_6
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("2,591113"), float.Parse("5,019875")), content, MessageManager, "Crate", float.Parse("0,05209585")));
            //Object_7
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("1,391542"), float.Parse("5,432818")), content, MessageManager, "Crate", float.Parse("0")));
            //Object_8
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("5,000917"), float.Parse("3,573443")), content, MessageManager, "Crate", float.Parse("0")));
            //Object_9
            AddPBObject(new PBObject(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("5,329042"), float.Parse("2,432818")), content, MessageManager, "Crate", float.Parse("0")));
            //Object_10
            AddPBMob(new PBMob(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("8,016542"), float.Parse("2,901568")), content, MessageManager,"Robot" ,float.Parse("0"),"mob1"));
            //Mob_11
            AddPBMob(new PBMob(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("7,969667"), float.Parse("5,010943")), content, MessageManager,"Robot" ,float.Parse("0"),"mob1"));
            //Mob_12
            AddPBMob(new PBMob(World, float.Parse("1"), float.Parse("1"), new Vector2(float.Parse("7,735292"), float.Parse("7,417193")), content, MessageManager,"Robot" ,float.Parse("0"),"mob1"));
            //Mob_13
            AddPBMob(new Vector2(float.Parse("7,735292"), float.Parse("7,417193")), float.Parse("1"), Monsters.monsters[1]);
            //LIGHT
            AddPBLight(new PointLight()
            {
                Color = new Vector4(float.Parse("0,5019608"), float.Parse("1"), float.Parse("0,5019608"), float.Parse("1")),
                Power = float.Parse("1"),
                SpecularPower = float.Parse("0,2"),
                LightDecay = 300,
                Position = new Vector3(float.Parse("237,2831"), float.Parse("206,2333"), float.Parse("20")),
                Direction = new Vector3(float.Parse("0"), float.Parse("0"), float.Parse("0")),
                IsEnabled = true
            });
            AddPBLight(new SpotLight()
            {
                Color = new Vector4(float.Parse("0"), float.Parse("0,5019608"), float.Parse("1"), float.Parse("1")),
                Power = float.Parse("2,83"),
                SpecularPower = float.Parse("0,2"),
                LightDecay = 429,
                Position = new Vector3(float.Parse("535,2831"), float.Parse("226,2333"), float.Parse("12")),
                Direction = new Vector3(float.Parse("0,07051922"), float.Parse("0,9923062"), float.Parse("0,1017618")),
                IsEnabled = true
            });
            base.Load();
        }
    }
} 
