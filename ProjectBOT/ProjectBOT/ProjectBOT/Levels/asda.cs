using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework; 
using PBEngine.Effect;  
namespace ProjectBuild.Levels{
class p2 :PBLevel { 
int[,] map = new int[,] { 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
};
//ENDMAP
public  p2 () : base()
{ Map=new PBLayer(map,80,80,World); } 
public override void Load() {
//OBJ
AddPBMob(float.Parse("9.928323"),float.Parse("11.66475"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_1
AddPBMob(float.Parse("10.70714"),float.Parse("13.47503"),float.Parse("0.03928118"),Monsters.FromName("Mad Robot"));
//Mad Robot_2
AddPBMob(float.Parse("9.256448"),float.Parse("14.28975"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_3
AddPBMob(float.Parse("7.693948"),float.Parse("13.91475"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_4
AddPBMob(float.Parse("6.397073"),float.Parse("13.11788"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_5
AddPBMob(float.Parse("5.967824"),float.Parse("12.04916"),float.Parse("-0.07961978"),Monsters.FromName("Mad Robot"));
//Mad Robot_6
AddPBMob(float.Parse("7.131448"),float.Parse("10.2585"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_7
AddPBMob(float.Parse("9.397073"),float.Parse("9.383502"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_8
AddPBMob(float.Parse("11.56036"),float.Parse("9.664806"),float.Parse("-0.02040688"),Monsters.FromName("Mad Robot"));
//Mad Robot_9
AddPBMob(float.Parse("12.80332"),float.Parse("10.78975"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_10
AddPBMob(float.Parse("12.20957"),float.Parse("12.68038"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_11
AddPBMob(float.Parse("11.76816"),float.Parse("13.71291"),float.Parse("0.1564895"),Monsters.FromName("Mad Robot"));
//Mad Robot_12
AddPBMob(float.Parse("11.39191"),float.Parse("14.71073"),float.Parse("0.09597904"),Monsters.FromName("Mad Robot"));
//Mad Robot_13
AddPBMob(float.Parse("10.33045"),float.Parse("15.13533"),float.Parse("-0.02794799"),Monsters.FromName("Mad Robot"));
//Mad Robot_14
AddPBMob(float.Parse("7.912698"),float.Parse("15.58663"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_15
AddPBMob(float.Parse("6.522073"),float.Parse("15.18038"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_16
AddPBMob(float.Parse("4.897073"),float.Parse("13.99288"),float.Parse("0"),Monsters.FromName("Mad Robot"));
//Mad Robot_17
AddPBMob(float.Parse("5.003727"),float.Parse("12.68752"),float.Parse("-0.1623813"),Monsters.FromName("Mad Robot"));
//Mad Robot_18
AddPBMob(float.Parse("4.922792"),float.Parse("11.41383"),float.Parse("0.04964464"),Monsters.FromName("Mad Robot"));
//Mad Robot_19
AddPBMob(float.Parse("5.534433"),float.Parse("9.625478"),float.Parse("0.1193753"),Monsters.FromName("Mad Robot"));
//Mad Robot_20
AddPBMob(float.Parse("6.339746"),float.Parse("8.656895"),float.Parse("0.0298735"),Monsters.FromName("Mad Robot"));
//Mad Robot_21
AddPBMob(float.Parse("7.363914"),float.Parse("8.430634"),float.Parse("0.02145299"),Monsters.FromName("Mad Robot"));
//Mad Robot_22
AddPBMob(float.Parse("8.810206"),float.Parse("8.274064"),float.Parse("0.01481868"),Monsters.FromName("Mad Robot"));
//Mad Robot_23
AddPBMob(float.Parse("9.897994"),float.Parse("8.160129"),float.Parse("0.1679344"),Monsters.FromName("Mad Robot"));
//Mad Robot_24
AddPBMob(float.Parse("10.91575"),float.Parse("8.227313"),float.Parse("0.1674843"),Monsters.FromName("Mad Robot"));
//Mad Robot_25
AddPBMob(float.Parse("11.90196"),float.Parse("8.479829"),float.Parse("0.1666308"),Monsters.FromName("Mad Robot"));
//Mad Robot_26
AddPBMob(float.Parse("12.93236"),float.Parse("8.6067"),float.Parse("0.1172554"),Monsters.FromName("Mad Robot"));
//Mad Robot_27
AddPBMob(float.Parse("12.80776"),float.Parse("9.615423"),float.Parse("0.1137192"),Monsters.FromName("Mad Robot"));
//Mad Robot_28
//LIGHT
//Spawn = new Vector2(float.Parse("8.334573"), float.Parse("12.11788"));
AddPBLight(new PointLight()
{
Color=new Vector4(float.Parse("1"),float.Parse("1"),float.Parse("1"),float.Parse("1")),
Power=float.Parse("1"),
SpecularPower=float.Parse("0.2"),
LightDecay=300,
Position=new Vector3(float.Parse("584.4127"),float.Parse("679.5441"),float.Parse("20")),
Direction=new Vector3(float.Parse("0"),float.Parse("0"),float.Parse("0")),
IsEnabled=true
});
base.Load(); } 
 } 
 } 
