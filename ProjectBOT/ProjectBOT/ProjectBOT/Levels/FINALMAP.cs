using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework; 
using PBEngine.Effect;  
namespace ProjectBuild.Levels{
class g :PBLevel { 
int[,] map = new int[,] { 
{103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,102,102,102,102,102,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,102,0,0,0,102,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,102,0,0,0,102,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,102,0,0,0,102,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,102,102,102,102,102,0,0,0,0,0,103,103,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,103,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,103,103,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,103,103,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,103,103,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,103,103,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,},
{103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103,},
};
//ENDMAP
public  g () : base()
{ Map=new PBLayer(map,80,80,World); } 
public override void Load() {
//OBJ
AddPBMob(float.Parse("9,259504"),float.Parse("5,732032"),float.Parse("2,432966"),Monsters.FromName("BOSS"));
//BOSS_0
AddPBObject(float.Parse("5,43138"),float.Parse("2,903907"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_1
AddPBObject(float.Parse("5,306382"),float.Parse("4,029132"),float.Parse("-0,02893317"),Objects.FromName("Crate"));
//Crate_2
AddPBObject(float.Parse("5,274394"),float.Parse("5,042622"),float.Parse("-0,03071192"),Objects.FromName("Crate"));
//Crate_3
AddPBObject(float.Parse("5,226568"),float.Parse("6,054339"),float.Parse("-0,03532544"),Objects.FromName("Crate"));
//Crate_4
AddPBObject(float.Parse("5,272625"),float.Parse("7,060665"),float.Parse("-0,04099533"),Objects.FromName("Crate"));
//Crate_5
AddPBObject(float.Parse("5,256273"),float.Parse("8,070035"),float.Parse("-0,04833384"),Objects.FromName("Crate"));
//Crate_6
AddPBObject(float.Parse("5,34942"),float.Parse("9,074951"),float.Parse("-0,05512599"),Objects.FromName("Crate"));
//Crate_7
AddPBObject(float.Parse("5,599621"),float.Parse("10,07373"),float.Parse("-0,05952231"),Objects.FromName("Crate"));
//Crate_8
AddPBObject(float.Parse("7,197005"),float.Parse("9,888283"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_9
AddPBObject(float.Parse("8,665754"),float.Parse("9,872658"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_10
AddPBObject(float.Parse("10,3845"),float.Parse("9,919533"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_11
AddPBObject(float.Parse("11,61888"),float.Parse("9,763283"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_12
AddPBObject(float.Parse("13,11888"),float.Parse("9,716408"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_13
AddPBObject(float.Parse("13,21263"),float.Parse("8,200783"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_14
AddPBObject(float.Parse("13,21274"),float.Parse("7,158432"),float.Parse("-0,005458544"),Objects.FromName("Crate"));
//Crate_15
AddPBObject(float.Parse("13,30639"),float.Parse("6,143175"),float.Parse("-0,005620777"),Objects.FromName("Crate"));
//Crate_16
AddPBObject(float.Parse("13,24376"),float.Parse("5,128865"),float.Parse("-0,005799738"),Objects.FromName("Crate"));
//Crate_17
AddPBObject(float.Parse("13,36888"),float.Parse("3,528907"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_18
AddPBObject(float.Parse("13,33763"),float.Parse("2,185157"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_19
AddPBObject(float.Parse("11,65013"),float.Parse("1,763282"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_20
AddPBObject(float.Parse("9,993879"),float.Parse("1,685157"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_21
AddPBObject(float.Parse("8,681379"),float.Parse("1,685157"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_22
AddPBObject(float.Parse("7,103255"),float.Parse("1,685157"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_23
AddPBObject(float.Parse("5,978255"),float.Parse("1,685157"),float.Parse("0"),Objects.FromName("Crate"));
//Crate_24
Spawn=new Vector2(float.Parse("20,1974"),float.Parse("16,30183"));
AddPBMob(float.Parse("7,416146"),float.Parse("13,48933"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_26
AddPBMob(float.Parse("17,58802"),float.Parse("6,723704"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_27
AddPBMob(float.Parse("15,96302"),float.Parse("4,239329"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_28
AddPBMob(float.Parse("3,007815"),float.Parse("5,908859"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_29
AddPBMob(float.Parse("3,757815"),float.Parse("2,205734"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_30
AddPBMob(float.Parse("18,38282"),float.Parse("2,471359"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_31
AddPBMob(float.Parse("4,601565"),float.Parse("11,78386"),float.Parse("0"),Monsters.FromName("Robot"));
//Robot_32
//LIGHT
AddPBLight(new PointLight() {
Color=new Vector4(float.Parse("1"),float.Parse("1"),float.Parse("0,8666667"),float.Parse("1")),
Power=float.Parse("0,67"),
SpecularPower=float.Parse("0,2"),
LightDecay=483,
Position=new Vector3(float.Parse("610,0172"),float.Parse("393,5466"),float.Parse("20")),
Direction=new Vector3(float.Parse("0"),float.Parse("0"),float.Parse("0")),
IsEnabled=true
});
AddPBLight(new SpotLight() {
Color=new Vector4(float.Parse("0,7411765"),float.Parse("0,945098"),float.Parse("0,9843137"),float.Parse("1")),
Power=float.Parse("1,42"),
SpecularPower=float.Parse("0,2"),
LightDecay=636,
Position=new Vector3(float.Parse("40,07559"),float.Parse("1534,763"),float.Parse("20")),
Direction=new Vector3(float.Parse("0,7759738"),float.Parse("0,6296685"),float.Parse("0,03717702")),
IsEnabled=true
});
AddPBLight(new SpotLight() {
Color=new Vector4(float.Parse("0,7686275"),float.Parse("0,9137255"),float.Parse("0,9686275"),float.Parse("1")),
Power=float.Parse("1,42"),
SpecularPower=float.Parse("0,2"),
LightDecay=636,
Position=new Vector3(float.Parse("1535,293"),float.Parse("1505,054"),float.Parse("20")),
Direction=new Vector3(float.Parse("0,8145199"),float.Parse("0,5774165"),float.Parse("0,05610202")),
IsEnabled=true
});
AddPBLight(new PointLight() {
Color=new Vector4(float.Parse("1"),float.Parse("0"),float.Parse("0"),float.Parse("1")),
Power=float.Parse("0,28"),
SpecularPower=float.Parse("0,2"),
LightDecay=336,
Position=new Vector3(float.Parse("1512,909"),float.Parse("236,9376"),float.Parse("20")),
Direction=new Vector3(float.Parse("0"),float.Parse("0"),float.Parse("0")),
IsEnabled=true
});
base.Load(); } 
 } 
 } 
