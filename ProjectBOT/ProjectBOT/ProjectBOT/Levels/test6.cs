using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using ProjectBuild.PBEngine; 
using Microsoft.Xna.Framework;
using PBEngine.Effect;
namespace ProjectBuild.Levels
{
    class W : PBLevel
    {
        int[,] map = new int[,] { 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,0,5,0,0,},
{0,0,0,0,0,3,5,5,5,5,5,5,5,5,5,3,5,0,0,0,},
{0,0,0,0,0,3,0,5,5,5,5,5,5,5,5,3,0,0,0,0,},
{0,0,0,0,0,3,3,3,0,5,5,5,5,5,3,3,0,0,0,0,},
{0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
};
        public W()
            : base()
        { Map = new PBLayer(map, 80, 80, World); }
        public override void Load()
        {
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("4,782817"), float.Parse("3,728254")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("4,302765"), float.Parse("5,962511")), content, MessageManager, "Crate", float.Parse("-0,1578325")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("4,256298"), float.Parse("6,999389")), content, MessageManager, "Crate", float.Parse("-0,1363595")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("4,289795"), float.Parse("8,017555")), content, MessageManager, "Crate", float.Parse("-0,1343816")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("5,235942"), float.Parse("9,056379")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("6,298442"), float.Parse("9,509504")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("7,627323"), float.Parse("9,962245")), content, MessageManager, "Crate", float.Parse("0,02522581")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("8,635434"), float.Parse("10,24364")), content, MessageManager, "Crate", float.Parse("0,02509202")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("9,64819"), float.Parse("10,33826")), content, MessageManager, "Crate", float.Parse("0,02490159")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("11,04844"), float.Parse("10,46263")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("12,15782"), float.Parse("10,2595")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("13,28282"), float.Parse("10,27513")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("15,11094"), float.Parse("10,02513")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("16,64806"), float.Parse("9,391594")), content, MessageManager, "Crate", float.Parse("-0,106895")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("17,61748"), float.Parse("8,912465")), content, MessageManager, "Crate", float.Parse("-0,1068954")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("18,60039"), float.Parse("8,558351")), content, MessageManager, "Crate", float.Parse("-0,1067216")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("19,72678"), float.Parse("8,668966")), content, MessageManager, "Crate", float.Parse("0,1590832")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("19,70791"), float.Parse("7,638461")), content, MessageManager, "Crate", float.Parse("0,1591815")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("19,94818"), float.Parse("6,650202")), content, MessageManager, "Crate", float.Parse("0,1591531")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("20,20469"), float.Parse("5,134504")), content, MessageManager, "Crate", float.Parse("0,1572566")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("20,87213"), float.Parse("3,717299")), content, MessageManager, "Crate", float.Parse("0,1573218")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("19,81004"), float.Parse("3,578202")), content, MessageManager, "Crate", float.Parse("0,04451533")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("19,78728"), float.Parse("2,558474")), content, MessageManager, "Crate", float.Parse("0,05207202")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("18,78113"), float.Parse("2,347565")), content, MessageManager, "Crate", float.Parse("0,05596802")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("17,7847"), float.Parse("1,993738")), content, MessageManager, "Crate", float.Parse("0,05655756")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("16,77897"), float.Parse("1,804518")), content, MessageManager, "Crate", float.Parse("0,05668272")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("15,67344"), float.Parse("1,728254")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("14,57969"), float.Parse("1,665754")), content, MessageManager, "Crate", float.Parse("0")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("13,49338"), float.Parse("1,683102")), content, MessageManager, "Crate", float.Parse("0,004548559")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("12,47851"), float.Parse("1,664031")), content, MessageManager, "Crate", float.Parse("0,004555828")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("11,34721"), float.Parse("1,663436")), content, MessageManager, "Crate", float.Parse("-0,08039096")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("10,33067"), float.Parse("1,647324")), content, MessageManager, "Crate", float.Parse("-0,08404955")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("9,317657"), float.Parse("1,645638")), content, MessageManager, "Crate", float.Parse("-0,09276479")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("8,318787"), float.Parse("1,769104")), content, MessageManager, "Crate", float.Parse("-0,1035482")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("7,317285"), float.Parse("1,858803")), content, MessageManager, "Crate", float.Parse("-0,1150804")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("6,328948"), float.Parse("2,070332")), content, MessageManager, "Crate", float.Parse("-0,1276903")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("5,351573"), float.Parse("2,373215")), content, MessageManager, "Crate", float.Parse("-0,1365867")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("4,372274"), float.Parse("2,673179")), content, MessageManager, "Crate", float.Parse("-0,1398056")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("3,149391"), float.Parse("4,129103")), content, MessageManager, "Crate", float.Parse("-0,2800407")));
            AddPBObject(new PBObject(World, 1, 1, new Vector2(float.Parse("3,509585"), float.Parse("5,079594")), content, MessageManager, "Crate", float.Parse("-0,2807336")));
            Spawn = new Vector2(float.Parse("12,13855"), float.Parse("5,686196"));
            AddPBLight(new SpotLight()
            {
                Color = new Vector4(0.6392157f, 0.9254902f, 0.5411765f, 1),
                Power = float.Parse("1,33"),
                SpecularPower = float.Parse("0,2"),
                LightDecay = 381,
                Position = new Vector3(float.Parse("835,867"), float.Parse("-72,08344"), float.Parse("20")),
                Direction = new Vector3(float.Parse("-0,0610504"), float.Parse("-0,9942495"), float.Parse("-0,0879822")),
                IsEnabled = true
            });
            AddPBLight(new PointLight()
            {
                Color = new Vector4(0.5019608f, 1, 0, 1),
                Power = float.Parse("0,82"),
                SpecularPower = float.Parse("0,2"),
                LightDecay = 381,
                Position = new Vector3(float.Parse("808,867"), float.Parse("369,9166"), float.Parse("20")),
                Direction = new Vector3(float.Parse("0"), float.Parse("0"), float.Parse("0")),
                IsEnabled = true
            });
            base.Load();
        }
    }
}
