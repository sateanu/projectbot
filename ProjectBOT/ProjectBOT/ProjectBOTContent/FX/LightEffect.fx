float screenWidth;
float screenHeight;
float4 ambientColor;

float lightStrength;
float lightDecay;
float3 lightPosition;
float4 lightColor;
float3 coneDirection;
float lightAngle;
float specularStrength;

Texture NormalMap;
sampler NormalMapSampler = sampler_state{
	texture=<NormalMap>;
	magfilter=LINEAR;
	minfilter=LINEAR;
	mipfilter=LINEAR;
	
};

Texture DepthMap;
sampler DepthMapSampler = sampler_state {
	texture = <DepthMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

struct VertexToPixel
{
	float4 Position:POSITION;
	float2 TexCoord:TEXCOORD0;
	float4 Color:COLOR0;
};

struct PixelToFrame
{
	float4 Color : COLOR0;
};

VertexToPixel MyVertexShader(float4 inPos: POSITION0, float2 texCoord: TEXCOORD0, float4 color: COLOR0)
{
	VertexToPixel Output = (VertexToPixel)0;
	
	Output.Position = inPos;
	Output.TexCoord = texCoord;
	Output.Color = color;
	
	return Output;
}
PixelToFrame SpotPointLightShader(VertexToPixel PSIn) : COLOR0
{	
	PixelToFrame Output = (PixelToFrame)0;
	half3 pixelPosition;
	pixelPosition.x = screenWidth * PSIn.TexCoord.x;
	pixelPosition.y = screenHeight * PSIn.TexCoord.y;
	pixelPosition.z = 0;
	float spotIntensity;
	half3 shading;
	
		
		half3 lightDirection = lightPosition - pixelPosition;
		half3 lightDirNorm = normalize(lightDirection);
		float ang=dot(coneDirection,lightDirNorm);
		if(ang<0.4480f)
		ang=0;
		else
		ang=pow(ang,35);
		float coneAttenuation = saturate(1.0f - length(lightDirection) / lightDecay); 

		shading =ang*coneAttenuation * lightColor * lightStrength;

	Output.Color=float4(shading.r,shading.g,shading.b,1.0f)*lightColor;
	return Output;
}
PixelToFrame PointLightShader(VertexToPixel PSIn) : COLOR0
{	
	PixelToFrame Output = (PixelToFrame)0;
		
	float3 pixelPosition;
	pixelPosition.x = screenWidth * PSIn.TexCoord.x;
	pixelPosition.y = screenHeight * PSIn.TexCoord.y;
	pixelPosition.z = 0;
	float3 shading;

		float3 lightDirection = lightPosition - pixelPosition;
		float3 lightDirNorm = normalize(lightDirection);
		float coneAttenuation = saturate(1.0f - length(lightDirection) / lightDecay); 

		shading =coneAttenuation * lightColor * lightStrength;

	Output.Color=float4(shading.r,shading.g,shading.b,1.0f)*lightColor;
	return Output;
}
technique DeferredSpotLight
{
    pass Pass1
    {
		VertexShader = compile vs_2_0 MyVertexShader();
        PixelShader = compile ps_2_0 SpotPointLightShader();
    }
}
technique DeferredPointLight
{
    pass Pass1
    {
		VertexShader = compile vs_2_0 MyVertexShader();
        PixelShader = compile ps_2_0 PointLightShader();
    }
}