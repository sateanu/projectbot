// TODO: add effect parameters here.
sampler TextureSampler: register(s0);
float percentage;

float4 PixelShaderFunction(float2 Tex: TEXCOORD0) : COLOR0
{
	float4 Color = tex2D(TextureSampler, Tex);
	if(percentage>0)
	return Color;
	float p1 = 1.0f - percentage;
	return Color * float4(p1, p1, p1,1.0f);
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
